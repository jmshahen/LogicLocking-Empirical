Logic Locking Emipirical
========================


Installation Instructions
=========================
This project was developed on Windows 10 machine using Eclipse, and tested using Ubuntu 18.04.2 LTS and the commandline (specifically `ILP/run`). 

You need the following programs installed on your machine before you can test:

1. Java 8+ Runtime and Development executables
    * You need `javac` and `java`, and should be on the `PATH`
1. [Apache Ant](https://ant.apache.org/)
1. [CPLEX](https://www.ibm.com/support/knowledgecenter/fr/SSSA5P_12.6.1/ilog.odms.studio.help/Optimization_Studio/topics/COS_installing.html)
    * You don't need the IDE, just OPL dll compiled for your system
    * If you are using Linux, you might need to update `ILP/run` and `ILP/cui` for the paths to CPLEX's `.so`
    * On windows you will need to convert the `ILP/run` and `ILP/cui` to batch script and change the directories to point to the opl `.dll` (or use eclipse)
1. SQLite3
    * Our results are stored in a SQLite database, we provide the sqlite3 jar from: https://bitbucket.org/xerial/sqlite-jdbc/downloads/ but you might need to install other software to get the connection working
    * On Linux: `sudo apt install sqlite3 sqlitebrowser`
    * On Windows: https://sqlite.org/download.html and https://sqlitebrowser.org/
1. Clone the `ShahenSharedClasses` repo into the same folder as this repo's parent (there root folders should appear at the same level)
    * `git clone https://jshahen@bitbucket.org/jshahen/shahensharedclasses.git ShahenSharedClasses`
    * Folder structure should look like this:
    ```
    ShahenSharedClasses/
    LogicLocking_Empirical/
    ```


Building and Running Using Commandline
--------------------------------------
1. **BUILD:** Run `ant` in the `ILP/` folder (`> cd ILP`, `> ant`)
    * This will build everything for you, including the parsers
1. **RUN CUI:** Run `./cui` in the `ILP/` folder (`> cd ILP`, `> ./cui`)
1. **RUN CMDs:** Run `./run PARAMS` in the `ILP/` folder (`> cd ILP`, `> ./run PARAMS`)
    * Replace `PARAMS` with the parameters you want to run

Using Eclipse
--------------
1. Import both the LogicLocking_Empirical and ShahenSharedClasses into Eclipse
1. Edit the build path if anything is screwed up (CPLEX and ShahenSharedClasses might be screwed)
1. Make sure that JUnit 5 is in the build path
1. Run the `shahen.logiclocking.LogicLocking_ILPCUI` as a Java Application
1. Fix any errors due to path changes


Background on ILP Solvers
=========================
TLDR: We are using CPLEX, but AMPL would be a good choice, but there is a small performance hit of converting from there generic version into which ever solver you use.


ILP Solvers
------------

1. Gurobi -- http://www.gurobi.com/products/gurobi-optimizer
1. Symphony -- https://projects.coin-or.org/SYMPHONY
1. GLPK -- https://www.gnu.org/software/glpk/
1. CPLEX -- https://www.ibm.com/analytics/cplex-optimizer
1. Lp_Solve -- http://lpsolve.sourceforge.net/5.5/

ILP Tool Packages (many solvers)
--------------------------------
1. AMPL -- https://ampl.com/products/academic-price-list/
1. Google OR -- https://developers.google.com/optimization/


ILP Benchmarks
----------------
1. Uses R and Gurobi/Symphony/GLPK -- http://strimas.com/prioritization/ilp-performance/

Java Interface
--------------
Someone created a generetic Java interface, see here: http://javailp.sourceforge.net/

To test the following ILP Solvers:

* lp_solve - free under LGPL license, available at http://lpsolve.sourceforge.net/5.5/
* ILOG CPLEX - commercial, limited trial version available at http://www.ilog.com/products/cplex/
* Gurobi (contribution by Fabio Genoese)- commercial, free academic version available http://www.gurobi.com/
* Mosek - commercial, limited trial version available at http://www.mosek.com/
* GLPK - free under GPL, available for [linux](http://www.gnu.org/software/glpk/) and 
    [windows](https://sourceforge.net/projects/winglpk/) (compatible with version 4.43 and later)
* SAT4J (restricted 0-1 ILP backtracking solver to binary variables and integer coefficients) - 
    free under LGPL license, available at http://sat4j.org/
* MiniSat+ (restricted 0-1 ILP backtracking solver to binary variables and integer coefficients) - 
    free under MIT license, available at http://minisat.se/MiniSat+.html, 
    JNI lib is provided by Java ILP (see downloads at sourceforge)


Other Links
-----------

* Papers in Chemistry where AMPL is used to solve the ILP problem (through AMPL they used CPLEX) -- http://compbio.cs.princeton.edu/scplp/
