CREATE TABLE IntermediateResult (
  idIntermediateResult INTEGER PRIMARY KEY AUTOINCREMENT,
  idResult INTEGER NOT NULL,
  -- The result
  result INTEGER,
  resultString TEXT,
  verifyStep TINYINT,
  correctKey TINYINT,
  -- What stage is this result
  -- 1 first stage (see LL_CPLEX_Solver_v1)
  -- 2 iter stage (see LL_CPLEX_Solver_v1)
  -- 3 last stage (see LL_CPLEX_Solver_v1)
  -- 4 new key
  -- 5 new distinguishing input
  -- 6 non-standard
  stage INTEGER,
  stageString TEXT,
  -- The order that the the solver is called (cplex/gurobi)
  iterNum INTEGER,
  keyIterNum INTEGER,
  inputIterNum INTEGER,
  -- Stats about this iternation
  numSolverConstraints INTEGER,
  numSolverVariables INTEGER,
  numCircuits INTEGER,
  -- Timing
  dateStart DATETIME,
  dateFinish DATETIME,
  durationMilliseconds INTEGER,
  durationPretty VARCHAR (30),
  -- Extra Details
  inputKeyJSON TEXT,
  FOREIGN KEY (idResult) REFERENCES Result (idResult)
);