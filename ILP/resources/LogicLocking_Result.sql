CREATE TABLE Result (
  -- Identify this result (unique and to the circuit)
  idResult INTEGER PRIMARY KEY AUTOINCREMENT,
  fileHash VARCHAR (257) NOT NULL,
  -- The result
  result INTEGER,
  resultString TEXT,
  correctKey TINYINT,
  -- Options and Version Numbers
  ilpInstanceVersion TEXT,
  ilpSolver TEXT,
  ilpSolverVersion TEXT,
  reduction TEXT,
  reductionVersion TEXT,
  -- Stats about the intermediate results
  lastNumConstraints INTEGER,
  lastNumVariables INTEGER,
  lastNumCircuits INTEGER,
  numStages INTEGER,
  -- Timing
  dateStart TIMESTAMP,
  dateFinish TIMESTAMP,
  totalDurationMilliseconds INTEGER,
  totalDurationPretty VARCHAR (30),
  -- Extra Information
  keyBoolString TEXT,
  commandString TEXT,
  FOREIGN KEY (fileHash) REFERENCES Circuit (fileHash)
);