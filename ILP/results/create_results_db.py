import sys
import os
import sqlite3


def create_db_connection(filepath):
    conn = sqlite3.connect(filepath)
    return conn


def get_file_contents(filepath):
    f = open(filepath, "r")
    return f.read()


def get_circuit_sql():
    return get_file_contents("../resources/LogicLocking_Circuit.sql")


def get_result_sql():
    return get_file_contents("../resources/LogicLocking_Result.sql")


def get_intermediate_result_sql():
    return get_file_contents("../resources/LogicLocking_IntermediateResult.sql")


if __name__ == "__main__":
    if len(sys.argv) == 2:
        filepath = sys.argv[1]
    else:
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        filepath = 'LogicLockingResults.sqlite3'

    conn = create_db_connection(filepath)

    if conn:
        c = conn.cursor()
        circuit_sql = get_circuit_sql()
        result_sql = get_result_sql()
        intermediate_result_sql = get_intermediate_result_sql()

        c.execute(circuit_sql)
        c.execute(result_sql)
        c.execute(intermediate_result_sql)
        conn.commit()
        conn.close()

        print(f'Created new database file {filepath}')
    else:
        print(f'ERROR: could not connect to {filepath}, with the current working directory: {os.getcwd()}')
