import sys
import os
import sqlite3


def create_db_connection(filepath):
    conn = sqlite3.connect(filepath)
    return conn


def getTableSchemaString(conn, tableName):
    if not conn:
        print("No connection was found when attempting to call SQLiteHelper.getTableSchemaString()")
        return None
    sb = ""

    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("PRAGMA table_info(" + tableName + ");")
    rs = c.fetchall()
    for r in rs:
        cid = r["cid"]
        name = r["name"]
        mytype = r["type"]
        notnull = r["notnull"]

        sb += f'{cid},{name},{mytype},{notnull}\n'

    return sb


if __name__ == "__main__":
    print('''\
############################################################################
This script is used to create the schema files in ../resources.
It is a non-destucive script, and if is run by accident, nothing bad will happen (even if you see an error).
You must copy the schema text into the files.

Steps to update the results database:
1. Edit the .sql files in ../resources/*.sql
2. Run ./create_results_db.py
3. Run ./internal_only_create_schema_file.py
4. Copy result from above into the files in ../resources/*.txt
############################################################################
''')
    if len(sys.argv) == 2:
        filepath = sys.argv[1]
    else:
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        filepath = 'LogicLockingResults.sqlite3'

    print(f'Reading from database file {filepath}')
    conn = create_db_connection(filepath)

    print('')
    if conn:
        c = conn.cursor()

        print('Circuit Schema:')
        print(getTableSchemaString(conn, 'Circuit'))
        print('\n\n')
        print('Result Schema:')
        print(getTableSchemaString(conn, 'Result'))
        print('\n\n')
        print('IntermediateResult Schema:')
        print(getTableSchemaString(conn, 'IntermediateResult'))
        print('\n')
