package shahen.logiclocking;

public class LL_Exception extends Exception {
    private static final long serialVersionUID = 5091017947379021529L;

    public LL_Exception(String msg) {
        super(msg);
    }
}
