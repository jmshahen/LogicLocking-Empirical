package shahen.logiclocking;

import java.util.logging.Logger;

public class LL_ILP {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) throws Exception {
        LL_ILPInstance inst = new LL_ILPInstance();

        inst.run(args);
    }

}
