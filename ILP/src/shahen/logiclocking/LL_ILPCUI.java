package shahen.logiclocking;

import java.io.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

import shahen.logiclocking.options.LL_OptionsHelper;

public class LL_ILPCUI {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static String previousCommandFilename = "CreePolicyAnalyzerCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] ignored) throws Exception {
        LL_ILPInstance inst = new LL_ILPInstance();
        ArrayList<String> argv = new ArrayList<String>();
        ArrayList<String[]> cmds = new ArrayList<String[]>();
        String arg = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        LL_OptionsHelper.setupOptions(inst, options);
        LL_OptionsHelper.printHelp(options, 120);

        printCommonCommands();

        System.out.print("Enter Commandline Argument ('!e' to run): ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            arg = user_input.next();
            fullCommand.append(arg + " ");

            if (quotedStr.isEmpty() && arg.startsWith("\"")) {
                println("Starting: " + arg);
                quotedStr = arg.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && arg.endsWith("\"")) {
                println("Ending: " + arg);
                argv.add(quotedStr + " " + arg.substring(0, arg.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + arg;
                continue;
            }

            if (arg.equals("!e")) {
                cmds.add(argv.toArray(new String[1]));
                break;
            }

            if (arg.equals("!p")) {
                argv.clear();
                cmds.add(previousCmd.split(" "));
                break;
            }
            argv.add(arg);
        }
        user_input.close();

        println("Commands: " + argv);

        if (!arg.equals("!p") && !arg.equals("!all_stats")) {
            try {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString());
                fw.close();
            } catch (IOException e) {
                println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
            }
        }

        long fullstart = System.currentTimeMillis();
        int count = 1;
        for (String[] c : cmds) {
            println("==============================================================");
            println("== CMD " + count + "/" + cmds.size() + ": " + getCMDString(c));
            println("==================START OF OUTPUT=============================");
            long start = System.currentTimeMillis();
            inst = new LL_ILPInstance();
            inst.run(c);
            Duration d = Duration.ofMillis(System.currentTimeMillis() - start);
            println("====================END OF OUTPUT============================");
            println("Done [" + humanReadableFormat(d) + "]");
            println("");

            count++;
        }

        if (cmds.size() > 1) {
            Duration d = Duration.ofMillis(System.currentTimeMillis() - fullstart);
            println("\n");
            println("===========================");
            println("===========================");
            println("Done all Tests [" + humanReadableFormat(d) + "]");
        }
    }

    /**
     * Create a string that can be copied and pasted into the CUI for quickly running a command
     * @param cmds
     * @return
     */
    private static String getCMDString(String[] cmds) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String s : cmds) {
            if (first) {
                first = false;
            } else {
                sb.append(" ");
            }
            if (s.contains(" ")) {
                sb.append("\"").append(s).append("\"");
            } else {
                sb.append(s);
            }
        }
        return sb.toString();
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
    }

    public static void printCommonCommands() {
        println("\n\n--- Common Commands ---");
        println("\n######################### REGRESSION TESTS #####################################");
        println("-solver cplex_v3 -reduction standard -gc 0 -verify_steps -locked ../data/regression -bulk -run solve !e");
        println("-solver cplex_v3 -reduction reduced -gc 0 -verify_steps -locked ../data/regression -bulk -run solve !e");
        println("-solver cplex_v3 -reduction fromsat -gc 0 -verify_steps -locked ../data/regression -bulk -run solve !e");
        println("-solver gurobi_v1 -reduction standard -gc 0 -verify_steps -locked ../data/regression -bulk -run solve !e");
        println("-solver gurobi_v1 -reduction reduced -gc 0 -verify_steps -locked ../data/regression -bulk -run solve !e");
        println("-solver gurobi_v1 -reduction fromsat -gc 0 -verify_steps -locked ../data/regression -bulk -run solve !e");
        println("-verify_steps -locked ../data/regression/regression01.bench -run solve !e");
        println("-verify_steps -locked ../data/regression/regression02.bench -run solve !e");
        println("-verify_steps -locked ../data/regression/regression03.bench -run solve !e");
        // println("\n######################### SMALL CIRCUITS #####################################");
        // println("-locked ../data/dac12/c499_enc05.bench -run solve !e");
        // println("-mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction standard -verify_steps -run solve
        // -locked easy_circuits.txt -bulk !e");
        // println("-mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction reduced -verify_steps -run solve
        // -locked easy_circuits.txt -bulk !e");
        // println("-mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction fromsat -verify_steps -run solve
        // -locked easy_circuits.txt -bulk !e");
        // println("-mip_emphasis 1 -timeout 36000 -solver gurobi_v1 -reduction standard -verify_steps -run solve
        // -locked easy_circuits.txt -bulk !e");
        // println("-mip_emphasis 1 -timeout 36000 -solver gurobi_v1 -reduction reduced -verify_steps -run solve -locked
        // easy_circuits.txt -bulk !e");
        // println("-mip_emphasis 1 -imeout 36000 -solver gurobi_v1 -reduction fromsat -verify_steps -run solve -locked
        // easy_circuits.txt -bulk !e");
        // println("\n######################### PERFORMANCE #####################################");
        // println("-threads 1 -alg 0 -locked ../data/dac12/enc05/c499_enc05.bench -run solve -solver cplex_v3
        // -reduction standard -verify_steps !e");
        // println("-threads 1 -alg 1 -locked ../data/dac12/enc05/c499_enc05.bench -run solve -solver cplex_v3
        // -reduction standard -verify_steps !e");
        // println("-threads 1 -alg 2 -locked ../data/dac12/enc05/c499_enc05.bench -run solve -solver cplex_v3
        // -reduction standard -verify_steps !e");
        // println("-threads 1 -alg 3 -locked ../data/dac12/enc05/c499_enc05.bench -run solve -solver cplex_v3
        // -reduction standard -verify_steps !e");
        // println("-threads 1 -alg 4 -locked ../data/dac12/enc05/c499_enc05.bench -run solve -solver cplex_v3
        // -reduction standard -verify_steps !e");
        // println("-threads 1 -alg 5 -locked ../data/dac12/enc05/c499_enc05.bench -run solve -solver cplex_v3
        // -reduction standard -verify_steps !e");
        // println("-threads 1 -alg 6 -locked ../data/dac12/enc05/c499_enc05.bench -run solve -solver cplex_v3
        // -reduction standard -verify_steps !e");
        println("\n######################### DATA GATHERING #####################################");
        println("-threads 15 -parallel -mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction standard -verify_steps -run solve -locked all_benchmarks.txt -bulk !e");
        println("-threads 15 -parallel -mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction reduced -verify_steps -run solve -locked all_benchmarks.txt -bulk !e");
        println("-threads 15 -parallel -mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction fromsat -verify_steps -run solve -locked all_benchmarks.txt -bulk !e");
        println("-mip_emphasis 1 -timeout 36000 -solver gurobi_v1 -reduction standard -verify_steps -run solve -locked all_benchmarks.txt -bulk !e");
        println("-mip_emphasis 1 -timeout 36000 -solver gurobi_v1 -reduction reduced -verify_steps -run solve -locked all_benchmarks.txt -bulk !e");
        println("-mip_emphasis 1 -timeout 36000 -solver gurobi_v1 -reduction fromsat -verify_steps -run solve -locked all_benchmarks.txt -bulk !e");
        println("###");
        println("-threads 15 -parallel -mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction standard -verify_steps -run solve -locked hard_circuits.txt -bulk !e");
        println("-threads 15 -parallel -mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction reduced -verify_steps -run solve -locked hard_circuits.txt -bulk !e");
        println("-threads 15 -parallel -mip_emphasis 1 -timeout 36000 -alg 6 -solver cplex_v3 -reduction fromsat -verify_steps -run solve -locked hard_circuits.txt -bulk !e");
        println("-mip_emphasis 1 -timeout 36000 -solver gurobi_v1 -reduction standard -verify_steps -run solve -locked hard_circuits.txt -bulk !e");
        println("-mip_emphasis 1 -timeout 36000 -solver gurobi_v1 -reduction reduced -verify_steps -run solve -locked hard_circuits.txt -bulk !e");
        println("-mip_emphasis 1 -imeout 36000 -solver gurobi_v1 -reduction fromsat -verify_steps -run solve -locked hard_circuits.txt -bulk !e");
        println("\n######################### DEBUG #####################################");
        println("-locked ../data/dac12/c499_enc05.bench -unlocked ../data/original/c499.bench -run verify_key !e");
        println("-solver cplex_v3 -reduction standard -verify_steps -locked ../data/dtc10lut/c432_enc.bench -unlocked ../data/original/c432.bench -run solve !e");
        println("-solver cplex_v3 -reduction standard -verify_steps -locked ../data/dtc10lut/c432_enc.bench -unlocked ../data/original/ -run solve !e");
        println("-solver cplex_v3 -reduction standard -verify_steps -locked ../data/dtc10lut -unlocked ../data/original/ -bulk -run solve !e");
        println("-solver cplex_v3 -reduction standard -verify_steps -locked ../data/sarlock_dac12/enc05 -unlocked ../data/sarlock_original/ -bulk -run solve !e");

        println("");
        println("---");
        println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            println("Previous Command: " + previousCmd);
        } catch (IOException e) {
            println("[ERROR] Unable to load previous command!");
        }
    }

    private static void println(String s) {
        System.out.println(s);
    }
}
