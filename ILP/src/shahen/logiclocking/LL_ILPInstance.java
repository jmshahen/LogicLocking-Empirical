package shahen.logiclocking;

import java.io.*;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.*;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import shahen.file.FileHelper;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.options.*;
import shahen.logiclocking.options.run.LL_RunOptionI;
import shahen.logiclocking.options.run.LL_RunOptions;
import shahen.logiclocking.parser.LL_AutoGen_CircuitParser;
import shahen.logiclocking.parser.LL_ParserHelper;
import shahen.logiclocking.reduction.*;
import shahen.logiclocking.results.LL_ResultsManager;
import shahen.logiclocking.solver.*;
import shahen.sql.SQLiteHelper;
import shahen.string.ShahenStrings;
import shahen.timing.ShahenTiming;

public class LL_ILPInstance {
    public static final String VERSION = "v1.3";
    public static final String AUTHORS = "Jonathan Shahen <jmshahen [AT] uwaterloo [DOT] ca>";

    // Logger Fields
    public Logger log = Logger.getLogger("shahen");
    public String Logger_filepath = "logs/Logic-Locking-Log.csv";
    public ConsoleHandler consoleHandler = new ConsoleHandler();
    public Level LoggerLevel;
    public FileHandler fileHandler;
    public Boolean WriteCSVFileHeader = true;
    private String timingFile = "logs/Logic-Locking-Timing.csv";

    // DB Settings
    public String dbFile = "results/LogicLockingResults.sqlite3";
    public static String dbTable = "Circuit";
    public static String dbSchemaCircuitFile = "resources/LogicLocking_Circuit_Schema.txt";
    public static String dbSchemaResultFile = "resources/LogicLocking_Result_Schema.txt";
    public static String dbSchemaIntermediateResultFile = "resources/LogicLocking_IntermediateResult_Schema.txt";

    // Result Classes
    public SQLiteHelper helper;
    public LL_ResultsManager results;

    // Helpers
    public ShahenTiming timing;
    public FileHelper fileHelper = new FileHelper(".bench");
    public static LL_ParserHelper parserHelper = new LL_ParserHelper();

    // Settings
    public String[] orig_args = null;
    public String locked_path = null;
    public boolean debug = false;
    public boolean verify_steps = false;
    public boolean dump_distinguishing_inputs = false;
    public Boolean extra_key_constraint = null;
    public File preload_file = null;
    public LL_ReductionI reduction = null;
    public Integer preload_num = null;
    public Boolean solver_parallel = null;
    public Integer solver_threads = null;
    public Integer solver_stagetimeout = null;
    public Integer solver_timeout = null;
    public Integer solver_algorithm = null;
    public ArrayList<Integer> solver_algorithms = new ArrayList<Integer>();
    public Integer solver_mip_emphasis = null;

    // Files to Write out to
    public File toIlpFile = new File("logs/circuit.ilp");
    public File toIlpInput = new File("logs/circuit_inputkey.ilp");
    public File toLpInput = new File("logs/circuit_inputkey.lp");

    /** 
     * <ul>
     * <li> cplex_v1 -- {@link LL_CPLEX_Solver_v1}
     * <li> cplex_v2 -- {@link LL_CPLEX_Solver_v2}
     * <li> cplex_v3 -- {@link LL_CPLEX_Solver_v3}
     * <li> gurobi_v1 -- {@link LL_GUROBI_Solver}
     * </ul> 
     */
    public String solver_name = "cplex_v3";
    /** The number of seconds to pause the threads execution and allow the garbage collection thread to run. */
    public int garbage_collector_duration_sec = 10;

    public int run(String[] args) throws Exception {
        try {
            orig_args = args;
            ////////////////////////////////////////////////////////////////////////////////
            // INITIALIZATION (BEFORE LOADING COMMANDLINE OPTIONS)
            timing = new ShahenTiming();
            /* Timing */timing.startTimer("totalTime");
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // COMMANDLINE OPTIONS
            CommandLine cmd = LL_OptionsHelper.init(this, args);
            if (cmd == null) { return 0; }

            // Only set tests.debug when equal to TRUE (this allows for default to be changed to TRUE)
            if (debug) {
                timing.printOnStop = true;
            }
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // Setup connection with the results database
            /* Timing */timing.startTimer("connectDB");
            helper = new SQLiteHelper(new File(dbFile), dbTable);
            int returnCode = checkDB();
            if (returnCode != 0) { return returnCode; }
            results = new LL_ResultsManager(helper);
            /* Timing */timing.stopTimer("connectDB");
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // LOADING ALL FILES
            /* Timing */timing.startTimer("loadFiles");
            fileHelper.loadFiles(locked_path);
            Integer numFiles = fileHelper.files.size();
            /* Timing */timing.stopTimer("loadFiles");
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // Execute the test cases
            if (cmd.hasOption(LL_OptionString.RUN.toString())) {
                LL_RunOptions runVal = LL_RunOptions.fromString(cmd.getOptionValue(LL_OptionString.RUN.toString()));
                if (runVal == null) {
                    log.severe("Run option '" + cmd.getOptionValue(LL_OptionString.RUN.toString())
                            + "' not found! Available Options: " + LL_RunOptions.availableOptions());
                    return -1;
                }
                log.config("[ACTION] Run paramter detected: " + runVal);
                getPerformanceOptions(cmd);

                ////////////////////////////////////////////////////////////////////////////////
                // COLLECT UNLOCKED FILES
                LL_Circuit unlocked_circuit = null; // stores the circuit if ONLY 1 unlocked file is given
                FileHelper unlocked_helper = null;
                if (cmd.hasOption(LL_OptionString.UNLOCKED.toString())) {
                    File unlocked_file = new File(cmd.getOptionValue(LL_OptionString.UNLOCKED.toString()));

                    if (!unlocked_file.exists()) {
                        unlocked_file = null;
                    } else {
                        unlocked_helper = new FileHelper(".bench");
                        if (unlocked_file.isDirectory()) {
                            unlocked_helper.loadFilesFromFolder(unlocked_file.getAbsolutePath());
                        } else if (unlocked_file.isFile()) {
                            unlocked_helper.addFile(unlocked_file.getAbsolutePath());
                        }
                    }
                }
                ////////////////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////////////////
                // INDIVIDUAL FILE LOOP
                log.info("Files to Unlock: " + fileHelper.printFileNames(300, true));
                boolean first = true;
                for (Integer i = 1; i <= numFiles; i++) {
                    File file = fileHelper.files.get(i - 1);
                    String timerName = "";

                    if (numFiles > 1) {
                        timerName = "file(" + i + "/" + numFiles + ")";
                    } else {
                        timerName = "file";
                    }

                    if (i > 1) printSplit();
                    /* TIMING */timing.startTimer(timerName);

                    ////////////////////////////////////////////////////////////////////////////////
                    // PARSING FILE
                    /* Timing */timing.startTimer(timerName + "-parseFile");
                    log.info("Processing File (" + i + "/" + numFiles + "): " + file.getAbsolutePath());
                    LL_Circuit circuit = parse_circuit(file);
                    /* Timing */timing.stopTimer(timerName + "-parseFile");

                    if (circuit == null) {
                        log.warning("[PARSING] ERROR: Skipping this file due to a parsing error");
                        continue;
                    } else {
                        log.fine("[PARSING] Parsed file successfully, continuing on to converting");
                    }
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // CONNECT TO UNLOCKED CIRCUIT (IF AVAILABLE)
                    if (unlocked_helper != null) {
                        if (unlocked_helper.size() == 1) {
                            File unlocked_file = unlocked_helper.get(0);
                            if (unlocked_circuit == null) {
                                /* Timing */timing.startTimer(timerName, "-parseUnlockedFile");
                                log.info("Processing Unlocked File: " + unlocked_file.getAbsolutePath());
                                unlocked_circuit = parse_circuit(unlocked_file);
                                /* Timing */timing.stopTimer(timerName, "-parseUnlockedFile");

                                if (unlocked_circuit == null) {
                                    log.warning("[PARSING] ERROR: Unable to parse the Unlocked file: " + unlocked_file);
                                    continue;
                                } else {
                                    log.fine("[PARSING] Parsed the unlocked file successfully");
                                }
                            }

                            log.config("Connecting circuit with unlocked file: " + unlocked_file);
                            circuit.unlocked_circuit = unlocked_circuit;
                        } else {
                            // search directory for the exact name match first
                            File unlocked_file = unlocked_helper.search(file.getName());

                            // search directory for name match when removing '_enc[0-9]+' from filename
                            if (unlocked_file == null) {
                                unlocked_file = unlocked_helper.search(file.getName().replaceAll("_enc[0-9]*", ""));
                            }

                            if (unlocked_file != null) {
                                /* Timing */timing.startTimer(timerName, "-parseUnlockedFile");
                                log.info("Processing Unlocked File: " + unlocked_file.getAbsolutePath());
                                circuit.unlocked_circuit = parse_circuit(unlocked_file);
                                /* Timing */timing.stopTimer(timerName, "-parseUnlockedFile");

                                if (circuit.unlocked_circuit == null) {
                                    log.warning("[PARSING] ERROR: Unable to parse the Unlocked file: " + unlocked_file);
                                    continue;
                                } else {
                                    log.fine("[PARSING] Parsed the unlocked file successfully");
                                }
                            }
                        }
                    }
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // LOAD/SAVE CIRCUIT STATS TO DATABASE
                    /* Timing */timing.startTimer(timerName + "-loadSaveCircuitStats");
                    // TODO save circuit stats to the database
                    /* Timing */timing.stopTimer(timerName + "-loadSaveCircuitStats");
                    ////////////////////////////////////////////////////////////////////////////////

                    if (solver_algorithms == null || solver_algorithms.size() == 0) {
                        solver_algorithms = new ArrayList<>();
                        solver_algorithms.add(null);
                    }

                    String orig_timerName = timerName;
                    for (Integer alg : solver_algorithms) {
                        solver_algorithm = alg;
                        if (solver_algorithm != null) timerName = orig_timerName + "-alg" + solver_algorithm;

                        ////////////////////////////////////////////////////////////////////////////////
                        // RUN A CERTAIN TASK ON THE FILE

                        LL_RunOptionI runValInst = runVal.getNewInstance();
                        if (runValInst.use_garbage_collector()) {
                            if (first) first = false;
                            else run_garbage_collector();
                        }

                        runValInst.run(this, circuit, timerName, cmd);

                        // END OF RUN A CERTAIN TASK ON THE FILE
                        ////////////////////////////////////////////////////////////////////////////////
                        /* TIMING */timing.stopTimer(timerName);
                    }
                }
                // END OF INDIVIDUAL FILE LOOP
                ////////////////////////////////////////////////////////////////////////////////
            }

            /* Timing */timing.stopTimer("totalTime");
            log.info("[TIMING] Logic Locking ILP Solver is done running; Total Time: "
                    + ShahenStrings.prettyDuration(timing.getLastElapsedTime()));
            log.fine("[TIMING] " + timing);
            timing.writeOut(new File(timingFile), false);

            if (!cmd.hasOption(LL_OptionString.NOSUMMARY.toString())) {
                System.out.println("Summary of Results:");
                System.out.println(results.getSummary());
            }

        } catch (ParseException e) {
            if (debug) {
                throw e;
            } else {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                log.severe(errors.toString());
                log.severe(e.getMessage());
                return -2;
            }
        } catch (Exception e) {
            if (debug) {
                throw e;
            } else {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                log.severe(errors.toString());
                log.severe(e.getMessage());

                return -1;
            }
        } finally {
            ////////////////////////////////////////////////////////////////////////////////
            // CLEAN UP FILE HANDLES
            for (Handler h : log.getHandlers()) {
                h.close();// must call h.close or a .LCK file will remain.
            }
            if (helper != null) helper.close();
            if (results != null) results.close();
            // CLEAN UP FILE HANDLES
            ////////////////////////////////////////////////////////////////////////////////
        }
        return 0;
    }

    private void run_garbage_collector() throws InterruptedException {
        if (garbage_collector_duration_sec > 0) {
            if (log.isLoggable(Level.INFO)) {
                System.out.println("#####################################################################");
                System.out.println("# Starting Garbage Collector (" + garbage_collector_duration_sec + " sec)");
                System.out.println("#####################################################################");
            }
            System.gc();
            for (int i = 0; i < garbage_collector_duration_sec; i++) {
                if (log.isLoggable(Level.INFO)) {
                    System.out.print((garbage_collector_duration_sec - i) + "...");
                }
                Thread.sleep(1000); // to allow GC do its job
            }
            if (log.isLoggable(Level.INFO)) {
                System.out.println("0");

                System.out.println("#####################################################################");
                System.out.println("# End of Garbage Collector");
                System.out.println("#####################################################################");
            }
        }
    }

    /**
     * Checks if the database file has the correct tables and each table has the correct schema
     * @return 0 if no errors, otherwise return error code to kill instance with
     * @throws IOException
     * @throws SQLException
     */
    public int checkDB() throws IOException, SQLException {
        if (helper.connect() != true) {
            log.severe("[Error] Unable to connect to SQLite database! file: " + dbFile + "; table: " + dbTable);
            return -1;
        }
        File schema;
        String table = "Circuit";
        if (helper.checkTableExists(table)) {
            schema = new File(dbSchemaCircuitFile);
            if (schema.exists()) {
                if (helper.verifyTableSchemaUsingString(new String(Files.readAllBytes(schema.toPath())),
                        table) != true) {
                    log.severe("[Error] SQLite database schema for table " + table + " is incorrect! file: " + dbFile);
                    return -2;
                }
            } else {
                log.warning("Schema Table File foir table " + table
                        + " is not present, unable to verify database. Schema Location: " + schema.getAbsolutePath());
            }
        } else {
            log.severe("[Error] " + table + " Table does not exists! file: " + dbFile);
            return -2;
        }
        table = "Result";
        if (helper.checkTableExists(table)) {
            schema = new File(dbSchemaResultFile);
            if (schema.exists()) {
                if (helper.verifyTableSchemaUsingString(new String(Files.readAllBytes(schema.toPath())),
                        table) != true) {
                    log.severe("[Error] SQLite database schema for table " + table + " is incorrect! file: " + dbFile);
                    return -2;
                }
            } else {
                log.warning("Schema Table File foir table " + table
                        + " is not present, unable to verify database. Schema Location: " + schema.getAbsolutePath());
            }
        } else {
            log.severe("[Error] " + table + " Table does not exists! file: " + dbFile);
            return -2;
        }
        table = "IntermediateResult";
        if (helper.checkTableExists(table)) {
            schema = new File(dbSchemaIntermediateResultFile);
            if (schema.exists()) {
                if (helper.verifyTableSchemaUsingString(new String(Files.readAllBytes(schema.toPath())),
                        table) != true) {
                    log.severe("[Error] SQLite database schema for table " + table + " is incorrect! file: " + dbFile);
                    return -2;
                }
            } else {
                log.warning("Schema Table File foir table " + table
                        + " is not present, unable to verify database. Schema Location: " + schema.getAbsolutePath());
            }
        } else {
            log.severe("[Error] " + table + " Table does not exists! file: " + dbFile);
            return -2;
        }
        return 0;
    }

    /**
     * Given a boolean array, it will increment to the next boolean array by adding the bit 1 to the input.
     * 
     * Returns null once it has overflowed the number of bits supplied
     * @param inputs
     * @return
     */
    @SuppressWarnings("unchecked")
    public static ArrayList<Boolean> getNextInput(ArrayList<Boolean> inputs) {
        boolean carry = false;

        ArrayList<Boolean> new_inputs = (ArrayList<Boolean>) inputs.clone();

        for (int i = new_inputs.size() - 1; i > -1; i--) {
            if (new_inputs.get(i)) {
                carry = true;
                new_inputs.set(i, false);
            } else {
                new_inputs.set(i, true);
                carry = false;
            }

            if (!carry) {
                break;
            }
        }

        if (carry) return null;

        return new_inputs;
    }
    public static Boolean[] getNextInput(Boolean[] inputs) {
        boolean carry = false;

        Boolean[] new_inputs = inputs.clone();

        for (int i = new_inputs.length - 1; i > -1; i--) {
            if (new_inputs[i]) {
                carry = true;
                new_inputs[i] = false;
            } else {
                new_inputs[i] = true;
                carry = false;
            }

            if (!carry) {
                break;
            }
        }

        if (carry) return null;

        return new_inputs;
    }

    public static LL_Circuit parse_circuit(File file) throws IOException {
        String inputFileContents = new String(Files.readAllBytes(file.toPath()));
        // Remove the '#' from "# key="
        inputFileContents = inputFileContents.replaceFirst("#[ \\t]*key[ \\t]*=", "key=");
        LL_AutoGen_CircuitParser parser = parserHelper.parseString(inputFileContents);

        if (parserHelper.error.errorFound) { return null; }

        LL_Circuit circuit = parser.circuit;
        circuit.filepath = file.getAbsolutePath();
        circuit.filename = file.getName();
        // remove the file extension
        circuit.filename_noext = circuit.filename.substring(0, circuit.filename.lastIndexOf('.'));
        return circuit;
    }

    private void getPerformanceOptions(CommandLine cmd) {
        ////////////////////////////////////////////////////////////////////////////////
        // READ PERFORMANCE OPTIONS
        solver_threads = LL_OptionFunctions.getIntFromOptions(cmd, LL_OptionString.THREADS.toString(), null);
        solver_stagetimeout = LL_OptionFunctions.getIntFromOptions(cmd, LL_OptionString.STAGE_TIMEOUT.toString(), null);
        solver_timeout = LL_OptionFunctions.getIntFromOptions(cmd, LL_OptionString.TIMEOUT.toString(), null);
        garbage_collector_duration_sec = LL_OptionFunctions.getIntFromOptions(cmd,
                LL_OptionString.GARBAGE_COLLECTION_SEC.toString(), garbage_collector_duration_sec);

        solver_mip_emphasis = LL_OptionFunctions.getIntFromOptions(cmd, LL_OptionString.MIP_EMPHASIS.toString(), null);
        if (solver_mip_emphasis != null && (solver_mip_emphasis < 0 || solver_mip_emphasis > 4)) {
            solver_mip_emphasis = null;
        }

        preload_num = LL_OptionFunctions.getIntFromOptions(cmd, LL_OptionString.PRELOAD.toString(), null);
        if (preload_num != null && preload_num < 0) {
            preload_num = null;
        }

        if (cmd.hasOption(LL_OptionString.PRELOAD_FILE.toString())) {
            preload_file = new File(cmd.getOptionValue(LL_OptionString.PRELOAD_FILE.toString()));

            if (!preload_file.exists()) {
                log.warning("[SETTING] Unable to find the preload file: " + preload_file.getAbsolutePath());
                preload_file = null;
            }
        }

        if (cmd.hasOption(LL_OptionString.SOLVER.toString())) {
            solver_name = cmd.getOptionValue(LL_OptionString.SOLVER.toString());
        }

        extra_key_constraint = LL_OptionFunctions.getBoolFromOptions(cmd,
                LL_OptionString.EXTRA_CONSTRAINT_KEYS_NOT_EQUAL.toString(), null);

        solver_parallel = cmd.hasOption(LL_OptionString.PARALLEL.toString());

        get_algorithms(cmd);

        reduction = get_reduction_algorithm(cmd, LL_OptionString.REDUCTION.toString(), new LL_Standard_Reduction(log));
        log.info("[SETTING] Reduction: " + reduction);
        ////////////////////////////////////////////////////////////////////////////////
    }
    /**
     * Sets all settings that were read in from the command line.
     * @param options
     */
    public void set_settings(LL_SolverOptions options, LL_Circuit circuit, LL_ReductionI reductionI, String timer_key) {
        options.log = log;
        options.circuit = circuit;

        options.timing = timing;
        options.timer_key = timer_key;
        options.results = results;

        options.set_reduction(reductionI);

        File f = new File(circuit.filename);
        options.folder_name = f.getName().substring(0, f.getName().lastIndexOf('.'));

        options.debug = debug;
        options.verify_steps = verify_steps;
        options.dump_distinguishing_inputs = dump_distinguishing_inputs;

        ////////////////////////////////////////////////////////////////////////////////
        // SET PERFORMANCE PARAMETERS
        if (reduction != null) {
            options.reduction = reduction;
        }
        log.config("[SETTING] Using Reduction Method " + options.reduction);

        if (solver_threads != null) {
            options.solver_threads = solver_threads;
        }
        log.config("[SETTING] Number of Threads set to " + options.solver_threads);

        if (solver_algorithm != null) {
            options.solver_algorithm = solver_algorithm;
        }
        log.config("[SETTING] Solver Algorithm set to " + options.solver_algorithm);

        if (solver_mip_emphasis != null) {
            options.solver_mip_emphasis = solver_mip_emphasis;
        }
        log.config("[SETTING] Solver MIP Emphasis set to " + options.solver_mip_emphasis);

        if (solver_stagetimeout != null) {
            options.solver_stagetimeout = solver_stagetimeout;
        }
        log.config("[SETTING] Solver Stage Timeout set to " + (options.solver_stagetimeout == null
                ? "null"
                : ShahenStrings.prettyDuration(options.solver_stagetimeout * 1000L)));

        if (solver_timeout != null) {
            options.solver_timeout = solver_timeout;
        }
        log.config("[SETTING] Solver Timeout set to " + (options.solver_timeout == null
                ? "null"
                : ShahenStrings.prettyDuration(options.solver_timeout * 1000L)));

        if (solver_parallel != null) {
            options.cplex_parallel = solver_parallel;
        }
        log.config("[SETTING] CPLEX Parallel set to " + options.cplex_parallel);

        if (preload_num != null) {
            options.preload_num = preload_num;
        }
        log.config("[SETTING] Number of Preloaded Equal Circuits: " + options.preload_num);

        if (preload_file != null) {
            options.preload_file = preload_file;
        }
        log.config("[SETTING] Number of Preloaded Equal Circuits File: "
                + (options.preload_file == null ? "null" : options.preload_file.getAbsolutePath()));

        if (extra_key_constraint != null) {
            options.extra_key_constraint = extra_key_constraint;
        }
        log.config("[SETTING] Extra Key Constraint: " + options.extra_key_constraint);
        ////////////////////////////////////////////////////////////////////////////////
    }

    public static Boolean[] get_boolean_string_option(CommandLine cmd, String optionStr, Boolean[] defaultVal) {
        if (cmd.hasOption(optionStr)) { return ShahenStrings.booleanStringToArray(cmd.getOptionValue(optionStr)); }
        return defaultVal;

    }

    private LL_ReductionI get_reduction_algorithm(CommandLine cmd, String optionStr, LL_Standard_Reduction defaultVal) {
        if (cmd.hasOption(optionStr)) {
            switch (cmd.getOptionValue(optionStr)) {
                case "standard" :
                    return new LL_Standard_Reduction(log);
                case "reduced" :
                    return new LL_Reduced_Reduction(log);
                case "fromsat" :
                    return new LL_CNFSAT_Reduction(log);
            }
        }
        return defaultVal;
    }

    private void get_algorithms(CommandLine cmd) {
        if (cmd.hasOption(LL_OptionString.ALGORITHM.toString())) {
            for (String s : cmd.getOptionValues(LL_OptionString.ALGORITHM.toString())) {
                if (s.toLowerCase().equals("cx")) {
                    // All available algorithms for CPLEX
                    solver_algorithms.clear();
                    for (int i = 0; i < 7; i++)
                        solver_algorithms.add(i);
                    return;
                } else if (s.toLowerCase().equals("gx")) {
                    // All available algorithms for GUROBI
                    solver_algorithms.clear();
                    for (int i = -1; i < 6; i++)
                        solver_algorithms.add(i);
                    return;
                }

                Integer i = LL_OptionFunctions.getIntFromString(s);
                if (i == null) continue;
                solver_algorithms.add(i);
            }
        }
    }

    /* ********* FUNCTIONS ********* */
    public Level getLoggerLevel() {
        return LoggerLevel;
    }

    public void setLoggerLevel(Level loggerLevel) {
        LoggerLevel = loggerLevel;
    }

    private void printSplit() {
        if (log.isLoggable(Level.INFO)) {
            System.out.println("#########################################################################");
            System.out.println("#########################################################################");
            System.out.println("#########################################################################");
        }
    }
}
