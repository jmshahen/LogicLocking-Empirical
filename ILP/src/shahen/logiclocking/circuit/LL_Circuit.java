package shahen.logiclocking.circuit;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;
import shahen.logiclocking.reduction.LL_ReductionI;
import shahen.string.ShahenStrings;

public class LL_Circuit {
    public static final Logger log = Logger.getLogger("shahen");
    /** If a circuit was read in from disk, this should be updated to reflect the filename*/
    public String filename = "unknown_circuit.bench";
    /** If a circuit was read in from disk, this should be updated to reflect the filename with no extension */
    public String filename_noext = "unknown_circuit";
    /** If a circuit was read in from disk, this should be updated to reflect the full path to the file*/
    public String filepath = "unknown_dir/unknown_circuit.bench";

    /** Boolean array of a known key (there can be potentially many keys that work)
     * in the same order as {@link #key_input_order} */
    public ArrayList<Boolean> known_key_list = new ArrayList<Boolean>();
    /** A mapping of the key wire name to the know key from {@link #known_key_list} */
    public HashMap<String, Boolean> known_key = new HashMap<String, Boolean>();
    /** Instead of a known key, we can use the unlocked circuit */
    public LL_Circuit unlocked_circuit = null;

    /** The names of the non-key input wires in the same order presented in the circuit file */
    public ArrayList<String> normal_input_order = new ArrayList<>();
    /** The names of the key input wires in the same order presented in the circuit file */
    public ArrayList<String> key_input_order = new ArrayList<>();
    /** The names of the output wires in the same order presented in the circuit file */
    public ArrayList<String> output_order = new ArrayList<>();
    /** The names of the internal wires in the same order presented in the circuit file */
    public ArrayList<String> internal_order = new ArrayList<>();

    /** The name of the input wire --&gt; the LL_Wire object */
    public HashMap<String, LL_Wire> normal_inputs = new HashMap<String, LL_Wire>();
    /** The name of the key wire --&gt; the LL_Wire object */
    public HashMap<String, LL_Wire> key_inputs = new HashMap<String, LL_Wire>();
    /** 
     * The name of the final circuit output wire --&gt; the LL_Wire object
     * These wires are not used as inputs any where in the circuit (or is assumed so)
     */
    public HashMap<String, LL_Wire> outputs = new HashMap<String, LL_Wire>();
    /** The name of the internal wire --&gt; the LL_Wire object */
    public HashMap<String, LL_Wire> internal_wires = new HashMap<String, LL_Wire>();
    /** The output wire of a gate  --&gt; gate object */
    public HashMap<LL_Wire, LL_Gate> gates = new HashMap<LL_Wire, LL_Gate>();
    /** Stores the {@link LL_On} and {@link LL_Off} gates in this circuit. */
    public HashMap<LL_Wire, LL_Gate> constant_gates = new HashMap<LL_Wire, LL_Gate>();

    /** Stores all gates that a wire is connected to as an input */
    public HashMap<LL_Wire, HashSet<LL_Gate>> wire_connections = new HashMap<LL_Wire, HashSet<LL_Gate>>();

    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    /**
     * Get the number of bits in a key
     * @return
     */
    public int keySize() {
        return key_inputs.size();
    }

    /**
     * Get the number of bits in the input
     * @return
     */
    public int inputSize() {
        return normal_inputs.size();
    }
    /**
     * Get the number of bits in the output
     * @return
     */
    public int outputSize() {
        return outputs.size();
    }
    /**
     * Get the number of bits in the output
     * @return
     */
    public int internalSize() {
        return internal_wires.size();
    }

    /**
     * Searches all of the types of wires and returns the exact reference the wire or null if the wire cannot be found
     * @param wire
     * @return
     */
    public LL_Wire get_wire(String wire) {
        if (normal_inputs.containsKey(wire)) return normal_inputs.get(wire);
        if (internal_wires.containsKey(wire)) return internal_wires.get(wire);
        if (key_inputs.containsKey(wire)) return key_inputs.get(wire);
        if (outputs.containsKey(wire)) return outputs.get(wire);
        return null;
    }

    /**
     * Return an array of size equal to {@link #key_inputs}, that is filled with false.
     * @return
     */
    public Boolean[] getZeroKey() {
        Boolean[] key = new Boolean[key_inputs.size()];
        for (int i = 0; i < key.length; i++) {
            key[i] = false;
        }
        return key;
    }
    /**
     * Return an array of size equal to {@link #key_inputs}, that is filled with true.
     * @return
     */
    public Boolean[] getOneKey() {
        Boolean[] key = new Boolean[key_inputs.size()];
        for (int i = 0; i < key.length; i++) {
            key[i] = true;
        }
        return key;
    }
    /**
     * Return an array of size equal to {@link #normal_inputs}, that is filled with false.
     * @return
     */
    public Boolean[] getZeroInputs() {
        Boolean[] inputs = new Boolean[normal_inputs.size()];
        for (int i = 0; i < inputs.length; i++) {
            inputs[i] = false;
        }
        return inputs;
    }
    /**
     * Return an array of size equal to {@link #normal_inputs}, that is filled with true.
     * @return
     */
    public Boolean[] getOneInputs() {
        Boolean[] inputs = new Boolean[normal_inputs.size()];
        for (int i = 0; i < inputs.length; i++) {
            inputs[i] = true;
        }
        return inputs;
    }
    /**
     * Return an array of size equal to {@link #normal_inputs}, that is filled with randomly selected true/false.
     * @return
     */
    public Boolean[] getRandomInputs() {
        return getRandomInputs(filename.hashCode() + System.currentTimeMillis());
    }
    /**
     * Return an array of size equal to {@link #normal_inputs}, that is filled with randomly selected true/false.
     * Random number generator is created with each call to this function.
     * 
     * @param seed able to provide a seed for the random generator
     * @return
     */
    public Boolean[] getRandomInputs(long seed) {
        Random r = new Random(seed);
        Boolean[] inputs = new Boolean[normal_inputs.size()];
        for (int i = 0; i < inputs.length; i++) {
            inputs[i] = r.nextBoolean();
        }
        return inputs;
    }

    /**
     * Return an array of size equal to {@link #normal_inputs}, that is filled with randomly selected true/false.
     * @return
     */
    public ArrayList<Boolean> getRandomInputList() {
        return getRandomInputList(filename.hashCode() + System.currentTimeMillis());
    }
    /**
     * Return an array of size equal to {@link #normal_inputs}, that is filled with randomly selected true/false.
     * Random number generator is created with each call to this function.
     * 
     * @param seed able to provide a seed for the random generator
     * @return
     */
    public ArrayList<Boolean> getRandomInputList(long seed) {
        Random r = new Random(seed);
        ArrayList<Boolean> inputs = new ArrayList<Boolean>(normal_inputs.size());
        for (int i = 0; i < normal_inputs.size(); i++) {
            inputs.add(r.nextBoolean());
        }
        return inputs;
    }

    /**
     * Return an array of size equal to {@link #key_inputs}, that is filled with randomly selected true/false.
     * @return
     */
    public Boolean[] getRandomKey() {
        return getRandomKey(filename.hashCode() + System.currentTimeMillis());
    }
    /**
     * Return an array of size equal to {@link #key_inputs}, that is filled with randomly selected true/false.
     * Random number generator is created with each call to this function.
     * 
     * @param seed able to provide a seed for the random generator
     * @return
     */
    public Boolean[] getRandomKey(long seed) {
        Random r = new Random(seed);
        Boolean[] bits = new Boolean[keySize()];
        for (int i = 0; i < bits.length; i++) {
            bits[i] = r.nextBoolean();
        }
        return bits;
    }

    public Boolean[] getActualKey() {
        return known_key_list.toArray(new Boolean[0]);
    }

    /**
     * Returns the output wires in the order they are given in the circuit description
     * @return
     */
    public ArrayList<LL_Wire> getOutputWires() {
        ArrayList<LL_Wire> out = new ArrayList<>(outputs.size());

        for (String s : output_order) {
            out.add(outputs.get(s));
        }

        return out;
    }

    /**
     * Returns the input wires in the order they are given in the circuit description
     * @return
     */
    public ArrayList<LL_Wire> getInputWires() {
        ArrayList<LL_Wire> in = new ArrayList<>(normal_inputs.size());

        for (String s : normal_input_order) {
            in.add(normal_inputs.get(s));
        }

        return in;
    }

    public Boolean[] getWireValueArray(Collection<LL_Wire> wires) {
        Boolean[] out = new Boolean[wires.size()];

        int i = 0;
        for (LL_Wire w : wires) {
            out[i] = w.get_value();
            i++;
        }

        return out;
    }

    /** 
     * Returns the output Boolean values, NULL if the wire was not set.
     * If {@link #unlocked_circuit} is NOT NULL, then that circuit's output values will be returned.
     * 
     * Call <code>getWireValueArray(getOutputWires());</code> to force the output of this circuit.
     */
    public Boolean[] getOutputValues() {
        if (unlocked_circuit != null) return unlocked_circuit.getWireValueArray(unlocked_circuit.getOutputWires());
        else return getWireValueArray(getOutputWires());
    }

    /**
     * Returns a string representation of the set of wires.
     * 
     * <br>
     * Short String Version: p00 = 1,p01 = X,p02 = 2
     * <br>
     * Long String Version:<br>
     * p00 = 1<br>
     * p01 = X<br>
     * p02 = 0
     * @param long_str
     * @return
     */
    public String getOutputWireString(Collection<LL_Wire> wires, boolean long_str) {
        StringBuilder sb = new StringBuilder();

        for (LL_Wire w : wires) {
            sb.append(w.get_name() + " = " + w.get_value_string());
            if (long_str) {
                sb.append("\n");
            } else {
                sb.append(",");
            }

        }

        return sb.toString();
    }

    public String getSingleILPString(LL_ReductionI reduction, String circuit_name, boolean include_execute_write,
            boolean include_execute_format, boolean verbose_comments) {
        StringBuilder sb = new StringBuilder();

        // Comment Header
        sb.append("/**************************************************************************\n");
        sb.append(" * Circuit Created using the Logic Locking Tool created by Jonathan Shahen\n");
        sb.append(" * Created from Filename: " + filename + "\n");
        sb.append(" * Created on: " + LocalDateTime.now().format(//
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n");
        sb.append(" * Number of Input Wires:          " + normal_inputs.size() + "\n");
        sb.append(" * Number of Output Wires:         " + outputs.size() + "\n");
        sb.append(" * Number of Internal Wires:       " + internal_wires.size() + "\n");
        // sb.append(" * Number of XOR/XNOR Extra Wires: " + xor_count + "\n");
        sb.append(" * Number of Gates:                " + gates.size() + "\n");
        sb.append(" *************************************************************************/\n\n\n");

        // Variable Declarations
        sb.append("dvar boolean " + //
                circuit_name + "_" + WIRE_TYPE.INPUT + "[0.." + (normal_inputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name + "_" + WIRE_TYPE.KEY + "[0.." + (key_inputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name + "_" + WIRE_TYPE.OUTPUT + "[0.." + (outputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + //
                circuit_name + "_" + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires.size() - 1) + "];\n");
        if (reduction.uses_extra_variables()) {
            sb.append("dvar boolean " + circuit_name + "_" + reduction.extra_variable_name() + "[0.."
                    + (reduction.set_extra_number_for_gates(gates.values()) - 1) + "];\n");
        }
        // sb.append("dvar boolean " + circuit_name + "_xor[0.." + (xor_count - 1) + "];\n\n");

        // Write the header and filename
        sb.append("execute {\n");
        sb.append("  writeln(\"Single Circuit ILP\");\n");
        sb.append("  writeln(\"Filename: " + filename.replace("\\", "\\\\").replace("\"", "\\\"") + "\\n\");\n");
        sb.append("}\n\n");

        // Subject To
        sb.append("subject to {\n");
        int constraint_count = 1;
        for (LL_Gate g : gates.values()) {
            if (verbose_comments) sb.append("  // c" + constraint_count + ": " + g.toString() + "\n");
            for (String constraint : g.getILPConstraints(reduction, circuit_name, circuit_name, circuit_name, null,
                    null)) {
                sb.append("  " + constraint + "\n");
            }

            constraint_count++;
        }
        sb.append("}\n\n");

        // Execute: Write solution to Console
        if (include_execute_write) {
            sb.append("execute {\n");

            sb.append("  writeln(\"Inputs:\");\n");
            for (LL_Wire wire : normal_inputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name, circuit_name) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name, circuit_name) + ");\n");
            }

            sb.append("  writeln(\"\\nKeys:\");\n");
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name, circuit_name) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name, circuit_name) + ");\n");
            }

            sb.append("  writeln(\"\\nOutputs:\");\n");
            for (LL_Wire wire : outputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name, circuit_name) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name, circuit_name) + ");\n");
            }

            sb.append("}\n\n");
        }

        // Execute: Format solution into more readable Tuple
        if (include_execute_format) {
            sb.append("tuple resultT {\n" + "  string type;\n" + "  string ilp_name;\n" + "  string actual_name;\n"
                    + "  int value;\n" + "};\n" + "{resultT} solution = {};\n" + "execute {\n");
            for (LL_Wire wire : normal_inputs.values()) {
                sb.append("  solution.add(\"Input\", \"" + wire.get_ilp_name(circuit_name, circuit_name) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name, circuit_name) + ");\n");
            }
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  solution.add(\"Key\", \"" + wire.get_ilp_name(circuit_name, circuit_name) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name, circuit_name) + ");\n");
            }
            for (LL_Wire wire : outputs.values()) {
                sb.append("  solution.add(\"Output\", \"" + wire.get_ilp_name(circuit_name, circuit_name) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name, circuit_name) + ");\n");
            }

            sb.append("  writeln(\"----------------------------------------\");\n");
            sb.append("  writeln(solution);\n");
            sb.append("}\n\n");
        }
        return sb.toString();
    }

    /**
     * Produce 2 circuits where the input wires are shared (using circuit_name1), unique keys/internal/outputs, 
     * and the output wires are then compared to see if there is at least 1 bit changed between the 2 sets.
     *  
     * @param circuit_name1
     * @param circuit_name2
     * @param include_execute_write
     * @param include_execute_format
     * @param verbose_comments
     * @return
     */
    public String getDiffILPString(LL_ReductionI reduction, String circuit_name1, String circuit_name2,
            boolean include_execute_write, boolean include_execute_format, boolean verbose_comments) {
        StringBuilder sb = new StringBuilder();

        // Comment Header
        sb.append("/**************************************************************************\n");
        sb.append(" * Circuit Created using the Logic Locking Tool created by Jonathan Shahen\n");
        sb.append(" * Created from Filename: " + filename + "\n");
        sb.append(" * Created on: " + LocalDateTime.now().format(//
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n");
        sb.append(" * Number of Input Wires:          " + normal_inputs.size() + "\n");
        sb.append(" * Number of Output Wires:         " + (2 * outputs.size()) + "\n");
        sb.append(" * Number of Internal Wires:       " + (2 * internal_wires.size()) + "\n");
        // sb.append(" * Number of XOR/XNOR Extra Wires: " + (2 * xor_count) + "\n");
        sb.append(" * Number of Gates:                " + (2 * gates.size()) + "\n");
        sb.append(" *************************************************************************/\n\n\n");

        // Variable Declarations
        sb.append("dvar boolean " + //
                circuit_name1 + "_" + WIRE_TYPE.INPUT + "[0.." + (normal_inputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.KEY + "[0.." + (key_inputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.KEY + "[0.." + (key_inputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.OUTPUT + "[0.." + (outputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.OUTPUT + "[0.." + (outputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + //
                circuit_name1 + "_" + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires.size() - 1) + "];\n");
        sb.append("dvar boolean " + //
                circuit_name2 + "_" + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires.size() - 1) + "];\n");
        if (reduction.uses_extra_variables()) {
            sb.append("dvar boolean " + circuit_name1 + "_" + reduction.extra_variable_name() + "[0.."
                    + (reduction.set_extra_number_for_gates(gates.values()) - 1) + "];\n");
            sb.append("dvar boolean " + circuit_name2 + "_" + reduction.extra_variable_name() + "[0.."
                    + (reduction.set_extra_number_for_gates(gates.values()) - 1) + "];\n");
        }
        // sb.append("dvar boolean " + circuit_name1 + "_xor[0.." + (xor_count - 1) + "];\n\n");
        // sb.append("dvar boolean " + circuit_name2 + "_xor[0.." + (xor_count - 1) + "];\n\n");

        // Write the header and filename
        sb.append("execute {\n");
        sb.append("  writeln(\"Diff Circuit ILP\");\n");
        sb.append("  writeln(\"Filename: " + filename.replace("\\", "\\\\").replace("\"", "\\\"") + "\\n\");\n");
        sb.append("}\n\n");

        // Subject To
        sb.append("subject to {\n");
        int constraint_count = 1;
        String circuit_name;
        for (int i = 0; i < 2; i++) {
            if (i == 0) circuit_name = circuit_name1;
            else circuit_name = circuit_name2;

            for (LL_Gate g : gates.values()) {
                if (verbose_comments) sb.append("  // c" + constraint_count + ": " + g.toString() + "\n");
                for (String constraint : g.getILPConstraints(reduction, circuit_name, circuit_name1, circuit_name, null,
                        null)) {
                    sb.append("  " + constraint + "\n");
                }

                constraint_count++;
            }
        }
        if (verbose_comments) {
            sb.append("  // c" + constraint_count + ": Ensure that both outputs are different "
                    + "for the same input but different keys\n");
        }
        // SUM (i in 0 .. num_outputs) |c1_outputs[i] - c2_outputs[i]| >= 1
        sb.append("  sum(i in 0.." + (outputs.size() - 1) + ") abs(" +//
                circuit_name1 + "_" + WIRE_TYPE.OUTPUT + "[i] - " + //
                circuit_name2 + "_" + WIRE_TYPE.OUTPUT + "[i])" +//
                " >= 1;\n");
        sb.append("}\n\n");

        // Execute: Write solution to Console
        if (include_execute_write) {
            sb.append("execute {\n");

            sb.append("  writeln(\"Inputs:\");\n");
            for (LL_Wire wire : normal_inputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name1, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }

            sb.append("  writeln(\"\\nKeys (" + circuit_name1 + "):\");\n");
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name1, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            sb.append("  writeln(\"\\nKeys (" + circuit_name2 + "):\");\n");
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name2, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name2, circuit_name1) + ");\n");
            }

            sb.append("  writeln(\"\\nOutputs (" + circuit_name1 + "):\");\n");
            for (LL_Wire wire : outputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name1, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            sb.append("  writeln(\"\\nOutputs (" + circuit_name2 + "):\");\n");
            for (LL_Wire wire : outputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name2, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name2, circuit_name1) + ");\n");
            }

            sb.append("}\n\n");
        }

        // Execute: Format solution into more readable Tuple
        if (include_execute_format) {
            sb.append("tuple resultT {\n" + "  string type;\n" + "  string ilp_name;\n" + "  string actual_name;\n"
                    + "  int value;\n" + "};\n" + "{resultT} solution = {};\n" + "execute {\n");
            for (LL_Wire wire : normal_inputs.values()) {
                sb.append("  solution.add(\"Input\", \"" + wire.get_ilp_name(circuit_name1, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  solution.add(\"Key\", \"" + wire.get_ilp_name(circuit_name1, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  solution.add(\"Key\", \"" + wire.get_ilp_name(circuit_name2, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name2, circuit_name1) + ");\n");
            }
            for (LL_Wire wire : outputs.values()) {
                sb.append("  solution.add(\"Output\", \"" + wire.get_ilp_name(circuit_name1, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            for (LL_Wire wire : outputs.values()) {
                sb.append("  solution.add(\"Output\", \"" + wire.get_ilp_name(circuit_name2, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name2, circuit_name1) + ");\n");
            }

            sb.append("  writeln(\"----------------------------------------\");\n");
            sb.append("  writeln(solution);\n");
            sb.append("}\n\n");
        }
        return sb.toString();
    }
    /**
     * Produce 2 circuits where the input wires are shared (using circuit_name1), unique keys/internal/outputs, 
     * and the output wires are then compared to see if there is at least 1 bit changed between the 2 sets.
     *  
     * @param circuit_name1
     * @param circuit_name2
     * @param forced_inputs
     * @param include_execute_write
     * @param include_execute_format
     * @param verbose_comments
     * @return
     */
    public String getEqualILPString(LL_ReductionI reduction, String circuit_name1, String circuit_name2,
            Boolean[] force_inputs, Boolean[] force_outputs, boolean include_execute_write,
            boolean include_execute_format, boolean verbose_comments) {
        StringBuilder sb = new StringBuilder();

        // Grab random inputs if none supplied
        if (force_inputs == null) {
            force_inputs = getRandomInputs();
        }
        // Calculate the blackbox output if no outputs provided
        if (force_outputs == null) {
            get_output(Arrays.asList(force_inputs));
            force_outputs = getOutputValues();
        }

        // Comment Header
        sb.append("/**************************************************************************\n");
        sb.append(" * Circuit Created using the Logic Locking Tool created by Jonathan Shahen\n");
        sb.append(" * Created from Filename: " + filename + "\n");
        sb.append(" * Created on: " + LocalDateTime.now().format(//
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n");
        sb.append(" * Number of Input Wires:          " + normal_inputs.size() + "\n");
        sb.append(" * Number of Output Wires:         " + (2 * outputs.size()) + "\n");
        sb.append(" * Number of Internal Wires:       " + (2 * internal_wires.size()) + "\n");
        // sb.append(" * Number of XOR/XNOR Extra Wires: " + (2 * xor_count) + "\n");
        sb.append(" * Number of Gates:                " + (2 * gates.size()) + "\n");
        sb.append(" *************************************************************************/\n\n\n");

        // Variable Declarations
        sb.append("dvar boolean " + //
                circuit_name1 + "_" + WIRE_TYPE.INPUT + "[0.." + (normal_inputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.KEY + "[0.." + (key_inputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.OUTPUT + "[0.." + (outputs.size() - 1) + "];\n");
        sb.append("dvar boolean " + //
                circuit_name1 + "_" + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires.size() - 1) + "];\n");
        if (reduction.uses_extra_variables()) {
            sb.append("dvar boolean " + circuit_name1 + "_" + reduction.extra_variable_name() + "[0.."
                    + (reduction.set_extra_number_for_gates(gates.values()) - 1) + "];\n");
        }

        // Circuit 2
        if (circuit_name2 != null) {
            sb.append(
                    "dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.KEY + "[0.." + (key_inputs.size() - 1) + "];\n");
            sb.append(
                    "dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.OUTPUT + "[0.." + (outputs.size() - 1) + "];\n");
            sb.append("dvar boolean " + //
                    circuit_name2 + "_" + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires.size() - 1) + "];\n");
            if (reduction.uses_extra_variables()) {
                sb.append("dvar boolean " + circuit_name2 + "_" + reduction.extra_variable_name() + "[0.."
                        + (reduction.set_extra_number_for_gates(gates.values()) - 1) + "];\n");
            }
        }
        // sb.append("dvar boolean " + circuit_name1 + "_xor[0.." + (xor_count - 1) + "];\n\n");
        // sb.append("dvar boolean " + circuit_name2 + "_xor[0.." + (xor_count - 1) + "];\n\n");

        // Write the header and filename
        sb.append("execute {\n");
        sb.append("  writeln(\"Equal Circuit ILP\");\n");
        sb.append("  writeln(\"Filename: " + filename.replace("\\", "\\\\").replace("\"", "\\\"") + "\\n\");\n");
        sb.append("  writeln(\"Forced Inputs : " + ShahenStrings.toBooleanString(force_inputs) + "\");\n");
        sb.append("  writeln(\"Forced Outputs: " + ShahenStrings.toBooleanString(force_outputs) + "\");\n");
        sb.append("}\n\n");

        // Subject To
        sb.append("subject to {\n");
        int constraint_count = 1;
        String circuit_name;
        for (int i = 0; i < 2; i++) {
            if (i == 0) circuit_name = circuit_name1;
            else circuit_name = circuit_name2;

            if (circuit_name == null) {
                break;
            }

            for (LL_Gate g : gates.values()) {
                if (verbose_comments) sb.append("  // c" + constraint_count + ": " + g.toString() + "\n");
                for (String constraint : g.getILPConstraints(reduction, circuit_name, circuit_name1, circuit_name,
                        force_inputs, force_outputs)) {
                    sb.append("  " + constraint + "\n");
                }

                constraint_count++;
            }
        }
        if (verbose_comments) {
            sb.append("\n  // Hard Code the Input's Values\n");
        }
        // Hard code the Inputs
        for (int i = 0; i < force_inputs.length; i++) {
            // to_double(inputs[i]) == circuita.inputs[i]
            sb.append("  " + ((force_inputs[i]) ? "1" : "0") + " == "
                    + normal_inputs.get(normal_input_order.get(i)).get_ilp_name(circuit_name1, circuit_name1) + ";\n");
        }

        if (verbose_comments) {
            sb.append("\n  // Force circuit outputs to match correct_outputs\n");
        }
        // force circuit outputs to match correct_outputs
        for (int i = 0; i < force_outputs.length; i++) {
            sb.append("  " + ((force_outputs[i]) ? "1" : "0") + " == "
                    + outputs.get(output_order.get(i)).get_ilp_name(circuit_name1, circuit_name1) + ";\n");
        }
        if (circuit_name2 != null) {
            for (int i = 0; i < force_outputs.length; i++) {
                sb.append("  " + ((force_outputs[i]) ? "1" : "0") + " == "
                        + outputs.get(output_order.get(i)).get_ilp_name(circuit_name2, circuit_name1) + ";\n");
            }
        }

        sb.append("}\n\n");// end of subject to

        // Execute: Write solution to Console
        if (include_execute_write) {
            sb.append("execute {\n");

            sb.append("  writeln(\"Inputs:\");\n");
            for (LL_Wire wire : normal_inputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name1, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }

            sb.append("  writeln(\"\\nKeys (" + circuit_name1 + "):\");\n");
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name1, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }

            if (circuit_name2 != null) {
                sb.append("  writeln(\"\\nKeys (" + circuit_name2 + "):\");\n");
                for (LL_Wire wire : key_inputs.values()) {
                    sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name2, circuit_name1) + "(" + wire.get_name()
                            + "): \", " + wire.get_ilp_name(circuit_name2, circuit_name1) + ");\n");
                }
            }

            sb.append("  writeln(\"\\nOutputs (" + circuit_name1 + "):\");\n");
            for (LL_Wire wire : outputs.values()) {
                sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name1, circuit_name1) + "(" + wire.get_name()
                        + "): \", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            if (circuit_name2 != null) {
                sb.append("  writeln(\"\\nOutputs (" + circuit_name2 + "):\");\n");
                for (LL_Wire wire : outputs.values()) {
                    sb.append("  writeln(\"  " + wire.get_ilp_name(circuit_name2, circuit_name1) + "(" + wire.get_name()
                            + "): \", " + wire.get_ilp_name(circuit_name2, circuit_name1) + ");\n");
                }
            }

            sb.append("}\n\n");
        }

        // Execute: Format solution into more readable Tuple
        if (include_execute_format) {
            sb.append("tuple resultT {\n" + "  string type;\n" + "  string ilp_name;\n" + "  string actual_name;\n"
                    + "  int value;\n" + "};\n" + "{resultT} solution = {};\n" + "execute {\n");
            for (LL_Wire wire : normal_inputs.values()) {
                sb.append("  solution.add(\"Input\", \"" + wire.get_ilp_name(circuit_name1, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            for (LL_Wire wire : key_inputs.values()) {
                sb.append("  solution.add(\"Key\", \"" + wire.get_ilp_name(circuit_name1, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            if (circuit_name2 != null) {
                for (LL_Wire wire : key_inputs.values()) {
                    sb.append("  solution.add(\"Key\", \"" + wire.get_ilp_name(circuit_name2, circuit_name1) + "\",\""
                            + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name2, circuit_name1) + ");\n");
                }
            }
            for (LL_Wire wire : outputs.values()) {
                sb.append("  solution.add(\"Output\", \"" + wire.get_ilp_name(circuit_name1, circuit_name1) + "\",\""
                        + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name1, circuit_name1) + ");\n");
            }
            if (circuit_name2 != null) {
                for (LL_Wire wire : outputs.values()) {
                    sb.append("  solution.add(\"Output\", \"" + wire.get_ilp_name(circuit_name2, circuit_name1)
                            + "\",\"" + wire.get_name() + "\", " + wire.get_ilp_name(circuit_name2, circuit_name1)
                            + ");\n");
                }
            }
            sb.append("  writeln(\"----------------------------------------\");\n");
            sb.append("  writeln(solution);\n");
            sb.append("}\n\n");
        }
        return sb.toString();
    }

    /**
     * Adds a new input wire, returns a non-zero if that wire has already been added (as output or input)
     * @param name the name of the wire
     * @return 0 if no errors occurred
     */
    public int add_output_wire(String name) {
        if (outputs.containsKey(name)) { return 1; }
        if (normal_inputs.containsKey(name)) { return 2; }
        if (key_inputs.containsKey(name)) { return 3; }

        output_order.add(name);
        outputs.put(name, new LL_Wire(name, outputs.size(), WIRE_TYPE.OUTPUT));

        return 0;
    }
    /**
     * Adds a new input wire, returns a non-zero if that wire has already been added (as output or input).
     * 
     * Key wires must be named 'keyinputX' (where X is an integer, starting at 0, no padding)
     * @param name the name of the wire
     * @return 0 if no errors occurred
     */
    public int add_input_wire(String name) {
        if (outputs.containsKey(name)) { return 1; }
        if (normal_inputs.containsKey(name)) { return 2; }
        if (key_inputs.containsKey(name)) { return 3; }

        if (name.startsWith("keyinput")) {
            key_input_order.add(name);
            key_inputs.put(name, new LL_Wire(name, key_inputs.size(), WIRE_TYPE.KEY));
        } else {
            normal_input_order.add(name);
            normal_inputs.put(name, new LL_Wire(name, normal_inputs.size(), WIRE_TYPE.INPUT));
        }

        return 0;
    }

    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * @param type
     * @param in1
     * @param in2
     * @param out
     * @return 0 if no errors occurred<br/>
     *    Error Codes:
     *    <ul>
     *    <li>1 -- in1 cannot be null
     *    <li>2 -- out cannot be null
     *    <li>3 -- g cannot be null
     *    <li>4 -- the out wire is already connected to the output of a gate
     *    <li>5 -- not all of the wires are set in the gate
     *    </ul>
     */
    public int add_gate(LL_Gate g, String in1, String in2, String out) {
        // Error Checks
        if (in1 == null) return 1;
        if (out == null) return 2;
        if (g == null) return 3;
        if (gates.containsKey(new LL_Wire(out, 0, WIRE_TYPE.INTERNAL))) return 4;

        g.in1 = get_wire(in1);
        if (g.in1 == null) {
            g.in1 = new_internal_wire(in1);
        }

        if (g.numInputs() > 1) {
            g.in2 = get_wire(in2);
            if (g.in2 == null) {
                g.in2 = new_internal_wire(in2);
            }
        }

        // Check if out is an output wire, internal wire, or if an internal wire needs to be created
        if (outputs.containsKey(out)) {
            g.out = outputs.get(out);
        } else if (internal_wires.containsKey(out)) {
            g.out = internal_wires.get(out);
        } else {
            g.out = new_internal_wire(out);
        }

        if (!g.areAllWiresSet()) return 5;

        // Add gate to the circuit structure
        connect_gate(g);

        return 0;
    }

    private void connect_gate(LL_Gate g) {
        gates.put(g.out, g);

        for (LL_Wire w : g.getInputWires()) {
            if (wire_connections.containsKey(w)) wire_connections.get(w).add(g);
            else {
                HashSet<LL_Gate> tmp = new HashSet<LL_Gate>();
                tmp.add(g);
                wire_connections.put(w, tmp);
            }
        }
    }

    private LL_Wire new_internal_wire(String wire) {
        // Create an internal wire
        LL_Wire internal = new LL_Wire(wire, internal_wires.size(), WIRE_TYPE.INTERNAL);

        internal_order.add(wire);
        internal_wires.put(wire, internal);
        return internal;
    }

    /**
     * Adds an {@link LL_On} or {@link LL_Off} gate to the circuit.
     * @param out
     * @param When TRUE, this will add an {@link LL_On} gate, otherwise an {@link LL_Off} gate will be added.
     * @return 0 if no errors occurred
     */
    public int add_constant_gate(String out, boolean on_gate) {
        // Error Checks
        if (out == null) return 1;
        if (gates.containsKey(new LL_Wire(out, 0, WIRE_TYPE.INTERNAL))) return 4;

        // Check if out is an output wire, internal wire, or if an internal wire needs to be created
        LL_Wire out_wire;
        if (outputs.containsKey(out)) {
            out_wire = outputs.get(out);
        } else if (internal_wires.containsKey(out)) {
            out_wire = internal_wires.get(out);
        } else {
            out_wire = new LL_Wire(out, internal_wires.size(), WIRE_TYPE.INTERNAL);

            internal_order.add(out);
            internal_wires.put(out, out_wire);
        }
        LL_Gate g = (on_gate) ? new LL_On(out_wire) : new LL_Off(out_wire);

        gates.put(g.out, g);
        constant_gates.put(g.out, g);

        return 0;
    }

    /**
     * Adds an {@link LL_On} gate to the circuit.
     * @param out
     * @return 0 if no errors occurred
     */
    public int add_on_gate(String out) {
        return add_constant_gate(out, true);
    }

    /**
     * Adds an {@link LL_Off} gate to the circuit.
     * @param out
     * @return 0 if no errors occurred
     */
    public int add_off_gate(String out) {
        return add_constant_gate(out, false);
    }

    /**
     * 
     * @param in
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_not_gate(String in, String out) {
        return add_gate(new LL_Not(), in, null, out);
    }
    /**
     * 
     * @param in
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_buf_gate(String in, String out) {
        return add_gate(new LL_Buf(), in, null, out);
    }
    /**
     * 
     * @param in1
     * @param in2
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_and_gate(String in1, String in2, String out) {
        return add_gate(new LL_And(), in1, in2, out);
    }
    /**
     * 
     * @param in1
     * @param in2
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_or_gate(String in1, String in2, String out) {
        return add_gate(new LL_Or(), in1, in2, out);

    }
    /**
     * 
     * @param in1
     * @param in2
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_xor_gate(String in1, String in2, String out) {
        return add_gate(new LL_Xor(), in1, in2, out);

    }
    /**
     * 
     * @param in1
     * @param in2
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_nand_gate(String in1, String in2, String out) {
        return add_gate(new LL_Nand(), in1, in2, out);

    }
    /**
     * 
     * @param in1
     * @param in2
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_nor_gate(String in1, String in2, String out) {
        return add_gate(new LL_Nor(), in1, in2, out);

    }
    /**
     * 
     * @param in1
     * @param in2
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_xnor_gate(String in1, String in2, String out) {
        return add_gate(new LL_Xnor(), in1, in2, out);
    }
    /**
     * 
     * @param in1
     * @param in2
     * @param out
     * @return non-zero if an error occurred<br/>
     *     Error Codes:
     *     <ul>
     *     <li>1 -- g cannot be null
     *     <li>2 -- s cannot be null
     *     <li>3 -- out cannot be null
     *     <li>4 -- out is already connected to the output of another gate
     *     <li>5 -- in1 or in2 cannot be null when adding a full Mux gate
     *     <li>6 -- both in1 and in2 cannot be null
     *     </ul>
     */
    public int add_mux_gate(LL_Mux g, String in1, String in2, String s, String out) {
        // Error Checks
        if (g == null) return 1;
        if (s == null) return 2;
        if (out == null) return 3;
        if (gates.containsKey(new LL_Wire(out, 0, WIRE_TYPE.INTERNAL))) return 4;
        if (g.numInputs() == 3 && (in1 == null || in2 == null)) return 5;
        if (in1 == null && in2 == null) return 6;

        // Grabbing or Setting-up the wires
        if (g.numInputs() == 3 || g instanceof LL_MuxX0 || g instanceof LL_MuxX1) {
            g.in1 = get_wire(in1);
            if (g.in1 == null) {
                g.in1 = new_internal_wire(in1);
            }
        }

        if (g.numInputs() == 3 || g instanceof LL_Mux0X || g instanceof LL_Mux1X) {
            g.in2 = get_wire(in2);
            if (g.in2 == null) {
                g.in2 = new_internal_wire(in2);
            }
        }
        g.s = get_wire(s);
        if (g.s == null) {
            g.s = new_internal_wire(s);
        }

        // Check if out is an output wire, internal wire, or if an internal wire needs to be created
        if (outputs.containsKey(out)) {
            g.out = outputs.get(out);
        } else if (internal_wires.containsKey(out)) {
            g.out = internal_wires.get(out);
        } else {
            g.out = new_internal_wire(out);
        }

        if (!g.areAllWiresSet()) return 5;

        // Add gate to the circuit structure
        connect_gate(g);

        return 0;
    }

    /**
     * 
     * @param type
     * @param in1
     * @param in2
     * @param out
     * @return 0 if no errors occurred
     */
    public int add_large_gate(LL_LargeGate g, ArrayList<String> inputs, String out) {
        // Error Checks
        if (g == null) { return 1; }
        for (String in : inputs) {
            if (in == null) return 2;
        }
        if (out == null) return 3;
        if (gates.containsKey(new LL_Wire(out, 0, WIRE_TYPE.INTERNAL))) return 4;

        LL_Wire tmpWire;
        for (String in : inputs) {
            tmpWire = get_wire(in);
            if (tmpWire == null) {
                // Create an internal wire
                tmpWire = new LL_Wire(in, internal_wires.size(), WIRE_TYPE.INTERNAL);

                internal_order.add(in);
                internal_wires.put(in, tmpWire);
            }
            g.inputs.add(tmpWire);
        }

        // Check if out is an output wire, internal wire, or if an internal wire needs to be created
        if (outputs.containsKey(out)) {
            g.out = outputs.get(out);
        } else if (internal_wires.containsKey(out)) {
            g.out = internal_wires.get(out);
        } else {
            tmpWire = new LL_Wire(out, internal_wires.size(), WIRE_TYPE.INTERNAL);

            internal_order.add(out);
            internal_wires.put(out, tmpWire);
            g.out = tmpWire;
        }

        if (!g.areAllWiresSet()) return 5;

        // Add gate to the circuit structure
        gates.put(g.out, g);
        for (LL_Wire in : g.inputs) {
            if (wire_connections.containsKey(in)) wire_connections.get(in).add(g);
            else {
                HashSet<LL_Gate> tmp = new HashSet<LL_Gate>();
                tmp.add(g);
                wire_connections.put(in, tmp);
            }
        }

        return 0;
    }

    /**
     * 
     * @param inputs
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_large_and_gate(ArrayList<String> inputs, String out) {
        return add_large_gate(new LL_Large_And(), inputs, out);
    }

    /**
     * 
     * @param inputs
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_large_or_gate(ArrayList<String> inputs, String out) {
        return add_large_gate(new LL_Large_Or(), inputs, out);
    }
    /**
     * 
     * @param inputs
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_large_nand_gate(ArrayList<String> inputs, String out) {
        return add_large_gate(new LL_Large_Nand(), inputs, out);
    }

    /**
     * 
     * @param inputs
     * @param out
     * @return non-zero if an error occurred
     */
    public int add_large_nor_gate(ArrayList<String> inputs, String out) {
        return add_large_gate(new LL_Large_Nor(), inputs, out);
    }

    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    /**
     * Calls {@link #get_output(Boolean[])}
     * @param input
     * @return
     */
    public boolean get_output(List<Boolean> input) {
        return get_output(input.toArray(new Boolean[0]));
    }

    /**
     * 
     * If {@link #unlocked_circuit} is NULL, Calls {@link #get_output(Boolean[], Boolean[])} using the {@link #known_key_list} as the second parameter.
     * Otherwise, we call <code>unlocked_circuit.get_output(input);</code>
     * @param input
     * @return
     */
    public boolean get_output(Boolean[] input) {
        if (unlocked_circuit != null) {
            return unlocked_circuit.get_output(input);
        } else {
            return get_output(input, known_key_list.toArray(new Boolean[0]));
        }
    }

    /**
     * Feeds the input values into the {@link #normal_inputs}
     * @param input
     * @param key
     * @return TRUE if no errors occurred
     */
    public boolean get_output(List<Boolean> input, List<Boolean> key) {
        return get_output(input.toArray(new Boolean[0]), key.toArray(new Boolean[0]));
    }

    /**
     * Feeds the input values into the {@link #normal_inputs}.
     * 
     * <b>Resets the circuit before propagating values</b>
     * 
     * @param input
     * @param key
     * @return TRUE if no errors occurred
     */
    public boolean get_output(Boolean[] input, Boolean[] key) {
        Stack<LL_Gate> working_gates = new Stack<>();
        Stack<LL_Gate> delayed_gates = new Stack<>();

        // Error checking
        if (input.length != normal_input_order.size()) {
            log.severe("The input binary string is not the correct size, expected " + normal_input_order.size()
                    + " inputs but received " + input.length + " inputs.");
            return false;
        }
        if (key.length != key_input_order.size()) {
            log.severe("The key binary string is not the correct size, expected " + key_input_order.size()
                    + " inputs but received " + key.length + " inputs.");
            return false;
        }

        // Reset circuit
        reset();

        for (int j = 0; j < input.length; j++) {
            LL_Wire k = normal_inputs.get(normal_input_order.get(j));
            k.set_value(input[j]);

            if (wire_connections.containsKey(k)) working_gates.addAll(wire_connections.get(k));
        }
        for (int j = 0; j < key_inputs.size(); j++) {
            LL_Wire k = key_inputs.get(key_input_order.get(j));
            k.set_value(key[j]);

            if (wire_connections.containsKey(k)) working_gates.addAll(wire_connections.get(k));
        }

        // Add all constant gates since we know their values
        for (LL_Wire w : constant_gates.keySet()) {
            working_gates.add(constant_gates.get(w));
        }

        boolean work_done = false;
        while (!working_gates.isEmpty() || !delayed_gates.isEmpty()) {
            LL_Gate g = working_gates.pop();
            // log.fine("Working on gate: " + g.longString());

            if (g.canPropagate()) {
                work_done = true;
                if (!g.propagate()) log.severe("An Error occurred when trying to proagate gate: " + g.longString());
                log.finest("Propagated Gate: " + g.longString());

                if (wire_connections.containsKey(g.out)) {
                    working_gates.addAll(wire_connections.get(g.out));
                } else log.finest("Skipping adding wire: " + g.out);
            } else if (!g.hasPropagated()) {
                delayed_gates.push(g);
            }

            // Move the delayed gates to the working set and
            if (working_gates.isEmpty() && !delayed_gates.isEmpty()) {
                if (!work_done) {
                    log.severe("Error: an infinite loop detected in the circuit propagation function. "
                            + "This means that the circuit is incomplete and not all gates can propagte there values.");
                    if (log.isLoggable(Level.FINE)) {
                        for (LL_Gate dg : delayed_gates) {
                            log.fine("Delayed Gates: " + dg.longString());
                        }
                    }
                    return false;
                }

                working_gates.addAll(delayed_gates);
                delayed_gates.clear();
                work_done = false;
            }
        }

        return true;
    }

    /**
     * Feeds the input values into the {@link #normal_inputs}
     * @param input
     * @param key
     * @return TRUE if no errors occurred
     */
    public boolean infer_output(Boolean[] input, Boolean[] key) {
        Set<LL_Gate> working_gates = new HashSet<>();

        // Reset circuit
        reset();

        if (input != null) {
            // Error checking
            if (input.length != inputSize()) {
                log.severe("The input binary string is not the correct size, expected " + normal_input_order.size()
                        + " inputs but received " + input.length + " inputs.");
                return false;
            }

            for (int j = 0; j < input.length; j++) {
                LL_Wire k = normal_inputs.get(normal_input_order.get(j));
                k.set_value(input[j]);

                if (wire_connections.containsKey(k)) working_gates.addAll(wire_connections.get(k));
            }
        }
        if (key != null) {
            // Error checking
            if (key.length != keySize()) {
                log.severe("The key binary string is not the correct size, expected " + keySize()
                        + " bits but received " + key.length + " bits.");
                return false;
            }

            for (int j = 0; j < key.length; j++) {
                LL_Wire k = key_inputs.get(key_input_order.get(j));
                k.set_value(key[j]);

                if (wire_connections.containsKey(k)) working_gates.addAll(wire_connections.get(k));
            }
        }

        while (true) {
            // Perform a pop
            Iterator<LL_Gate> iter = working_gates.iterator();
            if (!iter.hasNext()) break;
            LL_Gate g = iter.next();
            iter.remove();

            log.finest("Inferring Gate: " + g.longString());
            if (g.infer_value()) {
                if (wire_connections.containsKey(g.out)) {
                    working_gates.addAll(wire_connections.get(g.out));
                } else log.finest("Skipping adding wire: " + g.out);
            }
        }

        return true;
    }

    /**
     * Iterates through all wires and resets them to no value
     */
    public void reset() {
        for (LL_Wire w : normal_inputs.values()) {
            w.reset_wire();
        }
        for (LL_Wire w : key_inputs.values()) {
            w.reset_wire();
        }
        for (LL_Wire w : outputs.values()) {
            w.reset_wire();
        }
        for (LL_Wire w : internal_wires.values()) {
            w.reset_wire();
        }

        if (unlocked_circuit != null) unlocked_circuit.reset();
    }

    /**
     * Slices all gates and wires that actually effect the value of a wire (any wire)
     * @param wire usually an output wire, but can be any wire name (the name of the input bench file)
     * @return 
     */
    public LL_Circuit slice(String wire, boolean keepAllInputs) {
        LL_Circuit sliced = new LL_Circuit();

        ArrayList<LL_Gate> my_gates = new ArrayList<>();
        Stack<LL_Wire> working_wires = new Stack<>();

        HashSet<LL_Wire> my_input_wires = new HashSet<LL_Wire>();
        HashSet<LL_Wire> my_output_wires = new HashSet<LL_Wire>();
        HashSet<LL_Wire> my_key_wires = new HashSet<LL_Wire>();
        HashSet<LL_Wire> my_internal_wires = new HashSet<LL_Wire>();

        working_wires.add(new LL_Wire(wire, 0, WIRE_TYPE.OUTPUT));

        // Check if the wire name exists in the circuit
        if (!gates.containsKey(working_wires.get(0))) { return null; }

        LL_Wire cur_wire;
        LL_Gate cur_gate;
        while (!working_wires.isEmpty()) {
            cur_wire = working_wires.pop();

            switch (cur_wire.get_type()) {
                case INPUT :
                    my_input_wires.add(cur_wire);
                    break;
                case OUTPUT :
                    my_output_wires.add(cur_wire);
                    break;
                case KEY :
                    my_key_wires.add(cur_wire);
                    break;
                case INTERNAL :
                    my_internal_wires.add(cur_wire);
                    break;
            }

            if (gates.containsKey(cur_wire)) {
                cur_gate = gates.get(cur_wire);
                my_gates.add(cur_gate);

                working_wires.addAll(cur_gate.getInputWires());
            }
        }

        // Add input Wires
        for (int i = 0; i < normal_input_order.size(); i++) {
            if (keepAllInputs || my_input_wires.contains(new LL_Wire(normal_input_order.get(i), 0, WIRE_TYPE.INPUT))) {
                sliced.add_input_wire(normal_input_order.get(i));
            }
        }
        // Add key Wires
        for (int i = 0; i < key_input_order.size(); i++) {
            if (keepAllInputs || my_key_wires.contains(new LL_Wire(key_input_order.get(i), 0, WIRE_TYPE.INPUT))) {
                sliced.add_input_wire(key_input_order.get(i));
            }
        }
        // Add output Wire
        sliced.add_output_wire(wire);

        for (LL_Gate g : my_gates) {
            if (g.numInputs() == 1) {
                sliced.add_gate(g.new_gate(), g.in1.get_name(), null, g.out.get_name());
            } else if (g.numInputs() == 2) {
                sliced.add_gate(g.new_gate(), g.in1.get_name(), g.in2.get_name(), g.out.get_name());
            } else {
                ArrayList<String> cur_gate_inputs = new ArrayList<>();
                for (LL_Wire w : g.getInputWires()) {
                    cur_gate_inputs.add(w.get_name());
                }

                sliced.add_large_gate((LL_LargeGate) g.new_gate(), cur_gate_inputs, g.out.get_name());
            }
        }

        if (!my_key_wires.isEmpty()) {
            // Convert the key to these keys
            ArrayList<Boolean> my_key = new ArrayList<Boolean>();

            for (int i = 0; i < key_input_order.size(); i++) {
                if (my_key_wires.contains(new LL_Wire(key_input_order.get(i), 0, WIRE_TYPE.KEY))) {
                    my_key.add(known_key.get(key_input_order.get(i)));
                    sliced.known_key.put(key_input_order.get(i), my_key.get(my_key.size() - 1));
                }
            }
            sliced.known_key_list = my_key;
        }

        return sliced;
    }

    /**
     * Outputs the string for the .bench file format
     * @return
     */
    public String getBenchString() {
        StringBuilder sb = new StringBuilder();

        // Key
        if (!known_key_list.isEmpty()) {
            sb.append("# key=").append(ShahenStrings.toBooleanString(known_key_list)).append("\n");
        }

        // Add input Wires
        for (String s : normal_input_order) {
            sb.append("INPUT(").append(s).append(")\n");
        }
        // Add key Wires
        for (String s : key_input_order) {
            sb.append("INPUT(").append(s).append(")\n");
        }
        // Add output Wires
        for (String s : output_order) {
            sb.append("OUTPUT(").append(s).append(")\n");
        }

        // Gates
        for (LL_Gate g : gates.values()) {
            sb.append(g.getBenchString()).append("\n");
        }

        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (LL_Gate g : gates.values()) {
            sb.append(g.longString()).append("\n");
        }

        return sb.toString();
    }

    /** Returns TRUE if the wire does not exists in the circuit or does not have any connections. FALSE otherwise.*/
    public boolean is_wire_orphan(LL_Wire w) {
        if (!wire_connections.containsKey(w)) return true;
        if (wire_connections.get(w).size() == 0 && !gates.containsKey(w)) return true;

        return false;
    }
}
