package shahen.logiclocking.circuit;

import java.util.Stack;

import shahen.logiclocking.circuit.gates.LL_Gate;
import shahen.logiclocking.circuit.wires.LL_Wire;

public class LL_SimplifiedCircuit extends LL_Circuit {
    public LL_Circuit original_circuit;
    public Boolean[] forced_inputs = null;
    public Boolean[] forced_key = null;
    // public Boolean[] correct_outputs;

    public LL_SimplifiedCircuit(LL_Circuit circuit, Boolean[] inputs, Boolean[] key) {
        if (circuit == null) throw new IllegalArgumentException("Circuit cannot be null");
        if (inputs == null && key == null) throw new IllegalArgumentException("inputs and key cannot both be null");

        // if (circuit.get_output(inputs)) this.correct_outputs = circuit.getWireValueArray(circuit.getOutputWires());
        // else throw new IllegalArgumentException("Unable to obtain the output values from the input values
        // provided.");

        if (!circuit.infer_output(inputs, key))
            throw new IllegalArgumentException("Unable to Infer output using inputs provided");

        original_circuit = circuit;
        this.forced_inputs = inputs;
        this.forced_key = key;

        copy_simplified();
    }

    private void copy_simplified() {
        filepath = original_circuit.filepath;
        filename = original_circuit.filename;
        filename_noext = original_circuit.filename_noext;
        unlocked_circuit = original_circuit.unlocked_circuit;

        // Copy Output wires
        for (String o : original_circuit.output_order) {
            add_output_wire(o);
        }

        if (forced_key == null) {
            // Copy Key wires
            for (String k : original_circuit.key_input_order) {
                add_input_wire(k);
            }
        } else {
            // Copy Some Key wires
            for (int i = 0; i < forced_key.length; i++) {
                if (forced_key[i] == null) {
                    add_input_wire(original_circuit.key_input_order.get(i));
                }
            }
        }

        if (forced_inputs == null) {
            // Copy Input wires
            for (String k : original_circuit.normal_input_order) {
                add_input_wire(k);
            }
        } else {
            // Copy Some Input wires
            for (int i = 0; i < forced_inputs.length; i++) {
                if (forced_inputs[i] == null) {
                    add_input_wire(original_circuit.normal_input_order.get(i));
                }
            }
        }

        // Add all Gates connected to the Output Wires to a stack
        Stack<LL_Gate> working_gates = new Stack<>();
        for (LL_Wire w : outputs.values()) {
            working_gates.add(original_circuit.gates.get(w));
        }

        // Work backwards adding simplified gates
        while (!working_gates.isEmpty()) {
            LL_Gate g = working_gates.pop();

            if (g == null) continue;

            // Skip gates that are already added (must check wire)
            if (gates.containsKey(g.out)) continue;

            g.infer_value();
            LL_Gate sg = g.simplify();
            if (sg == null) System.out.println("Null Simplified Gate: " + g.longString());
            else sg.add_to_circuit(this);

            // if(sg instanceof LL_On || sg instanceof LL_Off) {
            //
            // }

            // Add more wires to the working gates
            for (LL_Wire w : sg.getInputWires()) {
                working_gates.add(original_circuit.gates.get(w));
            }
        }
    }

}
