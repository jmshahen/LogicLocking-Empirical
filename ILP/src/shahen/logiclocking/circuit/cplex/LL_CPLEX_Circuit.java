package shahen.logiclocking.circuit.cplex;

import java.util.logging.Logger;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.cppimpl.IloBoolVar;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.gates.LL_Gate;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;
import shahen.logiclocking.reduction.LL_ReductionI;

public class LL_CPLEX_Circuit {
    public static final Logger log = Logger.getLogger("shahen");
    public LL_Circuit circuit;
    public String circuit_name;

    public boolean include_variable_names = true;
    /**
     * Changes the variables names of the CPLEX model names, if they are included:<br/>
     * TRUE  -- uses the names found in the .bench file where we read the circuit in<br/>
     * FALSE -- bases on the wire type, we name them INPUT(1), KEY(1), OUTPUT(1), INTERNAL(1) 
     *          (where 1 is incremented for the next variable of the same type) 
     */
    public boolean use_input_names = false;

    public IloIntVar[] inputs = null;
    public IloIntVar[] key = null;
    public IloIntVar[] outputs = null;
    public IloIntVar[] internal = null;
    /** Only used when testing the non-reduced ILP constraints */
    public IloIntVar[] extra_wires = null;

    public LL_CPLEX_Circuit(LL_Circuit circuit, String circuit_name) {
        this.circuit = circuit;
        this.circuit_name = circuit_name;
    }

    /**
     * Allows you to create a new Cplex circuit and not generate new normal inputs,
     * but reuse normal inputs from another circuit.<br>
     * This is required by the the diff circuit and the 2 equal circuits.
     * @param circuit
     * @param circuit_name
     * @param inputs
     */
    public LL_CPLEX_Circuit(LL_Circuit circuit, String circuit_name, IloBoolVar[] inputs) {
        this.circuit = circuit;
        this.circuit_name = circuit_name;
        this.inputs = inputs;
    }

    /**
     * 
     * @param cplex
     * @param input_values
     * @param output_values
     * @throws IloException
     */
    public void addToCplex(LL_ReductionI reduction, IloCplex cplex, Boolean[] input_values, Boolean[] output_values)
            throws IloException {
        // Input Wires
        if (inputs == null)
            inputs = setup_new_var_array(cplex, circuit.normal_input_order.toArray(new String[0]), "INPUT");

        // Key Wires
        if (key == null) key = setup_new_var_array(cplex, circuit.key_input_order.toArray(new String[0]), "KEY");

        // Output Wires
        outputs = setup_new_var_array(cplex, circuit.output_order.toArray(new String[0]), "OUTPUT");

        // Internal Wires
        internal = setup_new_var_array(cplex, circuit.internal_order.toArray(new String[0]), "INTERNAL");

        if (reduction.uses_extra_variables()) {
            int extra_var_count = reduction.set_extra_number_for_gates(circuit.gates.values());
            if (extra_var_count > 0) {
                extra_wires = cplex.boolVarArray(extra_var_count);
            }
        }

        // Subject To
        for (LL_Gate g : circuit.gates.values()) {
            // System.out.println("ADDING Gate: " + g); // DEBUG STATEMENT
            g.addCPLEXConstraints(reduction, this, cplex, input_values, output_values);
        }

        // Add extra EASY constraints for any IloIntVar that is not represented in the model
        for (int i = 0; i < inputs.length; i++) {
            String s = circuit.normal_input_order.get(i);
            if (circuit.is_wire_orphan(circuit.normal_inputs.get(s))) {
                log.finest("ADDING ORPHAN: " + s); // DEBUG STATEMENT
                cplex.addGe(inputs[i], 0);
            }
        }
        for (int i = 0; i < key.length; i++) {
            String s = circuit.key_input_order.get(i);
            if (circuit.is_wire_orphan(circuit.key_inputs.get(s))) {
                log.finest("ADDING ORPHAN: " + s); // DEBUG STATEMENT
                cplex.addGe(key[i], 0);
            }
        }
    }

    private IloIntVar[] setup_new_var_array(IloCplex cplex, String[] names, String default_name) throws IloException {
        String prefix = (circuit_name.isEmpty()) ? "" : circuit_name + "_";
        IloIntVar[] vars = null;
        if (include_variable_names) {
            if (use_input_names && names != null) {
                if (!prefix.isEmpty()) {
                    for (int i = 0; i < names.length; i++) {
                        names[i] = prefix + names[i];
                    }
                }
                vars = cplex.boolVarArray(names.length, names);
            } else {
                String[] input_names = new String[names.length];
                for (int i = 0; i < input_names.length; i++) {
                    if (input_names.length > 100) {
                        input_names[i] = prefix + default_name + "#" + i;
                    } else {
                        input_names[i] = prefix + default_name + "(" + i + ")";
                    }
                }
                vars = cplex.boolVarArray(input_names.length, input_names);
            }
        } else {
            vars = cplex.boolVarArray(names.length);
        }

        return vars;
    }

    /**
     * Writes to 2 string builders, 
     * one for the constraints and one to write the variable declarations
    * @param constraints
    * @param variables
    * @param force_inputs
    * @param force_key
    * @param addBothEqualCircuits
    * @param verbose_comments
    */
    public void addToILPFile(LL_ReductionI reduction, StringBuilder constraints, StringBuilder variables,
            Boolean[] force_inputs, Boolean[] force_key, boolean addBothEqualCircuits, boolean verbose_comments) {

        int normal_inputs_size = circuit.normal_input_order.size();
        int key_inputs_size = circuit.key_input_order.size();
        int outputs_size = circuit.output_order.size();
        int internal_wires_size = circuit.internal_wires.size();

        String prefix = "";

        if (circuit_name != null && !circuit_name.isEmpty()) {
            prefix = circuit_name + "_";
        }

        // Variable Declarations
        variables.append("/******************************************************\n")//
                .append(" * CIRCUIT: " + "\n")//
                .append(" ******************************************************/\n");
        variables.append("dvar boolean " + //
                prefix + WIRE_TYPE.INPUT + "[0.." + (normal_inputs_size - 1) + "];\n");
        variables.append("dvar boolean " + prefix + WIRE_TYPE.KEY + "[0.." + (key_inputs_size - 1) + "];\n");
        variables.append("dvar boolean " + prefix + WIRE_TYPE.OUTPUT + "[0.." + (outputs_size - 1) + "];\n");
        variables.append("dvar boolean " + prefix + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires_size - 1) + "];\n");
        if (reduction.uses_extra_variables()) {
            variables.append("dvar boolean " + prefix + reduction.extra_variable_name() + "[0.."
                    + (reduction.set_extra_number_for_gates(circuit.gates.values()) - 1) + "];\n");
        }

        constraints.append("/******************************************************\n")//
                .append(" * CIRCUIT: " + "\n")//
                .append(" ******************************************************/\n");
        int constraint_count = 1;

        for (LL_Gate g : circuit.gates.values()) {
            if (verbose_comments) constraints.append("  // c" + constraint_count + ": " + g.toString() + "\n");
            for (String constraint : g.getILPConstraints(reduction, circuit_name, circuit_name, circuit_name,
                    force_inputs, null)) {
                constraints.append("  " + constraint + "\n");
            }

            constraint_count++;
        }

        // constraints.append("\n // Hard Code the Input's Values\n");
        // // Hard code the Inputs
        // for (int i = 0; i < force_inputs.length; i++) {
        // // to_double(inputs[i]) == circuita.inputs[i]
        // constraints
        // .append(" "
        // + ((force_inputs[i]) ? "1" : "0") + " == " + circuit.normal_inputs
        // .get(circuit.normal_input_order.get(i)).get_ilp_name(circuit_name, circuit_name)
        // + ";\n");
        // }

        if (force_key != null) {
            constraints.append("\n  // Force circuit key to match force_key\n");
            // force circuit outputs to match correct_outputs
            for (int i = 0; i < force_key.length; i++) {
                constraints
                        .append("  "
                                + ((force_key[i]) ? "1" : "0") + " == " + circuit.key_inputs
                                        .get(circuit.key_input_order.get(i)).get_ilp_name(circuit_name, circuit_name)
                                + ";\n");
            }
        }
    }

    /**
     * Returns a string containing the values that cplex solved for the Int array.
     * <br>
     * Format: [0,1,0,1,1,1]
     * 
     * Note: double values are cast to (int) to remove decimal places.
     * @param cplex
     * @param vars
     * @return
     * @throws IloException
     */
    public static String getVarString(IloCplex cplex, IloIntVar[] vars) throws IloException {
        StringBuilder sb = new StringBuilder();

        sb.append("[");

        boolean first = true;
        for (IloIntVar b : vars) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }
            sb.append((int) cplex.getValue(b));
        }

        sb.append("]");

        return sb.toString();
    }

    /**
     * After CPLEX has solved a circuit, 
     * you can call this function to retrieve the values of an IloIntVar array in a Boolean array format 
     * @param cplex
     * @param vars
     * @return
     * @throws IloException
     */
    public static Boolean[] getVarBoolArray(IloCplex cplex, IloIntVar[] vars) throws IloException {
        Boolean[] vals = new Boolean[vars.length];

        for (int i = 0; i < vars.length; i++) {
            try {
                double d = cplex.getValue(vars[i]);
                if (d > 0) vals[i] = true;
                else vals[i] = false;
            } catch (IloException e) {
                System.out.println("[LL_CPLEX_Circuit.getVarBoolArray] Failed at i=" + i + "; vars[i]=" + vars[i]);
                throw (e);
            }
        }

        return vals;
    }

    public Boolean[] getOutputVarBoolArray(IloCplex cplex) throws IloException {
        return getVarBoolArray(cplex, outputs);
    }
    public Boolean[] getKeyVarBoolArray(IloCplex cplex) throws IloException {
        return getVarBoolArray(cplex, key);
    }
    public Boolean[] getInputVarBoolArray(IloCplex cplex) throws IloException {
        return getVarBoolArray(cplex, inputs);
    }

    public void clearModel() {
        inputs = null;
        key = null;
        outputs = null;
        internal = null;
        extra_wires = null;
    }

}
