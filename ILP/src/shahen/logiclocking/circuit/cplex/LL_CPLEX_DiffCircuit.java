package shahen.logiclocking.circuit.cplex;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

import ilog.concert.IloException;
import ilog.concert.IloIntExpr;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.gates.LL_Gate;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;
import shahen.logiclocking.reduction.LL_ReductionI;
import shahen.logiclocking.solver.LL_SolverI;
import shahen.string.ShahenStrings;

public class LL_CPLEX_DiffCircuit {
    public Logger log;
    public LL_CPLEX_Circuit circuita;
    public LL_CPLEX_Circuit circuitb;

    /** When true, an extra constraint will be added that says the Key A != Key B */
    public boolean extra_keys_constraint = false;

    public LL_CPLEX_DiffCircuit(Logger log, LL_Circuit circuit, String circuit_name_a, String circuit_name_b) {
        circuita = new LL_CPLEX_Circuit(circuit, circuit_name_a);
        circuitb = new LL_CPLEX_Circuit(circuit, circuit_name_b);

        this.log = log;
    }
    public LL_CPLEX_DiffCircuit(Logger log, LL_Circuit circuit_a, String circuit_name_a, LL_Circuit circuit_b,
            String circuit_name_b) {
        circuita = new LL_CPLEX_Circuit(circuit_a, circuit_name_a);
        circuitb = new LL_CPLEX_Circuit(circuit_b, circuit_name_b);

        this.log = log;
    }

    /**
     * 
     * @param cplex
     * @throws IloException
     */
    public void addToCplex(LL_ReductionI reduction, IloCplex cplex) throws IloException {
        circuita.addToCplex(reduction, cplex, null, null);

        // Hook up inputs from Circuit A to Circuit B
        circuitb.inputs = circuita.inputs;
        circuitb.addToCplex(reduction, cplex, null, null);

        // Ensure that the output from both circuits is not equal
        IloIntExpr[] abs_diffs = new IloIntExpr[circuita.outputs.length];
        for (int i = 0; i < abs_diffs.length; i++) {
            abs_diffs[i] = cplex.abs(cplex.diff(circuita.outputs[i], circuitb.outputs[i]));
        }
        cplex.addGe(cplex.sum(abs_diffs), 1);

        if (extra_keys_constraint) {
            // Ensure that the keys for both circuits are not equal
            abs_diffs = new IloIntExpr[circuita.key.length];
            for (int i = 0; i < abs_diffs.length; i++) {
                abs_diffs[i] = cplex.abs(cplex.diff(circuita.key[i], circuitb.key[i]));
            }
            cplex.addGe(cplex.sum(abs_diffs), 1);
        }
    }

    public void clearModel() {
        circuita.clearModel();
        circuitb.clearModel();
    }

    /**
     * 
     * @param input
     * @param key_a The key from circuita
     * @param key_b the key from circuitb
     * @param ilp_output_a the output from circuita
     * @param ilp_output_b the output from circuitb
     * @return
     */
    public boolean verify(Boolean[] input, Boolean[] key_a, Boolean[] key_b, Boolean[] ilp_output_a,
            Boolean[] ilp_output_b) {
        String input_str = ShahenStrings.toBooleanString(input);
        String key_a_str = ShahenStrings.toBooleanString(key_a);
        String key_b_str = ShahenStrings.toBooleanString(key_b);

        if (key_a_str.equals(key_b_str)) {
            log.severe("[VerifyStep] Key_a and Key_b are the same key!" + //
                    ";\ninput: " + input_str + //
                    ";\nKey_a: " + key_a_str + ";" +//
                    "\nKey_b: " + key_b_str);
            return false;
        }

        if (!circuita.circuit.get_output(input, key_a)) {
            log.severe("[VerifyStep] Unable to propagate through circuit; " + "input: " + input_str + "; Key: "
                    + key_a_str);
            return false;
        }
        Boolean[] output_a = circuita.circuit.getWireValueArray(circuita.circuit.getOutputWires());
        String output_a_str = ShahenStrings.toBooleanString(output_a);

        if (!circuita.circuit.get_output(input, key_b)) {
            log.severe("[VerifyStep] Unable to propagate through circuit; " + "input: " + input_str + "; Key: "
                    + key_b_str);
            return false;
        }
        Boolean[] output_b = circuita.circuit.getWireValueArray(circuita.circuit.getOutputWires());
        String output_b_str = ShahenStrings.toBooleanString(output_b);

        String ilp_output_a_str = ShahenStrings.toBooleanString(ilp_output_a);
        String ilp_output_b_str = ShahenStrings.toBooleanString(ilp_output_b);

        boolean are_equal = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < output_a.length; i++) {
            if (!output_a[i].equals(output_b[i])) {
                are_equal = false;
                sb.append(" * Expected Output Wire[" + i + "]: ").append(circuita.circuit.output_order.get(i))
                        .append(" {output_a: ").append(output_a[i] ? "1" : "0").append(", output_b:")
                        .append(output_b[i] ? "1" : "0").append("}\n");
            }
        }

        boolean result = true;
        if (are_equal) {
            log.severe("[VerifyStep] Found input is not a distinguishing input" + //
                    "\ninput: " + input_str + //
                    "\nKey_a: " + key_a_str + //
                    "\nKey_b: " + key_b_str + //
                    "\nCircuit_Output_a: " + output_a_str + //
                    "\nCircuit_Output_b: " + output_b_str + //
                    "\nILP_Output_a: " + ilp_output_a_str +//
                    "\nILP_Output_b: " + ilp_output_b_str + //
                    "\nIncorrect Wires:\n  * None");
            result = false;
        }

        are_equal = true;
        StringBuilder sb_a = new StringBuilder();
        StringBuilder sb_b = new StringBuilder();
        for (int i = 0; i < output_a.length; i++) {
            if (!output_a[i].equals(ilp_output_a[i])) {
                are_equal = false;
                sb_a.append(" * Incorrect Output Wire[" + i + "]: ").append(circuita.circuit.output_order.get(i))
                        .append(" {output_a: ").append(output_a[i] ? "1" : "0").append(", ilp_output_a:")
                        .append(ilp_output_a[i] ? "1" : "0").append("}\n");
            }
            if (!output_b[i].equals(ilp_output_b[i])) {
                are_equal = false;
                sb_b.append(" * Incorrect Output Wire[" + i + "]: ").append(circuita.circuit.output_order.get(i))
                        .append(" {output_b: ").append(output_b[i] ? "1" : "0").append(", ilp_output_b:")
                        .append(ilp_output_b[i] ? "1" : "0").append("}\n");
            }
        }

        if (!are_equal) {
            log.severe("[VerifyStep] The ILP Outputs do not match the Circuits Outputs" + //
                    "\ninput: " + input_str + //
                    "\nKey_a: " + key_a_str + //
                    "\nKey_b: " + key_b_str + //
                    "\nCircuit_Output_a: " + output_a_str + //
                    "\n    ILP_Output_a: " + ilp_output_a_str +//
                    "\nIncorrect Wires_a:\n" + sb_a.toString() + //
                    "\nCircuit_Output_b: " + output_b_str + //
                    "\n    ILP_Output_b: " + ilp_output_b_str + //
                    "\nIncorrect Wires_b:\n" + sb_b.toString() +//
                    "\n\nCircuit Output Difference:\n" + sb.toString());
            result = false;
        }

        return result;
    }
    /**
     * Writes to 2 string builders, 
     * one for the constraints and one to write the variable declarations
     * @param constraints
     * @param variables
     */
    public int addToILPFile(LL_ReductionI reduction, StringBuilder constraints, StringBuilder variables,
            int constraint_count, boolean verbose_comments) {

        String circuit_name1 = circuita.circuit_name;
        String circuit_name2 = circuitb.circuit_name;

        variables.append("\n// Diff Circuit, Circuit A: " + circuita.circuit_name)//
                .append("\n// Inputs\n");
        // Only 1 set of input wires
        variables.append("dvar boolean " + //
                circuit_name1 + "_" + WIRE_TYPE.INPUT + "[0.." + (circuita.circuit.normal_inputs.size() - 1) + "];\n");
        // Duplicate all other wires
        variables.append("dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.KEY + "[0.."
                + (circuita.circuit.key_inputs.size() - 1) + "];\n");
        if (circuitb.circuit.key_inputs.size() > 0) {
            variables.append("dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.KEY + "[0.."
                    + (circuitb.circuit.key_inputs.size() - 1) + "];\n");
        }
        variables.append("dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.OUTPUT + "[0.."
                + (circuita.circuit.outputs.size() - 1) + "];\n");
        variables.append("dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.OUTPUT + "[0.."
                + (circuitb.circuit.outputs.size() - 1) + "];\n");
        variables.append("dvar boolean " + //
                circuit_name1 + "_" + WIRE_TYPE.INTERNAL + "[0.." + (circuita.circuit.internal_wires.size() - 1)
                + "];\n");
        variables.append("dvar boolean " + //
                circuit_name2 + "_" + WIRE_TYPE.INTERNAL + "[0.." + (circuita.circuit.internal_wires.size() - 1)
                + "];\n");
        if (reduction.uses_extra_variables()) {
            int extra_gate_count = reduction.set_extra_number_for_gates(circuita.circuit.gates.values());
            if (extra_gate_count > 0) {
                variables.append("dvar boolean " + circuit_name1 + "_" + reduction.extra_variable_name() + "[0.."
                        + (extra_gate_count - 1) + "];\n");
            }

            extra_gate_count = reduction.set_extra_number_for_gates(circuitb.circuit.gates.values());
            if (extra_gate_count > 0) {
                variables.append("dvar boolean " + circuit_name2 + "_" + reduction.extra_variable_name() + "[0.."
                        + (extra_gate_count - 1) + "];\n");
            }
        }

        int gate_count = 1;
        constraints.append("  /*****************************\n")//
                .append("   * DIFF CIRCUIT A\n")//
                .append("   *****************************/\n");
        for (LL_Gate g : circuita.circuit.gates.values()) {
            if (verbose_comments) constraints.append("  // Gate " + gate_count + ": " + g.toString() + "\n");
            for (String s : g.getILPConstraints(reduction, circuita.circuit_name, circuita.circuit_name, circuit_name1,
                    null, null)) {
                if (verbose_comments) constraints.append("  // c" + constraint_count + "\n");
                constraints.append("  ").append(s).append("\n");
                constraint_count++;
            }
            gate_count++;
        }
        // Diff 2, same input wires as 1 but different everything else
        constraints.append("  /*****************************\n")//
                .append("   * DIFF CIRCUIT B\n")//
                .append("   *****************************/\n");
        for (LL_Gate g : circuitb.circuit.gates.values()) {
            if (verbose_comments) constraints.append("  // Gate " + gate_count + ": " + g.toString() + "\n");
            for (String s : g.getILPConstraints(reduction, circuitb.circuit_name, circuita.circuit_name, circuit_name2,
                    null, null)) {
                if (verbose_comments) constraints.append("  // c" + constraint_count + "\n");
                constraints.append("  ").append(s).append("\n");
                constraint_count++;
            }
            gate_count++;
        }
        constraints.append("\n  // c" + constraint_count + ": Make sure outputs are different\n");
        constraints.append("  sum(i in 0.." + (circuita.circuit.outputs.size() - 1) + ") abs(" +//
                circuit_name1 + "_" + WIRE_TYPE.OUTPUT + "[i] - " + //
                circuit_name2 + "_" + WIRE_TYPE.OUTPUT + "[i])" +//
                " >= 1;\n");
        constraint_count++;

        if (extra_keys_constraint) {
            constraints.append("\n  // c" + constraint_count + ": Make sure keys are different\n");
            constraints.append("  sum(i in 0.." + (circuita.circuit.keySize() - 1) + ") abs(" +//
                    circuit_name1 + "_" + WIRE_TYPE.KEY + "[i] - " + //
                    circuit_name2 + "_" + WIRE_TYPE.KEY + "[i])" +//
                    " >= 1;\n");
            constraint_count++;
        }
        constraints.append("  /*******************************************/\n\n");

        return constraint_count;
    }

    /**
     * 
     * @param reduction
     * @param filename
     * @param verbose_comments
     */
    public void exportModel(LL_ReductionI reduction, String filename, boolean verbose_comments) {
        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();

        constraints.append("subject to {\n");
        addToILPFile(reduction, constraints, variables, 1, verbose_comments);
        constraints.append("\n}");

        LL_SolverI.add_comment_and_variables(constraints, variables);
        // Write out model to file name, if exception, then write to stdout
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(constraints.toString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out ILP Model to file: " + filename + "\n\n");
            System.out.println(constraints.toString());
        }
    }
}
