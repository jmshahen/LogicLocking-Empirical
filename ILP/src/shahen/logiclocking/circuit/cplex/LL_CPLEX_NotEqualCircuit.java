package shahen.logiclocking.circuit.cplex;

import java.util.logging.Logger;

import ilog.concert.*;
import ilog.opl.IloCplex;
import shahen.string.ShahenStrings;

/**
 * Force two sets of wires to not be equal, or force 1 set of wires and a constant to be not equal.
 * @author Jonathan Shahen
 *
 */
public class LL_CPLEX_NotEqualCircuit {
    public Logger log;

    public IloIntVar[] in1_wires = null;
    public IloIntVar[] in2_wires = null;
    public Boolean[] in2_bools = null;

    public LL_CPLEX_NotEqualCircuit(Logger log, IloIntVar[] in1, IloIntVar[] in2) {
        this.log = log;
        this.in1_wires = in1;
        this.in2_wires = in2;
    }
    public LL_CPLEX_NotEqualCircuit(Logger log, IloIntVar[] in1, Boolean[] in2) {
        this.log = log;
        this.in1_wires = in1;
        this.in2_bools = in2;
    }

    /**
     * 
     * @param cplex
     * @param bothCircuits
     * @throws IloException
     */
    public void addToCplex(IloCplex cplex) throws IloException {
        // Error Checking
        if (in1_wires == null || (in2_wires == null && in2_bools == null))
            throw new IllegalArgumentException("in1 cannot be null, and at least one version of in2 must not be null");
        if ((in2_wires != null && in1_wires.length != in2_wires.length)
                || (in2_bools != null && in1_wires.length != in2_bools.length))
            throw new IllegalArgumentException("in1 must be the same length as in2. |in1|=" + in1_wires.length
                    + "; |in2|=" + ((in2_wires != null) ? in2_wires.length : in2_bools.length));

        // Ensure that the output from both circuits is not equal
        IloIntExpr[] abs_diffs = new IloIntExpr[in1_wires.length];
        for (int i = 0; i < abs_diffs.length; i++) {
            if (in2_wires != null) abs_diffs[i] = cplex.abs(cplex.diff(in1_wires[i], in2_wires[i]));
            else abs_diffs[i] = cplex.abs(cplex.diff(in1_wires[i], (in2_bools[i]) ? 1 : 0));
        }
        cplex.addGe(cplex.sum(abs_diffs), 1);
    }

    /**
     * Writes to 2 string builders, 
     * one for the constraints and one to write the variable declarations
    * @param constraints a StringBuilder where all constraints will be appended to. 
    *   This StringBuilder should be able to fit within the <code>subject to { ...HERE... }</code>
    * @param variables
    * @param name_a if null, then new key variables will be created for the first equal circuit (<i>circuita.circuit_name + "_" + WIRE_TYPE.KEY</i>).<br/> 
    *   Otherwise it will use the name: keya_name + "_" + WIRE_TYPE.KEY<br/>
    *   Use this when you have already added a DLL_CPLEX_DiffCircuit.
    * @param name_b if null, then new key variables will be created for the second equal circuit (<i>circuitb.circuit_name + "_" + WIRE_TYPE.KEY</i>).<br/> 
    *   Otherwise it will use the name: keyb_name + "_" + WIRE_TYPE.KEY<br/>
    *   Use this when you have already added a DLL_CPLEX_DiffCircuit.
    * @param addBothEqualCircuits
    * @param verbose_comments
    */
    public int addToILPFile(StringBuilder constraints, StringBuilder variables, String name_a, String name_b,
            int constraint_count, boolean verbose_comments) {
        add_banner(constraints, name_a, name_b);

        if (verbose_comments) constraints.append("  // c" + constraint_count + "\n");
        if (in2_wires != null) {
            if (in1_wires.length > 1) {
                constraints.append("  sum(i in 0.." + (in1_wires.length - 1) + ") abs(" +//
                        name_a + "[i] - " + name_b + "[i])" +//
                        " >= 1;\n\n");
            } else {
                constraints.append("  abs(" + name_a + "[0] - " + name_b + "[0]) >= 1;\n\n");
            }
        } else {
            constraints.append("  (");
            for (int i = 0; i < in1_wires.length; i++) {
                if (i != 0) constraints.append(" + ");

                if (in2_bools[i]) constraints.append("(1-").append(name_a).append("[" + i + "])");
                else constraints.append(name_a).append("[" + i + "]");
            }
            constraints.append(") >= 1;\n\n");
        }

        return constraint_count++;
    }

    private void add_banner(StringBuilder sb, String name_a, String name_b) {
        sb//
                .append("  /******************************************************\n")//
                .append("   * Not-EQUAL CIRCUIT:\n")//
                .append("   * INPUT A: " + name_a + "\n");
        if (in2_wires != null) {
            sb.append("   * INPUT B: " + name_b + "\n");
        } else {
            sb.append("   * INPUT B: " + ShahenStrings.toBooleanString(in2_bools) + "\n");
        }
        sb.append("   ******************************************************/\n");
    }

    public boolean verify(Boolean[] new_key, Boolean[] old_key) {
        if (new_key == null || old_key == null) {
            log.warning("[Verify NotEqualCircuit] One or both of the provided keys is null. new_key=" + new_key
                    + "; old_key=" + old_key);

            return false;
        }

        if (new_key.length != old_key.length) {
            log.warning("[Verify NotEqualCircuit] The keys are not of the same length. new_key=" + new_key.length
                    + "; old_key=" + old_key.length);

            return false;
        }

        for (int i = 0; i < new_key.length; i++) {
            if (new_key[i] != old_key[i]) { return true; }
        }

        log.warning("[Verify NotEqualCircuit] The keys are equal to each other!");
        return false;
    }
}
