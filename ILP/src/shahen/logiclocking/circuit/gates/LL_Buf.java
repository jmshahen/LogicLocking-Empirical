package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;
import java.util.List;

import gurobi.GRBException;
import gurobi.GRBModel;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.reduction.LL_ReductionI;

/** A BUF gate does not change the binary input bit. It is used to amplify a signal.
 * Number of input pins: 1<br>
 * <style>
 * table { border-collapse:collapse; }
 * td {
 *  padding: 5px;
 *  text-align: center;
 *  border:1px solid black;
 * }
 * </style>
 * <table>
 * <tr><td><b>INPUT</b></td><td><b>OUTPUT</b></td></tr>
 * <tr><td>0</td><td>0</td></tr>
 * <tr><td>1</td><td>1</td></tr>
 * </table> 
 */
public class LL_Buf extends LL_Gate {

    public LL_Buf(LL_Wire in1, LL_Wire out) {
        this.in1 = in1;
        this.out = out;
    }

    public LL_Buf() {}

    /**
     * 
     * @param circuit_name Name of the circuit, which is how we obtain the wire names
     * @param input_name Name of the circuit who owns the input wires
     * @param use_reduced true if we are using the reduced ilp constraints
     * @return
     */
    @Override
    public String[] getILPConstraints(LL_ReductionI reduction, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return reduction.getBUFILPContraints(this, circuit_name, input_name, key_name, input_values, output_values);
    }

    /**
     * Adds ILP Constraint(s) for this gate into the provided CPLEX instance
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @param use_reduced
     * @throws IloException
     */
    @Override
    public void addCPLEXConstraints(LL_ReductionI reduction, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        reduction.addBUFCPLEXConstraints(this, cp, cplex, input_values, output_values);
    }

    @Override
    public void addGurobiConstraints(LL_ReductionI reduction, LL_GUROBI_Circuit ll_GUROBI_Circuit, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        reduction.addBUFGurobiConstraints(this, ll_GUROBI_Circuit, gurobi, input_values, output_values);
    }

    /**
     * Propagate the values stored in the input wires through to the output wire
     * (using the appropriate logic for the gate type).
     * @return true if succeeded, false if any errors occurred
     */
    public boolean propagate() {
        if (!areAllInputsSet()) return false;
        out.set_value(in1.get_value());
        return true;
    }

    /**
     * Using all, or a subset, of the inputs, try to infer the output value of the gate.
     * @return TRUE if able to infer, FALSE if unable to infer an output value.
     */
    public boolean infer_value() {
        return propagate();
    }

    /**
     * For each Gate Type, returns TRUE if all the input/output wires are not NULL
     * @return
     */
    public boolean areAllWiresSet() {
        return in1 != null && out != null;
    }

    /**
     * Returns TRUE if all the input wires to the gate have a value set to them.
     * @return
     */
    public boolean areAllInputsSet() {
        if (!areAllWiresSet()) return false;

        return in1.get_value() != null;
    }
    @Override
    public String toString() {
        return (out == null ? "null" : out.get_name()) + " = " + (in1 == null ? "null" : in1.get_name());
    }

    /**
     * Returns the gate with the wires can there values
     * @return
     */
    public String longString() {
        return (out == null ? "null" : out.get_name_value()) + " = " + (in1 == null ? "null" : in1.get_name_value());
    }

    /**
     * Returns 1 if the gate only accepts 1 input, 2 if the gate only accepts 2 inputs
     * 
     * see {@link LL_LargeGate} for return values greater than 2
     * @return
     */
    public int numInputs() {
        return 1;
    }

    /**
     * Returns a list of the input wires to this gate
     * @return
     */
    public List<LL_Wire> getInputWires() {
        List<LL_Wire> w = new ArrayList<LL_Wire>();
        w.add(in1);
        return w;
    }

    public String gate_name() {
        return "buf";
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    public LL_Gate new_gate() {
        return new LL_Buf();
    }

    /**
     * If the output bit is set to a value, then this will return a {@link LL_On} or {@link LL_Off} gate.
     * <br>
     * Otherwise, we check if any of the input wires are set and then return a simplified version of the gate.
     * <br>
     * If none of the input wires are set, then we return this gate.
     * @return
     */
    public LL_Gate simplify() {
        return simplify_on_off();
    }

    /**
     * A quick way to add a gate to a circuit (using 
     * {@link LL_Circuit#add_gate(LL_Gate, String, String, String)} and 
     * {@link LL_Circuit#add_large_gate(LL_LargeGate, ArrayList, String)})
     * @param circuit
     */
    public void add_to_circuit(LL_Circuit circuit) {
        circuit.add_buf_gate(in1.get_name(), out.get_name());
    }
}
