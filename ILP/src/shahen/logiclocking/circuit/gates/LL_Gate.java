package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;
import java.util.List;

import gurobi.GRBException;
import gurobi.GRBModel;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.reduction.LL_ReductionI;

public class LL_Gate {
    public LL_Wire in1;
    public LL_Wire in2;
    public LL_Wire out;

    /** If a reduction needs extra variables, then the extra variable ids will be stored here */
    public ArrayList<Integer> extra_variable_ids = new ArrayList<>();

    public void add_extra_constraint_id(int id) {
        extra_variable_ids.add(id);
    }
    public Integer get_extra_constraint_id(int id) {
        return extra_variable_ids.get(id);
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    public LL_Gate new_gate() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns TRUE only if all the wires are set and if the output wire's value has not been set
     * @return
     */
    public boolean canPropagate() {
        if (!areAllInputsSet()) return false;
        if (out.get_value() != null) return false;
        return true;
    }

    /**
     * For each Gate Type, returns TRUE if all the input/output wires are not NULL
     * @return
     */
    public boolean areAllWiresSet() {
        return in1 != null && in2 != null && out != null;
    }

    /**
     * Returns TRUE if all the input wires to the gate have a value set to them.
     * @return
     */
    public boolean areAllInputsSet() {
        if (!areAllWiresSet()) return false;
        return in1.has_value() && in2.has_value();
    }
    /**
     * Returns TRUE if the output wire has a value set to it and if all inputs have values.
     * <br/>
     * If the circuit is incorrect and multiple gates connect there out wire to the same wire,
     * then this will claim that a gate has propagated once 1 of the gates has propagated.
     * @return
     */
    public boolean hasPropagated() {
        if (!areAllInputsSet()) return false;
        return is_out_set();
    }

    /**
     * Returns true if the output wire has a non-null value
     * @return
     */
    public boolean is_out_set() {
        return out.get_value() != null;
    }

    /**
     * 
     * @param circuit_name Name of the circuit, which is how we obtain the wire names
     * @param input_name Name of the circuit who owns the input wires
     * @param use_reduced true if we are using the reduced ilp constraints
     * @return
     */
    public String[] getILPConstraints(LL_ReductionI reduction, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        throw new UnsupportedOperationException();
    }

    /**
     * Adds ILP Constraint(s) for this gate into the provided CPLEX instance
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @param use_reduced
     * @throws IloException
     */
    public void addCPLEXConstraints(LL_ReductionI reduction, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        throw new UnsupportedOperationException();
    }

    public void addGurobiConstraints(LL_ReductionI reduction, LL_GUROBI_Circuit ll_GUROBI_Circuit, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        throw new UnsupportedOperationException();
    }
    /**
     * Propagate the values stored in the input wires through to the output wire
     * (using the appropriate logic for the gate type).
     * @return true if succeeded, false if any errors occurred
     */
    public boolean propagate() {
        throw new UnsupportedOperationException();
    }

    /**
     * Using all, or a subset, of the inputs, try to infer the output value of the gate.
     * @return TRUE if able to infer, FALSE if unable to infer an output value.
     */
    public boolean infer_value() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return (out == null ? "null" : out.get_name()) + " = " + (in1 == null ? "null" : in1.get_name()) + " "
                + gate_name() + " " + (in2 == null ? "null" : in2.get_name());
    }

    public String gate_name() {
        throw new UnsupportedOperationException();
    }
    /**
     * Returns the gate with the wires can there values
     * @return
     */
    public String longString() {
        return (out == null ? "null" : out.get_name_value()) + " = " + (in1 == null ? "null" : in1.get_name_value())
                + " " + gate_name() + " " + (in2 == null ? "null" : in2.get_name_value());
    }

    /**
     * Returns 1 if the gate only accepts 1 input, 2 if the gate only accepts 2 inputs
     * 
     * see {@link LL_LargeGate} for return values greater than 2
     * @return
     */
    public int numInputs() {
        return 2;
    }

    /**
     * Returns a list of the input wires to this gate
     * @return
     */
    public List<LL_Wire> getInputWires() {
        List<LL_Wire> w = new ArrayList<LL_Wire>();
        w.add(in1);
        w.add(in2);
        return w;
    }

    /**
     * Returns the .bench format string.<br/>
     * Some examples:
     * <ul>
     *  <li>God9 = xor(Gid9, Ge9)
     *  <li>Gg5$enc = xnor(keyinput5, Gg5)
     *  <li>Gwd = and(Gy4l, Gs5, Gy6l, Gs7, Gu0)
     * </ul>
     * @return
     */
    public String getBenchString() {
        StringBuilder sb = new StringBuilder();

        sb.append(out.get_name()).append(" = ").append(gate_name()).append("(");

        boolean first = true;
        for (LL_Wire w : getInputWires()) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append(w.get_name());
        }
        sb.append(")");

        return sb.toString();
    }

    /**
     * If the output bit is set to a value, then this will return a {@link LL_On} or {@link LL_Off} gate.
     * <br>
     * Otherwise, we check if any of the input wires are set and then return a simplified version of the gate.
     * <br>
     * If none of the input wires are set, then we return this gate.
     * @return
     */
    public LL_Gate simplify() {
        throw new UnsupportedOperationException();
    }

    /**
     * If the output bit is set to a value, then this will return a {@link LL_On} or {@link LL_Off} gate.
     * <br>
     * If any of the input wires are NULL, then this gate is returned
     * <br>
     * If some of the inputs are set, then NULL is returned (which means further simplification can occur)
     * @return
     */
    protected LL_Gate simplify_on_off() {
        if (out == null) return this;

        if (out.has_value()) {
            if (out.get_value() == true) return new LL_On(out);
            else return new LL_Off(out);
        }

        if (!areAllInputsSet()) return this;

        return null;
    }

    /**
     * A quick way to add a gate to a circuit (using 
     * {@link LL_Circuit#add_gate(LL_Gate, String, String, String)} and 
     * {@link LL_Circuit#add_large_gate(LL_LargeGate, ArrayList, String)})
     * @param circuit
     */
    public void add_to_circuit(LL_Circuit circuit) {
        throw new UnsupportedOperationException(this.getClass().toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        return this.hashCode() == obj.hashCode();
    }

    /**
     * Resets the input and output wires of their values
     */
    public void reset() {
        if (in1 != null) in1.reset_wire();
        if (in2 != null) in2.reset_wire();
        if (out != null) out.reset_wire();
    }
}
