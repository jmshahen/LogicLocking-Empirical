package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;
import java.util.List;

import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;

public class LL_LargeGate extends LL_Gate {
    public ArrayList<LL_Wire> inputs = new ArrayList<LL_Wire>();

    /**
     * For each Gate Type, returns TRUE if all the input/output wires are not NULL
     * @return
     */
    @Override
    public boolean areAllWiresSet() {
        for (LL_Wire w : inputs) {
            if (w == null) return false;
        }
        if (out == null) return false;
        return true;
    }

    /**
     * Returns TRUE if all the input wires to the gate have a value set to them.
     * @return
     */
    @Override
    public boolean areAllInputsSet() {
        if (!areAllWiresSet()) return false;
        for (LL_Wire w : inputs) {
            if (w.get_value() == null) return false;
        }
        return true;
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    @Override
    public LL_LargeGate new_gate() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (LL_Wire in : inputs) {
            sb.append(in == null ? "null" : in.get_name()).append(", ");
        }
        return (out == null ? "null" : out.get_name()) + " = " + gate_name() + "(" + sb.toString() + ")";
    }

    /**
     * Returns the gate with the wires can there values
     * @return
     */
    @Override
    public String longString() {
        StringBuilder sb = new StringBuilder();

        for (LL_Wire in : inputs) {
            sb.append(in == null ? "null" : in.get_name_value()).append(", ");
        }
        return (out == null ? "null" : out.get_name_value()) + " = " + gate_name() + " (" + sb.toString() + ")";
    }

    /**
     * Returns how many input wires are connected to this gate
     * @return
     */
    @Override
    public int numInputs() {
        return inputs.size();
    }

    /**
     * Returns a list of the input wires to this gate
     * @return
     */
    @Override
    public List<LL_Wire> getInputWires() {
        return inputs;
    }

    /**
     * A quick way to add a gate to a circuit (using 
     * {@link LL_Circuit#add_gate(LL_Gate, String, String, String)} and 
     * {@link LL_Circuit#add_large_gate(LL_LargeGate, ArrayList, String)})
     * @param circuit
     */
    @Override
    public void add_to_circuit(LL_Circuit circuit) {
        ArrayList<String> input_names = new ArrayList<>(inputs.size());
        for (LL_Wire w : inputs) {
            input_names.add(w.get_name());
        }
        circuit.add_large_gate(new_gate(), input_names, out.get_name());
    }

    /**
     * Resets the input and output wires of their values
     */
    @Override
    public void reset() {
        if (out != null) out.reset_wire();

        for (LL_Wire in : inputs) {
            if (in != null) in.reset_wire();
        }
    }
}
