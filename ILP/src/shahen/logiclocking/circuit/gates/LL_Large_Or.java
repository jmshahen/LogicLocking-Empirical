package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;

import gurobi.GRBException;
import gurobi.GRBModel;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.reduction.LL_ReductionI;

/** A Large OR gate is true when any of the inputs are true.<br>
 * Number of input pins: unknown<br>
 * <style>
 * table { border-collapse:collapse; }
 * td {
 *  padding: 5px;
 *  text-align: center;
 *  border:1px solid black;
 * }
 * </style>
 * <table>
 * <tr>
 *  <td><b>INPUT 1</b></td>
 *  <td><b>INPUT 2</b></td>
 *  <td><b>INPUT 3</b></td>
 *  <td><b>INPUT 4</b></td>
 *  <td><b>OUTPUT</b></td>
 * </tr>
 * <tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
 * <tr><td>0</td><td>1</td><td>0</td><td>0</td><td>1</td></tr>
 * <tr><td>1</td><td>1</td><td>0</td><td>1</td><td>1</td></tr>
 * <tr><td>1</td><td>0</td><td>1</td><td>1</td><td>1</td></tr>
 * <tr><td>1</td><td>1</td><td>1</td><td>1</td><td>1</td></tr>
 * </table> 
 */
public class LL_Large_Or extends LL_LargeGate {
    @Override
    public String[] getILPConstraints(LL_ReductionI reduction, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return reduction.getLargeORILPContraints(this, circuit_name, input_name, key_name, input_values, output_values);
    }

    @Override
    public void addCPLEXConstraints(LL_ReductionI reduction, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        reduction.addLargeORCPLEXConstraints(this, cp, cplex, input_values, output_values);
    }

    @Override
    public void addGurobiConstraints(LL_ReductionI reduction, LL_GUROBI_Circuit ll_GUROBI_Circuit, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        reduction.addLargeORGurobiConstraints(this, ll_GUROBI_Circuit, gurobi, input_values, output_values);
    }

    @Override
    public boolean propagate() {
        boolean b = false;
        for (LL_Wire w : inputs) {
            Boolean bb = w.get_value();
            if (bb == null) return false;
            if (bb == true) b = true;
        }
        out.set_value(b);

        return true;
    }

    /**
     * If any of the inputs are true, then the output must be true.
     * @return TRUE if able to infer, FALSE if unable to infer an output value.
     */
    @Override
    public boolean infer_value() {
        if (propagate()) { return true; }

        for (LL_Wire w : inputs) {
            if (w.get_value(false)) {
                out.set_value(true);
                return true;
            }
        }

        return false;
    }

    @Override
    public String gate_name() {
        return "or";
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    @Override
    public LL_LargeGate new_gate() {
        return new LL_Large_Or();
    }

    /**
     * If the output bit is set to a value, then this will return a {@link LL_On} or {@link LL_Off} gate.
     * <br>
     * Otherwise, we check if any of the input wires are set and then return a simplified version of the gate.
     * <br>
     * If none of the input wires are set, then we return this gate.
     * @return
     */
    @Override
    public LL_Gate simplify() {
        LL_Gate g = this;
        if (out == null) return g;
        if (out.has_value()) {
            if (out.get_value() == true) return new LL_On(out);
            else return new LL_Off(out);
        } else {
            ArrayList<LL_Wire> new_inputs = new ArrayList<>();
            for (LL_Wire w : inputs) {
                if (!w.has_value()) new_inputs.add(w);
            }

            if (new_inputs.size() == inputs.size()) {
                return this;
            } else if (new_inputs.size() > 2) {
                g = new_gate();
                g.out = out;
                ((LL_LargeGate) g).inputs.addAll(new_inputs);
            } else if (new_inputs.size() == 2) {
                g = new LL_Or();
                g.out = out;
                g.in1 = new_inputs.get(0);
                g.in2 = new_inputs.get(1);
            } else if (new_inputs.size() == 1) {
                g = new LL_Buf();
                g.out = out;
                g.in1 = new_inputs.get(0);
            } else {
                throw new UnsupportedOperationException("There should be at least 1 wire.");
            }
        }

        return g;
    }
}
