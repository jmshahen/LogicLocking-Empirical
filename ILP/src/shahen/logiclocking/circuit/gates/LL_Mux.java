package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;
import java.util.List;

import gurobi.GRBException;
import gurobi.GRBModel;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.reduction.LL_ReductionI;

/** An MUX gate is a gate where a selector wire chooses which input to pass through (like a buffer gate)<br>
 * Number of input pins: 3<br>
 * Format: mux(s,a,b), where
 * <ul>
 * <li>s -- selector wire 
 * <li>a -- input wire
 * <li>b -- input wire
 * </ul>
 * <style>
 * table { border-collapse:collapse; }
 * td {
 *  padding: 5px;
 *  text-align: center;
 *  border:1px solid black;
 * }
 * </style>
 * <table>
 * <tr>
 *  <td><b>SELECTOR</b></td>
 *  <td><b>INPUT 1</b></td>
 *  <td><b>INPUT 2</b></td>
 *  <td><b>OUTPUT</b></td>
 * </tr>
 * <tr><td>0</td><td>0</td><td>0</td><td>0</td></tr>
 * <tr><td>0</td><td>0</td><td>1</td><td>0</td></tr>
 * <tr><td>0</td><td>1</td><td>0</td><td>1</td></tr>
 * <tr><td>0</td><td>1</td><td>1</td><td>1</td></tr>
 * <tr><td>1</td><td>0</td><td>0</td><td>0</td></tr>
 * <tr><td>1</td><td>0</td><td>1</td><td>1</td></tr>
 * <tr><td>1</td><td>1</td><td>0</td><td>0</td></tr>
 * <tr><td>1</td><td>1</td><td>1</td><td>1</td></tr>
 * </table> 
 */
public class LL_Mux extends LL_Gate {
    /** Special wire for a 2 bit Multiplexer */
    public LL_Wire s;

    @Override
    public int numInputs() {
        return 3;
    }

    @Override
    public String toString() {
        return (out == null ? "null" : out.get_name()) + " = " + gate_name() + "(" + (s == null ? "null" : s.get_name())
                + ", " + (in1 == null ? "null" : in1.get_name()) + ", " + (in2 == null ? "null" : in2.get_name()) + ")";
    }
    @Override
    public String longString() {
        return (out == null ? "null" : out.get_name_value()) + " = " + gate_name() + "("
                + (in1 == null ? "null" : in1.get_name_value()) + ", " + (in2 == null ? "null" : in2.get_name_value())
                + "; " + (s == null ? "null" : s.get_name_value()) + ")";
    }

    /**
     * For each Gate Type, returns TRUE if all the input/output wires are not NULL
     * @return
     */
    @Override
    public boolean areAllWiresSet() {
        return in1 != null && in2 != null && s != null && out != null;
    }

    /**
     * Returns TRUE only if enough of the wires are set and if the output wire's value has not been set
     * @return
     */
    @Override
    public boolean canPropagate() {
        if (!areEnoughInputsSet()) return false;
        if (out.get_value() != null) return false;
        return true;
    }

    /**
     * Returns TRUE if all the input wires to the gate have a value set to them.
     * @return
     */
    public boolean areEnoughInputsSet() {
        if (!areAllWiresSet()) return false;
        if (!s.has_value()) return false;
        return (!s.get_value(true) && in1.has_value()) || (in2.has_value() && s.get_value(false));
    }

    /**
     * Returns TRUE if all the input wires to the gate have a value set to them.
     * @return
     */
    @Override
    public boolean areAllInputsSet() {
        if (!areAllWiresSet()) return false;
        return in1.has_value() && in2.has_value() && s.has_value();
    }

    /**
     * Returns a list of the input wires to this gate
     * @return
     */
    public List<LL_Wire> getInputWires() {
        List<LL_Wire> w = new ArrayList<LL_Wire>();
        w.add(s);
        w.add(in1);
        w.add(in2);
        return w;
    }

    /**
     * 
     * @param circuit_name Name of the circuit, which is how we obtain the wire names
     * @param input_name Name of the circuit who owns the input wires
     * @param use_reduced true if we are using the reduced ilp constraints
     * @return
     */
    @Override
    public String[] getILPConstraints(LL_ReductionI reduction, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return reduction.getMUXILPContraints(this, circuit_name, input_name, key_name, input_values, output_values);
    }

    /**
    * Adds ILP Constraint(s) for this gate into the provided CPLEX instance
    * @param cp
    * @param cplex
    * @param input_values
    * @param output_values
    * @param use_reduced
    * @throws IloException
    */
    @Override
    public void addCPLEXConstraints(LL_ReductionI reduction, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        reduction.addMUXCPLEXConstraints(this, cp, cplex, input_values, output_values);
    }

    @Override
    public void addGurobiConstraints(LL_ReductionI reduction, LL_GUROBI_Circuit ll_GUROBI_Circuit, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        reduction.addMUXGurobiConstraints(this, ll_GUROBI_Circuit, gurobi, input_values, output_values);
    }

    /**
     * Propagate the values stored in the input wires through to the output wire
     * (using the appropriate logic for the gate type).
     * @return true if succeeded, false if any errors occurred
     */
    @Override
    public boolean propagate() {
        if (!s.has_value()) return false;

        if (s.get_value() == false) {
            if (!in1.has_value()) return false;
            out.set_value(in1.get_value());

        } else {
            if (!in2.has_value()) return false;
            out.set_value(in2.get_value());
        }

        return true;
    }

    /**
     * Using all, or a subset, of the inputs, try to infer the output value of the gate.
     * <br/>
     * Same as {@link LL_Mux#propagate()}
     * @return TRUE if able to infer, FALSE if unable to infer an output value.
     */
    @Override
    public boolean infer_value() {
        return propagate();
    }

    @Override
    public String gate_name() {
        return "mux";
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    @Override
    public LL_Mux new_gate() {
        return new LL_Mux();
    }

    /**
     * If the output bit is set to a value, then this will return a {@link LL_On} or {@link LL_Off} gate.
     * <br>
     * Otherwise, we check if any of the input wires are set and then return a simplified version of the gate.
     * <br>
     * If none of the input wires are set, then we return this gate.
     * @return
     */
    @Override
    public LL_Gate simplify() {
        if (out == null) return this;
        if (out.has_value()) {
            if (out.get_value() == true) return new LL_On(out);
            else return new LL_Off(out);
        } else {
            Boolean val_in1 = in1.get_value();
            Boolean val_in2 = in2.get_value();
            Boolean val_s = s.get_value();

            // Return this gate, as there are no input values to simplify
            // in1=X,in2=X,s=X
            if (val_in1 == null && val_in2 == null && val_s == null) { return this; }

            // in1==in2 (s can be 0|1|X)
            if (val_in1 != null && val_in2 != null && val_in1 == val_in2) {
                if (val_in1 == true) return new LL_On(out);
                else return new LL_Off(out);
            }
            // NOTE: From here on we know that in1!=in2 (except for X=X)

            if (val_s != null) {
                if (val_s == false) {
                    if (val_in1 == null) return new LL_Buf(in1, out);
                    else return ((val_in1) ? new LL_On(out) : new LL_Off(out));
                } else {
                    if (val_in2 == null) return new LL_Buf(in2, out);
                    else return ((val_in2) ? new LL_On(out) : new LL_Off(out));
                }
            } else {
                // NOTE: from here we know that s=X

                if (val_in1 != null && val_in2 != null) {
                    // s=X and in1!=in2
                    if (val_in1 == false) return new LL_Buf(s, out);
                    else return new LL_Not(s, out);
                } else if (val_in1 != null && val_in2 == null) {
                    // in1=1,in2=X,s=X
                    if (val_in1 == true) return new LL_Mux1X(this);
                    else return new LL_Mux0X(this);
                } else if (val_in1 == null && val_in2 != null) {
                    // in1=X,in2=1,s=X
                    if (val_in2 == true) return new LL_MuxX1(this);
                    else return new LL_MuxX0(this);
                }
            }
        }

        return this;
    }

    /**
     * A quick way to add a gate to a circuit (using 
     * {@link LL_Circuit#add_gate(LL_Gate, String, String, String)} and 
     * {@link LL_Circuit#add_large_gate(LL_LargeGate, ArrayList, String)})
     * @param circuit
     */
    @Override
    public void add_to_circuit(LL_Circuit circuit) {
        circuit.add_mux_gate(new_gate(), in1.get_name(), in2.get_name(), s.get_name(), out.get_name());
    }

    /**
     * Resets the input and output wires of their values
     */
    public void reset() {
        if (in1 != null) in1.reset_wire();
        if (in2 != null) in2.reset_wire();
        if (s != null) s.reset_wire();
        if (out != null) out.reset_wire();
    }
}
