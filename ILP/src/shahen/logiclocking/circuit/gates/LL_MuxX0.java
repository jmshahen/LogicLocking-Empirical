package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;
import java.util.List;

import gurobi.GRBException;
import gurobi.GRBModel;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.reduction.LL_ReductionI;

/**
 * For {@link LL_Mux}, if we give the input {in1=X, in2=0}, where X is not defined yet,
 * we can simply the circuit as follows:
 * 
 * <b>Normal Mux:</b>
 * <ul>
 * <li> ex1 = in1 AND NOT(s) 
 * <li> ex2 = in2 AND s
 * <li> out = ex1 OR ex2
 * </ul>
 * 
 * <b>LL_MuxX0</b>
 * <ul>
 * <li> out = in1 AND NOT(s)
 * </ul>
 * @author Jonathan Shahen
 *
 */
public class LL_MuxX0 extends LL_Mux {
    public LL_MuxX0() {}
    public LL_MuxX0(LL_Mux m) {
        copy(m);
    }
    public void copy(LL_Mux g) {
        this.in1 = g.in1;
        this.s = g.s;
        this.out = g.out;
    }

    /**
     * Returns a list of the input wires to this gate
     * @return
     */
    @Override
    public List<LL_Wire> getInputWires() {
        List<LL_Wire> w = new ArrayList<LL_Wire>();
        w.add(in1);
        // w.add(in2);
        w.add(s);
        return w;
    }

    @Override
    public int numInputs() {
        return 2;
    }

    @Override
    public String toString() {
        return (out == null ? "null" : out.get_name()) + " = " + gate_name() + "("
                + (in1 == null ? "null" : in1.get_name()) + ", 0, " + (s == null ? "null" : s.get_name()) + ")";
    }
    @Override
    public String longString() {
        return (out == null ? "null" : out.get_name_value()) + " = " + gate_name() + "("
                + (in1 == null ? "null" : in1.get_name_value()) + ", 0, " + (s == null ? "null" : s.get_name_value())
                + ")";
    }

    /**
     * For each Gate Type, returns TRUE if all the input/output wires are not NULL
     * @return
     */
    @Override
    public boolean areAllWiresSet() {
        return in1 != null && s != null && out != null;
    }

    /**
     * A quick way to add a gate to a circuit (using 
     * {@link LL_Circuit#add_gate(LL_Gate, String, String, String)} and 
     * {@link LL_Circuit#add_large_gate(LL_LargeGate, ArrayList, String)})
     * @param circuit
     */
    @Override
    public void add_to_circuit(LL_Circuit circuit) {
        circuit.add_mux_gate(new LL_MuxX0(), in1.get_name(), null, s.get_name(), out.get_name());
    }

    /**
     * 
     * @param circuit_name Name of the circuit, which is how we obtain the wire names
     * @param input_name Name of the circuit who owns the input wires
     * @param use_reduced true if we are using the reduced ilp constraints
     * @return
     */
    @Override
    public String[] getILPConstraints(LL_ReductionI reduction, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return reduction.getMUXX0ILPContraints(this, circuit_name, input_name, key_name, input_values, output_values);
    }

    /**
    * Adds ILP Constraint(s) for this gate into the provided CPLEX instance
    * @param cp
    * @param cplex
    * @param input_values
    * @param output_values
    * @param use_reduced
    * @throws IloException
    */
    @Override
    public void addCPLEXConstraints(LL_ReductionI reduction, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        reduction.addMUXX0CPLEXConstraints(this, cp, cplex, input_values, output_values);
    }

    @Override
    public void addGurobiConstraints(LL_ReductionI reduction, LL_GUROBI_Circuit ll_GUROBI_Circuit, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        reduction.addMUXX0GurobiConstraints(this, ll_GUROBI_Circuit, gurobi, input_values, output_values);
    }

    /**
     * Propagate the values stored in the input wires through to the output wire
     * (using the appropriate logic for the gate type).
     * @return true if succeeded, false if any errors occurred
     */
    @Override
    public boolean propagate() {
        if (!s.has_value()) return false;

        if (s.get_value() == true) {
            out.set_value(false);
        } else {
            if (!in1.has_value()) return false;
            out.set_value(in1.get_value());
        }

        return true;
    }

    /**
     * Using all, or a subset, of the inputs, try to infer the output value of the gate.
     * <br/>
     * Same as {@link LL_MuxX0#propagate()}
     * @return TRUE if able to infer, FALSE if unable to infer an output value.
     */
    @Override
    public boolean infer_value() {
        return propagate();
    }

    @Override
    public String gate_name() {
        return "muxX0";
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    @Override
    public LL_MuxX0 new_gate() {
        return new LL_MuxX0();
    }

    /**
     * If the output bit is set to a value, then this will return a {@link LL_On} or {@link LL_Off} gate.
     * <br>
     * Otherwise, we check if any of the input wires are set and then return a simplified version of the gate.
     * <br>
     * If none of the input wires are set, then we return this gate.
     * @return
     */
    @Override
    public LL_Gate simplify() {
        return this;
    }
}
