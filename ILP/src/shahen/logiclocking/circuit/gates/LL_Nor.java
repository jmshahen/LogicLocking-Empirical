package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;

import gurobi.GRBException;
import gurobi.GRBModel;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.reduction.LL_ReductionI;

/** A NOR gate is an OR gate followed by a NOT gate.<br>
 * Number of input pins: 2<br>
 * <style>
 * table { border-collapse:collapse; }
 * td {
 *  padding: 5px;
 *  text-align: center;
 *  border:1px solid black;
 * }
 * </style>
 * <table>
 * <tr>
 *  <td><b>INPUT 1</b></td>
 *  <td><b>INPUT 2</b></td>
 *  <td><b>OUTPUT</b></td>
 * </tr>
 * <tr><td>0</td><td>0</td><td>1</td></tr>
 * <tr><td>0</td><td>1</td><td>0</td></tr>
 * <tr><td>1</td><td>0</td><td>0</td></tr>
 * <tr><td>1</td><td>1</td><td>0</td></tr>
 * </table> 
 */
public class LL_Nor extends LL_Gate {
    /**
     * 
     * @param circuit_name Name of the circuit, which is how we obtain the wire names
     * @param input_name Name of the circuit who owns the input wires
     * @param use_reduced true if we are using the reduced ilp constraints
     * @return
     */
    @Override
    public String[] getILPConstraints(LL_ReductionI reduction, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return reduction.getNORILPContraints(this, circuit_name, input_name, key_name, input_values, output_values);
    }

    /**
     * Adds ILP Constraint(s) for this gate into the provided CPLEX instance
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @param use_reduced
     * @throws IloException
     */
    @Override
    public void addCPLEXConstraints(LL_ReductionI reduction, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        reduction.addNORCPLEXConstraints(this, cp, cplex, input_values, output_values);
    }

    @Override
    public void addGurobiConstraints(LL_ReductionI reduction, LL_GUROBI_Circuit ll_GUROBI_Circuit, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        reduction.addNORGurobiConstraints(this, ll_GUROBI_Circuit, gurobi, input_values, output_values);
    }

    /**
     * Propagate the values stored in the input wires through to the output wire
     * (using the appropriate logic for the gate type).
     * @return true if succeeded, false if any errors occurred
     */
    @Override
    public boolean propagate() {
        if (!areAllInputsSet()) return false;
        out.set_value(!(in1.get_value() | in2.get_value()));
        return true;
    }

    /**
     * If any of the inputs are true, then the output must be false.
     * @return TRUE if able to infer, FALSE if unable to infer an output value.
     */
    @Override
    public boolean infer_value() {
        if (!areAllWiresSet()) return false;

        if (areAllInputsSet()) {
            out.set_value(!(in1.get_value() | in2.get_value()));
            return true;
        }

        if (in1.get_value(false) || in2.get_value(false)) {
            out.set_value(false);
            return true;
        }

        return false;
    }

    @Override
    public String gate_name() {
        return "nor";
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    @Override
    public LL_Gate new_gate() {
        return new LL_Nor();
    }

    /**
     * If the output bit is set to a value, then this will return a {@link LL_On} or {@link LL_Off} gate.
     * <br>
     * Otherwise, we check if any of the input wires are set and then return a simplified version of the gate.
     * <br>
     * If none of the input wires are set, then we return this gate.
     * @return
     */
    @Override
    public LL_Gate simplify() {
        LL_Gate g = this;
        if (out == null) return g;
        if (out.has_value()) {
            if (out.get_value() == true) return new LL_On(out);
            else return new LL_Off(out);
        } else {
            Boolean val1 = in1.get_value();
            Boolean val2 = in2.get_value();

            if (val1 == null && val2 == null) return this;
            if (val1 == null && val2 == false) {
                g = new LL_Not();
                g.out = out;
                g.in1 = in1;
            } else if (val2 == null && val1 == false) {
                g = new LL_Not();
                g.out = out;
                g.in1 = in2;
            }
        }

        return g;
    }

    /**
     * A quick way to add a gate to a circuit (using 
     * {@link LL_Circuit#add_gate(LL_Gate, String, String, String)} and 
     * {@link LL_Circuit#add_large_gate(LL_LargeGate, ArrayList, String)})
     * @param circuit
     */
    @Override
    public void add_to_circuit(LL_Circuit circuit) {
        circuit.add_nor_gate(in1.get_name(), in2.get_name(), out.get_name());
    }
}
