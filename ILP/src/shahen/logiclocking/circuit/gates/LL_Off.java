package shahen.logiclocking.circuit.gates;

import java.util.ArrayList;
import java.util.List;

import gurobi.GRBException;
import gurobi.GRBModel;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.reduction.LL_ReductionI;

/** A ON gate takes no inputs and sets the output wire to TRUE.
 * Number of input pins: 0
 */
public class LL_Off extends LL_Gate {
    public LL_Off(LL_Wire output) {
        out = output;
    }

    /**
     * 
     * @param circuit_name Name of the circuit, which is how we obtain the wire names
     * @param input_name Name of the circuit who owns the input wires
     * @param use_reduced true if we are using the reduced ilp constraints
     * @return
     */
    @Override
    public String[] getILPConstraints(LL_ReductionI reduction, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return reduction.getOFFILPContraints(this, circuit_name, input_name, key_name, input_values, output_values);
    }

    /**
     * Adds ILP Constraint(s) for this gate into the provided CPLEX instance
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @param use_reduced
     * @throws IloException
     */
    @Override
    public void addCPLEXConstraints(LL_ReductionI reduction, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        reduction.addOFFCPLEXConstraints(this, cp, cplex, input_values, output_values);
    }

    @Override
    public void addGurobiConstraints(LL_ReductionI reduction, LL_GUROBI_Circuit ll_GUROBI_Circuit, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        reduction.addOFFGurobiConstraints(this, ll_GUROBI_Circuit, gurobi, input_values, output_values);
    }

    /**
     * returns TRUE if the output wire is not NULL
     * @return
     */
    @Override
    public boolean areAllWiresSet() {
        return out != null;
    }

    /**
     * Returns {@link LL_Off#areAllWiresSet()}
     * @return
     */
    @Override
    public boolean areAllInputsSet() {
        return areAllWiresSet();
    }
    /**
     * Propagate the values stored in the input wires through to the output wire
     * (using the appropriate logic for the gate type).
     * @return true if succeeded, false if any errors occurred
     */
    @Override
    public boolean propagate() {
        if (!areAllInputsSet()) return false;
        out.set_value(false);
        return true;
    }

    /**
     * Using all, or a subset, of the inputs, try to infer the output value of the gate.
     * @return TRUE if able to infer, FALSE if unable to infer an output value.
     */
    @Override
    public boolean infer_value() {
        return propagate();
    }

    @Override
    public String toString() {
        return (out == null ? "null" : out.get_name()) + " = 0";
    }

    /**
     * Returns the gate with the wires can there values
     * @return
     */
    @Override
    public String longString() {
        return toString();
    }

    /**
     * Returns 1 if the gate only accepts 1 input, 2 if the gate only accepts 2 inputs
     * 
     * see {@link LL_LargeGate} for return values greater than 2
     * @return
     */
    @Override
    public int numInputs() {
        return 0;
    }

    /**
     * Returns a list of the input wires to this gate
     * @return
     */
    @Override
    public List<LL_Wire> getInputWires() {
        return new ArrayList<LL_Wire>();
    }

    @Override
    public String gate_name() {
        return "off";
    }

    @Override
    public String getBenchString() {
        return out.get_name() + " = 0";
    }

    /**
     * Creates a new gate of the same sub-class.
     * @return
     */
    @Override
    public LL_Gate new_gate() {
        return new LL_Off(out);
    }

    /**
     * A quick way to add a gate to a circuit (using 
     * {@link LL_Circuit#add_gate(LL_Gate, String, String, String)} and 
     * {@link LL_Circuit#add_large_gate(LL_LargeGate, ArrayList, String)})
     * @param circuit
     */
    @Override
    public void add_to_circuit(LL_Circuit circuit) {
        circuit.add_off_gate(out.get_name());
    }

    @Override
    public LL_Gate simplify() {
        return this;
    }
}
