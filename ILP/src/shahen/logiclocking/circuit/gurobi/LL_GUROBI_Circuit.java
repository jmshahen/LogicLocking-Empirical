package shahen.logiclocking.circuit.gurobi;

import java.text.DecimalFormat;

import gurobi.*;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.opl.IloCplex;
import shahen.logiclocking.LL_Exception;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.gates.LL_Gate;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;
import shahen.logiclocking.reduction.LL_ReductionI;

public class LL_GUROBI_Circuit {
    public LL_Circuit circuit;
    public String circuit_name;

    public boolean include_variable_names = true;
    /**
     * Changes the variables names of the CPLEX model names, if they are included:<br/>
     * TRUE  -- uses the names found in the .bench file where we read the circuit in<br/>
     * FALSE -- bases on the wire type, we name them INPUT(1), KEY(1), OUTPUT(1), INTERNAL(1) (where 1 is incremented for the next variable of the same type) 
     */
    public boolean use_input_names = false;

    public GRBVar[] inputs = null;
    public GRBVar[] key = null;
    public GRBVar[] outputs = null;
    public GRBVar[] internal = null;
    /** Only used when testing the non-reduced ILP constraints */
    public GRBVar[] extra_wires = null;

    public LL_GUROBI_Circuit(LL_Circuit circuit, String circuit_name) {
        this.circuit = circuit;
        this.circuit_name = circuit_name;
    }

    /**
     * Allows you to create a new Cplex circuit and not generate new normal inputs,
     * but reuse normal inputs from another circuit.<br>
     * This is required by the the diff circuit and the 2 equal circuits.
     * @param circuit
     * @param circuit_name
     * @param inputs
     */
    public LL_GUROBI_Circuit(LL_Circuit circuit, String circuit_name, GRBVar[] inputs) {
        this.circuit = circuit;
        this.circuit_name = circuit_name;
        this.inputs = inputs;
    }

    /**
     * 
     * @param cplex
     * @param input_values
     * @param output_values
     * @throws GRBException 
     */
    public void addToGurobi(LL_ReductionI reduction, GRBModel gurobi, Boolean[] input_values, Boolean[] output_values)
            throws GRBException {
        String prefix = (circuit_name.isEmpty()) ? "" : circuit_name + "_";
        String name;
        // Input Wires
        if (inputs == null) {
            inputs = gurobi.addVars(circuit.normal_inputs.size(), GRB.BINARY);

            if (inputs == null) inputs = new GRBVar[0];

            if (include_variable_names) {
                for (int i = 0; i < inputs.length; i++) {
                    if (use_input_names) {
                        name = circuit.normal_input_order.get(i);
                    } else {
                        name = (inputs.length > 100) ? "INPUT#" + i : "INPUT(" + i + ")";
                    }
                    inputs[i].set(GRB.StringAttr.VarName, prefix + name);
                }
            }
        }
        // Key Wires
        if (key == null) {
            key = gurobi.addVars(circuit.key_inputs.size(), GRB.BINARY);
            if (key == null) key = new GRBVar[0];

            if (include_variable_names) {
                for (int i = 0; i < key.length; i++) {
                    if (use_input_names) {
                        name = circuit.key_input_order.get(i);
                    } else {
                        name = (key.length > 100) ? "KEY#" + i : "KEY(" + i + ")";
                    }
                    key[i].set(GRB.StringAttr.VarName, prefix + name);
                }
            }
        }
        // Output Wires
        outputs = gurobi.addVars(circuit.output_order.size(), GRB.BINARY);
        if (outputs == null) outputs = new GRBVar[0];

        if (include_variable_names) {
            for (int i = 0; i < outputs.length; i++) {
                if (use_input_names) {
                    name = circuit.output_order.get(i);
                } else {
                    name = (outputs.length > 100) ? "OUTPUT#" + i : "OUTPUT(" + i + ")";
                }
                outputs[i].set(GRB.StringAttr.VarName, prefix + name);
            }
        }

        // Internal Wires
        internal = gurobi.addVars(circuit.internal_order.size(), GRB.BINARY);
        if (internal == null) internal = new GRBVar[0];

        if (include_variable_names) {
            for (int i = 0; i < internal.length; i++) {
                if (use_input_names) {
                    name = circuit.internal_order.get(i);
                } else {
                    name = (internal.length > 100) ? "INTERNAL#" + i : "INTERNAL(" + i + ")";
                }
                internal[i].set(GRB.StringAttr.VarName, prefix + name);
            }
        }

        if (reduction.uses_extra_variables()) {
            extra_wires = gurobi.addVars(reduction.set_extra_number_for_gates(circuit.gates.values()), GRB.BINARY);
            if (extra_wires == null) extra_wires = new GRBVar[0];

            if (include_variable_names) {
                for (int i = 0; i < extra_wires.length; i++) {
                    name = (extra_wires.length > 100) ? "EXTRA#" + i : "EXTRA(" + i + ")";
                    extra_wires[i].set(GRB.StringAttr.VarName, prefix + name);
                }
            }
        }

        // Subject To
        for (LL_Gate g : circuit.gates.values()) {
            g.addGurobiConstraints(reduction, this, gurobi, input_values, output_values);
        }

        /* NOT NEEDED FOR GUROBI
        // Add extra EASY constraints for any IloIntVar that is not represented in the model
        for (int i = 0; i < inputs.length; i++) {
            String s = circuit.normal_input_order.get(i);
            if (circuit.is_wire_orphan(circuit.normal_inputs.get(s))) {
                // System.out.println("ADDING ORPHAN: " + s); // DEBUG STATEMENT
                gurobi.addConstr(inputs[i], GRB.GREATER_EQUAL, 0, null);
            }
        }
        for (int i = 0; i < key.length; i++) {
            String s = circuit.key_input_order.get(i);
            if (circuit.is_wire_orphan(circuit.key_inputs.get(s))) {
                // System.out.println("ADDING ORPHAN: " + s); // DEBUG STATEMENT
                gurobi.addConstr(key[i], GRB.GREATER_EQUAL, 0, null);
            }
        }
        */
    }

    /**
     * Writes to 2 string builders, 
     * one for the constraints and one to write the variable declarations
    * @param constraints
    * @param variables
    * @param force_inputs
    * @param force_key
    * @param addBothEqualCircuits
    * @param verbose_comments
    */
    public void addToILPFile(LL_ReductionI reduction, StringBuilder constraints, StringBuilder variables,
            Boolean[] force_inputs, Boolean[] force_key, boolean addBothEqualCircuits, boolean verbose_comments) {

        int normal_inputs_size = circuit.normal_input_order.size();
        int key_inputs_size = circuit.key_input_order.size();
        int outputs_size = circuit.output_order.size();
        int internal_wires_size = circuit.internal_wires.size();

        String prefix = "";

        if (circuit_name != null && !circuit_name.isEmpty()) {
            prefix = circuit_name + "_";
        }

        // Variable Declarations
        variables.append("/******************************************************\n")//
                .append(" * CIRCUIT: " + "\n")//
                .append(" ******************************************************/\n");
        variables.append("dvar boolean " + //
                prefix + WIRE_TYPE.INPUT + "[0.." + (normal_inputs_size - 1) + "];\n");
        variables.append("dvar boolean " + prefix + WIRE_TYPE.KEY + "[0.." + (key_inputs_size - 1) + "];\n");
        variables.append("dvar boolean " + prefix + WIRE_TYPE.OUTPUT + "[0.." + (outputs_size - 1) + "];\n");
        variables.append("dvar boolean " + prefix + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires_size - 1) + "];\n");
        if (reduction.uses_extra_variables()) {
            variables.append("dvar boolean " + prefix + reduction.extra_variable_name() + "[0.."
                    + (reduction.set_extra_number_for_gates(circuit.gates.values()) - 1) + "];\n");
        }

        constraints.append("/******************************************************\n")//
                .append(" * CIRCUIT: " + "\n")//
                .append(" ******************************************************/\n");
        int constraint_count = 1;

        for (LL_Gate g : circuit.gates.values()) {
            if (verbose_comments) constraints.append("  // c" + constraint_count + ": " + g.toString() + "\n");
            for (String constraint : g.getILPConstraints(reduction, circuit_name, circuit_name, circuit_name,
                    force_inputs, null)) {
                constraints.append("  " + constraint + "\n");
            }

            constraint_count++;
        }

        // constraints.append("\n // Hard Code the Input's Values\n");
        // // Hard code the Inputs
        // for (int i = 0; i < force_inputs.length; i++) {
        // // to_double(inputs[i]) == circuita.inputs[i]
        // constraints
        // .append(" "
        // + ((force_inputs[i]) ? "1" : "0") + " == " + circuit.normal_inputs
        // .get(circuit.normal_input_order.get(i)).get_ilp_name(circuit_name, circuit_name)
        // + ";\n");
        // }

        if (force_key != null) {
            constraints.append("\n  // Force circuit key to match force_key\n");
            // force circuit outputs to match correct_outputs
            for (int i = 0; i < force_key.length; i++) {
                constraints
                        .append("  "
                                + ((force_key[i]) ? "1" : "0") + " == " + circuit.key_inputs
                                        .get(circuit.key_input_order.get(i)).get_ilp_name(circuit_name, circuit_name)
                                + ";\n");
            }
        }
    }

    /**
     * Returns a string containing the values that cplex solved for the Int array.
     * <br>
     * Format: [0,1,0,1,1,1]
     * 
     * Note: double values are cast to (int) to remove decimal places.
     * @param cplex
     * @param vars
     * @return
     * @throws IloException
     */
    public static String getVarString(IloCplex cplex, IloIntVar[] vars) throws IloException {
        StringBuilder sb = new StringBuilder();

        sb.append("[");

        boolean first = true;
        for (IloIntVar b : vars) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }
            sb.append((int) cplex.getValue(b));
        }

        sb.append("]");

        return sb.toString();
    }

    /**
     * After CPLEX has solved a circuit, 
     * you can call this function to retrieve the values of an IloIntVar array in a Boolean array format 
     * @param cplex
     * @param vars
     * @return
     * @throws LL_Exception 
     * @throws IloException
     */
    public static Boolean[] getVarBoolArray(GRBModel gurobi, GRBVar[] vars) throws GRBException {
        Boolean[] vals = new Boolean[vars.length];

        for (int i = 0; i < vars.length; i++) {
            try {
                double d = vars[i].get(GRB.DoubleAttr.X);
                if (d > 1 - 1e-5 && d < 1 + 1e-5) {
                    vals[i] = true;
                } else if (d > -1e-5 && d < 1e-5) {
                    vals[i] = false;
                } else {
                    DecimalFormat formatter = new DecimalFormat("#,###.##########");
                    throw new GRBException(
                            "[getVarBoolArray] Unable to Convert Double(" + formatter.format(d) + ") to Binary Value");
                }
            } catch (GRBException e) {
                System.out.println("Failed at i=" + i + "; vars[i]=" + vars[i]);
                throw (e);
            }
        }

        return vals;
    }

    public Boolean[] getOutputVarBoolArray(GRBModel gurobi) throws GRBException {
        return getVarBoolArray(gurobi, outputs);
    }
    public Boolean[] getKeyVarBoolArray(GRBModel gurobi) throws GRBException {
        return getVarBoolArray(gurobi, key);
    }
    public Boolean[] getInputVarBoolArray(GRBModel gurobi) throws GRBException {
        return getVarBoolArray(gurobi, inputs);
    }

    public void clearModel() {
        inputs = null;
        key = null;
        outputs = null;
        internal = null;
        extra_wires = null;
    }
}
