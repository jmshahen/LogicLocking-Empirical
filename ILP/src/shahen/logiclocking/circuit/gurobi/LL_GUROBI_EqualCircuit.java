package shahen.logiclocking.circuit.gurobi;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.logging.Logger;

import gurobi.*;
import ilog.concert.IloException;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.LL_SimplifiedCircuit;
import shahen.logiclocking.circuit.gates.LL_Gate;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;
import shahen.logiclocking.reduction.LL_ReductionI;
import shahen.logiclocking.solver.LL_SolverI;
import shahen.string.ShahenStrings;

public class LL_GUROBI_EqualCircuit {
    private static int LL_GUROBI_EqualCircuit_COUNT = 0;
    private int eqcircuit_num;

    public Logger log;
    public LL_GUROBI_Circuit circuita;
    public LL_GUROBI_Circuit circuitb;

    public LL_Circuit circuit;
    public LL_Circuit simplified = null;

    public Boolean[] correct_outputs;
    public Boolean[] inputs;

    /** Convenient variable to keep track if this circuit has been added to a model */
    public boolean added_to_model_a = false;
    /** Convenient variable to keep track if this circuit has been added to a model, different from {@link #added_to_model_a} */
    public boolean added_to_model_b = false;

    /** When TRUE, this removes the decision variables for the inputs and replaces them with the literals (0/1) */
    public static boolean embed_inputs = true;
    /**
     * <b>This option breaks the ILP output and should not be turned on!</b>
     * <br/> 
     * When TRUE, this removes the decision variables for the outputs and replaces them with the literals (0/1)
     */
    public static boolean embed_outputs = false;

    /** When TRUE, this creates an {@link LL_SimplifiedCircuit} for each circuit with inputs */
    public static boolean simplify_circuits = true;

    /**
     * 
     * @param log
     * @param circuit
     * @param inputs
     * @param keya
     * @param keyb
     * @param circuit_name_a
     * @param circuit_name_b
     */
    public LL_GUROBI_EqualCircuit(Logger log, LL_Circuit circuit, Boolean[] inputs, //
            GRBVar[] keya, GRBVar[] keyb,//
            String circuit_name_a, String circuit_name_b) {
        eqcircuit_num = LL_GUROBI_EqualCircuit_COUNT;
        LL_GUROBI_EqualCircuit_COUNT++;

        this.log = log;
        this.circuit = circuit;

        if (simplify_circuits) {
            simplified = new LL_SimplifiedCircuit(circuit, inputs, null);
            circuita = new LL_GUROBI_Circuit(simplified, circuit_name_a);
            circuitb = new LL_GUROBI_Circuit(simplified, circuit_name_b);
        } else {
            circuita = new LL_GUROBI_Circuit(circuit, circuit_name_a);
            circuitb = new LL_GUROBI_Circuit(circuit, circuit_name_b);
        }

        setKeys(keya, keyb);

        this.inputs = inputs;
        if (circuit.get_output(inputs)) {
            correct_outputs = circuit.getOutputValues();
            circuit.reset();
        } else {
            throw new InvalidParameterException("Unable to obtain the blackbox value as the input values are invalid.");
        }
    }

    public LL_GUROBI_EqualCircuit(Logger log, LL_Circuit circuit, Boolean[] inputs, //
            GRBVar[] keya, String circuit_name_a) {
        eqcircuit_num = LL_GUROBI_EqualCircuit_COUNT;
        LL_GUROBI_EqualCircuit_COUNT++;

        this.log = log;
        this.circuit = circuit;

        if (simplify_circuits) {
            simplified = new LL_SimplifiedCircuit(circuit, inputs, null);
            circuita = new LL_GUROBI_Circuit(simplified, circuit_name_a);
        } else {
            circuita = new LL_GUROBI_Circuit(circuit, circuit_name_a);
        }

        setKeys(keya, null);

        this.inputs = inputs;
        if (circuit.get_output(inputs)) {
            correct_outputs = circuit.getOutputValues();
            circuit.reset();
        } else {
            throw new InvalidParameterException("Unable to obtain the blackbox value as the input values are invalid.");
        }
    }

    public void setKeys(GRBVar[] keya, GRBVar[] keyb) {
        circuita.key = keya;
        if (circuitb != null) circuitb.key = keyb;
    }

    /**
     * 
     * @param cplex
     * @param bothCircuits
     * @throws IloException
     */
    public void addToGurobi(LL_ReductionI reduction, GRBModel gurobi, boolean bothCircuits) throws GRBException {
        Boolean[] embeded_inputs = null;
        if (embed_inputs) {
            embeded_inputs = inputs;
        }
        Boolean[] embeded_outputs = null;
        if (embed_outputs) {
            embeded_outputs = correct_outputs;
        }

        circuita.addToGurobi(reduction, gurobi, embeded_inputs, embeded_outputs);

        if (bothCircuits) {
            // Hook up inputs from Circuit A to Circuit B
            circuitb.inputs = circuita.inputs;
            circuitb.addToGurobi(reduction, gurobi, embeded_inputs, embeded_outputs);
        }

        if (!embed_inputs) {
            add_input_value_conditions(gurobi);
        }

        if (!embed_outputs) {
            add_output_value_conditions(gurobi, bothCircuits);
        }
    }

    /**
     * Adds output constraints of the form "OUTPUT_VAR(i) == 0;"
     * @param cplex
     * @param bothCircuits
     * @throws IloException
     */
    private void add_output_value_conditions(GRBModel gurobi, boolean bothCircuits) throws GRBException {
        for (int i = 0; i < correct_outputs.length; i++) {
            gurobi.addConstr(circuita.outputs[i], GRB.EQUAL, to_double(correct_outputs[i]),
                    "FORCE_OUTPUT_eq" + eqcircuit_num + "_a#" + i);
        }
        if (bothCircuits) {
            for (int i = 0; i < correct_outputs.length; i++) {
                gurobi.addConstr(circuitb.outputs[i], GRB.EQUAL, to_double(correct_outputs[i]),
                        "FORCE_OUTPUT_eq" + eqcircuit_num + "_b#" + i);
            }
        }
    }

    private void add_input_value_conditions(GRBModel gurobi) throws GRBException {
        for (int i = 0; i < inputs.length; i++) {
            gurobi.addConstr(circuita.inputs[i], GRB.EQUAL, to_double(inputs[i]),
                    "FORCE_INPUT_eq" + eqcircuit_num + "_#" + i);
        }
    }

    /**
     * Writes to 2 string builders, 
     * one for the constraints and one to write the variable declarations
    * @param constraints a StringBuilder where all constraints will be appended to. 
    *   This StringBuilder should be able to fit within the <code>subject to { ...HERE... }</code>
    * @param variables
    * @param keya_name if null, then new key variables will be created for the first equal circuit (<i>circuita.circuit_name + "_" + WIRE_TYPE.KEY</i>).<br/> 
    *   Otherwise it will use the name: keya_name + "_" + WIRE_TYPE.KEY<br/>
    *   Use this when you have already added a DLL_CPLEX_DiffCircuit.
    * @param keyb_name if null, then new key variables will be created for the second equal circuit (<i>circuitb.circuit_name + "_" + WIRE_TYPE.KEY</i>).<br/> 
    *   Otherwise it will use the name: keyb_name + "_" + WIRE_TYPE.KEY<br/>
    *   Use this when you have already added a DLL_CPLEX_DiffCircuit.
    * @param addBothEqualCircuits
    * @param verbose_comments
    */
    public int addToILPFile(LL_ReductionI reduction, StringBuilder constraints, StringBuilder variables,
            String keya_name, String keyb_name, int constraint_count, boolean addBothEqualCircuits,
            boolean verbose_comments) {

        String circuit_name1 = circuita.circuit_name;
        String circuit_name2 = circuitb.circuit_name;

        int normal_inputs_size = circuit.normal_input_order.size();
        int key_inputs_size = circuit.key_input_order.size();
        int outputs_size = circuit.output_order.size();
        int internal_wires_size = circuit.internal_wires.size();

        // Variable Declarations
        add_banner(variables, circuit_name1, "");

        if (inputs == null || !embed_inputs) {
            variables.append("dvar boolean " + //
                    circuit_name1 + "_" + WIRE_TYPE.INPUT + "[0.." + (normal_inputs_size - 1) + "];\n");
        }
        if (correct_outputs == null || !embed_outputs) {
            variables.append(
                    "dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.OUTPUT + "[0.." + (outputs_size - 1) + "];\n");
        }
        if (keya_name == null) {
            variables.append(
                    "dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.KEY + "[0.." + (key_inputs_size - 1) + "];\n");
            keya_name = circuit_name1;
        }

        variables.append("dvar boolean " + circuit_name1 + "_" + WIRE_TYPE.INTERNAL + "[0.." + (internal_wires_size - 1)
                + "];\n");

        int extra_variable_count = 0;
        if (reduction.uses_extra_variables()) {
            extra_variable_count = reduction.set_extra_number_for_gates(circuit.gates.values());
            if (extra_variable_count > 0) {
                variables.append("dvar boolean " + circuit_name1 + "_" + reduction.extra_variable_name() + "[0.."
                        + (extra_variable_count - 1) + "];\n");
            }
        }
        if (addBothEqualCircuits) {
            add_banner(variables, circuit_name2, "");

            if (correct_outputs == null || !embed_outputs) {
                variables.append("dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.OUTPUT + "[0.." + (outputs_size - 1)
                        + "];\n");
            }
            if (keyb_name == null) {
                variables.append("dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.KEY + "[0.." + (key_inputs_size - 1)
                        + "];\n");
                keyb_name = circuit_name2;
            }
            variables.append("dvar boolean " + circuit_name2 + "_" + WIRE_TYPE.INTERNAL + "[0.."
                    + (internal_wires_size - 1) + "];\n");
            if (reduction.uses_extra_variables()) {
                if (extra_variable_count > 0) {
                    variables.append("dvar boolean " + circuit_name2 + "_" + reduction.extra_variable_name() + "[0.."
                            + (extra_variable_count - 1) + "];\n");
                }
            }
        }

        Boolean[] embedded_inputs = (embed_inputs) ? inputs : null;
        Boolean[] embedded_outputs = (embed_outputs) ? correct_outputs : null;

        String circuit_name;
        String key_name;
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                circuit_name = circuit_name1;
                key_name = keya_name;
            } else {
                circuit_name = circuit_name2;
                key_name = keyb_name;
            }

            if (i == 1 && !addBothEqualCircuits) {
                break;
            }

            add_banner(constraints, circuit_name, "  ");
            for (LL_Gate g : circuit.gates.values()) {
                if (verbose_comments) constraints.append("  // c" + constraint_count + ": " + g.toString() + "\n");
                for (String constraint : g.getILPConstraints(reduction, circuit_name, circuit_name1, key_name,
                        embedded_inputs, embedded_outputs)) {
                    constraints.append("  " + constraint + "\n");
                }

                constraint_count++;
            }
            constraints.append("\n");
        }

        /***  OLD VERSION -- specifying the input/output with extra constraints*/
        if (!embed_inputs) {
            constraints.append("  // Hard Code the Input's Values\n");
            // Hard code the Inputs
            for (int i = 0; i < inputs.length; i++) {
                // to_double(inputs[i]) == circuita.inputs[i]
                constraints.append("  " + ((inputs[i]) ? "1" : "0") + " == "
                        + circuit.normal_inputs.get(circuit.normal_input_order.get(i)).get_ilp_name(circuit_name1,
                                circuit_name1, keya_name, null, null)
                        + ";\n");
            }
        }

        if (!embed_outputs) {
            constraints.append("  // Force circuit outputs to match correct_outputs\n");
            for (int a = 0; a < 2; a++) {
                String name = (a == 0) ? circuit_name1 : circuit_name2;
                String name_key = (a == 0) ? keya_name : keyb_name;
                if (a == 1 && !addBothEqualCircuits) break;
                for (int i = 0; i < correct_outputs.length; i++) {
                    constraints.append("  " + ((correct_outputs[i]) ? "1" : "0") + " == " + circuit.outputs
                            .get(circuit.output_order.get(i)).get_ilp_name(name, circuit_name1, name_key, null, null)
                            + ";\n");
                }
            }
        }
        constraints.append("\n");

        return constraint_count;
    }

    private void add_banner(StringBuilder sb, String circuit_name, String line_prefix) {
        sb//
                .append(line_prefix + "/******************************************************\n")//
                .append(line_prefix + " * EQUAL CIRCUIT: " + circuit_name + "\n")//
                .append(line_prefix + " * INPUT : " + ShahenStrings.toBooleanString(inputs) + "\n")//
                .append(line_prefix + " * OUTPUT: " + ShahenStrings.toBooleanString(correct_outputs) + "\n")//
                .append(line_prefix + " ******************************************************/\n");
    }

    public void exportModel(LL_ReductionI reduction, String filename, boolean verbose_comments) {
        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();

        constraints.append("subject to {\n");

        String keya_name = null;
        String keyb_name = null;

        addToILPFile(reduction, constraints, variables, keya_name, keyb_name, 1, true, verbose_comments);
        constraints.append("\n}");

        LL_SolverI.add_comment_and_variables(constraints, variables);
        // Write out model to file name, if exception, then write to stdout
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(constraints.toString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out ILP Model to file: " + filename + "\n\n");
            System.out.println(constraints.toString());
        }
    }

    private double to_double(Boolean b) {
        if (b) return 1.0;
        return 0.0;
    }

    public void clearModel() {
        circuita.clearModel();
        circuitb.clearModel();
    }

    public boolean verify(Boolean[] key_a, Boolean[] key_b) {
        boolean result = true;
        String inputs_str = ShahenStrings.toBooleanString(inputs);
        String key_a_str = ShahenStrings.toBooleanString(key_a);
        String e_output_correct_str = ShahenStrings.toBooleanString(correct_outputs);

        if (!circuit.get_output(inputs, key_a)) {
            log.severe("[VerifyStep] Unable to propagate through circuit; " + "input: " + inputs_str + "; Key: "
                    + key_a_str);
            return false;
        }
        Boolean[] e_output_a = circuit.getOutputValues();
        String e_output_a_str = ShahenStrings.toBooleanString(e_output_a);

        boolean are_equal = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < correct_outputs.length; i++) {
            if (!correct_outputs[i].equals(e_output_a[i])) {
                are_equal = false;
                sb.append(" * Incorrect Output Bit[" + i + "]: ").append(circuit.output_order.get(i))
                        .append(" {correct: ").append(correct_outputs[i] ? "1" : "0").append(", using key:")
                        .append(e_output_a[i] ? "1" : "0").append("}\n");
            }
        }

        if (!are_equal) {
            log.severe("[VerifyStep] Input/Key does not return the correct output" +//
                    ";\nInput  : " + inputs_str + //
                    ";\nILP Key: " + key_a_str + //
                    ";\nCircuit Output: " + e_output_a_str + //
                    ";\nCorrect Output: " + e_output_correct_str +//
                    ";\nIncorrect Wires:\n" + sb.toString());
            result = false;
        }

        if (key_b != null) {
            String key_b_str = ShahenStrings.toBooleanString(key_b);
            if (!circuit.get_output(inputs, key_b)) {
                log.severe("[VerifyStep] Unable to propagate through circuit; " + "input: " + inputs_str + "; Key: "
                        + key_b_str);
                return false;
            }

            Boolean[] e_output_b = circuit.getOutputValues();
            String e_output_b_str = ShahenStrings.toBooleanString(e_output_b);

            are_equal = true;
            sb = new StringBuilder();
            for (int i = 0; i < correct_outputs.length; i++) {
                if (!correct_outputs[i].equals(e_output_b[i])) {
                    are_equal = false;
                    sb.append(" * Incorrect Output Bit[" + i + "]: ").append(circuit.output_order.get(i))
                            .append(" {correct: ").append(correct_outputs[i] ? "1" : "0").append(", using key:")
                            .append(e_output_b[i] ? "1" : "0").append("}\n");
                }
            }

            if (!are_equal) {
                log.severe("[VerifyStep] Input/Key does not return the correct output" +//
                        ";\nInput: " + inputs_str + //
                        ";\nKey  : " + key_b_str + //
                        ";\nCircuit Output: " + e_output_b_str + //
                        ";\nCorrect Output: " + e_output_correct_str +//
                        ";\nIncorrect Wires:\n" + sb.toString());
                result = false;
            }
        }
        return result;
    }
}
