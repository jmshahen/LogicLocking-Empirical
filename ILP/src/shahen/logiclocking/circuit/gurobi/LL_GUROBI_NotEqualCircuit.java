package shahen.logiclocking.circuit.gurobi;

import java.util.logging.Logger;

import gurobi.*;
import shahen.string.ShahenStrings;

/**
 * Force two sets of wires to not be equal, or force 1 set of wires and a constant to be not equal.
 * @author Jonathan Shahen
 *
 */
public class LL_GUROBI_NotEqualCircuit {
    public static Integer count = 0;
    public Logger log;

    public GRBVar[] in1_wires = null;
    public GRBVar[] in2_wires = null;
    public Boolean[] in2_bools = null;

    public GRBVar[] noteq_vars = null;

    public LL_GUROBI_NotEqualCircuit(Logger log, GRBVar[] in1, GRBVar[] in2) {
        this.log = log;
        this.in1_wires = in1;
        this.in2_wires = in2;
    }
    public LL_GUROBI_NotEqualCircuit(Logger log, GRBVar[] in1, Boolean[] in2) {
        this.log = log;
        this.in1_wires = in1;
        this.in2_bools = in2;
    }

    public Integer incr_count() {
        count++;
        return count;
    }

    /**
     * 
     * @param gurobi
     * @throws GRBException 
     */
    public void addToGurobi(GRBModel gurobi) throws GRBException {
        // Error Checking
        if (in1_wires == null || (in2_wires == null && in2_bools == null))
            throw new IllegalArgumentException("in1 cannot be null, and at least one version of in2 must not be null");
        if ((in2_wires != null && in1_wires.length != in2_wires.length)
                || (in2_bools != null && in1_wires.length != in2_bools.length))
            throw new IllegalArgumentException("in1 must be the same length as in2. |in1|=" + in1_wires.length
                    + "; |in2|=" + ((in2_wires != null) ? in2_wires.length : in2_bools.length));

        // Ensure that the output from both circuits is not equal
        if (in2_wires != null) {
            noteq_vars = gurobi.addVars(in1_wires.length, GRB.BINARY);

            GRBLinExpr sum = new GRBLinExpr();
            for (int i = 0; i < in1_wires.length; i++) {
                add_not_eq_constr(gurobi, noteq_vars[i], in1_wires[i], in2_wires[i]);
                sum.addTerm(1, noteq_vars[i]);
            }
            gurobi.addConstr(sum, GRB.GREATER_EQUAL, 1, "noteq_sum_" + incr_count());
        } else {
            GRBLinExpr sum_expr = new GRBLinExpr();
            for (int i = 0; i < in1_wires.length; i++) {
                if (in2_bools[i]) {
                    sum_expr.addTerm(-1, in1_wires[i]);
                    sum_expr.addConstant(1);
                } else {
                    sum_expr.addTerm(1, in1_wires[i]);
                }
            }
            gurobi.addConstr(sum_expr, GRB.GREATER_EQUAL, 1, "not_eq_" + incr_count());
        }
    }

    /**
     * Adds a constraint to store the following:
     * <ul>
     * <li>1 -- if wire1 != wire2
     * <li>0 -- if wire1 == wire2
     * </ul>
     * @param model
     * @param noteq_var
     * @param wire1
     * @param wire2
     * @throws GRBException 
     */
    private void add_not_eq_constr(GRBModel gurobi, GRBVar noteq_var, GRBVar wire1, GRBVar wire2) throws GRBException {
        int incr = incr_count();
        GRBVar z = gurobi.addVar(0, 1, 0, GRB.BINARY, "not_eq_" + incr + "_extra");

        // out == in1 + in2 + -2*z
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addTerm(1, wire1);
        e1.addTerm(1, wire2);
        e1.addTerm(-2, z);
        gurobi.addConstr(noteq_var, GRB.EQUAL, e1, "not_eq_" + incr);
    }

    /**
     * Writes to 2 string builders, 
     * one for the constraints and one to write the variable declarations
    * @param constraints a StringBuilder where all constraints will be appended to. 
    *   This StringBuilder should be able to fit within the <code>subject to { ...HERE... }</code>
    * @param variables
    * @param name_a if null, then new key variables will be created for the first equal circuit (<i>circuita.circuit_name + "_" + WIRE_TYPE.KEY</i>).<br/> 
    *   Otherwise it will use the name: keya_name + "_" + WIRE_TYPE.KEY<br/>
    *   Use this when you have already added a DLL_CPLEX_DiffCircuit.
    * @param name_b if null, then new key variables will be created for the second equal circuit (<i>circuitb.circuit_name + "_" + WIRE_TYPE.KEY</i>).<br/> 
    *   Otherwise it will use the name: keyb_name + "_" + WIRE_TYPE.KEY<br/>
    *   Use this when you have already added a DLL_CPLEX_DiffCircuit.
    * @param addBothEqualCircuits
    * @param verbose_comments
    */
    public int addToILPFile(StringBuilder constraints, StringBuilder variables, String name_a, String name_b,
            int constraint_count, boolean verbose_comments) {
        add_banner(constraints, name_a, name_b);

        if (verbose_comments) constraints.append("  // c" + constraint_count + "\n");
        if (in2_wires != null) {
            constraints.append("  sum(i in 0.." + (in1_wires.length - 1) + ") abs(" +//
                    name_a + "[i] - " + name_b + "[i])" +//
                    " >= 1;\n\n");
        } else {
            constraints.append("  (");
            for (int i = 0; i < in1_wires.length; i++) {
                if (i != 0) constraints.append(" + ");

                if (in2_bools[i]) constraints.append("(1-").append(name_a).append("[" + i + "])");
                else constraints.append(name_a).append("[" + i + "]");
            }
            constraints.append(") >= 1;\n\n");
        }

        return constraint_count++;
    }

    private void add_banner(StringBuilder sb, String name_a, String name_b) {
        sb//
                .append("  /******************************************************\n")//
                .append("   * Not-EQUAL CIRCUIT:\n")//
                .append("   * INPUT A: " + name_a + "\n");
        if (in2_wires != null) {
            sb.append("   * INPUT B: " + name_b + "\n");
        } else {
            sb.append("   * INPUT B: " + ShahenStrings.toBooleanString(in2_bools) + "\n");
        }
        sb.append("   ******************************************************/\n");
    }
    public boolean verify(Boolean[] new_key, Boolean[] old_key) {
        if (new_key == null || old_key == null) {
            log.warning("[Verify NotEqualCircuit] One or both of the provided keys is null. new_key=" + new_key
                    + "; old_key=" + old_key);

            return false;
        }

        if (new_key.length != old_key.length) {
            log.warning("[Verify NotEqualCircuit] The keys are not of the same length. new_key=" + new_key.length
                    + "; old_key=" + old_key.length);

            return false;
        }

        for (int i = 0; i < new_key.length; i++) {
            if (new_key[i] != old_key[i]) { return true; }
        }

        log.warning("[Verify NotEqualCircuit] The keys are equal to each other!");
        return false;
    }
}
