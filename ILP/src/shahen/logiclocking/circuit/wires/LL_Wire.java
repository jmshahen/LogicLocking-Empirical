package shahen.logiclocking.circuit.wires;

import gurobi.GRBLinExpr;
import gurobi.GRBVar;
import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;

public class LL_Wire {
    private int wire_num;
    private String name;
    private WIRE_TYPE type;
    private int hashCode;

    private Boolean value = null;

    public LL_Wire(String identifier, int wire_num, WIRE_TYPE type) {
        name = identifier;
        hashCode = identifier.hashCode();
        this.wire_num = wire_num;
        this.type = type;
    }

    public WIRE_TYPE get_type() {
        return type;
    }

    /** Returns true if the type is INPUT, otherwise false */
    public boolean is_input() {
        return (type == null) ? false : type == WIRE_TYPE.INPUT;
    }

    /** Returns true if the type is OUTPUT, otherwise false */
    public boolean is_output() {
        return (type == null) ? false : type == WIRE_TYPE.OUTPUT;
    }

    /** Returns true if the type is INTERNAL, otherwise false */
    public boolean is_internal() {
        return (type == null) ? false : type == WIRE_TYPE.INTERNAL;
    }

    /**
     * Return the name that was used to create this wire.
     * @return
     */
    public String get_name() {
        return name;
    }
    public String get_short_name() {
        return "w" + wire_num;
    }

    /**
     * Returns a variable name that can be used in the ILP configuration.
     * <br>
     * By providing different strings for {@code circuit_name} 
     * we can reuse this circuit to reference many ILP circuits.
     * @param circuit_name
     * @param input_name  
     * @return
     */
    public String get_ilp_name(String circuit_name, String input_name) {
        return get_ilp_name(circuit_name, input_name, circuit_name, null, null);
    }
    /**
     * Returns a variable name that can be used in the ILP configuration.
     * <br>
     * By providing different strings for {@code circuit_name} 
     * we can reuse this circuit to reference many ILP circuits.
     * @param circuit_name
     * @param input_name 
     * @param key_name 
     * @param input_values If null, then ignored; Otherwise the ILP name returned is "1" or "0" based on the input_value[wire_num]
     * @param output_values If null, then ignored; Otherwise the ILP name returned is "1" or "0" based on the output_value[wire_num]
     * @return
     */
    public String get_ilp_name(String circuit_name, String input_name, String key_name, Boolean[] input_values,
            Boolean[] output_values) {
        if (type == WIRE_TYPE.INPUT) {
            if (input_values != null) return (input_values[wire_num] ? "1" : "0");
            if (input_name == null || input_name.isEmpty()) return type.toString() + "[" + wire_num + "]";
            return input_name + "_" + type.toString() + "[" + wire_num + "]";
        }
        if (type == WIRE_TYPE.KEY) {
            if (key_name == null || key_name.isEmpty()) return type.toString() + "[" + wire_num + "]";
            return key_name + "_" + type.toString() + "[" + wire_num + "]";
        }
        if (type == WIRE_TYPE.OUTPUT) {
            if (output_values != null) return (output_values[wire_num] ? "1" : "0");
        }
        if (circuit_name == null || input_name.isEmpty()) return type.toString() + "[" + wire_num + "]";
        return circuit_name + "_" + type.toString() + "[" + wire_num + "]";
    }

    public IloNumExpr get_ilp_var(LL_CPLEX_Circuit cp) {
        switch (type) {
            case INPUT :
                return cp.inputs[wire_num];
            case INTERNAL :
                return cp.internal[wire_num];
            case OUTPUT :
                return cp.outputs[wire_num];
            case KEY :
                return cp.key[wire_num];
        }
        return null;
    }

    public GRBVar get_gurobi_var(LL_GUROBI_Circuit cp) {
        switch (type) {
            case INPUT :
                return cp.inputs[wire_num];
            case INTERNAL :
                return cp.internal[wire_num];
            case OUTPUT :
                return cp.outputs[wire_num];
            case KEY :
                return cp.key[wire_num];
        }
        return null;
    }

    /**
     * Returns the wire as a variable or as a constant representing its value
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws IloException 
     */
    public IloNumExpr get_cplex_value(LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
    
        if (is_input() && input_values != null) return cplex.constant(get_value_double(input_values));
        if (is_output() && output_values != null) return cplex.constant(get_value_double(output_values));
    
        return get_ilp_var(cp);
    }

    public void set_name(String new_name) {
        name = new_name;
        if (new_name == null) {
            hashCode = 0;
        } else {
            hashCode = name.hashCode();
        }
    }

    /**
     * Get the value of the wire
     * @return NULL if no value, True/False if a value was set 
     */
    public Boolean get_value() {
        return value;
    }
    /**
     * Returns true if a value has been set, false otherwise.
     * @return 
     */
    public boolean has_value() {
        return value != null;
    }

    /**
     * Get the value of the wire
     * @return returns default_val if no value was set, True/False if a value was set 
     */
    public boolean get_value(boolean default_val) {
        if (!has_value()) return default_val;
        return value;
    }

    /**
     * Get the value of the wire
     * @return -1 if no value, 1/0 if a value was set 
     */
    public double get_value_double() {
        if (!has_value()) return -1;
        return value ? 1 : 0;
    }

    /**
     * Get the value of the wire
     * @return returns default_val if no value was set, True/False if a value was set 
     */
    public double get_value_double(double default_val) {
        if (!has_value()) return default_val;
        return value ? 1 : 0;
    }
    /**
     * Get the value of the wire when given an input bitset (we use wire_num)
     * @return NULL if no value, True/False if a value was set 
     */
    public Boolean get_value(Boolean[] values) {
        if (values == null) return null;
        if (wire_num >= values.length) return null;
        return values[wire_num];
    }

    /**
     * Get the value of the wire when given an input bitset (we use wire_num)
     * @return -1 if no value, 1/0 if a value was set 
     */
    public double get_value_double(Boolean[] values) {
        if (values == null) return -1;
        if (wire_num >= values.length) return -1;
        return values[wire_num] ? 1 : 0;
    }

    /**
     * Update the value that this wire is currently set to
     * @param val
     */
    public void set_value(Boolean val) {
        value = val;
    }

    public String get_value_string() {
        if (!has_value()) return "X";
        if (value) return "1";
        return "0";
    }

    /**
     * Returns the name and value in the format: NAME(VALUE).
     * 
     * Example: n01(1), po1(X), po00(0)
     * @return
     */
    public String get_name_value() {
        return name + "(" + get_value_string() + ")";
    }

    /**
     * Resets the wire to remove any values that have been set
     */
    public void reset_wire() {
        value = null;
    }

    @Override
    public String toString() {
        return get_name() + " = " + get_value_string();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != LL_Wire.class) { return false; }
        return hashCode == ((LL_Wire) obj).hashCode();
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    /**
     * Adds two wires together, and inserts embedded input/output values if provided.
     * 
     * @param in2
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws IloException 
     */
    public IloNumExpr cplex_sum(LL_Wire in2, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = null;
        if (input_values != null && (is_input() || in2.is_input())) {
            if (is_input() && in2.is_input()) {
                sum = cplex.constant(get_value_double(input_values) + in2.get_value_double(input_values));
            } else if (is_input()) {
                sum = cplex.sum(get_value_double(input_values), in2.get_ilp_var(cp));
            } else {
                sum = cplex.sum(get_ilp_var(cp), in2.get_value_double(input_values));
            }
        } else sum = cplex.sum(get_ilp_var(cp), in2.get_ilp_var(cp));

        return sum;
    }

    /**
     * Adds to the expression:
     *  coeff1 * this + coeff2 * in2
     *  
     * To negate, use coeff1=-1 and coeff2=-1
     * 
     * @param in2
     * @param expr
     * @param cp
     * @param input_values
     * @param output_values
     * @param coeff1
     * @param coeff2
     */
    public void gurobi_sum(LL_Wire in2, GRBLinExpr expr, LL_GUROBI_Circuit cp, Boolean[] input_values,
            Boolean[] output_values, int coeff1, int coeff2) {
        if (input_values != null && (is_input() || in2.is_input())) {
            if (is_input() && in2.is_input()) {
                expr.addConstant(coeff1 * get_value_double(input_values) + coeff2 * in2.get_value_double(input_values));
            } else if (is_input()) {
                expr.addConstant(coeff1 * get_value_double(input_values));
                expr.addTerm(coeff2, in2.get_gurobi_var(cp));
            } else {
                expr.addTerm(coeff1, get_gurobi_var(cp));
                expr.addConstant(coeff2 * in2.get_value_double(input_values));
            }
        } else {
            expr.addTerm(coeff1, get_gurobi_var(cp));
            expr.addTerm(coeff2, in2.get_gurobi_var(cp));
        }
    }

    public void gurobi_sum(LL_Wire in2, GRBLinExpr expr, LL_GUROBI_Circuit cp, Boolean[] input_values,
            Boolean[] output_values, boolean negative) {
        if (negative) gurobi_sum(in2, expr, cp, input_values, output_values, -1, -1);
        else gurobi_sum(in2, expr, cp, input_values, output_values, 1, 1);
    }

    /**
     * Subtracts two wires together, and inserts embedded input/output values if provided.
     * <br>
     * Diff: <i>This</i> - <i>in2</i>
     * 
     * 
     * @param in2
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws IloException 
     */
    public IloNumExpr cplex_diff(LL_Wire in2, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        if (input_values != null && (is_input() || in2.is_input())) {
            // Optimization: do the arithmetic here
            if (is_input() && in2.is_input()) {
                return cplex.constant(get_value_double(input_values) - in2.get_value_double(input_values));
            }

            return cplex.diff(get_cplex_value(cp, cplex, input_values, output_values),
                    in2.get_cplex_value(cp, cplex, input_values, output_values));
        }

        return cplex.diff(get_ilp_var(cp), in2.get_ilp_var(cp));
    }

    /**
     * Returns the products of a wire with a double, and inserts the embedded output values if provided.
     * <br>
     * IF the WIRE is of type OUTPUT and output_values is not null, then it will return a constant using output_values[wire] * num
     * <br>
     * IF the WIRE is of type INPUT and input_values is not null, then it will return a constant using input_values[wire] * num
     * <br>
     * ELSE it will return the cplex product of the number and the cplex wire variable
     * 
     * @param num
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws IloException 
     */
    public IloNumExpr cplex_prod(double num, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        if (output_values != null && is_output()) return cplex.constant(num * get_value_double(output_values));
        if (input_values != null && is_input()) return cplex.constant(num * get_value_double(input_values));

        return cplex.prod(num, get_ilp_var(cp));
    }

    public void gurobi_prod(double num, GRBLinExpr expr, LL_GUROBI_Circuit cp, Boolean[] input_values,
            Boolean[] output_values) {
        if (output_values != null && is_output()) {
            expr.addConstant(num * get_value_double(output_values));
        } else if (input_values != null && is_input()) {
            expr.addConstant(num * get_value_double(input_values));
        } else {
            expr.addTerm(num, get_gurobi_var(cp));
        }
    }

}
