package shahen.logiclocking.circuit.wires;

public enum WIRE_TYPE {
    /**
     * 
     */
    INPUT,
    /**
     * 
     */
    KEY,
    /**
     * 
     */
    INTERNAL,
    /**
     * 
     */
    OUTPUT
}
