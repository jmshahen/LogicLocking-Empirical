package shahen.logiclocking.options;

import org.apache.commons.cli.CommandLine;

public class LL_OptionFunctions {
    /**
     * Returns an integer or null
     * @param val
     * @return
     */
    public static Integer getIntFromString(String val) {
        try {
            Integer i = Integer.valueOf(val);
            return i;
        } catch (Exception e) {
            // do nothing
        }
        return null;
    }

    public static Integer getIntFromOptions(CommandLine cmd, String optStr, Integer defaultVal) {
        if (cmd.hasOption(optStr)) {
            Integer i = getIntFromString(cmd.getOptionValue(optStr));
            if (i != null) return i;
        }
        return defaultVal;
    }

    /**
     * Returns TRUE for the following string (FALSE otherwise):
     * <ul>
     * <li>1
     * <li>true
     * <li>t
     * </ul>
     * All values above are case-insensitive.
     * @param cmd
     * @param optStr
     * @param defaultVal
     * @return
     */
    public static Boolean getBoolFromOptions(CommandLine cmd, String optStr, Boolean defaultVal) {
        if (cmd.hasOption(optStr)) {
            String i = cmd.getOptionValue(optStr).toLowerCase();

            if (i.equals("1") || i.equals("true") || i.equals("t")) { return true; }
            return false;
        }
        return defaultVal;
    }
}
