package shahen.logiclocking.options;

import shahen.logiclocking.reduction.LL_ReductionI;

public enum LL_OptionString {
    ////////////////////////////////////////////////////////////////////////
    // INSTANT RETURN (INFORMATIONAL)
    /** Displays the help message in the command prompt */
    HELP("help", "Displays the help message"),
    /** Displays a list of authors who have contributed to this project */
    AUTHORS("authors", "Displays a list of authors who have contributed to this project"),
    /** Displays the current version of this program */
    VERSION("version", "Displays the current version of this program"),
    // END OF INSTANT RETURN (INFORMATIONAL)
    ////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    // OUTPUT FILES
    /** The database file to save the results of this run */
    DBFILE("output", "Change the database file to save the results of this run to"),//
    /** Write out all distinguishing inputs from each iterative stage to a file in logs folder. */
    DUMP_DISTINGUISHING_INPUTS("dumpinputs", "Write all distinguishing inputs to an output file."),
    // END OF OUTPUT FILES
    ////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    // LOGGING / DEBUGGING
    /** Flag to turn on debug settings. */
    DEBUG("debug",
            "Flag to turn on debug settings. "
                    + "More messages printed and each model is exported into 2 formats before solving. "
                    + "(drastically slows down performance)"),
    /** Sets the logging level for the console and the log file */
    LOGLEVEL("loglevel", "Sets the logging level for the console and the log file"),
    /** Indicates which file to store the log file in */
    LOGFILE("logfile", "Indicates which file to store the log file in"),
    /** Do Not write out the results in a table after all tests have run (so you don't have to scroll through the data */
    NOSUMMARY("nosummary", "Do Not write a summary of each file and the result after all files have been run."),
    /** Debug Setting. Verify the keys after each step that they correctly match the input/output pairs 
     * and that the distinguishing input is indeed distinguishing.
     */
    VERIFY_STEPS("verify_steps", "Debug Setting. Verify the keys after each step that they "
            + "correctly match the input/output pairs and that the distinguishing input is indeed distinguishing."),
    // END OF LOGGING / DEBUGGING
    ////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    // DISPLAY SETTINGS
    /** Does not write the CSV file header to the output log */
    NOHEADER("noheader", "Does not write the CSV file header to the output log"),
    /** Sets the maximum character width of the console log. Use 0 for no word-wrapping. */
    MAXW("maxw", "Sets the maximum character width of the console log. Use 0 for no word-wrapping. (default 120)"),
    /** Change the wrap line string for the console. */
    LINESTR("linstr", "The new line string when wrapping a long line (default '\\n    ')"),
    // END OF DISPLAY SETTINGS
    ////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    // WHICH FILE(S) TO PROCESS
    /** The file or folder to test */
    LOCKED("locked", "The file or folder that points to locked .bench file(s)"),
    /** The unlocked circuit that accompanies the locked file in {@link LogicLockingILPOptionString#FILE} */
    UNLOCKED("unlocked", "The unlocked circuit .bench file, which has no key input wires."),
    /** The file extension to grab from a folder */
    EXT("ext", "Changes the default file extension that .bench files are saved with."),
    /** Indicates that the {@link LogicLockingILPOptionString#LOCKED} is a folder and 
     * for use to find all files with the extension {@link LogicLockingILPOptionString#EXT} */
    BULK("bulk", "Indicates that the locked input is a folder and to find all files within that folder "
            + "with the extension .bench or stored in EXT"),
    // END OF WHICH FILE(S) TO PROCESS
    ////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    // PERFORMANCE OPTIONS
    /** Pick which algorithm CPLEX should use to solve the linear model:
     * <table>
     * <tr><td><b>Value</b></td><td><b>Symbol</b></td><td><b>Meaning</b></td></tr>
     * <tr><td>0</td><td>CPX_ALG_AUTOMATIC</td><td>Automatic: let CPLEX choose; default</td></tr>
     * <tr><td>1</td><td>CPX_ALG_PRIMAL</td><td>Primal simplex</td></tr>
     * <tr><td>2</td><td>CPX_ALG_DUAL</td><td>Dual simplex</td></tr>
     * <tr><td>3</td><td>CPX_ALG_NET</td><td>Network simplex</td></tr>
     * <tr><td>4</td><td>CPX_ALG_BARRIER</td><td>Barrier</td></tr>
     * <tr><td>5</td><td>CPX_ALG_SIFTING</td><td>Sifting</td></tr>
     * <tr><td>6</td><td>CPX_ALG_CONCURRENT</td><td>Concurrent (Dual, Barrier, and Primal in opportunistic parallel mode; 
     * Dual and Barrier in deterministic parallel mode)</td></tr>
     * </table>
     * 
     * For Gurobi, here are the values available for algorithm:
     * <table>
     * <tr><td><b>Value</b></td><td><b>Symbol</b></td><td><b>Meaning</b></td></tr>
     * <tr><td>-1</td><td>AUTOMATIC</td><td>Automatic: let Gurobi choose; default</td></tr>
     * <tr><td>0</td><td>PRIMAL SIMPLEX</td><td>Primal simplex</td></tr>
     * <tr><td>1</td><td>DUAL SIMPLEX</td><td>Dual simplex</td></tr>
     * <tr><td>2</td><td>BARRIER</td><td>Barrier</td></tr>
     * <tr><td>3</td><td>CONCURRENT</td><td>Concurrent (Dual, Barrier, and Primal in opportunistic parallel mode)</td></tr>
     * <tr><td>4</td><td>CONCURRENT</td><td>Concurrent (Dual and Barrier in deterministic parallel mode)</td></tr>
     * <tr><td>5</td><td>DET CON SIMP</td><td>deterministic concurrent simplex</td></tr>
     * </table>
     * Options are: -1=automatic, 0=primal simplex, 1=dual simplex, 2=barrier, 
     * 3=concurrent, 4=deterministic concurrent, 5=deterministic concurrent simplex.
     */
    ALGORITHM("alg", "Pick which algorithm CPLEX[0,6]/Gurobi[-1,5] should use to solve the linear model. "
            + "'cx' for all CPLEX algorithms, 'gx' for all GUROBI."),
    /** The number of seconds each stage is allowed to run. Specifically, this value is passed to CPLEX as a timeout. */
    STAGE_TIMEOUT("stagetimeout",
            "The number of seconds each stage is allowed to run. Specifically, this value is passed to CPLEX as a timeout."),
    /** The number of seconds the solver is allowed to run.
     * Specifically, after X number of seconds the solver will time out and the next file or aglorithm option will try to be solved.
     */
    TIMEOUT("timeout", "The number of seconds the solver is allowed to run. "
            + "Specifically, after X number of seconds the solver will time out and the next file or aglorithm option will try to be solved."),
    /** The number of seconds to wait between executions to allow the garbage collection thread to run. */
    GARBAGE_COLLECTION_SEC("gc",
            "The number of seconds to wait between executions to allow the garbage collection thread to run."),
    /** The number of threads to run CPLEX with. 
     * Most times single thread is better than multiple threads, depends on operating system/motherboard/cpu/etc
     */
    THREADS("threads", "The number of threads to run CPLEX/GUROBI with."),
    /** Allows to potentially speed up CPLEX by minimizing synchronizing threads to maximize parallelizability */
    PARALLEL("parallel", "Runs CPLEX with the Parallel Mode Swith option in Opportunistic."),
    /** 
     * Controls trade-offs between speed, feasibility, optimality, and moving bounds in MIP.
     * <table>
     * <tr><td><b>Value</b></td><td><b>Symbol</b></td><td><b>Meaning</b></td></tr>
     * <tr><td>0</td><td>CPX_MIPEMPHASIS_BALANCED</td><td>Balance optimality and feasibility; default</td></tr>
     * <tr><td>1</td><td>CPX_MIPEMPHASIS_FEASIBILITY</td><td>Emphasize feasibility over optimality</td></tr>
     * <tr><td>2</td><td>CPX_MIPEMPHASIS_OPTIMALITY</td><td>Emphasize optimality over feasibility</td></tr>
     * <tr><td>3</td><td>CPX_MIPEMPHASIS_BESTBOUND</td><td>Emphasize moving best bound</td></tr>
     * <tr><td>4</td><td>CPX_MIPEMPHASIS_HIDDENFEAS</td><td>Emphasize finding hidden feasible solutions</td></tr>
     * </table>
     * 
     * Source: https://www.ibm.com/support/knowledgecenter/en/SS9UKU_12.4.0/com.ibm.cplex.zos.help/Parameters/topics/MIPEmphasis.html
     */
    MIP_EMPHASIS("mip_emphasis", "Runs CPLEX with a different MIP Emphasis. " +//
            "Allowed values: [0-4] (0 BAL default, 1 FEAS, 2 OPT, 3 BEST; 4 HIDDEN)."),
    /** The number of random equal circuits to populate Stage 0 with. Default number is 0.*/
    PRELOAD("preload", "The number of random equal circuits to populate Stage 0 with. Default number is 0."),
    /** Preload the circuit with specific inputs, allows to get the solution into a specific state.*/
    PRELOAD_FILE("preloadfile", "A file containing a binary string on each line of the file. new lines are ignored."),
    /** Ability to turn on or off the extra constraint that specifies the keys must not be equal. */
    EXTRA_CONSTRAINT_KEYS_NOT_EQUAL("keysconstraint",
            "Ability to turn on (1) or off (0) the extra constraint that specifies the keys must not be equal."),
    /** Ability to change the solver used. See all subclasses of {@link LL_Solver} */
    SOLVER("solver", "Available options: cplex_v1, cplex_v2, cplex_v3, gurobi_v1."),
    /** Ability to change the reduction method used. See all classes that implement {@link LL_ReductionI} */
    REDUCTION("reduction", "Available options: standard, reduced, or fromsat."),
    // END OF PERFORMANCE OPTIONS
    ////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    // THE RUN OPTION
    /** Sets the actions that will be performed on the input policies.
     * The following options are available: {@link LogicLockingILPRunOptions}. 
     */
    RUN("run", "What action should be applied to the input file(s)."),
    ////////////////////////////////////////////////////////////////////////
    // EXTRA INPUT OPTIONS
    /** The name of a wire the input circuit */
    WIRE("wire", "Some run options require an input wire, supply the name as seen in the .bench file."),
    /** A binary string to use as the inputs */
    INPUTVALUES("input", "Some run options require a binary string to use as input."),
    /** A binary string to use as the key inputs */
    KEYVALUES("key", "Some run options require a binary string to use as the key."),
    /** The number of inputs that should be tested */
    NUM_INPUTS("num_inputs", "The number of inputs that should be tested when verifying 2 circuits are equivalent");

    ////////////////////////////////////////////////////////////////////////
    private String _cmd;
    private String _description;

    private LL_OptionString(String cmd, String description) {
        _cmd = cmd;
        _description = description;
    }

    @Override
    public String toString() {
        return _cmd;
    }

    /** 
     * Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-" + _cmd + " ";
    }

    /** 
     * Returns the commandline equivalent with the hyphen and a space following: LOGLEVEL("debug") -> "-loglevel debug"
     */
    public String c(String param) {
        return "-" + _cmd + " " + param + " ";
    }

    /** Returns the description of the option */
    public String description() {
        return _description;
    }
    /** Returns the description of the option */
    public String d() {
        return _description;
    }
}
