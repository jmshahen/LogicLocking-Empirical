package shahen.logiclocking.options;

import java.io.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;

import org.apache.commons.cli.*;

import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.formatter.ShahenCSVFileFormatter;
import shahen.formatter.ShahenConsoleFormatter;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.options.run.LL_RunOptions;
import shahen.string.ShahenStrings;

public class LL_OptionsHelper {

    public static CommandLine init(LL_ILPInstance ll, String[] args)
            throws ParseException, SecurityException, IOException, Exception {
        Options options = new Options();
        setupOptions(ll, options);
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        setupLoggerOptions(ll, cmd, options);
        if (setupReturnImmediatelyOptions(ll, cmd, options)) { return null; }

        setupUserPreferenceOptions(ll, cmd, options);
        setupFileOptions(ll, cmd, options);
        setupResultOptions(ll, cmd, options);
        setupControlOptions(ll, cmd, options);

        return cmd;
    }

    /** Adds all of the available options to input parameter.
     * 
     * @param options */
    public static void setupOptions(LL_ILPInstance ll, Options options) {
        // Add Information Options
        options.addOption(LL_OptionString.HELP.toString(), false, LL_OptionString.HELP.d());
        options.addOption(LL_OptionString.VERSION.toString(), false,
                "Prints the version (" + LL_ILPInstance.VERSION + ") information");

        // Add Logging Level Options
        options.addOption(Option.builder(LL_OptionString.LOGLEVEL.toString()).argName("warning|info|fine|debug").desc(
                "Levels are shown in increasing level of details. Default is config, which is between info and fine.")
                .hasArg().build());

        options.addOption(Option.builder(LL_OptionString.LOGFILE.toString()).argName("logfile|'n'|'u'")
                .desc("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; default it creates a log called '"
                        + ll.Logger_filepath + "'")
                .hasArg().build());

        options.addOption(LL_OptionString.NOSUMMARY.toString(), false, LL_OptionString.NOSUMMARY.description());
        options.addOption(LL_OptionString.NOHEADER.toString(), false, LL_OptionString.NOHEADER.description());

        options.addOption(Option.builder(LL_OptionString.DBFILE.toString()).argName("dbfile")
                .desc("The file where the result should be stored").hasArg().build());

        // custom Console Logging Options
        options.addOption(Option.builder(LL_OptionString.MAXW.toString()).argName("num").desc(LL_OptionString.MAXW.d())
                .hasArg().build());

        options.addOption(Option.builder(LL_OptionString.LINESTR.toString()).argName("string")
                .desc(LL_OptionString.LINESTR.d()).hasArg().build());

        options.addOption(LL_OptionString.DEBUG.toString(), false, LL_OptionString.DEBUG.d());

        options.addOption(LL_OptionString.VERIFY_STEPS.toString(), false, LL_OptionString.VERIFY_STEPS.d());

        options.addOption(LL_OptionString.DUMP_DISTINGUISHING_INPUTS.toString(), false,
                LL_OptionString.DUMP_DISTINGUISHING_INPUTS.d());

        // Add File IO Options
        options.addOption(Option.builder(LL_OptionString.LOCKED.toString()).argName("file|folder")
                .desc("Path to the file (or Folder if the 'bulk' option is set)").hasArg().build());
        options.addOption(Option.builder(LL_OptionString.UNLOCKED.toString()).argName("file|folder")
                .desc("Path to the file").hasArg().build());

        options.addOption(Option.builder(LL_OptionString.EXT.toString()).argName("extension")
                .desc("File extention used when searching for files when the 'bulk' option is used. Default:'"
                        + ll.fileHelper.fileExt + "'")
                .hasArg().build());

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Performance Options
        options.addOption(Option.builder(LL_OptionString.STAGE_TIMEOUT.toString()).argName("seconds")
                .desc(LL_OptionString.STAGE_TIMEOUT.d()).hasArg().build());
        options.addOption(Option.builder(LL_OptionString.TIMEOUT.toString()).argName("seconds")
                .desc(LL_OptionString.TIMEOUT.d()).hasArg().build());

        options.addOption(Option.builder(LL_OptionString.GARBAGE_COLLECTION_SEC.toString()).argName("seconds")
                .desc(LL_OptionString.GARBAGE_COLLECTION_SEC.d()).hasArg().build());

        options.addOption(Option.builder(LL_OptionString.THREADS.toString()).argName("num")
                .desc("The number of threads to set CPLEX to use").hasArg().build());

        options.addOption(Option.builder(LL_OptionString.ALGORITHM.toString()).argName("num")
                .desc(LL_OptionString.ALGORITHM.d()).hasArg().build());

        options.addOption(Option.builder(LL_OptionString.SOLVER.toString()).argName("str")
                .desc(LL_OptionString.SOLVER.d()).hasArg().build());

        options.addOption(Option.builder(LL_OptionString.REDUCTION.toString()).argName("standard|reduced")
                .desc(LL_OptionString.REDUCTION.d()).hasArg().build());

        options.addOption(LL_OptionString.PARALLEL.toString(), false, LL_OptionString.PARALLEL.d());
        options.addOption(Option.builder(LL_OptionString.MIP_EMPHASIS.toString()).argName("num")
                .desc(LL_OptionString.MIP_EMPHASIS.d()).hasArg().build());
        options.addOption(Option.builder(LL_OptionString.PRELOAD.toString()).argName("num")
                .desc(LL_OptionString.PRELOAD.d()).hasArg().build());
        options.addOption(Option.builder(LL_OptionString.PRELOAD_FILE.toString()).argName("file")
                .desc(LL_OptionString.PRELOAD_FILE.d()).hasArg().build());
        options.addOption(Option.builder(LL_OptionString.EXTRA_CONSTRAINT_KEYS_NOT_EQUAL.toString()).argName("0|1")
                .desc(LL_OptionString.EXTRA_CONSTRAINT_KEYS_NOT_EQUAL.d()).hasArg().build());
        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Functional Options
        options.addOption(LL_OptionString.BULK.toString(), false,
                "Use the folder that rbacspec points to and run against all *.spec");
        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Control Flags
        options.addOption(Option.builder(LL_OptionString.INPUTVALUES.toString()).argName("binary_string")
                .desc("The input values to supple to the circuit").hasArg().build());
        options.addOption(Option.builder(LL_OptionString.KEYVALUES.toString()).argName("binary_string")
                .desc("The key input values to supple to the circuit").hasArg().build());

        options.addOption(Option.builder(LL_OptionString.WIRE.toString()).argName("wire_name")
                .desc("The name of a wire in the input circuit to slice").hasArg().build());
        options.addOption(Option.builder(LL_OptionString.NUM_INPUTS.toString()).argName("num")
                .desc("The number of inputs to test.").hasArg().build());

        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Actionable Options
        options.addOption(Option.builder(LL_OptionString.RUN.toString()).argName("option")
                .desc("Available options: " + LL_RunOptions.availableOptions()).hasArg().build());
        ///////////////////////////////////////////////////////////////////////////////////////////
    }

    public static void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(
                maxw, "logic_locking_ilp_solver", ShahenStrings.repeat("-", maxw) + "\nAuthors: "
                        + LL_ILPInstance.AUTHORS + "\n" + ShahenStrings.repeat("-", 20),
                options, ShahenStrings.repeat("-", maxw), true);
    }

    public static void printHelp(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(LL_OptionString.MAXW.toString())) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue(LL_OptionString.MAXW.toString()));
                printHelp(options, maxw);
            } catch (NumberFormatException e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new NumberFormatException("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    /** Sets up the options that do not process anything in detail, but returns a specific informative result.<br/>
     * Examples include:
     * <ul>
     * <li>Help
     * <li>Version
     * <li>Check NuSMV programs
     * </ul>
     * 
     * @param cmd
     * @param options
     * @return
     * @throws NumberFormatException */
    private static Boolean setupReturnImmediatelyOptions(LL_ILPInstance ll, CommandLine cmd, Options options)
            throws NumberFormatException {
        if (cmd.hasOption(LL_OptionString.HELP.toString()) == true || cmd.getOptions().length < 1) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(LL_OptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            try {
                System.out.println("Logic Locking ILP version: " + LL_ILPInstance.VERSION);
                System.out.println("Installed CPLEX version  : " + (new IloCplex()).getVersion());
            } catch (IloException e) {
                e.printStackTrace();
            }
            return true;
        }

        return false;
    }

    private static void setupLoggerOptions(LL_ILPInstance ll, CommandLine cmd, Options options)
            throws SecurityException, IOException {
        // Logging Level
        ll.log.setUseParentHandlers(false);
        ShahenConsoleFormatter scf = new ShahenConsoleFormatter();
        scf.same_line = true;
        scf.maxWidth = 0;
        ll.consoleHandler.setFormatter(scf);
        ll.setLoggerLevel(Level.CONFIG);// Default Level
        if (cmd.hasOption(LL_OptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(LL_OptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("warning")) {
                ll.setLoggerLevel(Level.WARNING);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                ll.setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("fine")) {
                ll.setLoggerLevel(Level.FINE);
            } else if (loglevel.equalsIgnoreCase("info")) {
                ll.setLoggerLevel(Level.INFO);
            }
        }

        ll.log.setLevel(ll.LoggerLevel);
        ll.consoleHandler.setLevel(ll.LoggerLevel);
        ll.log.addHandler(ll.consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption(LL_OptionString.NOHEADER.toString())) {
            ll.WriteCSVFileHeader = false;
        }

        // Set File Logger
        if (cmd.hasOption(LL_OptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(LL_OptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                ll.Logger_filepath = "";
            } else if (cmd.getOptionValue(LL_OptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                ll.Logger_filepath = "mohawk-ll.log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(cmd.getOptionValue(LL_OptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    ll.Logger_filepath = logfile.getAbsolutePath();

                    if (ll.WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(ShahenCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    ll.log.severe(e.getMessage());
                    return;
                }
            }
        }

        // Add Logger File Handler
        if (!ll.Logger_filepath.isEmpty()) {

            File logfile = new File(ll.Logger_filepath);

            if (!logfile.exists()) {
                logfile.getParentFile().mkdirs();

                logfile.createNewFile();
                if (ll.WriteCSVFileHeader) {
                    FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                    writer.write(ShahenCSVFileFormatter.csvHeaders().getBytes());
                    writer.flush();
                    writer.close();
                }
            }

            ll.fileHandler = new FileHandler(ll.Logger_filepath, true);
            ll.fileHandler.setLevel(ll.getLoggerLevel());
            ll.fileHandler.setFormatter(new ShahenCSVFileFormatter());
            ll.log.addHandler(ll.fileHandler);
        }
    }

    /** Looks for the following options and sets up the program accordingly:
     * <ul>
     * <li>{@link LL_OptionString#DEBUG}
     * <li>{@link LL_OptionString#VERIFY_STEPS}
     * </ul>
     * 
     * @param cmd
     * @param options */
    private static void setupControlOptions(LL_ILPInstance ll, CommandLine cmd, Options options) {
        if (cmd.hasOption(LL_OptionString.DEBUG.toString())) {
            ll.debug = true;
            if (!ll.log.isLoggable(Level.FINE)) {
                ll.log.setLevel(Level.FINE);
                ll.log.getHandlers()[0].setLevel(Level.FINE);
            }
        } else {
            ll.debug = false;
        }
        ll.log.config("[SETTING] Debug Mode: " + ((ll.debug) ? "ENABLED" : "DISABLED"));

        if (cmd.hasOption(LL_OptionString.VERIFY_STEPS.toString())) {
            ll.verify_steps = true;
        }
        ll.log.config("[SETTING] Verify Steps: " + ((ll.verify_steps) ? "ENABLED" : "DISABLED"));

        if (cmd.hasOption(LL_OptionString.DUMP_DISTINGUISHING_INPUTS.toString())) {
            ll.dump_distinguishing_inputs = true;
        }
        ll.log.config(
                "[SETTING] Dump Distinguishing Inputs: " + ((ll.dump_distinguishing_inputs) ? "ENABLED" : "DISABLED"));
    }

    private static void setupResultOptions(LL_ILPInstance ll, CommandLine cmd, Options options) throws IOException {
        if (cmd.hasOption(LL_OptionString.DBFILE.toString())) {
            ll.dbFile = cmd.getOptionValue(LL_OptionString.DBFILE.toString());
        }
        ll.log.config("[OPTION] Results File: " + ll.dbFile);
    }

    private static void setupFileOptions(LL_ILPInstance ll, CommandLine cmd, Options options) {
        // Grab the file
        if (cmd.hasOption(LL_OptionString.LOCKED.toString())) {
            ll.log.fine("[OPTION] Using a specific File: " + cmd.getOptionValue(LL_OptionString.LOCKED.toString()));
            ll.locked_path = cmd.getOptionValue(LL_OptionString.LOCKED.toString());
        } else {
            ll.log.fine("[OPTION] No File included");
        }

        if (cmd.hasOption(LL_OptionString.EXT.toString())) {
            ll.log.fine(
                    "[OPTION] Using a specific File Extension: " + cmd.getOptionValue(LL_OptionString.EXT.toString()));
            ll.fileHelper.fileExt = cmd.getOptionValue(LL_OptionString.EXT.toString());
        } else {
            ll.log.fine("[OPTION] Using the default File Extension: " + ll.fileHelper.fileExt);
        }

        // Load more than one file from the File?
        if (cmd.hasOption(LL_OptionString.BULK.toString())) {
            ll.log.fine("[OPTION] Bulk File inclusion: Enabled");
            ll.fileHelper.bulk = true;
        } else {
            ll.log.fine("[OPTION] Bulk File inclusion: Disabled");
            ll.fileHelper.bulk = false;
        }
    }

    private static void setupUserPreferenceOptions(LL_ILPInstance ll, CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(LL_OptionString.MAXW.toString())) {
            ll.log.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(LL_OptionString.MAXW.toString());
                ((ShahenConsoleFormatter) ll.consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                ll.log.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            ll.log.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(LL_OptionString.LINESTR.toString())) {
            ll.log.fine("[OPTION] Setting the console's new line string");
            ((ShahenConsoleFormatter) ll.consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(LL_OptionString.LINESTR.toString());
        } else {
            ll.log.fine("[OPTION] Default Line String Used");
        }

    }
}
