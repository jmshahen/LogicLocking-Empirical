package shahen.logiclocking.options.run;

import java.io.File;
import java.io.FileWriter;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;

public class LL_DiffToILP implements LL_RunOptionI {

    public File toIlpDiff = new File("logs/diff_circuit.ilp");

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.info("[ACTION: ILP DIFF CIRCUIT] ");
        /* Timing */ll.timing.startTimer(timerName + "-ilp_diff");

        String ilpStr = circuit.getDiffILPString(ll.reduction, "c1", "c2", true, true, true);

        try {
            FileWriter fw = new FileWriter(toIlpDiff);
            fw.write(ilpStr);
            fw.close();
            ll.log.info("Saved ILP to file: " + toIlpDiff.getAbsolutePath());
        } catch (Exception e) {
            ll.log.warning("Unable to write to file: " + toIlpDiff.getAbsolutePath());
            System.out.println(ilpStr);
        }

        /* Timing */ll.timing.stopTimer(timerName + "-ilp_diff");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_DiffToILP();
    }

    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
