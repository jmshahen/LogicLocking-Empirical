package shahen.logiclocking.options.run;

import java.io.File;
import java.io.FileWriter;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_EqualToILP implements LL_RunOptionI {

    public File toIlpEqual = new File("logs/equal_circuit.ilp");

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.info("[ACTION: EQUAL CIRCUIT TO ILP] ");
        /* Timing */ll.timing.startTimer(timerName + "-equal_to_ilp");

        Boolean[] inputs = null;
        if (cmd.hasOption(LL_OptionString.INPUTVALUES.toString())) {
            inputs = ShahenStrings.booleanStringToArray(cmd.getOptionValue(LL_OptionString.INPUTVALUES.toString()));
            ll.log.info("Using Loaded Input Values: " + ShahenStrings.toBooleanString(inputs));
        } else {
            inputs = circuit.getRandomInputs();
            ll.log.info("Using Random Input Values: " + ShahenStrings.toBooleanString(inputs));
        }

        String ilpStr = circuit.getEqualILPString(ll.reduction, "c1", "c2", inputs, null, true, true, true);
        try {
            FileWriter fw = new FileWriter(toIlpEqual);
            fw.write(ilpStr);
            fw.close();
            ll.log.info("Saved ILP to file: " + toIlpEqual.getAbsolutePath());
        } catch (Exception e) {
            ll.log.warning("Unable to write to file: " + toIlpEqual.getAbsolutePath());
            System.out.println(ilpStr);
        }

        /* Timing */ll.timing.stopTimer(timerName + "-equal_to_ilp");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_EqualToILP();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
