package shahen.logiclocking.options.run;

import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_Evaluate implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.config("[ACTION: EVALUATE] ");
        /* Timing */ll.timing.startTimer(timerName + "-evaluate");

        ArrayList<Boolean> inputs = null;
        if (cmd.hasOption(LL_OptionString.INPUTVALUES.toString())) {
            inputs = ShahenStrings.booleanStringToArrayList(cmd.getOptionValue(LL_OptionString.INPUTVALUES.toString()));
        } else {
            inputs = circuit.getRandomInputList();
        }

        ArrayList<Boolean> key_inputs = null;
        if (cmd.hasOption(LL_OptionString.KEYVALUES.toString())) {
            key_inputs = ShahenStrings
                    .booleanStringToArrayList(cmd.getOptionValue(LL_OptionString.KEYVALUES.toString()));
        } else {
            key_inputs = circuit.known_key_list;
        }

        ll.log.config("Using Input Values: " + ShahenStrings.toBooleanString(inputs));
        ll.log.config("Using Key Values: " + ShahenStrings.toBooleanString(key_inputs));

        if (circuit.get_output(inputs, key_inputs) == false) {
            ll.log.severe("An Error occurred when trying to get the output of the circuit.");
        } else {
            ll.log.info("Output Values: " + ShahenStrings.toBooleanString(circuit.getOutputValues()));
            ll.log.info("Output Values: " + circuit.getOutputWireString(circuit.getOutputWires(), false));
            // TESTING FOR: circuit.reset()
            // circuit.reset();
            // ll.log.info("Output Values (After Reset): "
            // + circuit.getOutputWireString(circuit.outputs.values(), false));
        }
        /* Timing */ll.timing.stopTimer(timerName + "-evaluate");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_Evaluate();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
