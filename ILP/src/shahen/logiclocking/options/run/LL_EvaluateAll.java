package shahen.logiclocking.options.run;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_EvaluateAll implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.config("[ACTION: EVALUATE ALL] ");
        /* Timing */ll.timing.startTimer(timerName + "-evaluate_all");

        ArrayList<Boolean> inputs = new ArrayList<Boolean>(Arrays.asList(circuit.getZeroInputs()));
        ArrayList<Boolean> key_inputs = null;
        if (cmd.hasOption(LL_OptionString.KEYVALUES.toString())) {
            key_inputs = ShahenStrings
                    .booleanStringToArrayList(cmd.getOptionValue(LL_OptionString.KEYVALUES.toString()));
        } else {
            key_inputs = circuit.known_key_list;
        }

        ll.log.config("Using Key Values: " + ShahenStrings.toBooleanString(key_inputs));

        StringBuilder csv_output = new StringBuilder();
        csv_output.append("Input,Output\n");
        while (true) {
            if (circuit.get_output(inputs, key_inputs) == false) {
                ll.log.severe("An Error occurred when trying to get the output of the circuit.");
            } else {
                csv_output.append(ShahenStrings.toBooleanString(inputs)).append(",");
                csv_output.append(ShahenStrings.toBooleanString(circuit.getOutputValues()));
                csv_output.append("\n");
            }
            circuit.reset();

            inputs = LL_ILPInstance.getNextInput(inputs);

            if (inputs == null) {
                break;
            }
        }

        String filename = "logs/evaluate_all_key_" + ShahenStrings.toBooleanString(key_inputs) + ".csv";
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(csv_output.toString());
                writer.close();
            }
        } catch (Exception e) {
            ll.log.severe("Unable to write out CSV to file: " + filename + "\n\n");
            System.out.println(csv_output.toString());
        }

        /* Timing */ll.timing.stopTimer(timerName + "-evaluate_all");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_EvaluateAll();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
