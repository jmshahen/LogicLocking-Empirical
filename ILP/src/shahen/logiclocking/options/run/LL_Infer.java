package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_Infer implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        /* Timing */ll.timing.startTimer(timerName + "-infer");
        ll.log.config("[ACTION: INFER] ");

        Boolean[] inputs = null;
        if (cmd.hasOption(LL_OptionString.INPUTVALUES.toString())) {
            inputs = ShahenStrings.booleanStringToArray(cmd.getOptionValue(LL_OptionString.INPUTVALUES.toString()));
        } else {
            inputs = circuit.getRandomInputs();
        }

        ll.log.config("Using Input Values: " + ShahenStrings.toBooleanString(inputs));

        if (circuit.infer_output(inputs, null) == false) {
            ll.log.severe("An Error occurred when trying to get the output of the circuit.");
        } else {
            ll.log.info("Circuit\n" + //
                    "######################################################################\n" + //
                    circuit.toString() + //
                    "\n######################################################################");
        }
        /* Timing */ll.timing.stopTimer(timerName + "-infer");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_Infer();
    }

    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
