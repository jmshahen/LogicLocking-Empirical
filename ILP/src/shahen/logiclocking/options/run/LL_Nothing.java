package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;

public class LL_Nothing implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.info("[ACTION: Nothing] ");
        /* Timing */ll.timing.startTimer(timerName + "-nothing");

        /* Timing */ll.timing.stopTimer(timerName + "-nothing");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_Nothing();
    }

    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
