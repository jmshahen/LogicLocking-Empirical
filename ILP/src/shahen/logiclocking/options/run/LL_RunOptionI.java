package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;

public interface LL_RunOptionI {
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException;

    public LL_RunOptionI getNewInstance();

    public boolean use_garbage_collector();
}
