package shahen.logiclocking.options.run;

import shahen.logiclocking.circuit.cplex.LL_CPLEX_EqualCircuit;
import shahen.logiclocking.options.LL_OptionString;

public enum LL_RunOptions {
    /** */
    NOTHING("nothing", new LL_Nothing()),
    /**
     * Evaluate a circuit using Java to propagate input and keys values supplied.
     * <br/>
     * If no Inputs are provided, then random ones will be used.
     * <br/>
     * If no Key is provided, then the one stored in the circuit will be used.
     */
    EVALUATE("evaluate", new LL_Evaluate()),
    /**
     * Evaluate a circuit using Java to propagate input values supplied and infer values as far a possible.
     * <br/>
     * If no Inputs are provided, then random ones will be used.
     */
    INFER("infer", new LL_Infer()),
    /** */
    EVALUATE_ALL("evaluate_all", new LL_EvaluateAll()),
    /** 
     * Creates a direct translation of a circuit to ILP.
     * Where all wires are decision variables and thus when solved, 
     * an ILP solver will return input, key, output, and internal wire values.
     */
    TO_ILP("to_ilp", new LL_ToILP()),
    /** Reduces a locked circuit (.bench) into ILP file. */
    TO_ILP_WITH_INPUTKEY("to_ilp_with_inputkey", new LL_ToILPWithInputKey()),
    /** */
    TO_LP("to_lp", new LL_ToLP()),
    /** Reduces a locked circuit (.bench) into LP file using CPLEX/GUROBI's export model method.
     * This is more accurate than the ILP format. */
    TO_LP_WITH_INPUTKEY("to_lp_with_inputkey", new LL_ToLPWithInputKey()),
    /** 
     * Creates a very simple DIFF circuit, where there are 2 circuits (the locked circuit) and
     * the output bits have the condition where they must have at minimum 1 bit difference.
     * <br>
     * There are additional conditions to the circuit:
     * <ul>
     * <li>The inputs to both circuits are the same decision variables
     * <li>The keys, internal wires, and output wires are all separate decision variables
     * </ul>
     * 
     * The circuits are called "c1" and "c2", and the ILP code has lots of debugging comments.
     * <br>
     * This is meant as a debugging tool, not for performance.
     */
    DIFF_TO_ILP("diff_to_ilp", new LL_DiffToILP()),
    /** */
    EQUAL_TO_ILP("equal_to_ilp", new LL_EqualToILP()),
    /**
     * Takes as input a circuit file and potentially an input and/or key.
     * <br>
     * If inputs are given, then inferred values will be propagated through.
     * <br>
     * The circuit will be converted to a simplified version, and then a bench file will be written out.
     */
    SIMPLIFY_CIRCUIT("simplify", new LL_Simplify()),
    /**
     * Does the same as {@link #SIMPLIFY_CIRCUIT} but also runs a series of random keys through the 
     * original circuit and the simplified circuit and outputs how many pairs were incorrect.
     * <br/>
     * The results of the whole test are written to a CSV file if the {@link LL_OptionString#DEBUG} option is present.
     */
    SIMPLIFY_AND_TEST_CIRCUIT("simplify_test", new LL_SimplifyAndTest()),
    /** 
     * Given a circuit, and a wire, write the corresponding circuit with only 1 output wire and 
     * only the circuit elements that affect the output of that wire.
     * <br>
     * The input wires are kept identical to the original circuit so that 
     * the same input can be passed to both to check for correctness.
     */
    SLICE_CIRCUIT_OUTPUT_WIRE("slice_output", new LL_SliceOutputWire()),
    /** Given a locked circuit, iteratively find as many distinguishing inputs, and once no more can be found, 
     * then solve for the key that correctly matches all of those input/output equal circuits*/
    SOLVE("solve", new LL_Solve()),
    /** Solves the last stage, only Equal Circuits that share a common key.
     * If the {@link LL_OptionString#PRELOAD} CML option is found and is above 0, then that number of randomly 
     * generated Equal Circuits will be created and solved.
     * <br/>
     * Otherwise 2 Equal Circuits will be created with the all Zeros and all Ones inputs.
     */
    SOLVE_LAST_STAGE("solve_last", new LL_SolveLastStage()),
    /** Converts a circuit to ILP Constraints and solves for any inputs and outputs the inputs and output values. */
    SOLVE_SINGLE_CIRCUIT("solve_single", new LL_SolveILPCircuit()),
    /** Creates a single DIFF Circuit using a locked file, solves it and returns the input and keys that it found */
    SOLVE_DIFF_CIRCUIT("solve_diff", new LL_SolveDiffCircuit()),
    /** */
    SOLVE_EQUAL_INPUT_CIRCUIT("solve_equal_input", new LL_SolveEqualCircuit()),
    /** */
    SOLVE_EQUAL_RANDOM_CIRCUIT("solve_equal_random", new LL_SolveEqualCircuit_RandomInput()),
    /** 
     * Creates an {@link LL_CPLEX_EqualCircuit}, where all inputs are one, and before it solves the model, 
     * if {@link LogicLockingILPOptionString#DEBUG} is set then the model will be 
     * written to the logs folder in ILP and lp format.
     * <br>
     * The model is then solved and the input, actual key, outputs, and found key are printed to the terminal
     */
    SOLVE_EQUAL_ONES_CIRCUIT("solve_equal_ones", new LL_SolveEqualCircuit_OnesInput()),
    /** 
     * Creates an {@link LL_CPLEX_EqualCircuit}, where all inputs are zero, and before it solves the model, 
     * if {@link LogicLockingILPOptionString#DEBUG} is set then the model will be 
     * written to the logs folder in ILP and lp format.
     * <br>
     * The model is then solved and the input, actual key, outputs, and found key are printed to the terminal
     */
    SOLVE_EQUAL_ZEROS_CIRCUIT("solve_equal_zeros", new LL_SolveEqualCircuit_ZerosInput()),
    /** 
     * Verify the key stored in a locked .bench file by also supplying the unlocked file along with it.
     * This used the {@link LogicLockingILPOptionString#LOCKED} to read the locked bench file and the 
     * {@link LogicLockingILPOptionString#UNLOCKED} for the original, unlocked file.
     * <br>
     * The {@link LogicLockingILPOptionString#UNLOCKED} option cannot be a folder, 
     * thus if {@link LogicLockingILPOptionString#LOCKED} is a folder and the {@link LogicLockingILPOptionString#BULK}
     * option is present, then we check all locked files against a single unlocked circuit. 
     * <br>
     * (Optional) Accepts the option {@link LL_OptionString#KEYVALUES} to check a specific key against the unlocked file. 
     */
    VERIFY_KEY_WITH_ORIGINAL("verify_key", new LL_VerifyKey()),
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    UPDATE_CIRCUIT_STATS("update_circuit_stats", new LL_UpdateCircuitStats()),
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /** A test that shows, or does not show, whether CPLEX is able to leverage previous solved instances. */
    TEST_INCREMENTAL_CPLEX("incr_cplex", new LL_TestCplexIncrementalMode()),
    /** A test that shows if modifying a model, using edits to previous variables, allows that model to be solved. */
    TEST_ASSUMPTION_CPLEX("assump_cplex", new LL_TestCplexAssumptions());

    private String _str;
    private LL_RunOptionI _inst;

    private LL_RunOptions(String s, LL_RunOptionI i) {
        _str = s;
        _inst = i;
    }

    public LL_RunOptionI getNewInstance() {
        return _inst.getNewInstance();
    }

    public static LL_RunOptions fromString(String text) {
        for (LL_RunOptions b : LL_RunOptions.values()) {
            if (b._str.equals(text)) { return b; }
        }
        return null;
    }
    public static String availableOptions() {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (LL_RunOptions b : LL_RunOptions.values()) {
            if (first) {
                first = false;
            } else {
                sb.append("|");
            }
            sb.append(b._str);
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-run " + _str + " ";
    }
}
