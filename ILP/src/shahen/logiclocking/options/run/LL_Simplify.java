package shahen.logiclocking.options.run;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.LL_SimplifiedCircuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_Simplify implements LL_RunOptionI {
    public LL_SimplifiedCircuit sc = null;
    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.config("[ACTION: Simplify] ");
        /* Timing */ll.timing.startTimer(timerName + "-simplify");

        Boolean[] inputs = null;
        String input_str = "none";
        if (cmd.hasOption(LL_OptionString.INPUTVALUES.toString())) {
            String val = cmd.getOptionValue(LL_OptionString.INPUTVALUES.toString());
            if (val.equals("random")) {
                inputs = circuit.getRandomInputs();
            } else {
                inputs = ShahenStrings.booleanStringToArray(val);
            }
            input_str = ShahenStrings.toBooleanString(inputs);
            ll.log.config("Using Input Values: " + input_str);
        }

        Boolean[] key = null;
        String key_str = "none";
        if (cmd.hasOption(LL_OptionString.KEYVALUES.toString())) {
            String val = cmd.getOptionValue(LL_OptionString.KEYVALUES.toString());
            if (val.equals("random")) {
                key = circuit.getRandomKey();
            } else {
                key = ShahenStrings.booleanStringToArray(val);
            }
            key_str = ShahenStrings.toBooleanString(key);
            ll.log.config("Using Key Values: " + key_str);
        }

        sc = new LL_SimplifiedCircuit(circuit, inputs, key);

        String filename = "logs/simplify_circuit_" + circuit.filename_noext + ".bench";
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write("# Simplified Circuit from File: " + circuit.filename + "\n");
                writer.write("# Embedding Input: " + input_str + "\n");
                writer.write("# Embedding Key  : " + key_str + "\n");
                writer.write(sc.getBenchString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out Bench String to file: " + filename + "\n\n");
            System.out.println(sc.getBenchString());
        }

        /* Timing */ll.timing.stopTimer(timerName + "-simplify");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_Simplify();
    }

    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
