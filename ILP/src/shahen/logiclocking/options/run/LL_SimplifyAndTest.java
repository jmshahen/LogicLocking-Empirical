package shahen.logiclocking.options.run;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.LL_SimplifiedCircuit;
import shahen.logiclocking.options.LL_OptionFunctions;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_SimplifyAndTest implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.config("[ACTION: Simplify and Test] ");
        /* Timing */ll.timing.startTimer(timerName + "-simplify_test");

        LL_Simplify s = new LL_Simplify();
        s.run(ll, circuit, timerName, cmd);
        LL_SimplifiedCircuit sc = s.sc;

        Boolean[] inputs = sc.forced_inputs;
        Boolean[] key_inputs = sc.forced_key;

        if (inputs == null) inputs = sc.getZeroInputs();
        if (key_inputs == null) key_inputs = sc.getZeroKey();

        boolean my_debug = cmd.hasOption(LL_OptionString.DEBUG.toString());
        StringBuilder csv_output = new StringBuilder();

        if (my_debug) {
            csv_output.append("Key,Input,Orig Output,Simplified Output,Result\n");
        }

        Integer max_tests = LL_OptionFunctions.getIntFromOptions(cmd, LL_OptionString.NUM_INPUTS.toString(), null);

        // Perform tests
        String output_orig;
        String output_simplified;
        boolean are_equal;
        int num_tests = 0;
        int num_errors = 0;
        boolean result;

        while (true) {
            num_tests++;

            if (my_debug && num_tests % 50 == 0) System.out.print(".");
            if (max_tests != null && num_tests > max_tests) break;

            if (circuit.get_output(inputs, key_inputs) == false) {
                ll.log.severe("An Error occurred when trying to get the output of the circuit.");
                return;
            }
            output_orig = ShahenStrings.toBooleanString(circuit.getOutputValues());

            if (sc.inputSize() == 0) {
                result = sc.get_output(new Boolean[0], key_inputs);
            } else {
                result = sc.get_output(inputs, new Boolean[0]);
            }

            if (result == false) {
                ll.log.severe("An Error occurred when trying to get the output of the simplified circuit.");
                return;
            }
            output_simplified = ShahenStrings.toBooleanString(sc.getOutputValues());

            are_equal = output_orig.equals(output_simplified);

            if (!are_equal) num_errors++;

            if (my_debug) {
                csv_output.append(ShahenStrings.toBooleanString(key_inputs)).append(",");
                csv_output.append(ShahenStrings.toBooleanString(inputs)).append(",");
                csv_output.append(output_orig).append(",");
                csv_output.append(output_simplified).append(",");
                csv_output.append(are_equal ? "Equal" : "ERROR").append("\n");
            }

            if (sc.inputSize() == 0) {
                key_inputs = LL_ILPInstance.getNextInput(key_inputs);
                if (key_inputs == null) break;
            } else {
                inputs = LL_ILPInstance.getNextInput(inputs);
                if (inputs == null) break;
            }
        }
        if (my_debug) System.out.println("");

        ll.log.info("Ran " + num_tests + " tests, with " + num_errors + " errors");
        // Write out test results if DEBUG is present
        if (my_debug) {
            String filename = "logs/simplify_test_circuit_" + circuit.filename_noext + "_results.csv";
            try {
                BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
                if (writer != null) {
                    writer.write(csv_output.toString());
                    writer.close();
                }
            } catch (Exception e) {
                System.out.println("Unable to write out CSV Results to file: " + filename + "\n\n");
                System.out.println(sc.getBenchString());
            }
        }

        /* Timing */ll.timing.stopTimer(timerName + "-simplify_test");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_SimplifyAndTest();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
