package shahen.logiclocking.options.run;

import java.io.File;
import java.io.FileWriter;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_SliceOutputWire implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        /* Timing */ll.timing.startTimer(timerName + "-slice_output");
        ll.log.info("[ACTION: SLICE CIRCUIT OUTPUT WIRE] ");

        if (!cmd.hasOption(LL_OptionString.WIRE.toString())) {
            throw new IloException(
                    "Must provide the wire name using the " + LL_OptionString.WIRE.toString() + " option");
        }

        StringBuilder sb = new StringBuilder();
        String wire = cmd.getOptionValue(LL_OptionString.WIRE.toString());
        File f = new File(circuit.filename);
        String str = f.getName();
        str = str.substring(0, str.lastIndexOf('.')); // remove the file extension
        String filename = "logs/sliced_circuits/";
        f = new File(filename);
        f.mkdirs();
        filename += str + "_" + wire + ".bench";

        LL_Circuit sliced = circuit.slice(wire, true);

        try {
            FileWriter fw = new FileWriter(filename);
            fw.write(" # key=" + ShahenStrings.toBooleanString(circuit.known_key_list) + "\n");
            fw.write(sliced.getBenchString());
            fw.close();
            ll.log.info("Wrote sliced file to: " + filename);
        } catch (Exception e) {
            ll.log.warning("Unable to write to file: " + filename);
            System.out.println(sb.toString());
        }
        /* Timing */ll.timing.stopTimer(timerName + "-slice_output");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_SliceOutputWire();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
