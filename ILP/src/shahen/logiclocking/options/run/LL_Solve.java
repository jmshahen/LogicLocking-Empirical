package shahen.logiclocking.options.run;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.solver.*;
import shahen.string.ShahenStrings;

public class LL_Solve implements LL_RunOptionI {
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.config("[ACTION: SOLVE] ");

        LL_SolverI cp = null;
        LL_SolverOptions options = new LL_SolverOptions();
        options.args = ll.orig_args;
        ll.set_settings(options, circuit, ll.reduction, timerName);

        switch (ll.solver_name) {
            case "cplex_v1" :
                ll.log.config("[SETTING] Using CPLEX v1 Solver");
                cp = new LL_CPLEX_Solver_v1(options);
                break;
            case "cplex_v2" :
                ll.log.config("[SETTING] Using CPLEX v2 Solver");
                cp = new LL_CPLEX_Solver_v2(options);
                break;
            case "cplex_v3" :
                ll.log.config("[SETTING] Using CPLEX v3 Solver");
                cp = new LL_CPLEX_Solver_v3(options);
                break;
            case "gurobi_v1" :
                ll.log.config("[SETTING] Using GUROBI v1 Solver");
                cp = new LL_GUROBI_Solver(options);
                break;
            default :
                ll.log.severe("[SETTING] UNKOWN Solver Chosen: " + ll.solver_name);
                throw new IloException("UNKOWN Solver Chosen: " + ll.solver_name);
        }

        ll.timing.startTimer("quickTiming");
        int result;
        String resultComments = "";
        try {
            result = cp.solve();
        } catch (Exception e) {
            result = -2;
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            ll.log.log(Level.SEVERE, "Error in LL_SolverI", e);
            resultComments = errors.toString();
        }
        ll.timing.stopTimer("quickTiming");
        String duration_str = ShahenStrings.prettyDuration(ll.timing.getLastElapsedTime());

        StringBuilder sb = new StringBuilder();
        switch (result) {
            case 1 :
                resultComments = ShahenStrings.toBooleanString(cp.get_found_key());
                sb.append("Result  : Key Found").append("\n");
                sb.append("File    : " + circuit.filepath).append("\n");
                sb.append("Duration: " + duration_str).append("\n");
                sb.append("Key        : " + resultComments).append("\n");
                sb.append("Correct Key: " + ShahenStrings.toBooleanString(circuit.getActualKey()));
                resultComments = "Key Found: " + resultComments;
                break;
            case 0 :
                resultComments = "No Key Found";
                sb.append("Result  : " + resultComments).append("\n");
                sb.append("Duration: " + duration_str).append("\n");
                break;
            case -1 :
                resultComments = "Timeout";
                sb.append("Result  : " + resultComments).append("\n");
                sb.append("Duration: " + duration_str).append("\n");
                break;
            default :
                sb.append("Result  : Unkown Return code: " + result).append("\n");
                sb.append("Duration: " + duration_str).append("\n");
                break;
        }
        ll.log.info("\n" + sb.toString());
        // System.out.println(sb.toString());

        ll.results.addQuickResult(circuit.filepath, circuit, result, options.iter_count,
                (ll.solver_algorithm != null) ? ll.solver_algorithm : 0, ll.timing.getTimerElapsedTime("quickTiming"),
                resultComments, ll.orig_args);
        cp.destroy();
        cp = null;
    }
    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_Solve();
    }
    @Override
    public boolean use_garbage_collector() {
        return true;
    }

}
