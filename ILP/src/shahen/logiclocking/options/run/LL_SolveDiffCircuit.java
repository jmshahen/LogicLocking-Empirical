package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_DiffCircuit;
import shahen.logiclocking.solver.LL_CPLEX_Solver_v1;
import shahen.logiclocking.solver.LL_SolverOptions;
import shahen.string.ShahenStrings;

public class LL_SolveDiffCircuit implements LL_RunOptionI {

    public String filename_ilp = "logs/diff_random.ilp";
    public String filename_lp = "logs/diff_random.lp";

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.info("[ACTION: SOLVE Diff Circuit] ");
        /* Timing */ll.timing.startTimer(timerName + "-solve_diff_circuit");

        LL_SolverOptions options = new LL_SolverOptions();
        options.args = ll.orig_args;
        ll.set_settings(options, circuit, ll.reduction, timerName);

        LL_CPLEX_Solver_v1 cp = new LL_CPLEX_Solver_v1(options);
        LL_CPLEX_DiffCircuit diff = new LL_CPLEX_DiffCircuit(ll.log, circuit, "c1", "c2");

        diff.addToCplex(ll.reduction, cp.cplex);

        if (cp.cplex.solve()) {
            ll.log.info("Key_a    : " + ShahenStrings.toBooleanString(diff.circuita.getKeyVarBoolArray(cp.cplex)));
            ll.log.info("Key_b    : " + ShahenStrings.toBooleanString(diff.circuitb.getKeyVarBoolArray(cp.cplex)));
            ll.log.info("Outputs_a: " + ShahenStrings.toBooleanString(diff.circuita.getOutputVarBoolArray(cp.cplex)));
            ll.log.info("Outputs_b: " + ShahenStrings.toBooleanString(diff.circuitb.getOutputVarBoolArray(cp.cplex)));
            ll.log.info("Inputs   : " + ShahenStrings.toBooleanString(diff.circuita.getInputVarBoolArray(cp.cplex)));
        } else {
            ll.log.severe("Unable to solve the ILP model! Writing to: " + filename_ilp + " and " + filename_lp);
            cp.cplex.exportModel(filename_lp);
            diff.exportModel(ll.reduction, filename_ilp, true);
        }

        /* Timing */ll.timing.stopTimer(timerName + "-solve_diff_circuit");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_SolveDiffCircuit();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
