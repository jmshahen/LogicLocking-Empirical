package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_EqualCircuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.logiclocking.solver.LL_CPLEX_Solver_v1;
import shahen.logiclocking.solver.LL_SolverOptions;
import shahen.string.ShahenStrings;

public class LL_SolveEqualCircuit implements LL_RunOptionI {

    public String timer_key = "-solve_equal_random_circuit";
    public Boolean[] inputs = null;
    public String filename_ilp = "logs/equal_random.ilp";
    public String filename_lp = "logs/equal_random.lp";

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.info("[ACTION: SOLVE EQUAL CIRCUIT USING INPUT] ");
        /* Timing */ll.timing.startTimer(timerName + timer_key);

        LL_SolverOptions options = new LL_SolverOptions();
        options.args = ll.orig_args;
        ll.set_settings(options, circuit, ll.reduction, timerName);

        LL_CPLEX_Solver_v1 cp = new LL_CPLEX_Solver_v1(options);

        if (!ll.debug) {
            cp.quiet();
        }

        if (inputs == null)
            inputs = LL_ILPInstance.get_boolean_string_option(cmd, LL_OptionString.INPUTVALUES.toString(),
                    circuit.getRandomInputs());

        LL_CPLEX_EqualCircuit equal = new LL_CPLEX_EqualCircuit(ll.log, circuit, inputs, null, null, "ce1", "ce2");
        ll.log.info("Inputs         : " + ShahenStrings.toBooleanString(inputs));
        ll.log.info("Actual Key     : " + ShahenStrings.toBooleanString(circuit.getActualKey()));
        ll.log.info("Correct Outputs: " + ShahenStrings.toBooleanString(equal.correct_outputs));

        equal.addToCplex(ll.reduction, cp.cplex, false);

        if (ll.debug) {
            equal.exportModel(ll.reduction, "logs/equal_input.ilp", true, true);
            cp.cplex.exportModel("logs/equal_input.lp");
        }

        if (cp.cplex.solve()) {
            ll.log.info("Key    : " + ShahenStrings.toBooleanString(equal.circuita.getKeyVarBoolArray(cp.cplex)));
            ll.log.info("Outputs: " + ShahenStrings.toBooleanString(equal.circuita.getOutputVarBoolArray(cp.cplex)));
            ll.log.info("Inputs : " + ShahenStrings.toBooleanString(equal.circuita.getInputVarBoolArray(cp.cplex)));
        } else {
            ll.log.severe("Unable to solve the ILP model! Writing to: " + filename_ilp + " and " + filename_lp);
            cp.cplex.exportModel(filename_lp);
            equal.exportModel(ll.reduction, filename_ilp, true, true);
        }

        /* Timing */ll.timing.stopTimer(timerName + timer_key);
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_SolveEqualCircuit();
    }

    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
