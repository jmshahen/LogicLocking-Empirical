package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;

public class LL_SolveEqualCircuit_OnesInput extends LL_SolveEqualCircuit implements LL_RunOptionI {
    public String timer_key = "-solve_equal_ones_circuit";
    public Boolean[] inputs = null;
    public String filename_ilp = "logs/equal_ones.ilp";
    public String filename_lp = "logs/equal_ones.lp";

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        inputs = circuit.getOneInputs();
        super.run(ll, circuit, timerName, cmd);
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_SolveEqualCircuit_OnesInput();
    }
}
