package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.solver.LL_CPLEX_Solver_v1;
import shahen.logiclocking.solver.LL_SolverOptions;

public class LL_SolveILPCircuit implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.info("[ACTION: SOLVE SINGLE CIRCUIT] ");
        /* Timing */ll.timing.startTimer(timerName + "-solve_single_circuit");

        LL_SolverOptions options = new LL_SolverOptions();
        options.args = ll.orig_args;
        ll.set_settings(options, circuit, ll.reduction, timerName);

        LL_CPLEX_Solver_v1 cp = new LL_CPLEX_Solver_v1(options);
        LL_CPLEX_Circuit single = new LL_CPLEX_Circuit(circuit, "c1");
        single.addToCplex(ll.reduction, cp.cplex, null, null);

        if (cp.cplex.solve()) {
            ll.log.info("Key    : " + LL_CPLEX_Circuit.getVarString(cp.cplex, single.key));
            ll.log.info("Outputs: " + LL_CPLEX_Circuit.getVarString(cp.cplex, single.outputs));
            ll.log.info("Inputs : " + LL_CPLEX_Circuit.getVarString(cp.cplex, single.inputs));
        } else {
            ll.log.severe("Unable to solve the ILP model!");
        }

        /* Timing */ll.timing.stopTimer(timerName + "-solve_single_circuit");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_SolveILPCircuit();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
