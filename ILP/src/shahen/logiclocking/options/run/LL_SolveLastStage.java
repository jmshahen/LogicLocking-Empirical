package shahen.logiclocking.options.run;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.solver.LL_CPLEX_Solver_v1;
import shahen.logiclocking.solver.LL_SolverOptions;
import shahen.string.ShahenStrings;

public class LL_SolveLastStage implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        /* Timing */ll.timing.startTimer(timerName + "-solve_last");
        ll.log.config("[ACTION: SOLVE_LAST] ");

        LL_SolverOptions options = new LL_SolverOptions();
        options.args = ll.orig_args;
        ll.set_settings(options, circuit, ll.reduction, timerName);
        LL_CPLEX_Solver_v1 cp = new LL_CPLEX_Solver_v1(options);

        ll.timing.startTimer("quickTiming");
        int result;
        String resultComments = "";
        try {
            cp.preload();
            result = cp.last_stage(0);
        } catch (Exception e) {
            result = -2;
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            ll.log.severe(errors.toString());
            ll.log.severe(e.getMessage());
            resultComments = errors.toString();
        }
        ll.timing.stopTimer("quickTiming");
        String duration_str = ShahenStrings.prettyDuration(ll.timing.getLastElapsedTime());

        switch (result) {
            case 1 :
                resultComments = ShahenStrings.toBooleanString(cp.get_key());
                ll.log.info("Result  : Key Found (SOLVE LAST)");
                ll.log.info("File    : " + circuit.filename);
                ll.log.info("Duration: " + duration_str);
                ll.log.info("Key        : " + resultComments);
                ll.log.info("Correct Key: " + ShahenStrings.toBooleanString(circuit.getActualKey()));
                resultComments = "Key Found: " + resultComments;
                break;
            case 0 :
                resultComments = "No Key Found";
                ll.log.info("Result  : " + resultComments);
                ll.log.info("Duration: " + duration_str);
                break;
            case -1 :
                resultComments = "Timeout";
                ll.log.info("Result  : " + resultComments);
                ll.log.info("Duration: " + duration_str);
                break;
            default :
                ll.log.info("Result  : Unkown Return code: " + result);
                ll.log.info("Duration: " + duration_str);
                break;
        }
        ll.results.addQuickResult(circuit.filename, circuit, result, options.iter_count, ll.solver_algorithm,
                ll.timing.getTimerElapsedTime("quickTiming"), resultComments, cmd.getArgs());
        /* Timing */ll.timing.stopTimer(timerName + "-solve_last");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_SolveLastStage();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
