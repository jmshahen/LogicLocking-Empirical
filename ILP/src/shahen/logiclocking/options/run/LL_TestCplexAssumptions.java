package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.opl.IloCplex;
import ilog.opl.IloCplex.CplexStatus;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.string.ShahenStrings;

public class LL_TestCplexAssumptions implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        String timer_key = timerName + ".assumption_cplex_test";
        /*TIMING*/ll.timing.startTimer(timer_key);

        /*TIMING*/ll.timing.startTimer(timer_key + ".creating_model()");

        IloCplex cplex = new IloCplex();
        LL_CPLEX_Circuit c_circuit = new LL_CPLEX_Circuit(circuit, "");

        c_circuit.addToCplex(ll.reduction, cplex, null, null);

        /*TIMING*/ll.timing.stopTimer(timer_key + ".creating_model()");
        ll.log.info("Took " + ll.timing.getLastElapsedTime() + "ms to build the model");

        /*TIMING*/ll.timing.startTimer(timer_key + ".first_solve()");
        boolean result = cplex.solve();
        /*TIMING*/ll.timing.stopTimer(timer_key + ".first_solve()");
        long first_solve_time = ll.timing.getLastElapsedTime();

        CplexStatus status = cplex.getCplexStatus();
        ll.log.info("[CPLEX STATUS] First Solve Status: " + status);
        if (!result) {
            ll.log.warning("Unable to SOLVE Circuit, test cannot be continued further!");
            /*TIMING*/ll.timing.stopTimer(timer_key);
            return;
        }

        String key_str1 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.key));
        String input_str1 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.inputs));
        String output_str1 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.outputs));

        ll.log.info("Solved Circuit in " + first_solve_time + "ms.\n" +//
                "key   =" + key_str1 + "\n" +//
                "input =" + input_str1 + "\n" +//
                "output=" + output_str1);

        ll.log.info("Updating key variables to be all 1s");

        for (IloIntVar i : c_circuit.key) {
            i.setLB(1);
            i.setUB(1);
        }

        /*TIMING*/ll.timing.startTimer(timer_key + ".second_solve()");
        result = cplex.solve();
        /*TIMING*/ll.timing.stopTimer(timer_key + ".second_solve()");
        long second_solve_time = ll.timing.getLastElapsedTime();

        status = cplex.getCplexStatus();
        ll.log.info("[CPLEX STATUS] Second Solve Status: " + status);

        if (!result) {
            ll.log.warning("Unable to SOLVE Circuit, test cannot be continued further!");
            /*TIMING*/ll.timing.stopTimer(timer_key);
            return;
        }
        String key_str2 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.key));
        String input_str2 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.inputs));
        String output_str2 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.outputs));

        ll.log.info("Solved Circuit in " + second_solve_time + "ms.\n" +//
                "test1   = No Assumptions\n" +//
                "test2   = Key set to all 1s\n" +//
                "test3   = Key set to all 0s, and input set to all 1s\n" +//
                "key1    = " + key_str1 + "\n" +//
                "key2    = " + key_str2 + "\n" +//
                "input1  = " + input_str1 + "\n" +//
                "input2  = " + input_str2 + "\n" +//
                "output1 = " + output_str1 + "\n" +//
                "output2 = " + output_str2);

        ll.log.info("Updating key variables to be all 0s");

        for (IloIntVar i : c_circuit.key) {
            i.setLB(0);
            i.setUB(0);
        }
        for (IloIntVar i : c_circuit.inputs) {
            i.setLB(1);
            i.setUB(1);
        }

        /*TIMING*/ll.timing.startTimer(timer_key + ".third_solve()");
        result = cplex.solve();
        /*TIMING*/ll.timing.stopTimer(timer_key + ".third_solve()");
        long third_solve_time = ll.timing.getLastElapsedTime();

        status = cplex.getCplexStatus();
        ll.log.info("[CPLEX STATUS] Third Solve Status: " + status);

        if (!result) {
            ll.log.warning("Unable to SOLVE Circuit, test cannot be continued further!");
            /*TIMING*/ll.timing.stopTimer(timer_key);
            return;
        }
        String key_str3 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.key));
        String input_str3 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.inputs));
        String output_str3 = ShahenStrings.toBooleanString(LL_CPLEX_Circuit.getVarBoolArray(cplex, c_circuit.outputs));

        ll.log.info("Solved Circuit in " + third_solve_time + "ms.\n" +//
                "test1   = No Assumptions\n" +//
                "test2   = Key set to all 1s\n" +//
                "test3   = Key set to all 0s, and input set to all 1s\n" +//
                "key1   =" + key_str1 + "\n" +//
                "key2   =" + key_str2 + "\n" +//
                "key3   =" + key_str3 + "\n" +//
                "input1 =" + input_str1 + "\n" +//
                "input2 =" + input_str2 + "\n" +//
                "input3 =" + input_str3 + "\n" +//
                "output1=" + output_str1 + "\n" +//
                "output2=" + output_str2 + "\n" +//
                "output3=" + output_str3);

        /*TIMING*/ll.timing.stopTimer(timer_key);
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_TestCplexAssumptions();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
