package shahen.logiclocking.options.run;

import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.opl.IloCplex;
import ilog.opl.IloCplex.CplexStatus;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_EqualCircuit;
import shahen.logiclocking.options.LL_OptionFunctions;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_TestCplexIncrementalMode implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        String timer_key = timerName + ".incremental_cplex_test";
        /*TIMING*/ll.timing.startTimer(timer_key);

        IloCplex cplex = new IloCplex();
        ArrayList<LL_CPLEX_EqualCircuit> equal_circuits = new ArrayList<>();

        Integer preload_num = LL_OptionFunctions.getIntFromOptions(cmd, LL_OptionString.PRELOAD.toString(), 10);
        if (preload_num != null && preload_num < 0) {
            preload_num = 10;
        }

        // Preload Equal Circuits
        /*TIMING*/ll.timing.startTimer(timer_key + ".creating_model()");
        IloIntVar[] key = cplex.boolVarArray(circuit.keySize());
        for (int p = 0; p < preload_num; p++) {
            LL_CPLEX_EqualCircuit equal = new LL_CPLEX_EqualCircuit(//
                    ll.log,//
                    circuit, //
                    circuit.getRandomInputs(), //
                    key, //
                    "ce_preload_" + p //
            );

            equal_circuits.add(equal);
            equal.addToCplex(ll.reduction, cplex, false);
        }
        /*TIMING*/ll.timing.stopTimer(timer_key + ".creating_model()");
        ll.log.info("Took " + ll.timing.getLastElapsedTime() + "ms to build the model");

        /*TIMING*/ll.timing.startTimer(timer_key + ".first_solve()");
        boolean result = cplex.solve();
        /*TIMING*/ll.timing.stopTimer(timer_key + ".first_solve()");

        CplexStatus status = cplex.getCplexStatus();
        ll.log.info("[CPLEX STATUS] First Solve Status: " + status);

        if (result) {
            Boolean[] key_value = LL_CPLEX_Circuit.getVarBoolArray(cplex, key);
            long first_solve_time = ll.timing.getLastElapsedTime();
            ll.log.info("Found key in " + first_solve_time + "ms. key=" + ShahenStrings.toBooleanString(key_value));

            ll.log.info("Adding simple constraint that the first bit of the key must equal the previously found key.");
            cplex.addEq(key[0], key_value[0] ? 1 : 0);

            /*TIMING*/ll.timing.startTimer(timer_key + ".second_solve()");
            result = cplex.solve();
            /*TIMING*/ll.timing.stopTimer(timer_key + ".second_solve()");

            status = cplex.getCplexStatus();
            ll.log.info("[CPLEX STATUS] Second Solve Status: " + status);

            if (result) {
                Boolean[] key_value_2 = LL_CPLEX_Circuit.getVarBoolArray(cplex, key);
                long second_solve_time = ll.timing.getLastElapsedTime();
                ll.log.info(
                        "Found key1 in " + first_solve_time + "ms. key=" + ShahenStrings.toBooleanString(key_value));
                ll.log.info(
                        "Found key2 in " + second_solve_time + "ms. key=" + ShahenStrings.toBooleanString(key_value_2));
            } else {
                ll.log.warning("Unable to perform the second solve!");
            }
        } else {
            ll.log.warning("Unable to solve simple equal circuits for the locked circuit! Path: " + circuit.filename);
        }
        /*TIMING*/ll.timing.stopTimer(timer_key);
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_TestCplexIncrementalMode();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
