package shahen.logiclocking.options.run;

import java.io.FileWriter;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;

public class LL_ToILP implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.config("[ACTION: TO ILP] ");
        /* Timing */ll.timing.startTimer(timerName + "-to_ilp");

        String ilpCircuit = circuit.getSingleILPString(ll.reduction, "c1", true, true, true);

        try {
            FileWriter fw = new FileWriter(ll.toIlpFile);
            fw.write(ilpCircuit);
            fw.close();
            ll.log.config("Written ILP to file: " + ll.toIlpFile.getAbsolutePath());
        } catch (Exception e) {
            ll.log.warning("Unable to write ILP to file: " + ll.toIlpFile.getAbsolutePath());
            ll.log.info("Written ILP to STDOUT");
            System.out.println(ilpCircuit);
        }

        /* Timing */ll.timing.stopTimer(timerName + "-to_ilp");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_ToILP();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
