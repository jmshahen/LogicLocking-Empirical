package shahen.logiclocking.options.run;

import java.io.FileWriter;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_ToILPWithInputKey implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.config("[ACTION: TO ILP WITH INPUT/KEY] ");
        /* Timing */ll.timing.startTimer(timerName + "-to_ilp_with_inputkey");

        Boolean[] inputs = LL_ILPInstance.get_boolean_string_option(cmd, LL_OptionString.INPUTVALUES.toString(),
                circuit.getRandomInputs());
        ll.log.config("Using Input Values: " + ShahenStrings.toBooleanString(inputs));

        Boolean[] key_inputs = LL_ILPInstance.get_boolean_string_option(cmd, LL_OptionString.KEYVALUES.toString(),
                circuit.getActualKey());
        ll.log.config("Using Key Input Values: " + ShahenStrings.toBooleanString(key_inputs));

        LL_CPLEX_Circuit single_circuit = new LL_CPLEX_Circuit(circuit, "");

        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();
        single_circuit.addToILPFile(ll.reduction, constraints, variables, inputs, key_inputs, false, true);

        circuit.get_output(Arrays.asList(inputs), Arrays.asList(key_inputs));
        String actual_output = ShahenStrings.toBooleanStringCPLEXFormat(circuit.getOutputValues());

        StringBuilder sb = new StringBuilder();
        sb.append("// Output Bits: " + actual_output + "\n");
        sb.append(variables.toString());
        sb.append("\n\nsubject to {\n");
        sb.append(constraints.toString());
        sb.append("\n}\nexecute{\n");
        sb.append("  writeln(\"Input        : \", INPUT);\n");
        sb.append("  writeln(\"Key          : \", KEY);\n");
        sb.append("  writeln(\"Found Output : \", OUTPUT);\n");
        sb.append("  writeln(\"Actual Output:  " + actual_output + "\");\n");
        sb.append("}");
        try {
            FileWriter fw = new FileWriter(ll.toIlpInput);
            fw.write(sb.toString());
            fw.close();
        } catch (Exception e) {
            ll.log.warning("Unable to write to file: " + ll.toIlpInput.getAbsolutePath());
            System.out.println(sb.toString());
        }
        /* Timing */ll.timing.stopTimer(timerName + "-to_ilp_with_inputkey");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_ToILPWithInputKey();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
