package shahen.logiclocking.options.run;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.solver.LL_CPLEX_Solver_v1;
import shahen.logiclocking.solver.LL_SolverOptions;

public class LL_ToLP implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.info("[ACTION: TO LP] ");
        /* Timing */ll.timing.startTimer(timerName + "-to_lp");

        LL_SolverOptions options = new LL_SolverOptions();
        options.args = ll.orig_args;
        ll.set_settings(options, circuit, ll.reduction, timerName);

        LL_CPLEX_Solver_v1 cp = new LL_CPLEX_Solver_v1(options);
        LL_CPLEX_Circuit single = new LL_CPLEX_Circuit(circuit, "c");
        single.addToCplex(ll.reduction, cp.cplex, null, null);

        String filename = "logs/circuit.lp";
        ll.log.info("Writing CPLEX lp Model to file: " + filename);
        cp.cplex.exportModel(filename);
        /* Timing */ll.timing.stopTimer(timerName + "-to_lp");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_ToLP();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
