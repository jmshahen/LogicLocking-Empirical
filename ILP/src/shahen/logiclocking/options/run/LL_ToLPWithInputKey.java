package shahen.logiclocking.options.run;

import java.io.FileWriter;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;

import gurobi.*;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.string.ShahenStrings;

public class LL_ToLPWithInputKey implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        // TODO complete this function to help with debugging issue #81
        ll.log.config("[ACTION: TO LP WITH KEY] ");
        /* Timing */ll.timing.startTimer(timerName + "-to_lp_with_inputkey");

        Boolean[] inputs = LL_ILPInstance.get_boolean_string_option(cmd, LL_OptionString.INPUTVALUES.toString(),
                circuit.getRandomInputs());
        ll.log.config("Using Input Values: " + ShahenStrings.toBooleanString(inputs));

        Boolean[] key_inputs = LL_ILPInstance.get_boolean_string_option(cmd, LL_OptionString.KEYVALUES.toString(),
                circuit.getActualKey());
        ll.log.config("Using Key Input Values: " + ShahenStrings.toBooleanString(key_inputs));

        if (ll.solver_name.contains("cplex")) {
            try {
                IloCplex cplex = new IloCplex();
                LL_CPLEX_Circuit single_circuit = new LL_CPLEX_Circuit(circuit, "");
                single_circuit.addToCplex(ll.reduction, cplex, inputs, null);

                for (int k = 0; k < key_inputs.length; k++) {
                    double b = (key_inputs[k]) ? 1.0 : 0.0;
                    single_circuit.key[k].setLB(b);
                    single_circuit.key[k].setUB(b);
                }

                cplex.exportModel(ll.toLpInput.getAbsolutePath());
            } catch (IloException e) {
                e.printStackTrace();
            }
        } else if (ll.solver_name.contains("gurobi")) {
            try {
                GRBEnv env = new GRBEnv();
                GRBModel gurobi = new GRBModel(env);
                LL_GUROBI_Circuit single_circuit = new LL_GUROBI_Circuit(circuit, "");
                single_circuit.addToGurobi(ll.reduction, gurobi, inputs, null);

                for (int k = 0; k < key_inputs.length; k++) {
                    double b = (key_inputs[k]) ? 1.0 : 0.0;
                    single_circuit.key[k].set(GRB.DoubleAttr.LB, b);
                    single_circuit.key[k].set(GRB.DoubleAttr.UB, b);
                }

                gurobi.write(ll.toLpInput.getAbsolutePath());
            } catch (GRBException e) {
                e.printStackTrace();
            }
        }

        circuit.get_output(Arrays.asList(inputs), Arrays.asList(key_inputs));
        String actual_output = ShahenStrings.toBooleanString(circuit.getOutputValues());
        ll.log.info("Expected Output Values: " + actual_output);

        try {
            FileWriter fw = new FileWriter(ll.toLpInput, true);
            fw.write("\\ Using Input Values    : " + ShahenStrings.toBooleanString(inputs) + "\n");
            fw.write("\\ Using Key Input Values: " + ShahenStrings.toBooleanString(key_inputs) + "\n");
            fw.write("\\ Expected Output Values: " + actual_output + "\n");
            fw.close();
        } catch (Exception e) {
            ll.log.warning("Unable to write to file: " + ll.toLpInput.getAbsolutePath());
        }

        /* Timing */ll.timing.stopTimer(timerName + "-to_lp_with_inputkey");
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_ToLPWithInputKey();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
