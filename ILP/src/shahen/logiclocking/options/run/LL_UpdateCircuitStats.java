package shahen.logiclocking.options.run;

import java.sql.SQLException;
import java.util.logging.Level;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.results.LL_Circuit_Stats;

public class LL_UpdateCircuitStats implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        ll.log.fine("[ACTION: Update Circuit Stats] ");
        timerName += "-updateCircuitStats";
        /* Timing */ll.timing.startTimer(timerName);

        try {
            LL_Circuit_Stats cs = new LL_Circuit_Stats(circuit, ll.results.sqlh.conn, false);
            cs.save(ll.results.sqlh.conn);
            ll.results.summary.append(" * ").append(cs.filepath)
                    .append(": [Success] Updated the Circuits stats for this file.\n");
        } catch (SQLException e) {
            ll.log.log(Level.SEVERE, "", e);
            ll.results.summary.append(" * ").append(circuit.filepath)
                    .append(": [Failed] Unable to updated the Circuits stats for this file.\n");
        }

        /* Timing */ll.timing.stopTimer(timerName);
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_UpdateCircuitStats();
    }
    @Override
    public boolean use_garbage_collector() {
        return false;
    }

}
