package shahen.logiclocking.options.run;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;

import gurobi.GRBException;
import ilog.concert.IloException;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.options.LL_OptionString;
import shahen.logiclocking.parser.LL_AutoGen_CircuitParser;
import shahen.string.ShahenStrings;

public class LL_VerifyKey implements LL_RunOptionI {

    @Override
    public void run(LL_ILPInstance ll, LL_Circuit circuit, String timerName, CommandLine cmd)
            throws IloException, GRBException {
        //////////////////////////////////////////////////////////////////////////////
        // READ IN NUMBER OF INPUTS (OPTIONAL)
        Integer num_inputs = 100000;
        if (cmd.hasOption(LL_OptionString.NUM_INPUTS.toString())) {
            try {
                num_inputs = Integer.parseInt(cmd.getOptionValue(LL_OptionString.NUM_INPUTS.toString()));
            } catch (Exception e) {
                ll.log.warning("[Verify] Unable to read the number of inputs as an integer. Value received: "
                        + cmd.getOptionValue(LL_OptionString.NUM_INPUTS.toString()));
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        // READ IN KEY VALUE (OPTIONAL)
        ArrayList<Boolean> key_inputs = circuit.known_key_list;
        if (cmd.hasOption(LL_OptionString.KEYVALUES.toString())) {
            key_inputs = ShahenStrings
                    .booleanStringToArrayList(cmd.getOptionValue(LL_OptionString.KEYVALUES.toString()));
        }

        if (key_inputs == null) {
            ll.log.severe("[Verify] Unable to verify the key because we either do not have a key supplied "
                    + "in the locked file or the key supplied was malformed.");
            return;
        }
        ll.log.info("[Verfiy] Using key: " + ShahenStrings.toBooleanString(key_inputs));

        //////////////////////////////////////////////////////////////////////////////
        // READ IN UNLOCKED FILE (REQUIRED)
        if (!cmd.hasOption(LL_OptionString.UNLOCKED.toString())) {
            ll.log.severe("[Verify] You must supply the unlocked file for comparison.");
            return;
        }

        File unlocked_file = new File(cmd.getOptionValue(LL_OptionString.UNLOCKED.toString()));

        if (!unlocked_file.exists() || !unlocked_file.isFile()) {
            ll.log.severe("[Verify] Unlocked file must exist and must be a file.");
            return;
        }

        //////////////////////////////////////////////////////////////////////////////
        // PARSE UNLOCKED CIRCUIT
        /* Timing */ll.timing.startTimer(timerName + "-parseUnlockedFile");
        LL_AutoGen_CircuitParser parser;
        try {
            parser = ll.parserHelper.parseFile(unlocked_file);
        } catch (IOException e) {
            e.printStackTrace();
            ll.log.severe("[PARSING] ERROR: Skipping this file due to a parsing error");
            return;
        }
        /* Timing */ll.timing.stopTimer(timerName + "-parseUnlockedFile");

        if (ll.parserHelper.error.errorFound) {
            ll.log.severe("[PARSING] ERROR: Skipping this file due to a parsing error");
            return;
        } else {
            ll.log.fine("[PARSING] No errors found while parsing file, continuing on to converting");
        }

        LL_Circuit unlocked = parser.circuit;
        unlocked.filename = unlocked_file.getAbsolutePath();

        //////////////////////////////////////////////////////////////////////////////
        // SANITY CHECKS: NUMBER OF INPUTS AND OUTPUTS ARE THE SAME
        if (unlocked.keySize() != 0) {
            ll.log.severe("[Verify] Unlocked file is not allowed to have key bits.");
            return;
        }
        if (unlocked.normal_input_order.size() != circuit.normal_input_order.size()) {
            ll.log.severe("[Verify] Unlocked file does not have the same number of input bits.");
            return;
        }
        if (unlocked.output_order.size() != circuit.output_order.size()) {
            ll.log.severe("[Verify] Unlocked file does not have the same number of output bits.");
            return;
        }
        for (String wire : unlocked.normal_input_order) {
            if (!circuit.normal_inputs.containsKey(wire)) {
                ll.log.severe("[Verify] Input wire \"" + wire + "\" not found in locked file.");
                return;
            }
        }
        for (String wire : unlocked.output_order) {
            if (!circuit.outputs.containsKey(wire)) {
                ll.log.severe("[Verify] Output wire \"" + wire + "\" not found in locked file.");
                return;
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        // TEST RANDOM INPUTS
        int incorrect_count = 0;
        long seed = System.currentTimeMillis();
        for (int i = 0; i < num_inputs; i++) {
            seed++;
            ArrayList<Boolean> random_input = circuit.getRandomInputList(seed);

            if (!circuit.get_output(random_input, key_inputs)) {
                ll.log.severe("[Verify] Unable to propagate the input value \""
                        + ShahenStrings.toBooleanString(random_input) + "\" and key value \""
                        + ShahenStrings.toBooleanString(key_inputs) + "\" through the locked circuit.");
                return;
            }
            if (!unlocked.get_output(random_input)) {
                ll.log.severe("[Verify] Unable to propagate the input value \""
                        + ShahenStrings.toBooleanString(random_input) + "\" through the unlocked circuit.");
                return;
            }

            String locked_output = circuit.getOutputWireString(circuit.getOutputWires(), false);
            String unlocked_output = unlocked.getOutputWireString(unlocked.getOutputWires(), false);

            if (!locked_output.equals(unlocked_output)) {
                if (ll.debug || incorrect_count < 1) {
                    ll.log.severe("[Verify] (" + (i + 1) + "/" + num_inputs + ") Not the same output for input value \""
                            + ShahenStrings.toBooleanString(random_input) + "\" and key value \""
                            + ShahenStrings.toBooleanString(key_inputs) + "\"; Locked circuit output \"" + locked_output
                            + "\"; Unlocked circuit output \"" + unlocked_output + "\"");
                }
                incorrect_count++;
            }
        }

        if (incorrect_count > 0) {
            ll.log.severe("[Verify] RESULT: Key with Locked Circuit DO NOT MATCH. Found " + incorrect_count
                    + " inputs out of " + num_inputs + " random inputs, where the circuit outputs did not match");
        } else {
            ll.log.info("[Verify] RESULT: Found only matching outputs for " + num_inputs + " random inputs");
        }
    }

    @Override
    public LL_RunOptionI getNewInstance() {
        return new LL_VerifyKey();
    }
    @Override
    public boolean use_garbage_collector() {
        return true;
    }

}
