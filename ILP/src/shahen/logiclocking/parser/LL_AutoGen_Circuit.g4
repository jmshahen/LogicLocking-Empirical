grammar LL_AutoGen_Circuit;

@header {
package shahen.logiclocking.parser;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.*;

import shahen.logiclocking.circuit.*;
import shahen.logiclocking.circuit.gates.*;
import shahen.string.ShahenStrings;
}

@members {
public final Logger log = Logger.getLogger("shahen");
int tabsize = 1;
/* Global States */
public LL_Circuit circuit = new LL_Circuit();


private void logmsg(String msg) {
	if (log.isLoggable(Level.FINEST)) {
		System.out.println(ShahenStrings.repeat("  ", tabsize) + msg);
	}
}
}

init: key? input+ output+ gate+;

name
	returns[String str]:
	n = NAME {
		$str = $n.text;
	};
key:
	KEY b = BINARY {
		circuit.known_key_list = ShahenStrings.booleanStringToArrayList($b.text);
		
		logmsg("Key: "+ShahenStrings.toBooleanString(circuit.known_key_list));
		// Fill the known key using the common naming convention
		for(int i=0; i < circuit.known_key_list.size(); i++) {
			circuit.known_key.put("keyinput" + i, circuit.known_key_list.get(i));
		}
};

input:
	INPUT '(' n = name ')' {
	logmsg("Input: "+$n.str);
	circuit.add_input_wire($n.str);
};
output:
	OUTPUT '(' n = name ')' {
	logmsg("Output: "+$n.str);
	circuit.add_output_wire($n.str);
};

gate:
	out = name '=' (
		GATE_NOT '(' in = name ')' {
			logmsg("GATE: "+$out.str+" = NOT "+$in.str);
			int err = circuit.add_not_gate($in.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_BUF '(' in = name ')' {
			logmsg("GATE: "+$out.str+" = BUF "+$in.str);
			int err = circuit.add_buf_gate($in.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_AND '(' a = name ',' b = name ')' {
			logmsg("GATE: "+$out.str+" = "+$a.str+" AND "+$b.str);
			int err = circuit.add_and_gate($a.str, $b.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_NAND '(' a = name ',' b = name ')' {
			logmsg("GATE: "+$out.str+" = "+$a.str+" NAND "+$b.str);
			int err = circuit.add_nand_gate($a.str, $b.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_OR '(' a = name ',' b = name ')' {
			logmsg("GATE: "+$out.str+" = "+$a.str+" OR "+$b.str);
			int err = circuit.add_or_gate($a.str, $b.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_NOR '(' a = name ',' b = name ')' {
			logmsg("GATE: "+$out.str+" = "+$a.str+" NOR "+$b.str);
			int err = circuit.add_nor_gate($a.str, $b.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_XOR '(' a = name ',' b = name ')' {
			logmsg("GATE: "+$out.str+" = "+$a.str+" XOR "+$b.str);
			int err = circuit.add_xor_gate($a.str, $b.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_XNOR '(' a = name ',' b = name ')' {
			logmsg("GATE: "+$out.str+" = "+$a.str+" XNOR "+$b.str);
			int err = circuit.add_xnor_gate($a.str, $b.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| GATE_MUX '(' s = name ',' a = name ',' b = name ')' {
			logmsg("GATE: "+$out.str+" = MUX ("+$a.str+", "+$b.str+"; "+$s.str+")");
			int err = circuit.add_mux_gate(new LL_Mux(), $a.str, $b.str, $s.str, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		/////////////////////////////////////////////////////////////////////////
		// Large Gates
		| {ArrayList<String> gate_inputs = new ArrayList<String>();} GATE_AND '(' a = name ',' b =
			name (',' c = name {gate_inputs.add($c.str);})+ ')' {
			gate_inputs.add($a.str);
			gate_inputs.add($b.str);
			logmsg("GATE: "+$out.str+" = LARGE_AND ("+gate_inputs+")");
			int err = circuit.add_large_and_gate(gate_inputs, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| {ArrayList<String> gate_inputs = new ArrayList<String>();} GATE_OR '(' a = name ',' b =
			name (',' c = name {gate_inputs.add($c.str);})+ ')' {
			gate_inputs.add($a.str);
			gate_inputs.add($b.str);
			logmsg("GATE: "+$out.str+" = LARGE_OR ("+gate_inputs+")");
			int err = circuit.add_large_or_gate(gate_inputs, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| {ArrayList<String> gate_inputs = new ArrayList<String>();} GATE_NAND '(' a = name ',' b =
			name (',' c = name {gate_inputs.add($c.str);})+ ')' {
			gate_inputs.add($a.str);
			gate_inputs.add($b.str);
			logmsg("GATE: "+$out.str+" = LARGE_NAND ("+gate_inputs+")");
			int err = circuit.add_large_nand_gate(gate_inputs, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
		| {ArrayList<String> gate_inputs = new ArrayList<String>();} GATE_NOR '(' a = name ',' b =
			name (',' c = name {gate_inputs.add($c.str);})+ ')' {
			gate_inputs.add($a.str);
			gate_inputs.add($b.str);
			logmsg("GATE: "+$out.str+" = LARGE_NOR ("+gate_inputs+")");
			int err = circuit.add_large_nor_gate(gate_inputs, $out.str);
			if(err != 0) {
				logmsg("[ERROR] Unable to add gate, error code: " +err);
			}
		}
	);

///////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////////////////////////////////////////////////// CAPITAL
// LEXER RULES MUST BE AT THE BOTTOM

/* Whitespace and Comments */
Whitespace: [ \t]+ -> skip;

Newline: ( '\r' '\n'? | '\n') -> skip;
Comment: '#' ~[\r\n]* -> skip;

KEY: 'key=';
INPUT: I N P U T;
OUTPUT: O U T P U T;
GATE_NOT: N O T;
GATE_BUF: B U F;
GATE_AND: A N D;
GATE_NAND: N A N D;
GATE_OR: O R;
GATE_NOR: N O R;
GATE_XOR: X O R;
GATE_XNOR: X N O R;
GATE_MUX: M U X;
BINARY: [01]+;
NAME: [a-zA-Z][0-9a-zA-Z$_]*;

fragment A: [aA]; // match either an 'a' or 'A'
fragment B: [bB];
fragment C: [cC];
fragment D: [dD];
fragment E: [eE];
fragment F: [fF];
fragment G: [gG];
fragment H: [hH];
fragment I: [iI];
fragment J: [jJ];
fragment K: [kK];
fragment L: [lL];
fragment M: [mM];
fragment N: [nN];
fragment O: [oO];
fragment P: [pP];
fragment Q: [qQ];
fragment R: [rR];
fragment S: [sS];
fragment T: [tT];
fragment U: [uU];
fragment V: [vV];
fragment W: [wW];
fragment X: [xX];
fragment Y: [yY];
fragment Z: [zZ];