package shahen.logiclocking.parser;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.antlr.v4.runtime.*;

public class LL_ParserHelper {
    public final static Logger logger = Logger.getLogger("shahen");
    public BooleanErrorListener error = new BooleanErrorListener();

    public LL_AutoGen_CircuitParser parseFile(File file) throws IOException {
        error.errorFound = false; // reset the error listener
        LL_AutoGen_CircuitParser parser = LL_ParserHelper.runParser(CharStreams.fromFileName(file.getAbsolutePath()),
                error, false);

        if (error.errorFound) {
            logger.warning("Unable to parse the file: " + file.getAbsolutePath());
        }

        return parser;
    }
    public LL_AutoGen_CircuitParser parseString(String input) throws IOException {
        error.errorFound = false; // reset the error listener
        LL_AutoGen_CircuitParser parser = LL_ParserHelper.runParser(CharStreams.fromString(input), error, false);

        if (error.errorFound) {
            logger.warning("Unable to parse the string: " + input.substring(0, 200));
        }

        return parser;
    }

    public static LL_AutoGen_CircuitParser runParser(CharStream cs, BaseErrorListener errorListener,
            Boolean displayStats) throws IOException {
        LL_AutoGen_CircuitLexer lexer = new LL_AutoGen_CircuitLexer(cs);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LL_AutoGen_CircuitParser parser = new LL_AutoGen_CircuitParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        parser.init();

        if (displayStats) {
            System.out.println(parser.circuit.toString());
        }

        return parser;
    }
}
