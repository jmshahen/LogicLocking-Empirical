package shahen.logiclocking.reduction;

import java.util.Collection;
import java.util.logging.Logger;

import gurobi.*;
import ilog.concert.*;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;

public class LL_CNFSAT_Reduction implements LL_ReductionI {
    public Logger log;

    public LL_CNFSAT_Reduction(Logger log) {
        this.log = log;
    }

    @Override
    public String get_version() {
        return "v1.0.1";
    }

    @Override
    public String get_name() {
        return "CNFSAT Reduction";
    }
    @Override
    public String toString() {
        return "cnfsat_reduction";
    }

    /** Returns TRUE if extra variables are required to add to the model. */
    public boolean uses_extra_variables() {
        return true;
    }

    /** Returns the number of extra variables required to add to the model. */
    public int set_extra_number_for_gates(Collection<LL_Gate> gates) {
        int num = 0;

        for (LL_Gate g : gates) {
            if (g instanceof LL_Mux1X || g instanceof LL_MuxX1) {
                g.add_extra_constraint_id(num);
                num += 1;
            } else if (g instanceof LL_Mux) {
                g.add_extra_constraint_id(num);
                num += 1;
                g.add_extra_constraint_id(num);
                num += 1;
            }
        }

        return num;
    }

    /** Returns the string for extra variables */
    public String extra_variable_name() {
        return "extra";
    }

    /////////////////////////////////////////////////////////////////////
    // AND GATE
    @Override
    public String[] getANDILPContraints(LL_And g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // in1 - out >= 0;
        // in2 - out >= 0;
        // out - in1 - in2 >= -1;
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        return new String[]{//
                in1 + " - " + out + " >= 0;",//
                in2 + " - " + out + " >= 0;",//
                out + " - " + in1 + " - " + in2 + " >= -1;"};
    }

    @Override
    public void addANDCPLEXConstraints(LL_And g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // ____________________in1 - out >= 0;
        cplex.addGe(cplex.diff(in1, out), 0);
        // ____________________in2 - out >= 0;
        cplex.addGe(cplex.diff(in2, out), 0);
        // ____________________out - (in1 + in2) >= -1;
        cplex.addGe(cplex.diff(out, cplex.sum(in1, in2)), -1);
    }

    @Override
    public void addANDGurobiConstraints(LL_And g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;

        // in1 + -out >= 0;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in1, expr, cp, input_values, output_values, -1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "AND_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // in2 + -out >= 0;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in2, expr, cp, input_values, output_values, -1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "AND_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // out + -in1 + -in2 >= -1;
        expr = new GRBLinExpr();
        expr.addTerm(1, g.out.get_gurobi_var(cp));
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, -1, -1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, -1, "AND_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////
    // BUF GATE
    @Override
    public String[] getBUFILPContraints(LL_Buf g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out == in1;
        return new String[]{//
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "==" + //
                        g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + ";"//
        };
    }

    @Override
    public void addBUFCPLEXConstraints(LL_Buf g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in_cplex = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out_cplex = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == in1;
        cplex.addEq(out_cplex, in_cplex);
    }

    @Override
    public void addBUFGurobiConstraints(LL_Buf g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        // out == in1;
        gurobi.addConstr(g.in1.get_gurobi_var(cp), GRB.EQUAL, g.out.get_gurobi_var(cp),
                "BUF_GATE_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large AND GATE
    @Override
    public String[] getLargeANDILPContraints(LL_Large_And g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out - (in1 + in2 + in3 + ... + inN) >= 1 - n
        // in1 - out >= 0
        // in2 - out >= 0
        // ...
        // inN - out >= 0
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        int n = g.numInputs();

        String[] constraints = new String[1 + g.numInputs()];

        for (int i = 0; i < g.numInputs(); i++) {
            constraints[i] = g.inputs.get(i).get_ilp_name(circuit_name, input_name, key_name, input_values,
                    output_values) + " - " + out + " >= 0;";
        }
        constraints[g.numInputs()] = out + " - " + sum + " >= " + (1 - n) + ";";

        return constraints;
    }

    @Override
    public void addLargeANDCPLEXConstraints(LL_Large_And g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        // out - (in1 + in2 + in3 + ... + inN) >= 1-n
        // in1 - out >= 0
        // in2 - out >= 0
        // ...
        // inN - out >= 0
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);
        int n = g.numInputs();

        for (LL_Wire w : g.inputs) {
            cplex.addGe(cplex.diff(w.get_cplex_value(cp, cplex, input_values, output_values), out), 0);
        }
        cplex.addGe(cplex.diff(out, sum), 1 - n);
    }

    @Override
    public void addLargeANDGurobiConstraints(LL_Large_And g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        int n = g.numInputs();
        GRBLinExpr expr;
        GRBVar out = g.out.get_gurobi_var(cp);

        // out + -in1 + -in2 + -in3 + ... + -inN >= 1-n
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, true);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1 - n, "LARGE_AND_a_" + cp.circuit_name + "_" + g.out.get_name());

        int i = 1;
        for (LL_Wire in : g.inputs) {
            // in1 - out >= 0
            // in2 - out >= 0
            // ...
            // inN - out >= 0
            expr = new GRBLinExpr();
            expr.addTerm(1, in.get_gurobi_var(cp));
            expr.addTerm(-1, out);
            gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0,
                    "LARGE_AND_b_" + cp.circuit_name + "_" + g.out.get_name() + "_" + i);

            i++;
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large NAND GATE
    @Override
    public String[] getLargeNANDILPContraints(LL_Large_Nand g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out + (in0 + in1 + in2 + ... + inN-1) <= n
        // in1 + out >= 1
        // in2 + out >= 1
        // ...
        // inN-1 + out >= 1
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        int n = g.numInputs();

        String[] constraints = new String[1 + g.numInputs()];

        for (int i = 0; i < g.numInputs(); i++) {
            constraints[i] = g.inputs.get(i).get_ilp_name(circuit_name, input_name, key_name, input_values,
                    output_values) + " + " + out + " >= 1;";
        }
        constraints[g.numInputs()] = out + " + " + sum + " <= " + n + ";";

        return constraints;
    }

    @Override
    public void addLargeNANDCPLEXConstraints(LL_Large_Nand g, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        // out + (in1 + in2 + in3 + ... + inN) <= n
        // in1 + out >= 1
        // in2 + out >= 1
        // ...
        // inN + out >= 1
        int n = g.numInputs();
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        for (LL_Wire w : g.inputs) {
            cplex.addGe(cplex.sum(w.get_cplex_value(cp, cplex, input_values, output_values), out), 1);
        }
        cplex.addLe(cplex.sum(out, sum), n);
    }

    @Override
    public void addLargeNANDGurobiConstraints(LL_Large_Nand g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        int n = g.numInputs();
        GRBLinExpr expr;
        GRBVar out = g.out.get_gurobi_var(cp);

        // out + in1 + in2 + in3 + ... + inN <= n
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, false);
        gurobi.addConstr(expr, GRB.LESS_EQUAL, n, "LARGE_NAND_a_" + cp.circuit_name + "_" + g.out.get_name());

        int i = 1;
        for (LL_Wire in : g.inputs) {
            // in1 + out >= 1
            // in2 + out >= 1
            // ...
            // inN + out >= 1
            expr = new GRBLinExpr();
            expr.addTerm(1, in.get_gurobi_var(cp));
            expr.addTerm(1, out);
            gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1,
                    "LARGE_NAND_b" + cp.circuit_name + "_" + g.out.get_name() + "_" + i);

            i++;
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large NOR GATE
    @Override
    public String[] getLargeNORILPContraints(LL_Large_Nor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out + (in1 + in2 + in3 + ... + inN) >= 1
        // in1 + out <= 1
        // in2 + out <= 1
        // ...
        // inN + out <= 1
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        String[] constraints = new String[1 + g.numInputs()];

        for (int i = 0; i < g.numInputs(); i++) {
            constraints[i] = g.inputs.get(i).get_ilp_name(circuit_name, input_name, key_name, input_values,
                    output_values) + " + " + out + " <= 1;";
        }
        constraints[g.numInputs()] = out + " + " + sum + " >= 1;";

        return constraints;
    }

    @Override
    public void addLargeNORCPLEXConstraints(LL_Large_Nor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        // out + (in1 + in2 + in3 + ... + inN) >= 1
        // in1 + out <= 1
        // in2 + out <= 1
        // ...
        // inN + out <= 1
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        for (LL_Wire w : g.inputs) {
            cplex.addLe(cplex.sum(w.get_cplex_value(cp, cplex, input_values, output_values), out), 1);
        }
        cplex.addGe(cplex.sum(out, sum), 1);
    }

    @Override
    public void addLargeNORGurobiConstraints(LL_Large_Nor g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;
        GRBVar out = g.out.get_gurobi_var(cp);

        // out + in1 + in2 + in3 + ... + inN >= 1
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, false);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1, "LARGE_NOR_a_" + cp.circuit_name + "_" + g.out.get_name());

        int i = 1;
        for (LL_Wire in : g.inputs) {
            // in1 + out <= 1
            // in2 + out <= 1
            // ...
            // inN + out <= 1
            expr = new GRBLinExpr();
            expr.addTerm(1, in.get_gurobi_var(cp));
            expr.addTerm(1, out);
            gurobi.addConstr(expr, GRB.LESS_EQUAL, 1,
                    "LARGE_NOR_b_" + cp.circuit_name + "_" + g.out.get_name() + "_" + i);

            i++;
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large OR GATE
    @Override
    public String[] getLargeORILPContraints(LL_Large_Or g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // (in1 + in2 + in3 + ... + inN) - out >= 0
        // out - in1 >= 0
        // out - in2 >= 0
        // ...
        // out - inN >= 0
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        String[] constraints = new String[1 + g.numInputs()];

        for (int i = 0; i < g.numInputs(); i++) {
            constraints[i] = out + " - "
                    + g.inputs.get(i).get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                    + " >= 0;";
        }
        constraints[g.numInputs()] = sum + " - " + out + " >= 0;";

        return constraints;
    }

    @Override
    public void addLargeORCPLEXConstraints(LL_Large_Or g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        // (in1 + in2 + in3 + ... + inN) - out >= 0
        // out - in1 >= 0
        // out - in2 >= 0
        // ...
        // out - inN >= 0
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        for (LL_Wire w : g.inputs) {
            cplex.addGe(cplex.diff(out, w.get_cplex_value(cp, cplex, input_values, output_values)), 0);
        }
        cplex.addGe(cplex.diff(sum, out), 0);
    }

    @Override
    public void addLargeORGurobiConstraints(LL_Large_Or g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;
        GRBVar out = g.out.get_gurobi_var(cp);

        // in1 + in2 + in3 + ... + inN - out >= 0
        expr = new GRBLinExpr();
        expr.addTerm(-1, out);
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, false);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "LARGE_OR_a_" + cp.circuit_name + "_" + g.out.get_name());

        int i = 1;
        for (LL_Wire in : g.inputs) {
            // out + -in1 >= 0
            // out + -in2 >= 0
            // ...
            // out + -inN >= 0
            expr = new GRBLinExpr();
            expr.addTerm(1, out);
            expr.addTerm(-1, in.get_gurobi_var(cp));
            gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0,
                    "LARGE_OR_b_" + cp.circuit_name + "_" + g.out.get_name() + "_" + i);

            i++;
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUX GATE
    @Override
    public String[] getMUXILPContraints(LL_Mux g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String s = g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String ex1 = get_extra_variable_name(g, circuit_name, 0);
        String ex2 = get_extra_variable_name(g, circuit_name, 1);

        return new String[]{//
                // AND GATE
                // in1 - ex1 >= 0;
                in1 + "-" + ex1 + ">=0;",//
                // 1 - s - ex1 >= 0;
                "1-" + s + "-" + ex1 + ">=0;",//
                // ex1 - in1 + s >= 0;
                ex1 + "-" + in1 + "+" + s + ">=0;",//
                // AND GATE
                // in2 - ex2 >= 0;
                in2 + "-" + ex2 + ">=0;",//
                // s - ex2 >= 0;
                s + "-" + ex2 + ">=0;",//
                // ex2 - in2 - s >= -1;
                ex2 + "-" + in2 + "-" + s + ">=-1;",//
                // OR GATE
                // ex1 + ex2 - out >= 0;
                ex1 + "+" + ex2 + "-" + out + ">=0;",//
                // out - ex1 >= 0;
                out + "-" + ex1 + ">=0;",//
                // out - ex2 >= 0;
                out + "-" + ex2 + ">=0;",//
        };
    }

    @Override
    public void addMUXCPLEXConstraints(LL_Mux g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr s = g.s.get_cplex_value(cp, cplex, input_values, output_values);
        IloIntVar ex1 = get_extra_cplex_variable(g, cp, 0);
        IloIntVar ex2 = get_extra_cplex_variable(g, cp, 1);

        // AND GATE
        // in1 - ex1 >= 0;
        cplex.addGe(cplex.diff(in1, ex1), 0);
        // ((1 - s) - ex1) >= 0;
        cplex.addGe(cplex.diff(cplex.diff(1, s), ex1), 0);
        // ((ex1 - in1) + s) >= 0;
        cplex.addGe(cplex.sum(cplex.diff(ex1, in1), s), 0);

        // AND GATE
        // in2 - ex2 >= 0;
        cplex.addGe(cplex.diff(in2, ex2), 0);
        // s - ex2 >= 0;
        cplex.addGe(cplex.diff(s, ex2), 0);
        // ex2 - in2 - s >= -1;
        cplex.addGe(cplex.diff(cplex.diff(ex2, in2), s), -1);

        // OR GATE
        // ex1 + ex2 - out >= 0;
        cplex.addGe(cplex.diff(cplex.sum(ex1, ex2), out), 0);
        // out - ex1 >= 0;
        cplex.addGe(cplex.diff(out, ex1), 0);
        // out - ex2 >= 0;
        cplex.addGe(cplex.diff(out, ex2), 0);
    }

    @Override
    public void addMUXGurobiConstraints(LL_Mux g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in1 = g.in1.get_gurobi_var(cp);
        GRBVar in2 = g.in2.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBVar ex1 = get_extra_gurobi_variable(g, cp, 0);
        GRBVar ex2 = get_extra_gurobi_variable(g, cp, 1);
        GRBLinExpr e;

        // AND GATE
        // in1 - ex1 >= 0;
        gurobi.addConstr(gurobi_diff(in1, ex1), GRB.GREATER_EQUAL, 0,
                "MUX_GATE_AND1_a_" + cp.circuit_name + "_" + g.out.get_name());
        // 1 -s -ex1 >= 0;
        e = new GRBLinExpr();
        e.addConstant(1);
        e.addTerm(-1, s);
        e.addTerm(-1, ex1);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUX_GATE_AND1_b_" + cp.circuit_name + "_" + g.out.get_name());
        // ex1 -in1 + s >= 0;
        e = new GRBLinExpr();
        e.addTerm(1, ex1);
        e.addTerm(-1, in1);
        e.addTerm(1, s);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUX_GATE_AND1_c_" + cp.circuit_name + "_" + g.out.get_name());

        // AND GATE
        // in2 -ex2 >= 0;
        gurobi.addConstr(gurobi_diff(in2, ex2), GRB.GREATER_EQUAL, 0,
                "MUX_GATE_AND2_a_" + cp.circuit_name + "_" + g.out.get_name());
        // s -ex2 >= 0;
        gurobi.addConstr(gurobi_diff(s, ex2), GRB.GREATER_EQUAL, 0,
                "MUX_GATE_AND2_b_" + cp.circuit_name + "_" + g.out.get_name());
        // ex2 -in2 -s >= -1;
        e = new GRBLinExpr();
        e.addTerm(1, ex2);
        e.addTerm(-1, in2);
        e.addTerm(-1, s);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, -1, "MUX_GATE_AND2_c_" + cp.circuit_name + "_" + g.out.get_name());

        // OR GATE
        // ex1 + ex2 -out >= 0;
        e = new GRBLinExpr();
        e.addTerm(1, ex1);
        e.addTerm(1, ex2);
        e.addTerm(-1, out);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUX_GATE_OR3_a_" + cp.circuit_name + "_" + g.out.get_name());
        // out -ex1 >= 0;
        gurobi.addConstr(gurobi_diff(out, ex1), GRB.GREATER_EQUAL, 0,
                "MUX_GATE_OR3_b_" + cp.circuit_name + "_" + g.out.get_name());
        // out -ex2 >= 0;
        gurobi.addConstr(gurobi_diff(out, ex2), GRB.GREATER_EQUAL, 0,
                "MUX_GATE_OR3_c_" + cp.circuit_name + "_" + g.out.get_name());
    }

    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUX0X GATE
    @Override
    public String[] getMUX0XILPContraints(LL_Mux0X g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String s = g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // AND GATE
                // in2 - out >= 0;
                in2 + "-" + out + ">=0;",//
                // s - out >= 0;
                s + "-" + out + ">=0;",//
                // out - in2 - s >= -1;
                out + "-" + in2 + "-" + s + ">=-1;",//
        };
    }

    @Override
    public void addMUX0XCPLEXConstraints(LL_Mux0X g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr s = g.s.get_cplex_value(cp, cplex, input_values, output_values);

        // AND GATE
        // in2 - out >= 0;
        cplex.addGe(cplex.diff(in2, out), 0);
        // s - out >= 0;
        cplex.addGe(cplex.diff(s, out), 0);
        // out - in2 - s >= -1;
        cplex.addGe(cplex.diff(cplex.diff(out, in2), s), -1);
    }

    @Override
    public void addMUX0XGurobiConstraints(LL_Mux0X g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in2 = g.in2.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBLinExpr e;

        // AND GATE
        // in2 -out >= 0;
        gurobi.addConstr(gurobi_diff(in2, out), GRB.GREATER_EQUAL, 0,
                "MUX0X_GATE_AND2_a_" + cp.circuit_name + "_" + g.out.get_name());
        // s -out >= 0;
        gurobi.addConstr(gurobi_diff(s, out), GRB.GREATER_EQUAL, 0,
                "MUX0X_GATE_AND2_b_" + cp.circuit_name + "_" + g.out.get_name());
        // out -in2 -s >= -1;
        e = new GRBLinExpr();
        e.addTerm(1, out);
        e.addTerm(-1, in2);
        e.addTerm(-1, s);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, -1, "MUX0X_GATE_AND2_c_" + cp.circuit_name + "_" + g.out.get_name());
    }

    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUX1X GATE
    @Override
    public String[] getMUX1XILPContraints(LL_Mux1X g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String s = g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String ex1 = get_extra_variable_name(g, circuit_name, 0);

        return new String[]{//
                // AND GATE
                // in2 - ex1 >= 0;
                in2 + "-" + ex1 + ">=0;",//
                // s - ex1 >= 0;
                s + "-" + ex1 + ">=0;",//
                // ex1 - in2 - s >= -1;
                ex1 + "-" + in2 + "-" + s + ">=-1;",//
                // OR GATE
                // ex1 + 1-s - out >= 0;
                ex1 + "+" + "1-" + s + "-" + out + ">=0;",//
                // out - ex1 >= 0;
                out + "-" + ex1 + ">=0;",//
                // out - 1 + s >= 0;
                out + "-1+" + s + ">=0;",//
        };
    }

    @Override
    public void addMUX1XCPLEXConstraints(LL_Mux1X g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr s = g.s.get_cplex_value(cp, cplex, input_values, output_values);
        IloIntVar ex1 = get_extra_cplex_variable(g, cp, 0);

        // AND GATE
        // in2 - ex1 >= 0;
        cplex.addGe(cplex.diff(in2, ex1), 0);
        // s - ex1 >= 0;
        cplex.addGe(cplex.diff(s, ex1), 0);
        // ex1 - in2 - s >= -1;
        cplex.addGe(cplex.diff(cplex.diff(ex1, in2), s), -1);

        // OR GATE
        // ex1 + 1-s - out >= 0;
        cplex.addGe(cplex.diff(cplex.sum(ex1, cplex.diff(1, s)), out), 0);
        // out - ex1 >= 0;
        cplex.addGe(cplex.diff(out, ex1), 0);
        // out - 1-s >= 0;
        cplex.addGe(cplex.diff(out, cplex.diff(1, s)), 0);
    }

    @Override
    public void addMUX1XGurobiConstraints(LL_Mux1X g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in2 = g.in2.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBVar ex1 = get_extra_gurobi_variable(g, cp, 0);
        GRBLinExpr e;

        // AND GATE
        // in2 -ex1 >= 0;
        gurobi.addConstr(gurobi_diff(in2, ex1), GRB.GREATER_EQUAL, 0,
                "MUX1X_GATE_AND1_a_" + cp.circuit_name + "_" + g.out.get_name());
        // s -ex2 >= 0;
        gurobi.addConstr(gurobi_diff(s, ex1), GRB.GREATER_EQUAL, 0,
                "MUX1X_GATE_AND1_b_" + cp.circuit_name + "_" + g.out.get_name());
        // ex2 -in2 -s >= -1;
        e = new GRBLinExpr();
        e.addTerm(1, ex1);
        e.addTerm(-1, in2);
        e.addTerm(-1, s);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, -1, "MUX1X_GATE_AND1_c_" + cp.circuit_name + "_" + g.out.get_name());

        // OR GATE
        // ex1 + 1 -s -out >= 0;
        e = new GRBLinExpr();
        e.addTerm(1, ex1);
        e.addConstant(1);
        e.addTerm(-1, s);
        e.addTerm(-1, out);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUX1X_GATE_OR2_a_" + cp.circuit_name + "_" + g.out.get_name());
        // out -ex1 >= 0;
        gurobi.addConstr(gurobi_diff(out, ex1), GRB.GREATER_EQUAL, 0,
                "MUX1X_GATE_OR2_b_" + cp.circuit_name + "_" + g.out.get_name());
        // out -1 +s >= 0;
        e = new GRBLinExpr();
        e.addTerm(1, out);
        e.addConstant(-1);
        e.addTerm(1, s);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUX1X_GATE_OR2_c_" + cp.circuit_name + "_" + g.out.get_name());
    }

    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUXX0 GATE
    @Override
    public String[] getMUXX0ILPContraints(LL_MuxX0 g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String s = g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // AND GATE
                // in1 - out >= 0;
                in1 + "-" + out + ">=0;",//
                // 1 - s - out >= 0;
                "1-" + s + "-" + out + ">=0;",//
                // out - in1 + s >= 0;
                out + "-" + in1 + "+" + s + ">=0;",//
        };
    }

    @Override
    public void addMUXX0CPLEXConstraints(LL_MuxX0 g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr s = g.s.get_cplex_value(cp, cplex, input_values, output_values);
        // AND GATE
        // in1 - out >= 0;
        cplex.addGe(cplex.diff(in1, out), 0);
        // ((1 - s) - out) >= 0;
        cplex.addGe(cplex.diff(cplex.diff(1, s), out), 0);
        // ((out - in1) + s) >= 0;
        cplex.addGe(cplex.sum(cplex.diff(out, in1), s), 0);
    }

    @Override
    public void addMUXX0GurobiConstraints(LL_MuxX0 g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in1 = g.in1.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBLinExpr e;

        // AND GATE
        // in1 - out >= 0;
        gurobi.addConstr(gurobi_diff(in1, out), GRB.GREATER_EQUAL, 0,
                "MUXX0_GATE_AND_a_" + cp.circuit_name + "_" + g.out.get_name());
        // 1 -s -out >= 0;
        e = new GRBLinExpr();
        e.addConstant(1);
        e.addTerm(-1, s);
        e.addTerm(-1, out);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUXX0_GATE_AND_b_" + cp.circuit_name + "_" + g.out.get_name());
        // out -in1 + s >= 0;
        e = new GRBLinExpr();
        e.addTerm(1, out);
        e.addTerm(-1, in1);
        e.addTerm(1, s);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUXX0_GATE_AND_c_" + cp.circuit_name + "_" + g.out.get_name());
    }

    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUXX1 GATE
    @Override
    public String[] getMUXX1ILPContraints(LL_MuxX1 g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String s = g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String ex1 = get_extra_variable_name(g, circuit_name, 0);

        return new String[]{//
                // AND GATE
                // in1 - ex1 >= 0;
                in1 + "-" + ex1 + ">=0;",//
                // 1 - s - ex1 >= 0;
                "1-" + s + "-" + ex1 + ">=0;",//
                // ex1 - in1 + s >= 0;
                ex1 + "-" + in1 + "+" + s + ">=0;",//
                // OR GATE
                // ex1 + s - out >= 0;
                ex1 + "+" + s + "-" + out + ">=0;",//
                // out - ex1 >= 0;
                out + "-" + ex1 + ">=0;",//
                // out - s >= 0;
                out + "-" + s + ">=0;",//
        };
    }

    @Override
    public void addMUXX1CPLEXConstraints(LL_MuxX1 g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr s = g.s.get_cplex_value(cp, cplex, input_values, output_values);
        IloIntVar ex1 = get_extra_cplex_variable(g, cp, 0);

        // AND GATE
        // in1 - ex1 >= 0;
        cplex.addGe(cplex.diff(in1, ex1), 0);
        // ((1 - s) - ex1) >= 0;
        cplex.addGe(cplex.diff(cplex.diff(1, s), ex1), 0);
        // ((ex1 - in1) + s) >= 0;
        cplex.addGe(cplex.sum(cplex.diff(ex1, in1), s), 0);

        // OR GATE
        // ex1 + s - out >= 0;
        cplex.addGe(cplex.diff(cplex.sum(ex1, s), out), 0);
        // out - ex1 >= 0;
        cplex.addGe(cplex.diff(out, ex1), 0);
        // out - s >= 0;
        cplex.addGe(cplex.diff(out, s), 0);
    }

    @Override
    public void addMUXX1GurobiConstraints(LL_MuxX1 g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in1 = g.in1.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBVar ex1 = get_extra_gurobi_variable(g, cp, 0);
        GRBLinExpr e;

        // AND GATE
        // in1 - ex1 >= 0;
        gurobi.addConstr(gurobi_diff(in1, ex1), GRB.GREATER_EQUAL, 0,
                "MUXX1_GATE_AND1_a_" + cp.circuit_name + "_" + g.out.get_name());
        // 1 -s -ex1 >= 0;
        e = new GRBLinExpr();
        e.addConstant(1);
        e.addTerm(-1, s);
        e.addTerm(-1, ex1);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUXX1_GATE_AND1_b_" + cp.circuit_name + "_" + g.out.get_name());
        // ex1 -in1 + s >= 0;
        e = new GRBLinExpr();
        e.addTerm(1, ex1);
        e.addTerm(-1, in1);
        e.addTerm(1, s);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUXX1_GATE_AND1_c_" + cp.circuit_name + "_" + g.out.get_name());

        // OR GATE
        // ex1 + s -out >= 0;
        e = new GRBLinExpr();
        e.addTerm(1, ex1);
        e.addTerm(1, s);
        e.addTerm(-1, out);
        gurobi.addConstr(e, GRB.GREATER_EQUAL, 0, "MUXX1_GATE_OR2_a_" + cp.circuit_name + "_" + g.out.get_name());
        // out -ex1 >= 0;
        gurobi.addConstr(gurobi_diff(out, ex1), GRB.GREATER_EQUAL, 0,
                "MUXX1_GATE_OR2_b_" + cp.circuit_name + "_" + g.out.get_name());
        // out -s >= 0;
        gurobi.addConstr(gurobi_diff(out, s), GRB.GREATER_EQUAL, 0,
                "MUXX1_GATE_OR2_c_" + cp.circuit_name + "_" + g.out.get_name());
    }

    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // NAND GATE
    @Override
    public String[] getNANDILPContraints(LL_Nand g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // out + (in1 + in2) <= 2
                out + " + " + in1 + " + " + in2 + " <= 2;",
                // in1 + out >= 1
                in1 + " + " + out + " >= 1;",
                // in2 + out >= 1
                in2 + " + " + out + " >= 1;"//
        };
    }

    @Override
    public void addNANDCPLEXConstraints(LL_Nand g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // ____________________out + in1 + in2 <= 2;
        cplex.addLe(cplex.sum(out, cplex.sum(in1, in2)), 2);
        // ____________________in1 + out >= 1;
        cplex.addGe(cplex.sum(in1, out), 1);
        // ____________________in2 + out >= 1;
        cplex.addGe(cplex.sum(in2, out), 1);
    }

    @Override
    public void addNANDGurobiConstraints(LL_Nand g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;

        // in1 + out >= 1;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in1, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1, "NAND_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // in2 + out >= 1;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1, "NAND_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // out + in1 + in2 <= 2;
        expr = new GRBLinExpr();
        expr.addTerm(1, g.out.get_gurobi_var(cp));
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.LESS_EQUAL, 2, "NAND_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // NOR GATE
    @Override
    public String[] getNORILPContraints(LL_Nor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // out + (in1 + in2) >= 1
                out + " + " + in1 + " + " + in2 + " >= 1;",
                // in1 + out <= 1
                in1 + " + " + out + " <= 1;",
                // in2 + out <= 1
                in2 + " + " + out + " <= 1;"//
        };
    }

    @Override
    public void addNORCPLEXConstraints(LL_Nor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // ____________________out + in1 + in2 >= 1;
        cplex.addGe(cplex.sum(out, cplex.sum(in1, in2)), 1);
        // ____________________in1 + out <= 1;
        cplex.addLe(cplex.sum(in1, out), 1);
        // ____________________in2 + out <= 1;
        cplex.addLe(cplex.sum(in2, out), 1);
    }

    @Override
    public void addNORGurobiConstraints(LL_Nor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;

        // out + in1 + in2 >= 1;
        expr = new GRBLinExpr();
        expr.addTerm(1, g.out.get_gurobi_var(cp));
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1, "NOR_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // in1 + out <= 1;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in1, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.LESS_EQUAL, 1, "NOR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // in2 + out <= 1;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.LESS_EQUAL, 1, "NOR_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // NOT GATE
    @Override
    public String[] getNOTILPContraints(LL_Not g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // out + in1 >= 1
                out + " + " + in1 + " >= 1;",//
                // out + in1 <= 1
                out + " + " + in1 + " <= 1;"//
        };
    }

    @Override
    public void addNOTCPLEXConstraints(LL_Not g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == 1 - in1;
        if (g.in1.is_input() && input_values != null) {
            cplex.addEq(out, 1 - g.in1.get_value_double(input_values));
        } else if (g.in1.is_output() && output_values != null) {
            cplex.addEq(out, 1 - g.in1.get_value_double(output_values));
        } else {
            IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
            // out + in1 >= 1
            cplex.addGe(cplex.sum(out, in1), 1);
            // out + in1 <= 1
            cplex.addLe(cplex.sum(out, in1), 1);
        }
    }

    @Override
    public void addNOTGurobiConstraints(LL_Not g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);

        if (g.in1.is_input() && input_values != null) {
            // out == 1 - in1;
            gurobi.addConstr(out, GRB.EQUAL, 1 - g.in1.get_value_double(input_values),
                    "NOT_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else if (g.in1.is_output() && output_values != null) {
            // out == 1 - in1;
            gurobi.addConstr(out, GRB.EQUAL, 1 - g.in1.get_value_double(output_values),
                    "NOT_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            GRBLinExpr expr;

            // out + in1 >= 1
            expr = new GRBLinExpr();
            g.in1.gurobi_sum(g.out, expr, cp, input_values, output_values, false);
            gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1, "NOT_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

            // out + in1 <= 1
            expr = new GRBLinExpr();
            g.in1.gurobi_sum(g.out, expr, cp, input_values, output_values, false);
            gurobi.addConstr(expr, GRB.LESS_EQUAL, 1, "NOT_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // OFF GATE
    @Override
    public String[] getOFFILPContraints(LL_Off g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        // out == 0;
        return new String[]{out + " == 0;"};
    }

    @Override
    public void addOFFCPLEXConstraints(LL_Off g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == 0;
        cplex.addEq(out, 0);
    }

    @Override
    public void addOFFGurobiConstraints(LL_Off g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        // out == 0;
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, 0,
                "OFF_GATE_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // ON GATE
    @Override
    public String[] getONILPContraints(LL_On g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        // out == 1;
        return new String[]{out + " == 1;"};
    }

    @Override
    public void addONCPLEXConstraints(LL_On g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == 1;
        cplex.addEq(out, 1);
    }

    @Override
    public void addONGurobiConstraints(LL_On g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        // out == 1;
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, 1, "ON_GATE_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // OR GATE
    @Override
    public String[] getORILPContraints(LL_Or g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // (in1 + in2) - out >= 0
                "(" + in1 + " + " + in2 + ") - " + out + " >= 0;",
                // out - in1 >= 0
                out + " - " + in1 + " >= 0;",
                // out - in2 >= 0
                out + " - " + in2 + " >= 0;"//
        };
    }

    @Override
    public void addORCPLEXConstraints(LL_Or g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // ____________________(in1 + in2) - out >= 0;
        cplex.addGe(cplex.diff(cplex.sum(in1, in2), out), 0);
        // ____________________out - in1 >= 0;
        cplex.addGe(cplex.diff(out, in1), 0);
        // ____________________out - in2 >= 0;
        cplex.addGe(cplex.diff(out, in2), 0);
    }

    @Override
    public void addORGurobiConstraints(LL_Or g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;

        // in1 + in2 - out >= 0;
        expr = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        expr.addTerm(-1, g.out.get_gurobi_var(cp));
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "OR_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // out + -in1 >= 0;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in1, expr, cp, input_values, output_values, 1, -1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "OR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // out + -in2 >= 0;
        expr = new GRBLinExpr();
        g.out.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, -1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "OR_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // XNOR GATE
    @Override
    public String[] getXNORILPContraints(LL_Xnor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // out + in1 + in2 >= 1
                out + " + " + in1 + " + " + in2 + " >= 1;",
                // out - in1 - in2 >= -1
                out + " - " + in1 + " - " + in2 + " >= -1;",
                // in2 - out - in1 >= -1
                in2 + " - " + out + " - " + in1 + " >= -1;",
                // in1 - out - in2 >= -1
                in1 + " - " + out + " - " + in2 + " >= -1;"//
        };
    }

    @Override
    public void addXNORCPLEXConstraints(LL_Xnor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out + in1 + in2 >= 1
        cplex.addGe(cplex.sum(cplex.sum(out, in1), in2), 1);

        // out - (in1 + in2) >= -1
        cplex.addGe(cplex.diff(out, cplex.sum(in1, in2)), -1);

        // in2 - (out + in1) >= -1
        cplex.addGe(cplex.diff(in2, cplex.sum(out, in1)), -1);

        // in1 - (out + in2) >= -1
        cplex.addGe(cplex.diff(in1, cplex.sum(out, in2)), -1);
    }

    @Override
    public void addXNORGurobiConstraints(LL_Xnor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;
        GRBVar out = g.out.get_gurobi_var(cp);

        // out + in1 + in2 >= 1
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 1, "XNOR_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // out + -in1 + -in2 >= -1
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, -1, -1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, -1, "XNOR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // in2 + -out + -in1 >= -1
        expr = new GRBLinExpr();
        expr.addTerm(-1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, -1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, -1, "XNOR_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());

        // in1 + -out + -in2 >= -1
        expr = new GRBLinExpr();
        expr.addTerm(-1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, -1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, -1, "XNOR_GATE_d_" + cp.circuit_name + "_" + g.out.get_name());

    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // XOR GATE
    @Override
    public String[] getXORILPContraints(LL_Xor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {

        String in1 = g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String in2 = g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);
        String out = g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{//
                // in1 + in2 - out >= 0
                in1 + " + " + in2 + " - " + out + " >= 0;",
                // out + in1 + in2 <= 2
                out + " + " + in1 + " + " + in2 + " <= 2;",
                // out + in2 - in1 >= 0
                out + " + " + in2 + " - " + in1 + " >= 0;",
                // out + in1 - in2 >= 0
                out + " + " + in1 + " - " + in2 + " >= 0;"//
        };
    }

    @Override
    public void addXORCPLEXConstraints(LL_Xor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in1 = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr in2 = g.in2.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // in1 + (in2 - out) >= 0
        cplex.addGe(cplex.sum(in1, cplex.diff(in2, out)), 0);

        // out + (in1 + in2) <= 2
        cplex.addLe(cplex.sum(cplex.sum(out, in1), in2), 2);

        // out + (in2 - in1) >= 0
        cplex.addGe(cplex.sum(out, cplex.diff(in2, in1)), 0);

        // out + (in1 - in2) >= 0
        cplex.addGe(cplex.sum(out, cplex.diff(in1, in2)), 0);
    }

    @Override
    public void addXORGurobiConstraints(LL_Xor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBLinExpr expr;
        GRBVar out = g.out.get_gurobi_var(cp);

        // in1 + in2 + -out >= 0
        expr = new GRBLinExpr();
        expr.addTerm(-1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0,
                "XOR_GATE_a_" + cp.circuit_name + "_" + cp.circuit_name + "_" + g.out.get_name());

        // out + in1 + in2 <= 2
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, 1);
        gurobi.addConstr(expr, GRB.LESS_EQUAL, 2, "XOR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // out + in2 + -in1 >= 0
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, -1, 1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "XOR_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());

        // out + in1 + -in2 >= 0
        expr = new GRBLinExpr();
        expr.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, 1, -1);
        gurobi.addConstr(expr, GRB.GREATER_EQUAL, 0, "XOR_GATE_d_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /**
     * returns the expression:
     *    1 first -1 second
     * @param first
     * @param second
     * @return
     */
    private GRBLinExpr gurobi_diff(GRBVar first, GRBVar second) {
        GRBLinExpr e = new GRBLinExpr();
        e.addTerm(1, first);
        e.addTerm(-1, second);
        return e;
    }
}
