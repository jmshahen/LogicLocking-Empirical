package shahen.logiclocking.reduction;

import java.util.Collection;
import java.util.logging.Logger;

import gurobi.*;
import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;

public class LL_Reduced_Reduction extends LL_Standard_Reduction implements LL_ReductionI {
    public LL_Reduced_Reduction(Logger log) {
        super(log);
    }

    @Override
    public String get_version() {
        return "v1.0.1";
    }

    @Override
    public String get_name() {
        return "Reduced Reduction";
    }

    @Override
    public String toString() {
        return "reduced_reduction";
    }

    /** Returns TRUE if extra variables are required to add to the model. */
    public boolean uses_extra_variables() {
        return true;
    }

    /** Returns the number of extra variables required to add to the model. */
    public int set_extra_number_for_gates(Collection<LL_Gate> gates) {
        int num = 0;

        for (LL_Gate g : gates) {
            if (g instanceof LL_Mux) {
                g.add_extra_constraint_id(num);
                num += 1;
                g.add_extra_constraint_id(num);
                num += 1;
            }
        }

        return num;
    }

    /////////////////////////////////////////////////////////////////////
    // NAND GATE
    @Override
    public String[] getNANDILPContraints(LL_Nand g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // 0 <= in1 + in2 - 2 + 2*out <= 1;
        return new String[]{"0<=" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                + "+" + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-2+2*"
                + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "<=1;"};
    }

    @Override
    public void addNANDCPLEXConstraints(LL_Nand g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloNumExpr prod = g.out.cplex_prod(2, cp, cplex, input_values, output_values);

        // ___________ 0 <= ((in1 + in2) - 2) + (2*out) <= 1;
        cplex.addRange(0, cplex.sum(cplex.diff(sum, 2), prod), 1);

    }

    @Override
    public void addNANDGurobiConstraints(LL_Nand g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        // ___________ 0 <= in1 + in2 + -2 + 2*out <= 1;
        GRBLinExpr e1 = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, e1, cp, input_values, output_values, false);
        e1.addConstant(-2);
        e1.addTerm(2, g.out.get_gurobi_var(cp));
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "NAND_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "NAND_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "NAND_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // NOR GATE
    @Override
    public String[] getNORILPContraints(LL_Nor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // 0 <= 2 - 2*out - in1 - in2 <= 1
        return new String[]{
                "0<=" + "2 - 2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-"
                        + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "<=1;"};
    }

    @Override
    public void addNORCPLEXConstraints(LL_Nor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloNumExpr prod = g.out.cplex_prod(2, cp, cplex, input_values, output_values);

        // ___________ 0 <= ((2 - 2*out) - (in1 + in2)) <= 1
        cplex.addRange(0, cplex.diff(cplex.diff(2, prod), sum), 1);
    }

    @Override
    public void addNORGurobiConstraints(LL_Nor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        // 0 <= 2 + -2*out + -in1 + -in2 <= 1
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addConstant(2);
        e1.addTerm(-2, g.out.get_gurobi_var(cp));
        g.in1.gurobi_sum(g.in2, e1, cp, input_values, output_values, true);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "NOR_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "NOR_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "NOR_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // XNOR GATE
    @Override
    public String[] getXNORILPContraints(LL_Xnor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        /** OLD
        // out == 1 + 2*t - in1 - in2
        // t is a unique variable created for each XOR and XNOR gates
        return new String[]{out.get_ilp_name(circuit_name, input_name) + "==1+2*"
                + get_xor_ilp_name(circuit_name) + "-" + in1.get_ilp_name(circuit_name, input_name) + "-"
                + in2.get_ilp_name(circuit_name, input_name) + ";"};
                */
        // out == 1- |in1 - in2|
        return new String[]{//
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        " == 1 - abs("
                        + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-"
                        + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + ");"//
        };
    }

    @Override
    public void addXNORCPLEXConstraints(LL_Xnor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr diff = g.in1.cplex_diff(g.in2, cp, cplex, input_values, output_values);

        // out == 1- |in1 - in2|
        cplex.addEq(//
                g.out.get_cplex_value(cp, cplex, input_values, output_values), //
                cplex.diff(1, cplex.abs(diff))//
        );
    }

    @Override
    public void addXNORGurobiConstraints(LL_Xnor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        /** Citation -- Not specifically stated
        @MISC {12118,
            TITLE = {Express boolean logic operations in zero-one integer linear programming (ILP)},
            AUTHOR = {D.W. (https://cs.stackexchange.com/users/755/d-w)},
            HOWPUBLISHED = {Computer Science Stack Exchange},
            NOTE = {URL:https://cs.stackexchange.com/q/12118 (version: 2019-01-21)},
            EPRINT = {https://cs.stackexchange.com/q/12118},
            URL = {https://cs.stackexchange.com/q/12118}
        }
         */
        GRBVar out = g.out.get_gurobi_var(cp);

        // 1 <= out + x1 + x2
        GRBLinExpr e = new GRBLinExpr();
        e.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, false);
        gurobi.addConstr(1, GRB.LESS_EQUAL, e, "XNOR_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // 1 >= out + x1 + -x2
        e = new GRBLinExpr();
        e.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, 1, -1);
        gurobi.addConstr(1, GRB.GREATER_EQUAL, e, "XNOR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // 1 >= out + -x1 + x2
        e = new GRBLinExpr();
        e.addTerm(1, out);
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, -1, 1);
        gurobi.addConstr(1, GRB.GREATER_EQUAL, e, "XNOR_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());

        // 1 <= out + 2 + -x1 + -x2
        e = new GRBLinExpr();
        e.addTerm(1, out);
        e.addConstant(2);
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, -1, -1);
        gurobi.addConstr(1, GRB.LESS_EQUAL, e, "XNOR_GATE_d_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // XOR GATE
    @Override
    public String[] getXORILPContraints(LL_Xor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out == |in1 - in2|
        return new String[]{//
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        " == abs(" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                        + "-" + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                        + ");"};
    }

    @Override
    public void addXORCPLEXConstraints(LL_Xor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr diff = g.in1.cplex_diff(g.in2, cp, cplex, input_values, output_values);

        // out == |in1 - in2|
        cplex.addEq(//
                g.out.get_cplex_value(cp, cplex, input_values, output_values), //
                cplex.abs(diff)//
        );
    }

    @Override
    public void addXORGurobiConstraints(LL_Xor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        /** Citation
        @MISC {12118,
            TITLE = {Express boolean logic operations in zero-one integer linear programming (ILP)},
            AUTHOR = {D.W. (https://cs.stackexchange.com/users/755/d-w)},
            HOWPUBLISHED = {Computer Science Stack Exchange},
            NOTE = {URL:https://cs.stackexchange.com/q/12118 (version: 2019-01-21)},
            EPRINT = {https://cs.stackexchange.com/q/12118},
            URL = {https://cs.stackexchange.com/q/12118}
        }
         */
        GRBVar out = g.out.get_gurobi_var(cp);

        // out <= x1 + x2
        GRBLinExpr e = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, false);
        gurobi.addConstr(out, GRB.LESS_EQUAL, e, "XNOR_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // out >= x1 + -x2
        e = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, 1, -1);
        gurobi.addConstr(out, GRB.GREATER_EQUAL, e, "XNOR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());

        // out >= -x1 + x2
        e = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, -1, 1);
        gurobi.addConstr(out, GRB.GREATER_EQUAL, e, "XNOR_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());

        // out <= 2 + -x1 + -x2
        e = new GRBLinExpr();
        e.addConstant(2);
        g.in1.gurobi_sum(g.in2, e, cp, input_values, output_values, -1, -1);
        gurobi.addConstr(out, GRB.LESS_EQUAL, e, "XNOR_GATE_d_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////
}
