package shahen.logiclocking.reduction;

import java.util.Collection;

import gurobi.*;
import ilog.concert.*;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;
import shahen.logiclocking.circuit.wires.LL_Wire;

public interface LL_ReductionI {
    /** In Gurobi, using addRange makes a weird LP constraint.
     * Setting this to FALSE turns all addRange() calls into 2 addConstr calls. */
    public boolean use_range_gurobi = false;

    /** Setting this to FALSE turns all addRange() calls into 2 addConstr calls. */
    public boolean use_range_cplex = true;

    /** The version of the Reduction being used; this version number will be changed with any change to logic */
    public String get_version();

    /** Full name of the reduction */
    public String get_name();

    /** Returns TRUE if extra variables are required to add to the model. */
    public boolean uses_extra_variables();

    /** Returns the number of extra variables required to add to the model. */
    public int set_extra_number_for_gates(Collection<LL_Gate> gates);

    /** Returns the string for extra variables */
    public String extra_variable_name();

    /////////////////////////////////////////////////////////////////////
    // AND GATE
    public String[] getANDILPContraints(LL_And g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addANDCPLEXConstraints(LL_And g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addANDGurobiConstraints(LL_And g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // BUF GATE
    public String[] getBUFILPContraints(LL_Buf g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addBUFCPLEXConstraints(LL_Buf g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addBUFGurobiConstraints(LL_Buf g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large AND GATE
    public String[] getLargeANDILPContraints(LL_Large_And g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addLargeANDCPLEXConstraints(LL_Large_And g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addLargeANDGurobiConstraints(LL_Large_And g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large NAND GATE
    public String[] getLargeNANDILPContraints(LL_Large_Nand g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addLargeNANDCPLEXConstraints(LL_Large_Nand g, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException;

    public void addLargeNANDGurobiConstraints(LL_Large_Nand g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large NOR GATE
    public String[] getLargeNORILPContraints(LL_Large_Nor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addLargeNORCPLEXConstraints(LL_Large_Nor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addLargeNORGurobiConstraints(LL_Large_Nor g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // Large OR GATE
    public String[] getLargeORILPContraints(LL_Large_Or g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addLargeORCPLEXConstraints(LL_Large_Or g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addLargeORGurobiConstraints(LL_Large_Or g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // MUX GATE
    public String[] getMUXILPContraints(LL_Mux g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addMUXCPLEXConstraints(LL_Mux g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addMUXGurobiConstraints(LL_Mux g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // MUX0X GATE
    public String[] getMUX0XILPContraints(LL_Mux0X g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addMUX0XCPLEXConstraints(LL_Mux0X g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addMUX0XGurobiConstraints(LL_Mux0X g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // MUX1X GATE
    public String[] getMUX1XILPContraints(LL_Mux1X g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addMUX1XCPLEXConstraints(LL_Mux1X g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addMUX1XGurobiConstraints(LL_Mux1X g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // MUXX0 GATE
    public String[] getMUXX0ILPContraints(LL_MuxX0 g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addMUXX0CPLEXConstraints(LL_MuxX0 g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addMUXX0GurobiConstraints(LL_MuxX0 g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // MUXX1 GATE
    public String[] getMUXX1ILPContraints(LL_MuxX1 g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addMUXX1CPLEXConstraints(LL_MuxX1 g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addMUXX1GurobiConstraints(LL_MuxX1 g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // NAND GATE
    public String[] getNANDILPContraints(LL_Nand g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addNANDCPLEXConstraints(LL_Nand g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addNANDGurobiConstraints(LL_Nand g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // NOR GATE
    public String[] getNORILPContraints(LL_Nor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addNORCPLEXConstraints(LL_Nor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addNORGurobiConstraints(LL_Nor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // NOT GATE
    public String[] getNOTILPContraints(LL_Not g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addNOTCPLEXConstraints(LL_Not g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addNOTGurobiConstraints(LL_Not g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // OFF GATE
    public String[] getOFFILPContraints(LL_Off g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addOFFCPLEXConstraints(LL_Off g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addOFFGurobiConstraints(LL_Off g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // ON GATE
    public String[] getONILPContraints(LL_On g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addONCPLEXConstraints(LL_On g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addONGurobiConstraints(LL_On g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // OR GATE
    public String[] getORILPContraints(LL_Or g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addORCPLEXConstraints(LL_Or g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addORGurobiConstraints(LL_Or g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // XNOR GATE
    public String[] getXNORILPContraints(LL_Xnor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addXNORCPLEXConstraints(LL_Xnor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addXNORGurobiConstraints(LL_Xnor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // XOR GATE
    public String[] getXORILPContraints(LL_Xor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values);

    public void addXORCPLEXConstraints(LL_Xor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException;

    public void addXORGurobiConstraints(LL_Xor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException;
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    public default String get_extra_variable_name(LL_Gate g, String circuit_name, int id) {
        return circuit_name + "_" + extra_variable_name() + "[" + g.get_extra_constraint_id(id) + "]";
    }

    public default IloIntVar get_extra_cplex_variable(LL_Gate g, LL_CPLEX_Circuit cp, int id) {
        return cp.extra_wires[g.get_extra_constraint_id(id)];
    }

    public default GRBVar get_extra_gurobi_variable(LL_Gate g, LL_GUROBI_Circuit cp, int id) {
        return cp.extra_wires[g.get_extra_constraint_id(id)];
    }

    public default String get_ILP_prod_string(LL_LargeGate g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        int n = g.inputs.size();
        return "(" + n + "*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                + ")";
    }

    public default String get_ILP_sum_string(LL_LargeGate g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        StringBuilder sum = new StringBuilder();

        boolean first = true;
        for (LL_Wire w : g.inputs) {
            if (first) {
                first = false;
                sum.append("(").append(w.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values));
            } else {
                sum.append("+").append(w.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values));
            }
        }
        sum.append(")");
        return sum.toString();
    }

    /**
     * Returns the summation of all the inputs wires.
     * <br/>
     * If the input_values is not null, and one of the input wires to the gate is of type INPUT, 
     * then the returned expression will be the constant: wire.get_value(input_values). 
     * 
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws IloException
     */
    public default IloNumExpr get_ILP_sum_cplex(LL_LargeGate g, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        IloNumExpr sum = null;

        boolean first = true;
        for (LL_Wire w : g.inputs) {
            if (first) {
                first = false;
                if (w.is_input() && input_values != null) sum = cplex.constant(w.get_value_double(input_values));
                else sum = w.get_ilp_var(cp);
            } else {
                if (w.is_input() && input_values != null)
                    sum = cplex.sum(sum, cplex.constant(w.get_value_double(input_values)));
                else
                    sum = cplex.sum(sum, w.get_ilp_var(cp));
            }
        }
        return sum;
    }

    /**
     * Returns the product of the number of inputs and the output wire.
     * <br/>
     * If the output_values is not null, and the output wire is of type OUTPUT, 
     * then the returned expression will be the constant: n * out.get_value(output_values). 
     * 
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws IloException
     */
    public default IloNumExpr get_ILP_prod_cplex(LL_LargeGate g, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        int n = g.inputs.size();
        IloNumExpr prod = null;
        if (g.out.is_output() && output_values != null)
            prod = cplex.constant(n * g.out.get_value_double(output_values));
        else
            prod = cplex.prod(n, g.out.get_ilp_var(cp));
        return prod;
    }

    /**
     * Returns the summation of all the inputs wires.
     * <br/>
     * If the input_values is not null, and one of the input wires to the gate is of type INPUT, 
     * then the returned expression will be the constant: wire.get_value(input_values). 
     * 
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws GRBException
     */
    public default void get_ILP_sum_gurobi(LL_LargeGate g, LL_GUROBI_Circuit cp, GRBLinExpr expr,
            Boolean[] input_values, Boolean[] output_values, boolean negative) throws GRBException {

        int coeff = 1;
        if (negative) coeff = -1 * coeff;

        for (LL_Wire w : g.inputs) {
            if (w.is_input() && input_values != null) {
                expr.addConstant(coeff * w.get_value_double(input_values));
            } else {
                expr.addTerm(coeff, w.get_gurobi_var(cp));
            }
        }
    }

    /**
     * Returns the product of the number of inputs and the output wire.
     * <br/>
     * If the output_values is not null, and the output wire is of type OUTPUT, 
     * then the returned expression will be the constant: n * out.get_value(output_values). 
     * 
     * @param cp
     * @param cplex
     * @param input_values
     * @param output_values
     * @return
     * @throws GRBException
     */
    public default void get_ILP_prod_gurobi(LL_LargeGate g, LL_GUROBI_Circuit cp, GRBLinExpr expr,
            Boolean[] input_values, Boolean[] output_values, boolean negative) throws GRBException {
        int n = g.inputs.size();
        if (negative) n = -1 * n;

        if (g.out.is_output() && output_values != null) expr.addConstant(n * g.out.get_value_double(output_values));
        else expr.addTerm(n, g.out.get_gurobi_var(cp));
    }
}
