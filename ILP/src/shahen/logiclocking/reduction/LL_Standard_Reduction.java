package shahen.logiclocking.reduction;

import java.util.Collection;
import java.util.logging.Logger;

import gurobi.*;
import ilog.concert.*;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_Circuit;

public class LL_Standard_Reduction implements LL_ReductionI {
    public Logger log;
    public LL_Standard_Reduction(Logger log) {
        this.log = log;
    }

    @Override
    public String get_version() {
        return "v1.0.1";
    }

    @Override
    public String get_name() {
        return "Standard Reduction";
    }
    @Override
    public String toString() {
        return "standard_reduction";
    }

    /** Returns TRUE if extra variables are required to add to the model. */
    public boolean uses_extra_variables() {
        return true;
    }

    /** Returns the number of extra variables required to add to the model. */
    public int set_extra_number_for_gates(Collection<LL_Gate> gates) {
        int num = 0;

        for (LL_Gate g : gates) {
            if (g instanceof LL_Nand || g instanceof LL_Nor || g instanceof LL_Xor || g instanceof LL_Mux1X
                    || g instanceof LL_MuxX1) {
                g.add_extra_constraint_id(num);
                num += 1;
            } else if (g instanceof LL_Xnor || g instanceof LL_Mux) {
                g.add_extra_constraint_id(num);
                num += 1;
                g.add_extra_constraint_id(num);
                num += 1;
            }
        }

        return num;
    }

    /** Returns the string for extra variables */
    public String extra_variable_name() {
        return "extra";
    }

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // AND GATE
    @Override
    public String[] getANDILPContraints(LL_And g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // 0<= in1 + in2 - 2*out <= 1;
        return new String[]{//
                "0 <= " + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) +//
                        " + " + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        " -2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                        + " <= 1;"};
    }

    @Override
    public void addANDCPLEXConstraints(LL_And g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloNumExpr prod = g.out.cplex_prod(2, cp, cplex, input_values, output_values);

        // ___________ 0<= (in1 + in2) - (2*out) <= 1;
        cplex.addRange(0, cplex.diff(sum, prod), 1);
    }

    @Override
    public void addANDGurobiConstraints(LL_And g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {

        GRBLinExpr expr = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, false);
        g.out.gurobi_prod(-2, expr, cp, input_values, output_values);

        // ___________ 0<= (in1 + in2) - (2*out) <= 1;
        if (use_range_gurobi) {
            gurobi.addRange(expr, 0, 1, "AND_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, expr, "AND_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(expr, GRB.LESS_EQUAL, 1, "AND_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // BUF GATE
    @Override
    public String[] getBUFILPContraints(LL_Buf g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out == in1;
        return new String[]{//
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "==" + //
                        g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + ";"//
        };
    }

    @Override
    public void addBUFCPLEXConstraints(LL_Buf g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr in_cplex = g.in1.get_cplex_value(cp, cplex, input_values, output_values);
        IloNumExpr out_cplex = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == in1;
        cplex.addEq(out_cplex, in_cplex);
    }

    @Override
    public void addBUFGurobiConstraints(LL_Buf g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {

        // out == in1;
        gurobi.addConstr(g.in1.get_gurobi_var(cp), GRB.EQUAL, g.out.get_gurobi_var(cp),
                "BUF_GATE_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // Large AND GATE
    @Override
    public String[] getLargeANDILPContraints(LL_Large_And g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        int n = g.inputs.size();
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String prod = get_ILP_prod_string(g, circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{"0 <= " + sum + " - " + prod + " <= " + (n - 1) + ";"};
    }

    @Override
    public void addLargeANDCPLEXConstraints(LL_Large_And g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        int n = g.inputs.size();
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr prod = get_ILP_prod_cplex(g, cp, cplex, input_values, output_values);

        // 0 <= (sum) - (prod) <= n - 1
        cplex.addRange(0, cplex.diff(sum, prod), n - 1);
    }

    @Override
    public void addLargeANDGurobiConstraints(LL_Large_And g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        int n = g.inputs.size();
        GRBLinExpr expr = new GRBLinExpr();
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, false);
        get_ILP_prod_gurobi(g, cp, expr, input_values, output_values, true);

        // 0 <= (sum) - (prod) <= n - 1
        if (use_range_gurobi) {
            gurobi.addRange(expr, 0, n - 1, "LARGE_AND_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, expr, "LARGE_AND_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(expr, GRB.LESS_EQUAL, n - 1,
                    "LARGE_AND_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // Large NAND GATE
    @Override
    public String[] getLargeNANDILPContraints(LL_Large_Nand g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        int n = g.inputs.size();
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String prod = get_ILP_prod_string(g, circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{n + " <= " + sum + " + " + prod + " <= " + (2 * n - 1) + ";"};
    }

    @Override
    public void addLargeNANDCPLEXConstraints(LL_Large_Nand g, LL_CPLEX_Circuit cp, IloCplex cplex,
            Boolean[] input_values, Boolean[] output_values) throws IloException {
        int n = g.inputs.size();
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr prod = get_ILP_prod_cplex(g, cp, cplex, input_values, output_values);

        cplex.addRange(n, cplex.sum(sum, prod), 2 * n - 1);
    }

    @Override
    public void addLargeNANDGurobiConstraints(LL_Large_Nand g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        int n = g.inputs.size();
        GRBLinExpr expr = new GRBLinExpr();
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, false);
        get_ILP_prod_gurobi(g, cp, expr, input_values, output_values, false);

        // n <= (sum) + (prod) <= 2*n - 1
        if (use_range_gurobi) {
            gurobi.addRange(expr, n, 2 * n - 1, "LARGE_NAND_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(n, GRB.LESS_EQUAL, expr,
                    "LARGE_NAND_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(expr, GRB.LESS_EQUAL, 2 * n - 1,
                    "LARGE_NAND_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // Large NOR GATE
    @Override
    public String[] getLargeNORILPContraints(LL_Large_Nor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        int n = g.inputs.size();
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String prod = get_ILP_prod_string(g, circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{"1 <= " + sum + " + " + prod + " <= " + n + ";"};
    }

    @Override
    public void addLargeNORCPLEXConstraints(LL_Large_Nor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        int n = g.inputs.size();
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr prod = get_ILP_prod_cplex(g, cp, cplex, input_values, output_values);

        // 1 <= (sum) + (prod) <= n
        cplex.addRange(1, cplex.sum(sum, prod), n);
    }

    @Override
    public void addLargeNORGurobiConstraints(LL_Large_Nor g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        int n = g.inputs.size();
        GRBLinExpr expr = new GRBLinExpr();
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, false);
        get_ILP_prod_gurobi(g, cp, expr, input_values, output_values, false);

        // 1 <= (sum) + (prod) <= n
        if (use_range_gurobi) {
            gurobi.addRange(expr, 1, n, "LARGE_NOR_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(1, GRB.LESS_EQUAL, expr, "LARGE_NOR_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(expr, GRB.LESS_EQUAL, n, "LARGE_NOR_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // Large OR GATE
    @Override
    public String[] getLargeORILPContraints(LL_Large_Or g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        int n = g.inputs.size();
        String sum = get_ILP_sum_string(g, circuit_name, input_name, key_name, input_values, output_values);
        String prod = get_ILP_prod_string(g, circuit_name, input_name, key_name, input_values, output_values);

        return new String[]{(1 - n) + " <= " + sum + " - " + prod + " <= 0;"};
    }

    @Override
    public void addLargeORCPLEXConstraints(LL_Large_Or g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        int n = g.inputs.size();
        IloNumExpr sum = get_ILP_sum_cplex(g, cp, cplex, input_values, output_values);
        IloNumExpr prod = get_ILP_prod_cplex(g, cp, cplex, input_values, output_values);

        cplex.addRange(1 - n, cplex.diff(sum, prod), 0);
    }

    @Override
    public void addLargeORGurobiConstraints(LL_Large_Or g, LL_GUROBI_Circuit cp, GRBModel gurobi,
            Boolean[] input_values, Boolean[] output_values) throws GRBException {
        int n = g.inputs.size();
        GRBLinExpr expr = new GRBLinExpr();
        get_ILP_sum_gurobi(g, cp, expr, input_values, output_values, false);
        get_ILP_prod_gurobi(g, cp, expr, input_values, output_values, true);

        // 1-n <= (sum) - (prod) <= 0
        if (use_range_gurobi) {
            gurobi.addRange(expr, 1 - n, 0, "LARGE_OR_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(1 - n, GRB.LESS_EQUAL, expr,
                    "LARGE_OR_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(expr, GRB.LESS_EQUAL, 0, "LARGE_OR_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUX GATE
    @Override
    public String[] getMUXILPContraints(LL_Mux g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // -1<= in1 - s - 2*ex1 <= 0
                "-1<=" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-" + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-2*" + get_extra_variable_name(g, circuit_name, 0) + "<=0;",//
                // 0<= (in2 + s) - (2*ex2) <= 1
                "0<=" + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "+" + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-2*" + get_extra_variable_name(g, circuit_name, 1) + "<=1;",//
                // 0<= ((2*out) - (ex1 + ex2)) <= 1
                "0<=2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-"
                        + get_extra_variable_name(g, circuit_name, 0) + "-"
                        + get_extra_variable_name(g, circuit_name, 1) + "<=1;"//
        };
    }

    @Override
    public void addMUXCPLEXConstraints(LL_Mux g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloIntVar ex1 = get_extra_cplex_variable(g, cp, 0);
        IloIntVar ex2 = get_extra_cplex_variable(g, cp, 1);

        // -1<= (((in1 - s) - (2*ex1)) <= 0
        cplex.addRange(-1, //
                cplex.diff(//
                        cplex.diff(//
                                g.in1.get_cplex_value(cp, cplex, input_values, output_values),//
                                g.s.get_cplex_value(cp, cplex, input_values, output_values)),//
                        cplex.prod(2, ex1)//
                ), 0);

        // 0<= (in2 + s) - (2*ex2) <= 1
        cplex.addRange(0, //
                cplex.sum(//
                        g.in2.cplex_sum(g.s, cp, cplex, input_values, output_values),//
                        cplex.prod(-2, ex2)//
                ), 1);

        // 0<= ((2*out) - (ex1 + ex2)) <= 1
        cplex.addRange(0, //
                cplex.diff(//
                        cplex.prod(2, g.out.get_cplex_value(cp, cplex, input_values, output_values)),//
                        cplex.sum(ex1, ex2)//
                ), 1);
    }

    @Override
    public void addMUXGurobiConstraints(LL_Mux g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in1 = g.in1.get_gurobi_var(cp);
        GRBVar in2 = g.in2.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBVar ex1 = get_extra_gurobi_variable(g, cp, 0);
        GRBVar ex2 = get_extra_gurobi_variable(g, cp, 1);

        // -1 <= in1 - s - 2*ex1 <= 0
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addTerm(1, in1);
        e1.addTerm(-1, s);
        e1.addTerm(-2, ex1);
        if (use_range_gurobi) {
            gurobi.addRange(e1, -1, 0, "MUX_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(-1, GRB.LESS_EQUAL, e1, "MUX_GATE_a_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 0, "MUX_GATE_a_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

        // 0 <= in2 + s -2*ex2 <= 1
        e1 = new GRBLinExpr();
        e1.addTerm(1, in2);
        e1.addTerm(1, s);
        e1.addTerm(-2, ex2);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "MUX_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "MUX_GATE_b_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "MUX_GATE_b_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

        // 0 <= 2*out -ex1 -ex2 <= 1
        e1 = new GRBLinExpr();
        e1.addTerm(2, out);
        e1.addTerm(-1, ex1);
        e1.addTerm(-1, ex2);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "MUX_GATE_c_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "MUX_GATE_c_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "MUX_GATE_c_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUX0X GATE
    @Override
    public String[] getMUX0XILPContraints(LL_Mux0X g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // 0<= (in2 + s) - (2*out) <= 1
                "0<=" + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "+" + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                        + "<=1;",//
        };
    }

    @Override
    public void addMUX0XCPLEXConstraints(LL_Mux0X g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        // 0<= (in2 + s) - (2*ex2) <= 1
        cplex.addRange(0, //
                cplex.sum(//
                        g.in2.cplex_sum(g.s, cp, cplex, input_values, output_values),//
                        cplex.prod(-2, g.out.get_cplex_value(cp, cplex, input_values, output_values))//
                ), 1);
    }

    @Override
    public void addMUX0XGurobiConstraints(LL_Mux0X g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in2 = g.in2.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);

        // 0 <= in2 + s -2*out <= 1
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addTerm(1, in2);
        e1.addTerm(1, s);
        e1.addTerm(-2, out);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "MUX0X_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "MUX0X_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "MUX0X_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUX1X GATE
    @Override
    public String[] getMUX1XILPContraints(LL_Mux1X g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // 0<= (in2 + s) - (2*ex1) <= 1
                "0<=" + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "+" + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-2*" + get_extra_variable_name(g, circuit_name, 0) + "<=1;",//
                // 0<= 2*out -1 +s -ex1 <= 1
                "0<=2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-1+"
                        + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-"
                        + get_extra_variable_name(g, circuit_name, 0) + "<=1;"//
        };
    }

    @Override
    public void addMUX1XCPLEXConstraints(LL_Mux1X g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloIntVar ex1 = get_extra_cplex_variable(g, cp, 0);

        // 0<= (in2 + s) - (2*ex1) <= 1
        cplex.addRange(0, //
                cplex.sum(//
                        g.in2.cplex_sum(g.s, cp, cplex, input_values, output_values),//
                        cplex.prod(-2, ex1)//
                ), 1);

        // 0<= ((2*out) - (ex1 + (1 - s))) <= 1
        cplex.addRange(0, //
                cplex.diff(//
                        cplex.prod(2, g.out.get_cplex_value(cp, cplex, input_values, output_values)),//
                        cplex.sum(ex1, //
                                cplex.diff(1, g.s.get_cplex_value(cp, cplex, input_values, output_values)))//
                ), 1);
    }

    @Override
    public void addMUX1XGurobiConstraints(LL_Mux1X g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in2 = g.in2.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBVar ex1 = get_extra_gurobi_variable(g, cp, 0);

        // 0 <= in2 + s -2*ex1 <= 1
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addTerm(1, in2);
        e1.addTerm(1, s);
        e1.addTerm(-2, ex1);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "MUX1X_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "MUX1X_GATE_a_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "MUX1X_GATE_a_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

        // 0 <= 2*out -ex1 -1 +s <= 1
        e1 = new GRBLinExpr();
        e1.addTerm(2, out);
        e1.addTerm(-1, ex1);
        e1.addConstant(-1);
        e1.addTerm(1, s);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "MUX1X_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "MUX1X_GATE_b_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "MUX1X_GATE_b_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUXX0 GATE
    @Override
    public String[] getMUXX0ILPContraints(LL_MuxX0 g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // -1<= in1 - s - 2*out <= 0
                "-1<=" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-" + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                        + "<=0;",//
        };
    }

    @Override
    public void addMUXX0CPLEXConstraints(LL_MuxX0 g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        // -1<= (((in1 - s) - (2*out)) <= 0
        cplex.addRange(-1, //
                cplex.diff(//
                        cplex.diff(//
                                g.in1.get_cplex_value(cp, cplex, input_values, output_values),//
                                g.s.get_cplex_value(cp, cplex, input_values, output_values)),//
                        cplex.prod(2, g.out.get_cplex_value(cp, cplex, input_values, output_values))//
                ), 0);
    }

    @Override
    public void addMUXX0GurobiConstraints(LL_MuxX0 g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in1 = g.in1.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);

        // -1 <= in1 - s - 2*out <= 0
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addTerm(1, in1);
        e1.addTerm(-1, s);
        e1.addTerm(-2, out);
        if (use_range_gurobi) {
            gurobi.addRange(e1, -1, 0, "MUXX0_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(-1, GRB.LESS_EQUAL, e1, "MUXX0_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 0, "MUXX0_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // MUXX1 GATE
    @Override
    public String[] getMUXX1ILPContraints(LL_MuxX1 g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // -1<= in1 - s - 2*ex1 <= 0
                "-1<=" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-" + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-2*" + get_extra_variable_name(g, circuit_name, 0) + "<=0;",//
                // 0<= 2*out -ex1 -s <= 1
                "0<=2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-"
                        + get_extra_variable_name(g, circuit_name, 0) + "-"
                        + g.s.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "<=1;"//
        };
    }

    @Override
    public void addMUXX1CPLEXConstraints(LL_MuxX1 g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloIntVar ex1 = get_extra_cplex_variable(g, cp, 0);

        // -1<= (((in1 - s) - (2*ex1)) <= 0
        cplex.addRange(-1, //
                cplex.diff(//
                        cplex.diff(//
                                g.in1.get_cplex_value(cp, cplex, input_values, output_values),//
                                g.s.get_cplex_value(cp, cplex, input_values, output_values)),//
                        cplex.prod(2, ex1)//
                ), 0);

        // 0<= ((2*out) - (ex1 + s)) <= 1
        cplex.addRange(0, //
                cplex.diff(//
                        cplex.prod(2, g.out.get_cplex_value(cp, cplex, input_values, output_values)),//
                        cplex.sum(ex1, g.s.get_cplex_value(cp, cplex, input_values, output_values))//
                ), 1);
    }

    @Override
    public void addMUXX1GurobiConstraints(LL_MuxX1 g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);
        GRBVar in1 = g.in1.get_gurobi_var(cp);
        GRBVar s = g.s.get_gurobi_var(cp);
        GRBVar ex1 = get_extra_gurobi_variable(g, cp, 0);

        // -1 <= in1 - s - 2*ex1 <= 0
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addTerm(1, in1);
        e1.addTerm(-1, s);
        e1.addTerm(-2, ex1);
        if (use_range_gurobi) {
            gurobi.addRange(e1, -1, 0, "MUXX1_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(-1, GRB.LESS_EQUAL, e1, "MUXX1_GATE_a_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 0, "MUXX1_GATE_a_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

        // 0 <= 2*out -ex1 -s <= 1
        e1 = new GRBLinExpr();
        e1.addTerm(2, out);
        e1.addTerm(-1, ex1);
        e1.addTerm(-1, s);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "MUXX1_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "MUXX1_GATE_b_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "MUXX1_GATE_b_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // NAND GATE
    @Override
    public String[] getNANDILPContraints(LL_Nand g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // 0 <= in1 + in2 - 2*z <= 1
                "0<=" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "+" + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-2*" + get_extra_variable_name(g, circuit_name, 0) + "<=1;",//
                // out == 1 - z
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "==1-"
                        + get_extra_variable_name(g, circuit_name, 0) + ";"//
        };
    }

    @Override
    public void addNANDCPLEXConstraints(LL_Nand g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloIntVar z = get_extra_cplex_variable(g, cp, 0);

        // 0 <= (in1 + in2) - 2*z <= 1
        cplex.addRange(0, //
                cplex.diff(//
                        sum,//
                        cplex.prod(2, z)//
                ), 1);
        // out == 1 - z
        cplex.addEq(//
                g.out.get_cplex_value(cp, cplex, input_values, output_values),//
                cplex.diff(//
                        1,//
                        z//
                )//
        );
    }

    @Override
    public void addNANDGurobiConstraints(LL_Nand g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar z = get_extra_gurobi_variable(g, cp, 0);

        // 0 <= in1 + in2 - 2*z <= 1
        GRBLinExpr e1 = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, e1, cp, input_values, output_values, false);
        e1.addTerm(-2, z);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "NAND_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "NAND_GATE_a_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "NAND_GATE_a_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

        // out == 1 - z
        GRBLinExpr e2 = new GRBLinExpr();
        e2.addConstant(1);
        e2.addTerm(-1, z);
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, e2,
                "NAND_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // NOR GATE
    @Override
    public String[] getNORILPContraints(LL_Nor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // 0 <= 2 * z - in1 - in2 <= 1
                "0<=" + "2*" + get_extra_variable_name(g, circuit_name, 0) + //
                        "-" + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        "-" + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values)
                        + "<=1;",//
                // out == 1 - z
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "==1-"
                        + get_extra_variable_name(g, circuit_name, 0) + ";"//
        };
    }

    @Override
    public void addNORCPLEXConstraints(LL_Nor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloIntVar z = get_extra_cplex_variable(g, cp, 0);

        // 0 <= ((2 * z) - (in1 + in2)) <= 1
        cplex.addRange(//
                0, //
                cplex.diff(//
                        cplex.prod(2, z),//
                        sum),//
                1);
        // out == 1 - z
        cplex.addEq(//
                g.out.get_cplex_value(cp, cplex, input_values, output_values),//
                cplex.diff(1, z)//
        );
    }

    @Override
    public void addNORGurobiConstraints(LL_Nor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar z = get_extra_gurobi_variable(g, cp, 0);

        // 0 <= 2*z + -in1 + -in2 <= 1
        GRBLinExpr e1 = new GRBLinExpr();
        e1.addTerm(2, z);
        g.in1.gurobi_sum(g.in2, e1, cp, input_values, output_values, true);
        if (use_range_gurobi) {
            gurobi.addRange(e1, 0, 1, "NOR_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, e1, "NOR_GATE_a_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(e1, GRB.LESS_EQUAL, 1, "NOR_GATE_a_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }

        // out == 1 - z
        GRBLinExpr e2 = new GRBLinExpr();
        e2.addConstant(1);
        e2.addTerm(-1, z);
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, e2,
                "NOR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // NOT GATE
    @Override
    public String[] getNOTILPContraints(LL_Not g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out == 1 - in1;
        return new String[]{//
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        " == 1 - " + //
                        g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + ";"//
        };
    }

    @Override
    public void addNOTCPLEXConstraints(LL_Not g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == 1 - in1;
        if (g.in1.is_input() && input_values != null) cplex.addEq(out, 1 - g.in1.get_value_double(input_values));
        if (g.in1.is_output() && output_values != null) cplex.addEq(out, 1 - g.in1.get_value_double(output_values));
        else cplex.addEq(out, cplex.diff(1, g.in1.get_ilp_var(cp)));
    }

    @Override
    public void addNOTGurobiConstraints(LL_Not g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar out = g.out.get_gurobi_var(cp);

        // out == 1 - in1;
        if (g.in1.is_input() && input_values != null)
            gurobi.addConstr(out, GRB.EQUAL, 1 - g.in1.get_value_double(input_values),
                    "NOT_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        if (g.in1.is_output() && output_values != null)
            gurobi.addConstr(out, GRB.EQUAL, 1 - g.in1.get_value_double(output_values),
                    "NOT_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        else {
            GRBLinExpr e2 = new GRBLinExpr();
            e2.addConstant(1);
            e2.addTerm(-1, g.in1.get_gurobi_var(cp));
            gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, e2,
                    "NOT_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // OFF GATE
    @Override
    public String[] getOFFILPContraints(LL_Off g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out == 0;
        return new String[]{
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + " == 0;"};
    }

    @Override
    public void addOFFCPLEXConstraints(LL_Off g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == 0;
        cplex.addEq(out, 0);
    }

    @Override
    public void addOFFGurobiConstraints(LL_Off g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, 0,
                "OFF_GATE_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // ON GATE
    @Override
    public String[] getONILPContraints(LL_On g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out == 1;
        return new String[]{
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + " == 1;"};
    }

    @Override
    public void addONCPLEXConstraints(LL_On g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr out = g.out.get_cplex_value(cp, cplex, input_values, output_values);

        // out == 1;
        cplex.addEq(out, 1);
    }

    @Override
    public void addONGurobiConstraints(LL_On g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, 1, "ON_GATE_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // OR GATE
    @Override
    public String[] getORILPContraints(LL_Or g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // 0<= 2*out - in1 - in2 <= 1;
        return new String[]{//
                "0 <= 2*" + g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        " - " + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        " - " + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + //
                        " <= 1;"//
        };
    }

    @Override
    public void addORCPLEXConstraints(LL_Or g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloNumExpr prod = g.out.cplex_prod(2, cp, cplex, input_values, output_values);

        // 0<= ((2*out) - (in1 + in2)) <= 1;
        cplex.addRange(0, cplex.diff(prod, sum), 1);
    }

    @Override
    public void addORGurobiConstraints(LL_Or g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBLinExpr expr = new GRBLinExpr();
        g.out.gurobi_prod(2, expr, cp, input_values, output_values);
        g.in1.gurobi_sum(g.in2, expr, cp, input_values, output_values, true);

        // 0<= (2*out) + -in1 + -in2 <= 1;
        if (use_range_gurobi) {
            gurobi.addRange(expr, 0, 1, "OR_GATE_" + cp.circuit_name + "_" + g.out.get_name());
        } else {
            gurobi.addConstr(0, GRB.LESS_EQUAL, expr, "OR_GATE_lhs_" + cp.circuit_name + "_" + g.out.get_name());
            gurobi.addConstr(expr, GRB.LESS_EQUAL, 1, "OR_GATE_rhs_" + cp.circuit_name + "_" + g.out.get_name());
        }
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // XNOR GATE
    @Override
    public String[] getXNORILPContraints(LL_Xnor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        return new String[]{//
                // z == in1 + in2 -2*t
                get_extra_variable_name(g, circuit_name, 0) + "=="
                        + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "+"//
                        + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-2*"
                        + get_extra_variable_name(g, circuit_name, 1) + ";",//
                // out == 1 - z
                g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "==1-"
                        + get_extra_variable_name(g, circuit_name, 0) + ";"//
        };
    }

    @Override
    public void addXNORCPLEXConstraints(LL_Xnor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloIntVar z = get_extra_cplex_variable(g, cp, 0);
        IloIntVar t = get_extra_cplex_variable(g, cp, 1);

        // z == in1 + in2 -2*t
        cplex.addEq(z, //
                cplex.diff(//
                        sum,//
                        cplex.prod(2, t)//
                ));
        // out == 1 - z
        cplex.addEq(//
                g.out.get_cplex_value(cp, cplex, input_values, output_values),//
                cplex.diff(//
                        1,//
                        z//
                )//
        );
    }

    @Override
    public void addXNORGurobiConstraints(LL_Xnor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar z = get_extra_gurobi_variable(g, cp, 0);
        GRBVar t = get_extra_gurobi_variable(g, cp, 1);

        // z == in1 + in2 -2*t
        GRBLinExpr e1 = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, e1, cp, input_values, output_values, false);
        e1.addTerm(-2, t);
        gurobi.addConstr(z, GRB.EQUAL, e1, "XNOR_GATE_a_" + cp.circuit_name + "_" + g.out.get_name());

        // out == 1 - z
        GRBLinExpr e2 = new GRBLinExpr();
        e2.addConstant(1);
        e2.addTerm(-1, z);
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, e2,
                "XNOR_GATE_b_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////
    // XOR GATE
    @Override
    public String[] getXORILPContraints(LL_Xor g, String circuit_name, String input_name, String key_name,
            Boolean[] input_values, Boolean[] output_values) {
        // out == in1 + in2 - 2*t
        // t is a unique variable created for each XOR and XNOR gates
        return new String[]{g.out.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "=="
                + g.in1.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "+"
                + g.in2.get_ilp_name(circuit_name, input_name, key_name, input_values, output_values) + "-2*"
                + get_extra_variable_name(g, circuit_name, 0) + ";"};
    }

    @Override
    public void addXORCPLEXConstraints(LL_Xor g, LL_CPLEX_Circuit cp, IloCplex cplex, Boolean[] input_values,
            Boolean[] output_values) throws IloException {
        IloNumExpr sum = g.in1.cplex_sum(g.in2, cp, cplex, input_values, output_values);
        IloIntVar z = get_extra_cplex_variable(g, cp, 0);

        // out == (in1 + in2) - (2*z)
        // z is a unique variable created for each XOR and XNOR gates
        cplex.addEq(g.out.get_cplex_value(cp, cplex, input_values, output_values), //
                cplex.diff(//
                        sum,//
                        cplex.prod(2, z)//
                ));
    }

    @Override
    public void addXORGurobiConstraints(LL_Xor g, LL_GUROBI_Circuit cp, GRBModel gurobi, Boolean[] input_values,
            Boolean[] output_values) throws GRBException {
        GRBVar z = get_extra_gurobi_variable(g, cp, 0);
        // log.fine("[addXORGurobi] Extra Variable: " + z);

        // out == in1 + in2 + -2*z
        GRBLinExpr e1 = new GRBLinExpr();
        g.in1.gurobi_sum(g.in2, e1, cp, input_values, output_values, false);
        e1.addTerm(-2, z);
        gurobi.addConstr(g.out.get_gurobi_var(cp), GRB.EQUAL, e1,
                "XOR_GATE_" + cp.circuit_name + "_" + g.out.get_name());
    }
    /////////////////////////////////////////////////////////////////////

}
