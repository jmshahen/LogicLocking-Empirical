package shahen.logiclocking.results;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.logging.Logger;

import shahen.file.FileHelper;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.gates.LL_Gate;

public class LL_Circuit_Stats {
    public static final Logger log = Logger.getLogger("shahen");

    private static String sqlSelectFileHash = "SELECT * FROM Circuit WHERE fileHash = ?";

    public String fileHash = null;
    public String filepath = null;
    public String folder = null;
    /** One folder above the {@link #folder} */
    public String folder_parent = null;
    public String filename = null;
    public String filename_noext = null;

    public Integer numTotalWires = null;
    public Integer numInputWires = null;
    public Integer numKeyWires = null;
    public Integer numOutputWires = null;
    public Integer numInternalWires = null;

    public Integer numTotalGates = null;
    public Integer numBufGates = null;
    public Integer numNotGates = null;

    public Integer numTotalAndGates = null;
    public Integer numTotalOrGates = null;
    public Integer numTotalNandGates = null;
    public Integer numTotalNorGates = null;

    public Integer numLargeAndGates = null;
    public Integer numLargeOrGates = null;
    public Integer numLargeNandGates = null;
    public Integer numLargeNorGates = null;

    public Integer numSmallAndGates = null;
    public Integer numSmallOrGates = null;
    public Integer numSmallNandGates = null;
    public Integer numSmallNorGates = null;

    public Integer numXorGates = null;
    public Integer numXnorGates = null;
    public Integer numMuxGates = null;

    /**
     * Creates a new LL_Circuit_Stats class. 
     * 
     * If <code>try_load</code> is TRUE, then an attempt will be made to populated the class via the database.
     * 
     *  If the class is unable to load the circuit stats via the database, then we populate the class via the circuit, 
     *  and then save the circuit stats to the database.
     * 
     * @param circuit
     * @param conn
     * @param try_load if TRUE, then data will be loaded from the database instead of calculating it
     * @throws SQLException 
     */
    public LL_Circuit_Stats(LL_Circuit circuit, Connection conn, boolean try_load) throws SQLException {
        attachToFile(circuit.filepath);

        boolean load_result = false;
        if (try_load) {
            load_result = load(conn);
        }

        if (!load_result) {
            extract_stats(circuit);
            save(conn);
        }
    }
    public LL_Circuit_Stats(String fileHash, LL_Circuit circuit, Connection conn) {
        this.fileHash = fileHash;
        this.filepath = "none";
        this.folder = "none";
        this.folder_parent = "none";
        this.filename = "none";
        this.filename_noext = "none";

        if (!load(conn)) {
            extract_stats(circuit);
        }
    }

    /**
     * 
     * @param circuitFile
     * @return TRUE if no errors have occurred, False otherwise
     */
    public boolean attachToFile(String circuitFile) {
        boolean result = true;
        try {
            File f = new File(circuitFile);
            if (f.exists()) {
                fileHash = FileHelper.sha256(f);
                filepath = f.getAbsolutePath();

                File parent = f.getParentFile();
                File parent_parent = (parent == null) ? null : parent.getParentFile();

                this.folder = (parent == null) ? "none" : parent.getName();
                this.folder_parent = (parent_parent == null) ? "none" : parent_parent.getName();
                this.filename = f.getName();
                this.filename_noext = filename.substring(0, filename.lastIndexOf('.'));

            } else {
                result = false;
            }
        } catch (NoSuchAlgorithmException | IOException e) {
            result = false;
        }
        return result;
    }

    /**
     * 
     * @param c
     * @return TRUE if no errors have occurred, False otherwise
     */
    public boolean extract_stats(LL_Circuit c) {
        if (c == null) return false;
        boolean result = true;

        numInputWires = c.inputSize();
        numInternalWires = c.internalSize();
        numKeyWires = c.keySize();
        numOutputWires = c.outputSize();
        numTotalWires = numInputWires + numInternalWires + numKeyWires + numOutputWires;

        numBufGates = 0;
        numNotGates = 0;

        numTotalAndGates = 0;
        numTotalOrGates = 0;
        numTotalNandGates = 0;
        numTotalNorGates = 0;

        numLargeAndGates = 0;
        numLargeOrGates = 0;
        numLargeNandGates = 0;
        numLargeNorGates = 0;

        numSmallAndGates = 0;
        numSmallOrGates = 0;
        numSmallNandGates = 0;
        numSmallNorGates = 0;

        numXorGates = 0;
        numXnorGates = 0;
        numMuxGates = 0;
        numTotalGates = c.gates.size();

        for (LL_Gate g : c.gates.values()) {
            switch (g.gate_name()) {
                case "buf" :
                    numBufGates++;
                    break;
                case "not" :
                    numNotGates++;
                    break;
                case "and" :
                    numTotalAndGates++;
                    if (g.numInputs() > 2) numLargeAndGates++;
                    else numSmallAndGates++;
                    break;
                case "or" :
                    numTotalOrGates++;
                    if (g.numInputs() > 2) numLargeOrGates++;
                    else numSmallOrGates++;
                    break;
                case "nand" :
                    numTotalNandGates++;
                    if (g.numInputs() > 2) numLargeNandGates++;
                    else numSmallNandGates++;
                    break;
                case "nor" :
                    numTotalNorGates++;
                    if (g.numInputs() > 2) numLargeNorGates++;
                    else numSmallNorGates++;
                    break;
                case "xor" :
                    numXorGates++;
                    break;
                case "xnor" :
                    numXnorGates++;
                    break;
                case "mux" :
                    numMuxGates++;
                    break;
                default :
                    log.severe("Unknown Gate Type: " + g.gate_name() + "; gate: " + g);
            }

        }

        return result;
    }

    public ResultSet getRowFromFileHash(Connection conn, String sqlSelectCircuitByFileHash) {
        try {
            PreparedStatement pstmt = conn.prepareStatement(sqlSelectCircuitByFileHash);
            pstmt.setString(1, fileHash);
            ResultSet rs = pstmt.executeQuery();

            // Check if the ResultSet is empty
            if (!rs.isBeforeFirst()) { return null; }
            return rs;
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Tries to load the circuit stats from an active SQL connection, 
     * using sql: "SELECT * FROM Circuit WHERE fileHash = ?"
     * @param conn
     * @return
     */
    public boolean load(Connection conn) {
        return load(conn, sqlSelectFileHash);
    }

    /**
     * Tries to load the circuit stats from an active SQL connection
     * It uses the supplied SQL, and will give the prepared statement {@link LL_Circuit_Stats#fileHash}
     * 
     * @param conn
     * @param sqlSelectCircuitByFileHash
     * @return
     */
    public boolean load(Connection conn, String sqlSelectCircuitByFileHash) {
        try {
            if (conn == null) return false;
            if (sqlSelectCircuitByFileHash == null || sqlSelectCircuitByFileHash.isEmpty()) return false;

            ResultSet rs = getRowFromFileHash(conn, sqlSelectCircuitByFileHash);
            if (rs == null) { return false; }

            filepath = rs.getString("filePath");
            folder_parent = rs.getString("folder_parent");
            folder = rs.getString("folder");
            filename = rs.getString("filename");
            filename_noext = rs.getString("filename_noext");

            numTotalWires = rs.getInt("numTotalWires");
            numInputWires = rs.getInt("numInputWires");
            numKeyWires = rs.getInt("numKeyWires");
            numOutputWires = rs.getInt("numOutputWires");
            numInternalWires = rs.getInt("numInternalWires");

            numTotalGates = rs.getInt("numTotalGates");
            numBufGates = rs.getInt("numBufGates");
            numNotGates = rs.getInt("numNotGates");

            numTotalAndGates = rs.getInt("numTotalAndGates");
            numTotalOrGates = rs.getInt("numTotalOrGates");
            numTotalNandGates = rs.getInt("numTotalNandGates");
            numTotalNorGates = rs.getInt("numTotalNorGates");

            numLargeAndGates = rs.getInt("numLargeAndGates");
            numLargeOrGates = rs.getInt("numLargeOrGates");
            numLargeNandGates = rs.getInt("numLargeNandGates");
            numLargeNorGates = rs.getInt("numLargeNorGates");

            numSmallAndGates = rs.getInt("numSmallAndGates");
            numSmallOrGates = rs.getInt("numSmallOrGates");
            numSmallNandGates = rs.getInt("numSmallNandGates");
            numSmallNorGates = rs.getInt("numSmallNorGates");

            numXorGates = rs.getInt("numXorGates");
            numXnorGates = rs.getInt("numXnorGates");
            numMuxGates = rs.getInt("numMuxGates");

            return true;
        } catch (SQLException e) {
            log.severe(e.toString());
            return false;
        }
    }

    public boolean save(Connection conn) throws SQLException {
        if (fileHash == null) {
            log.severe("[ERROR] Unable to save LL_Circuit_Stats if there is no fileHash");
            return false;
        }
        if (conn == null) {
            log.severe("[ERROR] Connection cannot be null");
            return false;
        }

        String sql = sqlUpdateCircuit;

        ResultSet rs = getRowFromFileHash(conn, sqlSelectFileHash);
        if (rs == null) {
            sql = sqlInsertCircuit;
        }

        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, filepath);
        pstmt.setString(2, folder_parent);
        pstmt.setString(3, folder);
        pstmt.setString(4, filename);
        pstmt.setString(5, filename_noext);
        pstmt.setInt(6, numTotalWires);
        pstmt.setInt(7, numInputWires);
        pstmt.setInt(8, numKeyWires);
        pstmt.setInt(9, numOutputWires);
        pstmt.setInt(10, numInternalWires);
        pstmt.setInt(11, numTotalGates);
        pstmt.setInt(12, numBufGates);
        pstmt.setInt(13, numNotGates);
        pstmt.setInt(14, numTotalAndGates);
        pstmt.setInt(15, numTotalOrGates);
        pstmt.setInt(16, numTotalNandGates);
        pstmt.setInt(17, numTotalNorGates);
        pstmt.setInt(18, numSmallAndGates);
        pstmt.setInt(19, numSmallOrGates);
        pstmt.setInt(20, numSmallNandGates);
        pstmt.setInt(21, numSmallNorGates);
        pstmt.setInt(22, numLargeAndGates);
        pstmt.setInt(23, numLargeOrGates);
        pstmt.setInt(24, numLargeNandGates);
        pstmt.setInt(25, numLargeNorGates);
        pstmt.setInt(26, numXorGates);
        pstmt.setInt(27, numXnorGates);
        pstmt.setInt(28, numMuxGates);
        pstmt.setString(29, fileHash);
        if (pstmt.executeUpdate() == 1) {
            // conn.commit();
            return true;
        } else {
            return false;
        }
    }

    private String sqlUpdateCircuit = "UPDATE Circuit\n" + //
            "   SET \n" + //
            "filePath = ?,\n" + //
            "folder_parent = ?,\n" + //
            "folder = ?,\n" + //
            "filename = ?,\n" + //
            "filename_noext = ?,\n" +//
            "numTotalWires = ?,\n" + //
            "numInputWires = ?,\n" + //
            "numKeyWires = ?,\n" + //
            "numOutputWires = ?,\n" +//
            "numInternalWires = ?,\n" +//
            "numTotalGates = ?,\n" + //
            "numBufGates = ?,\n" + //
            "numNotGates = ?,\n" + //
            "numTotalAndGates = ?,\n" +//
            "numTotalOrGates = ?,\n" + //
            "numTotalNandGates = ?,\n" + //
            "numTotalNorGates = ?,\n" + //
            "numSmallAndGates = ?,\n" + //
            "numSmallOrGates = ?,\n" + //
            "numSmallNandGates = ?,\n" + //
            "numSmallNorGates = ?,\n" + //
            "numLargeAndGates = ?,\n" + //
            "numLargeOrGates = ?,\n" + //
            "numLargeNandGates = ?,\n" + //
            "numLargeNorGates = ?,\n" + //
            "numXorGates = ?,\n" + //
            "numXnorGates = ?,\n" + //
            "numMuxGates = ?\n" + //
            " WHERE fileHash = ?;";

    /** fileHash must be the LAST element */
    private String sqlInsertCircuit = "INSERT INTO Circuit (\n" + //
            "filePath,\n" + //
            "folder_parent,\n" + //
            "folder,\n" + //
            "filename,\n" + //
            "filename_noext,\n" + //
            "numTotalWires,\n" + //
            "numInputWires,\n" + //
            "numKeyWires,\n" + //
            "numOutputWires,\n" +//
            "numInternalWires,\n" +//
            "numTotalGates,\n" + //
            "numBufGates,\n" + //
            "numNotGates,\n" + //
            "numTotalAndGates,\n" +//
            "numTotalOrGates,\n" + //
            "numTotalNandGates,\n" + //
            "numTotalNorGates,\n" + //
            "numSmallAndGates,\n" + //
            "numSmallOrGates,\n" + //
            "numSmallNandGates,\n" + //
            "numSmallNorGates,\n" + //
            "numLargeAndGates,\n" + //
            "numLargeOrGates,\n" + //
            "numLargeNandGates,\n" + //
            "numLargeNorGates,\n" + //
            "numXorGates,\n" + //
            "numXnorGates,\n" + //
            "numMuxGates,\n" + //
            "fileHash)\n" + //
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
}
