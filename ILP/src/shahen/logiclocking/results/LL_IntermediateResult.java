package shahen.logiclocking.results;

import java.sql.Timestamp;

import shahen.string.ShahenStrings;

public class LL_IntermediateResult {
    /** This is auto incremented by the sql server, and only set after this LL_IntermediateResult is stored */
    public Integer idIntermediateResult;

    public Integer idResult;

    // RESULT
    public Integer result;
    public String resultString;
    public Integer verifyStep;
    public Integer correctKey;

    // PLACEMENT
    /**
     * What stage is this result:
     * <ul>
     * <li>1 first stage (see LL_CPLEX_Solver_v1)
     * <li>2 iter stage (see LL_CPLEX_Solver_v1)
     * <li>3 last stage (see LL_CPLEX_Solver_v1)
     * <li>4 new key
     * <li>5 new distinguishing input
     * <li>6 non-standard
     * </ul>
     */
    public Integer stage;
    public String stageString;
    /** The order that the the solver is called (cplex/gurobi) */
    public Integer iterNum;
    public Integer keyIterNum;
    public Integer inputIterNum;

    // STATS
    public Integer numSolverConstraints;
    public Integer numSolverVariables;
    public Integer numCircuits;

    // TIMING
    public Timestamp dateStart;
    public Timestamp dateFinish;
    public Long durationMilliseconds;
    public String durationPretty;

    // EXTRA
    public String inputKeyJSON;

    public LL_IntermediateResult(LL_Result r) {
        idResult = r.idResult;
    }

    public void start() {
        dateStart = new Timestamp(System.currentTimeMillis());
    }
    public void finish() {
        dateFinish = new Timestamp(System.currentTimeMillis());
        durationMilliseconds = dateFinish.getTime() - dateStart.getTime();
        durationPretty = ShahenStrings.prettyDuration(durationMilliseconds);
    }
    public static String insert_sql = "INSERT INTO\n" + //
            "  IntermediateResult (\n" + //
            "    idResult,\n" + //
            "    result,\n" + //
            "    resultString,\n" + //
            "    verifyStep,\n" + //
            "    correctKey,\n" + //
            "    stage,\n" + //
            "    stageString,\n" + //
            "    iterNum,\n" + //
            "    keyIterNum,\n" + //
            "    inputIterNum,\n" + //
            "    numSolverConstraints,\n" + //
            "    numSolverVariables,\n" + //
            "    numCircuits,\n" + //
            "    dateStart,\n" + //
            "    dateFinish,\n" + //
            "    durationMilliseconds,\n" + //
            "    durationPretty,\n" + //
            "    inputKeyJSON\n" + //
            "  )\n" + //
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
}
