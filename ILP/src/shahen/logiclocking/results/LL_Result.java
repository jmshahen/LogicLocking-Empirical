package shahen.logiclocking.results;

import java.sql.Timestamp;
import java.util.ArrayList;

import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.solver.LL_SolverI;
import shahen.string.ShahenStrings;

public class LL_Result {
    /** This is auto incremented by the sql server, and only set after this LL_Result is stored */
    public Integer idResult;
    public String fileHash;

    /**
     * Can be the values:
     * <ul>
     * <li> 0  : Key NOT Found
     * <li> 1  : Key Found
     * <li> -1 : Timeout
     * <li> -2 : Forcibly Ended
     * </ul>
     */
    public Integer result;
    public String resultString;
    /** 1 if the correct key was found; 0 if another key was found (could still be correct) */
    public Integer correctKey;

    // Options and Version Numbers
    public String ilpInstanceVersion;
    public String ilpSolver;
    public String ilpSolverVersion;
    public String reduction;
    public String reductionVersion;

    public Integer lastNumConstraints;
    public Integer lastNumVariables;
    public Integer lastNumCircuits;
    public Integer numStages;

    // Timing
    public Timestamp dateStart;
    public Timestamp dateFinish;
    public Long totalDurationMilliseconds;
    public String totalDurationPretty;

    // Extra Information
    public String keyBoolString;
    public String commandsString;

    public ArrayList<LL_IntermediateResult> intermediateResults = new ArrayList<>();

    public LL_Result(String fileHash, LL_SolverI solver) {
        this.fileHash = fileHash;
        ilpInstanceVersion = "ILP Shahen " + LL_ILPInstance.VERSION;
        ilpSolver = solver.get_name();
        ilpSolverVersion = solver.get_version();
        reduction = solver.get_options().reduction.get_name();
        reductionVersion = solver.get_options().reduction.get_version();
        commandsString = LL_ResultsManager.arrayString(solver.get_options().args);

        // In case finish() is never called
        result = -2;
        resultString = "Forcibly Ended";
    }

    public void start() {
        dateStart = new Timestamp(System.currentTimeMillis());
    }
    public void finish() {
        dateFinish = new Timestamp(System.currentTimeMillis());
        totalDurationMilliseconds = dateFinish.getTime() - dateStart.getTime();
        totalDurationPretty = ShahenStrings.prettyDuration(totalDurationMilliseconds);

        numStages = intermediateResults.size();
        lastNumConstraints = intermediateResults.get(numStages - 1).numSolverConstraints;
        lastNumVariables = intermediateResults.get(numStages - 1).numSolverVariables;
        lastNumCircuits = intermediateResults.get(numStages - 1).numCircuits;
    }

    public static String resultToString(int r) {
        switch (r) {
            case 0 :
                return "Key NOT Found";
            case 1 :
                return "Key Found";
            case -1 :
                return "Timeout";
            case -2 :
                return "Forcibly Ended";
        }
        return "Unknown Result Code";
    }

    public static String update_sql = "UPDATE Result\n" + //
            "   SET fileHash = ?,\n" + //
            "       result = ?,\n" + //
            "       resultString = ?,\n" + //
            "       correctKey = ?,\n" + //
            "       ilpInstanceVersion = ?,\n" + //
            "       ilpSolver = ?,\n" + //
            "       ilpSolverVersion = ?,\n" + //
            "       reduction = ?,\n" + //
            "       reductionVersion = ?,\n" + //
            "       lastNumConstraints = ?,\n" + //
            "       lastNumVariables = ?,\n" + //
            "       lastNumCircuits = ?,\n" + //
            "       numStages = ?,\n" + //
            "       dateStart = ?,\n" + //
            "       dateFinish = ?,\n" + //
            "       totalDurationMilliseconds = ?,\n" + //
            "       totalDurationPretty = ?,\n" + //
            "       keyBoolString = ?,\n" + //
            "       commandString = ?\n" + //
            " WHERE idResult = ?;";
}
