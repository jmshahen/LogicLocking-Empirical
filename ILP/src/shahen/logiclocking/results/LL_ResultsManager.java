package shahen.logiclocking.results;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.solver.LL_SolverI;
import shahen.sql.SQLiteHelper;
import shahen.string.ShahenStrings;

public class LL_ResultsManager {
    public final Logger log = Logger.getLogger("shahen");
    public DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public SQLiteHelper sqlh;

    public File quickResultsFile = new File("logs/quickResults.csv");
    public FileWriter quickResultsFW = null;

    public ArrayList<LL_Result> results = new ArrayList<>();

    public StringBuilder summary = new StringBuilder();

    public LL_ResultsManager(SQLiteHelper helper) {
        sqlh = helper;
    }

    /**
     * Creates a new {@link LL_Result} using the solver and circuit.
     * @param solver
     * @param circuit
     * @return
     */
    public LL_Result create_new_result(LL_SolverI solver, LL_Circuit circuit) {
        LL_Circuit_Stats circuit_stats;
        try {
            circuit_stats = new LL_Circuit_Stats(circuit, sqlh.conn, true);
        } catch (SQLException e1) {
            log.log(Level.SEVERE, e1.toString(), e1);
            return null;
        }
        LL_Result result = new LL_Result(circuit_stats.fileHash, solver);

        PreparedStatement stmt = null;
        try {
            stmt = sqlh.conn.prepareStatement("INSERT INTO Result (fileHash) VALUES(?);");
            int i = 1;
            stmt.setString(i++, circuit_stats.fileHash);
            int num = stmt.executeUpdate();

            if (num != 1) {
                result = null;
            } else {
                result.idResult = sqlh.getLargestInt("idResult", "Result");
                results.add(result);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "", e);
            result = null;
        } finally {
            try {
                stmt.close();
            } catch (Exception e) {}
        }

        return result;
    }

    /**
     * Updates a result record in the database
     * @param result
     * @return true if no errors occurred; false otherwise
     */
    public boolean update_result(LL_Result result) {
        boolean no_errors = true;
        PreparedStatement stmt = null;
        try {
            stmt = sqlh.conn.prepareStatement(LL_Result.update_sql);
            int i = 1;
            stmt.setObject(i++, result.fileHash);
            stmt.setObject(i++, result.result);
            stmt.setObject(i++, result.resultString);
            stmt.setObject(i++, result.correctKey);
            stmt.setObject(i++, result.ilpInstanceVersion);
            stmt.setObject(i++, result.ilpSolver);
            stmt.setObject(i++, result.ilpSolverVersion);
            stmt.setObject(i++, result.reduction);
            stmt.setObject(i++, result.reductionVersion);
            stmt.setObject(i++, result.lastNumConstraints);
            stmt.setObject(i++, result.lastNumVariables);
            stmt.setObject(i++, result.lastNumCircuits);
            stmt.setObject(i++, result.numStages);
            stmt.setObject(i++, result.dateStart);
            stmt.setObject(i++, result.dateFinish);
            stmt.setObject(i++, result.totalDurationMilliseconds);
            stmt.setObject(i++, result.totalDurationPretty);
            stmt.setObject(i++, result.keyBoolString);
            stmt.setObject(i++, result.commandsString);
            stmt.setObject(i++, result.idResult);
            int num = stmt.executeUpdate();

            if (num != 1) {
                no_errors = false;
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "", e);
            no_errors = false;
        } finally {
            try {
                stmt.close();
            } catch (Exception e) {}
        }
        return no_errors;
    }

    /**
     * 
     * @param result
     * @return
     */
    public boolean save_intermediate_result(LL_IntermediateResult result) {
        boolean no_errors = true;
        PreparedStatement stmt = null;
        try {
            stmt = sqlh.conn.prepareStatement(LL_IntermediateResult.insert_sql);
            int i = 1;
            stmt.setObject(i++, result.idResult);
            stmt.setObject(i++, result.result);
            stmt.setObject(i++, result.resultString);
            stmt.setObject(i++, result.verifyStep);
            stmt.setObject(i++, result.correctKey);
            stmt.setObject(i++, result.stage);
            stmt.setObject(i++, result.stageString);
            stmt.setObject(i++, result.iterNum);
            stmt.setObject(i++, result.keyIterNum);
            stmt.setObject(i++, result.inputIterNum);
            stmt.setObject(i++, result.numSolverConstraints);
            stmt.setObject(i++, result.numSolverVariables);
            stmt.setObject(i++, result.numCircuits);
            stmt.setObject(i++, result.dateStart);
            stmt.setObject(i++, result.dateFinish);
            stmt.setObject(i++, result.durationMilliseconds);
            stmt.setObject(i++, result.durationPretty);
            stmt.setObject(i++, result.inputKeyJSON);
            int num = stmt.executeUpdate();

            if (num != 1) {
                no_errors = false;
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "", e);
            no_errors = false;
        } finally {
            try {
                stmt.close();
            } catch (Exception e) {}
        }
        return no_errors;
    }

    /**
     * Adds a quick result to a CSV file
     * @param filePath
     * @param result
     * @param durationMilliseconds duration in milliseconds
     * @param resultComments 
     */
    public void addQuickResult(String filePath, LL_Circuit circuit, int result, int iteration_count, int algorithm,
            Long durationMilliseconds, String resultComments, String[] args) {
        try {
            String prettyDuration = ShahenStrings.prettyDuration(durationMilliseconds);
            // Build the summary
            summary.append(filePath).append(": {Result:").append(result).append(", Duration:\"").append(prettyDuration)
                    .append("\", ResultComment:\"").append(resultComments).append("\", Num Stages:\"")
                    .append(iteration_count).append("\"}\n");

            if (quickResultsFW == null) {
                boolean file_exists = quickResultsFile.exists();
                quickResultsFW = new FileWriter(quickResultsFile, true);
                if (!file_exists) {
                    quickResultsFW.write("File Path, " +//
                            "Date," +//
                            "Result, " +//
                            "Iteration Steps, " +//
                            "Algortihm, " +//
                            "Input Wires, " +//
                            "Key Wires, " +//
                            "Output Wires, " +//
                            "Internal Wires, " +//
                            "Time (ms), " +//
                            "Time (pretty), " +//
                            "Result Comments," +//
                            "Arguments\n");
                }
            }

            quickResultsFW.write(filePath + "," + //
                    current_date_time_string() + "," + //
                    result + "," + //
                    iteration_count + "," + //
                    algorithm + "," + //
                    circuit.inputSize() + "," +//
                    circuit.keySize() + "," +//
                    circuit.outputSize() + "," +//
                    circuit.internalSize() + "," +//
                    durationMilliseconds + "," +//
                    "\"" + prettyDuration + "\"," +//
                    "" + ShahenStrings.CSVEscapeString(resultComments) + "," +//
                    "\"" + arrayString(args) + "\"\n");
            quickResultsFW.flush();
        } catch (IOException e) {
            log.log(Level.WARNING, "", e);
            log.warning("[Results] Unable to save Quick Result: <" + filePath + ", " + result + ", "
                    + durationMilliseconds + ">");
        }
    }

    private String current_date_time_string() {
        return dateFormat.format(new Date());
    }

    /**
     * Closes the quickResultsFile, if it was opened
     */
    public void closeQuickResultFile() {
        try {
            if (quickResultsFW != null) {
                quickResultsFW.close();
            }
        } catch (IOException e) {}
    }

    /**
     * Closes all open handlers
     */
    public void close() {
        closeQuickResultFile();
    }

    public String getSummary() {
        return summary.toString();
    }

    public static String arrayString(String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (String s : args) {
            sb.append(s).append("; ");
        }
        sb.append("]");
        return sb.toString();
    }

}
