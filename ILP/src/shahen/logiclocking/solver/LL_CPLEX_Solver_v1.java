package shahen.logiclocking.solver;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.opl.IloCplex;
import ilog.opl.IloCplex.CplexStatus;
import shahen.logiclocking.LL_Exception;
import shahen.logiclocking.circuit.cplex.*;
import shahen.logiclocking.results.LL_IntermediateResult;
import shahen.string.ShahenStrings;

public class LL_CPLEX_Solver_v1 implements LL_SolverI {
    public static String VERSION = "v1.2";
    public LL_SolverOptions options;

    public IloCplex cplex;
    /** We only require 1 Diff Circuit. */
    public LL_CPLEX_DiffCircuit diff_circuit;
    public ArrayList<LL_CPLEX_EqualCircuit> equal_circuits = new ArrayList<>();
    private IloIntVar[] key = null;

    public LL_CPLEX_Solver_v1(LL_SolverOptions options) throws IloException {
        this.options = options;
        cplex = new IloCplex();

        if (!options.debug) quiet();
    }

    @Override
    public String get_name() {
        return "LL_CPLEX_Solver_v1";
    }

    @Override
    public String get_version() {
        return VERSION;
    }

    @Override
    public String get_result_found() {
        return options.result_found;
    }

    @Override
    public Boolean[] get_found_key() {
        return options.found_key;
    }

    @Override
    public LL_SolverOptions get_options() {
        return options;
    }

    public Boolean[] get_key() {
        try {
            return LL_CPLEX_Circuit.getVarBoolArray(cplex, key);
        } catch (IloException e) {}
        return null;
    }

    /**
     * 
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a distinguishing input
     *  <li> 1 -- Able to find a new distinguishing input
     * </ul>
     * @throws IloException
     * @throws LL_Exception 
     */
    public int solve() throws IloException, LL_Exception {
        /*TIMING*/options.startEvent = options.timing.startTimer(options.timer_key + ".solve()");

        LL_SolverI.start_result(this, options);

        if (options.debug) {
            File f = new File("logs/" + options.folder_name + "/");
            if (!f.exists()) f.mkdir();
        }

        setup_settings();

        // Check if the first stage (diff circuit) is solvable
        int result = first_stage();
        if (result == 0) {
            // Unable to solve the simple diff circuit, must be infeasible or only 1 key works
            clear_model();

            diff_circuit.circuita.addToCplex(options.reduction, cplex, null, null);

            for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
                eq.setKeys(diff_circuit.circuita.key, null);
                eq.addToCplex(options.reduction, cplex, false);
            }

            if (options.debug) {
                cplex.exportModel("logs/" + options.folder_name + "/00b_failed_first_stage.lp");
                export_model("logs/" + options.folder_name + "/00b_failed_first_stage.ilp", 0, true);
            }

            options.log.info("Solving the First Failed Stage; # Constraints=" + cplex.getNrows() + "; # Bin Vars="
                    + cplex.getNbinVars());
            boolean cplex_result = cplex.solve();

            CplexStatus status = cplex.getCplexStatus();
            options.log.info("[STATUS] " + status);

            if (cplex_result) {
                // Store the key from circuita and return true
                key = diff_circuit.circuita.key;
                options.result_found = "Single Circuit Key -- Only 1 possible key or all keys work";

                /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve()");

                options.found_key = get_key();
                options.log.info("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Key Found from Failed First Stage: " + ShahenStrings.toBooleanString(options.found_key));
                return 1;
            } else {
                /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve()");

                if (status == CplexStatus.AbortDetTimeLim) {
                    options.result_found = "Single Circuit -- Timeout";
                    return -1;
                } else {
                    // Infeasible circuit -- weird
                    options.result_found = "Single Circuit -- could not be solved, infeasible";
                    return 0;
                }
            }
        }

        // Keep track of the iteration count (useful statistic)
        do {
            options.iter_count++;
            options.log.info("Beginning Stage " + options.iter_count + " for file: " + options.circuit.filename);
            result = iterative_stage(options.iter_count);
            LL_SolverI.garbage_collection(options, options.iter_count);
        } while (result == 1);

        if (result == 0) {
            result = last_stage(options.iter_count);
        }

        LL_SolverI.finish_result(options, result);

        /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve()");
        return result;
    }

    /**
     * 
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a distinguishing input
     *  <li> 1 -- Able to find a new distinguishing input
     * </ul>
     * @throws IloException
     * @throws LL_Exception 
     */
    public int first_stage() throws IloException, LL_Exception {
        /*TIMING*/options.timing.startTimer(options.timer_key + ".first_stage()");

        setup_model();

        preload();

        LL_IntermediateResult inter_result = new LL_IntermediateResult(options.last_result);
        inter_result.stage = 1;
        inter_result.stageString = "First Stage";
        inter_result.numSolverConstraints = cplex.getNrows();
        inter_result.numSolverVariables = cplex.getNbinVars();
        inter_result.numCircuits = equal_circuits.size() + 2;
        inter_result.iterNum = options.last_result.intermediateResults.size() + 1;
        inter_result.start();

        if (options.debug) {
            cplex.exportModel("logs/" + options.folder_name + "/00_first_stage.lp");
            export_model("logs/" + options.folder_name + "/00_first_stage.ilp", 0, true);
        }

        int new_stagetimeout = LL_SolverI.calculate_stagetimeout(options);
        options.log.config("[CPLEX SETTING] Updating Stage Timeout to " + new_stagetimeout + " seconds");
        if (new_stagetimeout > 0) {
            cplex.setParam(IloCplex.Param.TimeLimit, new_stagetimeout);
        } else if (new_stagetimeout == 0) { return -1; }

        options.log.info(
                "Solving the First Stage; # Constraints=" + cplex.getNrows() + "; # Bin Vars=" + cplex.getNbinVars());
        boolean result = cplex.solve();
        inter_result.finish();
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".first_stage()");

        CplexStatus status = cplex.getCplexStatus();
        options.log.info("[STATUS] " + status);

        if (result) {
            Boolean[] inputs = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuita.inputs);
            String inputs_str = ShahenStrings.toBooleanString(inputs);
            Boolean[] key_a = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuita.key);
            String key_a_str = ShahenStrings.toBooleanString(key_a);
            Boolean[] key_b = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuitb.key);
            Boolean[] output_a = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuita.outputs);
            Boolean[] output_b = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuitb.outputs);

            inter_result.result = 1;
            inter_result.resultString = "Found Distinguishing Input";
            inter_result.inputKeyJSON = "{\"input\":\"" + inputs_str + "\"}";

            options.log.info("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                    + "] Distincting Input from First Stage: " + inputs_str);
            options.log.info("Key A from First Stage: " + ShahenStrings.toBooleanString(key_a));
            options.log.info("Key B from First Stage: " + ShahenStrings.toBooleanString(key_b));

            LL_SolverI.save_distinguishing_input(options, inputs_str, false);
            LL_SolverI.save_new_key(options, key_a_str, false);

            if (options.verify_steps) {
                if (verify_step(inputs, key_a, key_b, output_a, output_b)) {
                    inter_result.verifyStep = 1;
                    options.log.info("[First Stage] Step was verified");
                } else {
                    inter_result.verifyStep = 0;
                    options.last_result.intermediateResults.add(inter_result);
                    options.results.save_intermediate_result(inter_result);

                    throw new LL_Exception("[First Stage] Step's results are INCORRECT!");
                }
            }

            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 1;
        } else {
            if (status == CplexStatus.AbortTimeLim) {
                options.result_found = "First Stage -- Timeout";
                options.log.warning("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Result from First Stage: " + status);

                inter_result.result = -1;
                inter_result.resultString = "Timeout";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return -1;
            } else {
                options.log
                        .info("[" + options.timing.getLastElapsedTimePretty() + "] Result from First Stage: " + status);

                inter_result.result = 0;
                inter_result.resultString = "No Input Found";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return 0;
            }
        }
    }

    /** Create any preloaded circuits that might be requested */
    public void preload() {
        if (options.preload_file != null) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(options.preload_file));

                String line = br.readLine();
                int p = 0;
                while (line != null) {
                    Boolean[] input = null;
                    try {
                        input = ShahenStrings.booleanStringToArray(line);
                    } catch (Exception e) {
                        continue;
                    }

                    new_equal_circuit(input, p);

                    line = br.readLine();
                    p++;

                    if (options.preload_num != null) {
                        if (options.preload_num < p) {
                            options.log.info("Skipping rest of the preload inputs");
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                options.log.warning(
                        "Unable to read distinguishing inputs from file: " + options.preload_file.getAbsolutePath());
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {}
                }
            }

        }
        if (options.preload_num != null) {
            for (int p = 0; p < options.preload_num; p++) {
                new_equal_circuit(options.circuit.getRandomInputs(), p);
            }
        }
    }
    private void new_equal_circuit(Boolean[] inputs, int preload_count) {
        LL_CPLEX_EqualCircuit equal = new LL_CPLEX_EqualCircuit(//
                options.log,//
                options.circuit, //
                inputs, //
                (diff_circuit != null) ? diff_circuit.circuita.key : null, //
                (diff_circuit != null) ? diff_circuit.circuitb.key : null, //
                "ce_preload_" + preload_count + "_a", //
                "ce_preload_" + preload_count + "_b"//
        );

        equal_circuits.add(equal);
    }
    /**
     * 
     * @param iter_num
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a distinguishing input
     *  <li> 1 -- Able to find a new distinguishing input
     * </ul>
     * @throws IloException
     * @throws LL_Exception 
     */
    public int iterative_stage(int iter_num) throws IloException, LL_Exception {
        printSplit();
        /*TIMING*/options.timing.startTimer(options.timer_key + ".iterative_stage(" + iter_num + ")");
        Boolean[] prev_distinguishing_inputs = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuita.inputs);

        LL_CPLEX_EqualCircuit equal = new LL_CPLEX_EqualCircuit(//
                options.log,//
                options.circuit, //
                prev_distinguishing_inputs, //
                diff_circuit.circuita.key, //
                diff_circuit.circuitb.key, //
                "ce" + iter_num + "_a", //
                "ce" + iter_num + "_b"//
        );

        equal_circuits.add(equal);
        // equal.addToCplex(cplex, true);

        clear_model();
        setup_model();

        LL_IntermediateResult inter_result = new LL_IntermediateResult(options.last_result);
        inter_result.stage = 2;
        inter_result.stageString = "Iter Stage";
        inter_result.numSolverConstraints = cplex.getNrows();
        inter_result.numSolverVariables = cplex.getNbinVars();
        inter_result.numCircuits = equal_circuits.size() + 2;
        inter_result.iterNum = options.last_result.intermediateResults.size() + 1;
        inter_result.start();

        if (options.debug) {
            cplex.exportModel("logs/" + options.folder_name + "/" + ShahenStrings.zeroPad(iter_num, 2) + "_stage.lp");
            export_model("logs/" + options.folder_name + "/" + ShahenStrings.zeroPad(iter_num, 2) + "_stage.ilp",
                    iter_num, true);
        }

        int new_stagetimeout = LL_SolverI.calculate_stagetimeout(options);
        options.log.config("[CPLEX SETTING] Updating Stage Timeout to " + new_stagetimeout + " seconds");
        if (new_stagetimeout > 0) {
            cplex.setParam(IloCplex.Param.TimeLimit, new_stagetimeout);
        } else if (new_stagetimeout == 0) { return -1; }

        options.log.info("Solving the Iteration Stage " + iter_num + "; # Constraints=" + cplex.getNrows()
                + "; # Bin Vars=" + cplex.getNbinVars());
        boolean result = cplex.solve();
        inter_result.finish();
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".iterative_stage(" + iter_num + ")");

        CplexStatus status = cplex.getCplexStatus();
        options.log.info("[STATUS] " + status);

        if (result) {
            Boolean[] diff_inputs = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuita.inputs);
            Boolean[] key_a = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuita.key);
            Boolean[] key_b = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuitb.key);

            String diff_inputs_str = ShahenStrings.toBooleanString(diff_inputs);
            String key_a_str = ShahenStrings.toBooleanString(key_a);
            String key_b_str = ShahenStrings.toBooleanString(key_b);
            String actual_key_str = ShahenStrings.toBooleanString(options.circuit.known_key_list);
            String iter_num_str = ShahenStrings.zeroPad(iter_num, 2);

            inter_result.result = 1;
            inter_result.resultString = "Found Input";
            inter_result.inputKeyJSON = "{\"input\":\"" + diff_inputs_str + "\"}";

            options.log.info("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                    + "] Distincting Input from Iteration Stage " + iter_num_str + ": " + diff_inputs_str);

            if (options.log.isLoggable(Level.FINE)) {
                options.log.fine("Key A from Iteration Stage " + iter_num_str + ": " + key_a_str);
                options.log.fine("Key B from Iteration Stage " + iter_num_str + ": " + key_b_str);
                options.log.fine("Actual Key from Circuit      : " + actual_key_str);
                options.log.fine("Key A == Actual Key => " + (actual_key_str.equals(key_a_str)));
                options.log.fine("Key B == Actual Key => " + (actual_key_str.equals(key_b_str)));
            }

            LL_SolverI.save_distinguishing_input(options, diff_inputs_str, true);
            LL_SolverI.save_new_key(options, key_a_str, (iter_num > 1) ? true : false);

            if (options.verify_steps) {
                Boolean[] output_a = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuita.outputs);
                Boolean[] output_b = LL_CPLEX_Circuit.getVarBoolArray(cplex, diff_circuit.circuitb.outputs);
                if (verify_step(diff_inputs, key_a, key_b, output_a, output_b)) {
                    inter_result.verifyStep = 1;
                    options.log.info("[Iteration Stage" + iter_num_str + "] Step was verified");
                } else {
                    inter_result.verifyStep = 0;
                    options.last_result.intermediateResults.add(inter_result);
                    options.results.save_intermediate_result(inter_result);

                    throw new LL_Exception("[Iteration Stage" + iter_num_str + "] Step's results are INCORRECT!");
                }
            }

            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 1;
        } else {
            if (status == CplexStatus.AbortTimeLim) {
                options.result_found = "Iterative Stage " + iter_num + " -- Timeout";
                options.log.warning("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Result from Iteration Stage " + iter_num + ": " + status);

                inter_result.result = -1;
                inter_result.resultString = "Timeout";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return -1;
            } else {
                options.log.info("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Result from Iteration Stage " + iter_num + ": " + status);

                inter_result.result = 0;
                inter_result.resultString = "No Key Found";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return 0;
            }
        }
    }

    /**
     * 
     * @param iter_count
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a distinguishing input
     *  <li> 1 -- Able to find a new distinguishing input
     * </ul>
     * @throws IloException
     */
    public int last_stage(int iter_count) throws IloException {
        /*TIMING*/options.timing.startTimer(options.timer_key + ".last_stage()");

        clear_model();

        String[] key_names = options.circuit.key_input_order.toArray(new String[0]);
        key = cplex.boolVarArray(options.circuit.key_inputs.size(), key_names);

        int num = 1;
        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            eq.circuita.key = key;
            eq.circuita.circuit_name = "eq_" + num;
            eq.circuita.inputs = null; // force circuit to create new variables

            eq.addToCplex(options.reduction, cplex, false);
            num++;
        }

        LL_IntermediateResult inter_result = new LL_IntermediateResult(options.last_result);
        inter_result.stage = 3;
        inter_result.stageString = "Last Stage";
        inter_result.numSolverConstraints = cplex.getNrows();
        inter_result.numSolverVariables = cplex.getNbinVars();
        inter_result.numCircuits = equal_circuits.size();
        inter_result.iterNum = options.last_result.intermediateResults.size() + 1;
        inter_result.start();

        if (options.debug) {
            cplex.exportModel(
                    "logs/" + options.folder_name + "/" + ShahenStrings.zeroPad(iter_count + 1, 2) + "_last_stage.lp");
            export_model(
                    "logs/" + options.folder_name + "/" + ShahenStrings.zeroPad(iter_count + 1, 2) + "_last_stage.ilp",
                    -1, true);
        }

        int new_stagetimeout = LL_SolverI.calculate_stagetimeout(options);
        options.log.config("[CPLEX SETTING] Updating Stage Timeout to " + new_stagetimeout + " seconds");
        if (new_stagetimeout > 0) {
            cplex.setParam(IloCplex.Param.TimeLimit, new_stagetimeout);
        } else if (new_stagetimeout == 0) { return -1; }

        options.log.info(
                "Solving the Last Stage; # Constraints=" + cplex.getNrows() + "; # Bin Vars=" + cplex.getNbinVars());
        boolean result = cplex.solve();
        inter_result.finish();
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".last_stage()");

        CplexStatus status = cplex.getCplexStatus();
        options.log.info("[STATUS] " + status);

        if (!result) {
            if (status == CplexStatus.AbortTimeLim) {
                options.log.warning("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Result from Last Stage: " + status);

                inter_result.result = -1;
                inter_result.resultString = "Timeout";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return -1;
            } else {
                options.result_found = "Unable to solve the Last Stage";
                options.log.warning("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Result from Last Stage: " + status);

                inter_result.result = 0;
                inter_result.resultString = "No Key Found";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return 0;
            }
        } else {
            options.found_key = get_key();
            String key_str = ShahenStrings.toBooleanString(options.found_key);
            String actual_key_str = ShahenStrings.toBooleanString(options.circuit.known_key_list);
            options.log
                    .info("[" + options.timing.getLastElapsedTimePretty() + "] Key Found from Last Stage: " + key_str);

            inter_result.result = 1;
            inter_result.resultString = "Found Key";
            inter_result.correctKey = actual_key_str.equals(key_str) ? 1 : 0;
            inter_result.verifyStep = -1;
            inter_result.inputKeyJSON = "{\"key\":\"" + key_str + "\"}";

            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 1;
        }
    }

    /**
     * 
     * @param filename
     * @param stage The following values are allowed:
     *  <ul>
     *      <li>0&nbsp;&nbsp; -- First stage, only the diff circuit</li>
     *      <li>&ge;1 -- Iterative stage (1, 2, 3, 4, ...), Diff Circuit and Equal Circuits</li>
     *      <li>-1 &nbsp;-- Last Stage, only Equal Circuits</li>
     *  </ul>
     */
    public void export_model(String filename, int stage, boolean verbose_comments) {
        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();

        constraints.append("subject to {\n");

        int constraint_count = 1;
        String keya_name = null;
        String keyb_name = null;
        if (stage != -1) {
            constraint_count = diff_circuit.addToILPFile(options.reduction, constraints, variables, constraint_count,
                    verbose_comments);
            keya_name = diff_circuit.circuita.circuit_name;
            keyb_name = diff_circuit.circuitb.circuit_name;
        }

        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            constraint_count = eq.addToILPFile(options.reduction, constraints, variables, keya_name, keyb_name,
                    constraint_count, (stage != -1), verbose_comments);
        }

        constraints.append("\n}");

        LL_SolverI.add_comment_and_variables(constraints, variables);
        // Write out model to file name, if exception, then write to stdout
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(constraints.toString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out ILP Model to file: " + filename + "\n\n");
            System.out.println(constraints.toString());
        }
    }

    private void clear_model() throws IloException {
        cplex.clearModel();

        if (diff_circuit != null) diff_circuit.clearModel();

        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            eq.clearModel();
        }
    }
    /**
     * Add the Diff Circuit and all of the Equal Circuits to the cplex model <br/>
     * <b>It is highly suggested you create a new CPLEX variable or call cplex.clearModel()
     * 
     * @throws IloException
     */
    private void setup_model() throws IloException {
        if (diff_circuit == null) {
            diff_circuit = new LL_CPLEX_DiffCircuit(options.log, options.circuit, "diff1", "diff2");
            if (options.extra_key_constraint != null) diff_circuit.extra_keys_constraint = options.extra_key_constraint;
        }
        diff_circuit.addToCplex(options.reduction, cplex);

        IloIntVar[] keya = diff_circuit.circuita.key;
        IloIntVar[] keyb = diff_circuit.circuitb.key;
        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            eq.setKeys(keya, keyb);
            eq.addToCplex(options.reduction, cplex, true);
        }
    }
    /**
     * Sets all of the tuning parameters for CPLEX 
     * @throws IloException 
     */
    public void setup_settings() throws IloException {
        options.setup_cplex_settings(cplex);
    }

    /**
     * Validate that the inputs is indeed a distinguishing input, 
     * and check that both keys produce the correct output for all equal circuits in 
     * {@link LL_CPLEX_Solver_v1#equal_circuits}. 
     * @param forced_inputs
     * @param key_a
     * @param key_b
     * @return boolean TRUE when the inputs and keys are validated by the circuit, FALSE otherwise
     */
    public boolean verify_step(Boolean[] input, Boolean[] key_a, Boolean[] key_b, Boolean[] output_a,
            Boolean[] output_b) {
        boolean diff_result = true;
        boolean equal_result = true;

        // CHECK IF IT IS A DISTINGUISHING INPUT
        diff_result = diff_circuit.verify(input, key_a, key_b, output_a, output_b);

        if (diff_result) {
            options.log.info("[VerifyStep] Diff Circuit Verified.");
        }

        // CONFIRM KEYS MATCH PREVIOUS EQUAL CIRCUITS
        for (LL_CPLEX_EqualCircuit e : equal_circuits) {
            equal_result = equal_result && e.verify(key_a, key_b);
        }

        if (equal_result) {
            options.log.info("[VerifyStep] Equal Circuits Verified.");
        }

        // Confirm that the distinguishing input is different
        boolean not_same = input_different_from_previous(input);

        return diff_result && equal_result && not_same;
    }

    /**
     * Verifies that the new distinguishing input is different from previous distinguishing inputs
     * @param input
     * @return
     */
    public boolean input_different_from_previous(Boolean[] input) {
        for (LL_CPLEX_EqualCircuit e : equal_circuits) {
            if (LL_SolverI.are_equal(input, e.inputs)) {
                options.log.severe("[VerifyStep] The distinguishing input is the same from previous equal circuit.");
                return false;
            }
        }
        options.log.fine("[VerifyStep] Distinguishing Input is Different from Previous");

        return true;
    }

    private void printSplit() {
        if (options.log.isLoggable(Level.INFO)) {
            System.out.println("################################################");
            System.out.println("################################################");
        }
    }
    /**
     * Stops CPLEX from writing out to the console
     */
    public void quiet() {
        cplex.setOut(null);
        cplex.setWarning(null);
    }

    @Override
    public void destroy() {
        cplex.end();
        equal_circuits.clear();
        diff_circuit = null;
    }
}
