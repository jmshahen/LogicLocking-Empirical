package shahen.logiclocking.solver;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.opl.IloCplex;
import ilog.opl.IloCplex.CplexStatus;
import shahen.logiclocking.LL_Exception;
import shahen.logiclocking.circuit.cplex.*;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;
import shahen.logiclocking.results.LL_IntermediateResult;
import shahen.string.ShahenStrings;

public class LL_CPLEX_Solver_v3 implements LL_SolverI {
    public static String VERSION = "v1.2";
    public LL_SolverOptions options;

    public IloCplex cplex_new_key;
    public IloCplex cplex_distinguishing_input;
    /** We only require 1 Diff Circuit. */
    public LL_CPLEX_DiffCircuit diff_circuit;
    /** We only require 1 Not Equal Circuit. */
    public LL_CPLEX_NotEqualCircuit noteq_circuit;
    public ArrayList<LL_CPLEX_EqualCircuit> equal_circuits = new ArrayList<>();

    /** Stores the key that was found during the New Key step */
    private IloIntVar[] nk_key_wires = null;
    /** Stores the key found in the new distinguishing input; not required */
    private IloIntVar[] di_key_wires = null;
    /** Stores the input found in the new distinguishing input; required */
    private IloIntVar[] di_input_wires = null;
    /** Stores the ASSUMPTIONS for the last key. Change the assumptions for each iteration to the last key found. */
    private IloIntVar[] di_last_key_wires = null;

    public LL_CPLEX_Solver_v3(LL_SolverOptions options) throws IloException {
        this.options = options;
        cplex_new_key = new IloCplex();
        cplex_distinguishing_input = new IloCplex();
        if (!options.debug) quiet();
    }

    public Boolean[] get_new_key() throws IloException {
        return LL_CPLEX_Circuit.getVarBoolArray(cplex_new_key, nk_key_wires);
    }
    public Boolean[] get_distinguishing_key_a() throws IloException {
        return LL_CPLEX_Circuit.getVarBoolArray(cplex_distinguishing_input, di_key_wires);
    }
    public Boolean[] get_distinguishing_key_b() throws IloException {
        return LL_CPLEX_Circuit.getVarBoolArray(cplex_distinguishing_input, di_last_key_wires);
    }
    public Boolean[] get_distinguishing_output_a() throws IloException {
        return diff_circuit.circuita.getOutputVarBoolArray(cplex_distinguishing_input);
    }
    public Boolean[] get_distinguishing_output_b() throws IloException {
        return diff_circuit.circuitb.getOutputVarBoolArray(cplex_distinguishing_input);
    }
    /**
     * Get the values of the new distinguishing input, and for any inputs that are not set, set them to FALSE.
     * @return
     * @throws IloException 
     */
    public Boolean[] get_distinguishing_input() throws IloException {
        return LL_CPLEX_Circuit.getVarBoolArray(cplex_distinguishing_input, di_input_wires);
    }

    /**
     * 
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a key
     *  <li> 1 -- Able to find a key
     * </ul>
     * @throws IloException
     * @throws LL_Exception 
     */
    public int solve() throws IloException, LL_Exception {
        /*TIMING*/options.startEvent = options.timing.startTimer(options.timer_key + ".solve()");

        LL_SolverI.start_result(this, options);

        if (options.debug) {
            File f = new File("logs/" + options.folder_name + "/");
            if (!f.exists()) f.mkdir();
        }

        setup_settings();
        preload();

        // Must be after PRELOAD
        setup_key_model();
        setup_distinguishing_input_model();

        int result;
        do {
            printSplit();
            options.iter_count++;
            options.log.config("Beginning Stage " + options.iter_count + " for file: " + options.circuit.filename);

            result = solve_for_key(options.iter_count);
            if (result == 1) {
                options.found_key = get_new_key();

                result = solve_for_new_distinguishing_input(options.found_key, options.iter_count);
                if (result == 1) {
                    Boolean[] new_input = get_distinguishing_input();
                    update_key_model(new_input);
                }
            }
        } while (result == 1);

        if (options.found_key != null && result != -1) {
            result = 1;
        }

        LL_SolverI.finish_result(options, result);

        /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve()");
        return result;
    }

    /** Create any preloaded circuits that might be requested */
    public void preload() {
        if (options.preload_file != null) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(options.preload_file));

                String line = br.readLine();
                int p = 0;
                while (line != null) {
                    Boolean[] input = null;
                    try {
                        input = ShahenStrings.booleanStringToArray(line);
                    } catch (Exception e) {
                        continue;
                    }

                    new_equal_circuit(input, "preload", p);

                    line = br.readLine();
                    p++;

                    if (options.preload_num != null) {
                        if (options.preload_num < p) {
                            options.log.warning("Skipping rest of the preload inputs");
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                options.log.severe(
                        "Unable to read distinguishing inputs from file: " + options.preload_file.getAbsolutePath());
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {}
                }
            }

        } else if (options.preload_num != null) {
            for (int p = 0; p < options.preload_num; p++) {
                new_equal_circuit(options.circuit.getRandomInputs(), "p", p);
            }
        } else {
            new_equal_circuit(options.circuit.getZeroInputs(), "d", 0);
            new_equal_circuit(options.circuit.getOneInputs(), "d", 1);
        }
    }
    /**
     * 
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a key
     *  <li> 1 -- Able to find a key
     * </ul>
     * @throws IloException
     * @throws LL_Exception 
     */
    private int solve_for_key(int iter_num) throws IloException, LL_Exception {
        /*TIMING*/options.timing.startTimer(options.timer_key + ".solve_for_key(" + iter_num + ")");

        LL_IntermediateResult inter_result = new LL_IntermediateResult(options.last_result);
        inter_result.stage = 4;
        inter_result.stageString = "New Key";
        inter_result.numSolverConstraints = cplex_new_key.getNrows();
        inter_result.numSolverVariables = cplex_new_key.getNbinVars();
        inter_result.numCircuits = equal_circuits.size();
        inter_result.iterNum = options.last_result.intermediateResults.size() + 1;
        inter_result.keyIterNum = iter_num;
        inter_result.start();

        if (options.debug) {
            cplex_new_key.exportModel(
                    "logs/" + options.folder_name + "/new_key_" + ShahenStrings.zeroPad(iter_num, 2) + "_stage.lp");
            export_new_key_model(
                    "logs/" + options.folder_name + "/new_key_" + ShahenStrings.zeroPad(iter_num, 2) + "_stage.ilp",
                    iter_num, true);
        }

        int new_stagetimeout = LL_SolverI.calculate_stagetimeout(options);
        options.log.config("[CPLEX SETTING] Updating Stage Timeout to " + new_stagetimeout + " seconds");
        if (new_stagetimeout > 0) {
            cplex_new_key.setParam(IloCplex.Param.TimeLimit, new_stagetimeout);
        } else if (new_stagetimeout == 0) { return -1; }

        options.log.info("Solving for New Key, Stage " + iter_num + "; # Constraints=" + cplex_new_key.getNrows()
                + "; # Bin Vars=" + cplex_new_key.getNbinVars());
        boolean result = cplex_new_key.solve();
        inter_result.finish();
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve_for_key(" + iter_num + ")");

        CplexStatus status = cplex_new_key.getCplexStatus();
        options.log.fine("[CPLEX STATUS] " + status);

        if (result) {
            options.found_key = get_new_key();

            String new_key_str = ShahenStrings.toBooleanString(options.found_key);
            String actual_key_str = ShahenStrings.toBooleanString(options.circuit.known_key_list);
            String iter_num_str = ShahenStrings.zeroPad(iter_num, 2);

            inter_result.result = 1;
            inter_result.resultString = "Found Key";
            inter_result.correctKey = actual_key_str.equals(new_key_str) ? 1 : 0;
            inter_result.inputKeyJSON = "{\"key\":\"" + new_key_str + "\"}";

            options.log.info("[" + options.timing.getLastElapsedTimePretty() + "] Key A from Iteration Stage "
                    + iter_num_str + ": " + new_key_str);
            options.log.fine("Actual Key from Circuit      : " + actual_key_str);
            options.log.fine("Key A == Actual Key => " + (actual_key_str.equals(new_key_str)));

            LL_SolverI.save_new_key(options, new_key_str, (iter_num > 1) ? true : false);

            if (options.verify_steps) {
                if (verify_key(options.found_key)) {
                    inter_result.verifyStep = 1;
                    options.log.fine("[Iteration Stage" + iter_num_str + "] Step was verified");
                } else {
                    inter_result.verifyStep = 0;
                    options.last_result.intermediateResults.add(inter_result);
                    options.results.save_intermediate_result(inter_result);

                    throw new LL_Exception("[Iteration Stage" + iter_num_str + "] Step's results are INCORRECT!");
                }
            }

            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 1;
        } else {
            if (status == CplexStatus.AbortTimeLim) {
                options.result_found = "Iterative Stage " + iter_num + " -- Timeout";
                options.log.warning("[" + options.timing.getLastElapsedTimePretty() + "] Result from Solve Key, Stage "
                        + iter_num + ": " + status);

                inter_result.result = -1;
                inter_result.resultString = "Timeout";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return -1;
            } else {
                options.log.info("[" + options.timing.getLastElapsedTimePretty() + "] Result from Solve Key, Stage "
                        + iter_num + ": " + status);

                inter_result.result = 0;
                inter_result.resultString = "No Key Found";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return 0;
            }
        }
    }
    /**
     * 
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a distinguishing input
     *  <li> 1 -- Able to find a new distinguishing input
     * </ul>
     * @throws IloException
     * @throws LL_Exception 
     */
    private int solve_for_new_distinguishing_input(Boolean[] key, int iter_num) throws IloException, LL_Exception {
        /*TIMING*/options.timing.startTimer(options.timer_key + ".solve_for_input(" + iter_num + ")");

        update_distinguishing_input_model(key);

        LL_IntermediateResult inter_result = new LL_IntermediateResult(options.last_result);
        inter_result.stage = 5;
        inter_result.stageString = "New Distinguishing Input";
        inter_result.numSolverConstraints = cplex_distinguishing_input.getNrows();
        inter_result.numSolverVariables = cplex_distinguishing_input.getNbinVars();
        inter_result.numCircuits = equal_circuits.size() + 2; // +2 is for the diff circuit
        inter_result.iterNum = options.last_result.intermediateResults.size() + 1;
        inter_result.inputIterNum = iter_num;
        inter_result.start();

        if (options.debug) {
            String num_str = ShahenStrings.zeroPad(iter_num, 2);
            cplex_distinguishing_input
                    .exportModel("logs/" + options.folder_name + "/new_distinguishing_input_" + num_str + "_stage.lp");
            export_new_distinguishing_input_model(
                    "logs/" + options.folder_name + "/new_distinguishing_input_" + num_str + "_stage.ilp", true);

            try {
                FileWriter fw = new FileWriter(
                        "logs/" + options.folder_name + "/simplified_circuit_" + num_str + ".bench");
                fw.append(diff_circuit.circuitb.circuit.getBenchString());
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        int new_stagetimeout = LL_SolverI.calculate_stagetimeout(options);
        options.log.config("[CPLEX SETTING] Updating Stage Timeout to " + new_stagetimeout + " seconds");
        if (new_stagetimeout > 0) {
            cplex_distinguishing_input.setParam(IloCplex.Param.TimeLimit, new_stagetimeout);
        } else if (new_stagetimeout == 0) { return -1; }

        options.log.info("Solving for New Distinguishing Input, Stage " + iter_num + "; # Constraints="
                + cplex_distinguishing_input.getNrows() + "; # Bin Vars=" + cplex_distinguishing_input.getNbinVars());
        boolean result = cplex_distinguishing_input.solve();
        inter_result.finish();
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve_for_input(" + iter_num + ")");

        CplexStatus status = cplex_distinguishing_input.getCplexStatus();
        options.log.fine("[STATUS] " + status);

        if (result) {
            Boolean[] new_input = get_distinguishing_input();

            String new_input_str = ShahenStrings.toBooleanString(new_input);
            String iter_num_str = ShahenStrings.zeroPad(iter_num, 2);
            options.log.info("[" + options.timing.getLastElapsedTimePretty()
                    + "] Input from Distinguishing Input, Stage " + iter_num_str + ": " + new_input_str);

            inter_result.result = 1;
            inter_result.resultString = "Found Input";
            inter_result.inputKeyJSON = "{\"input\":\"" + new_input_str + "\"}";

            LL_SolverI.save_distinguishing_input(options, new_input_str, (iter_num > 1) ? true : false);

            if (options.log.isLoggable(Level.FINE)) {
                options.log.fine("Input: " + ShahenStrings.toBooleanString(new_input));
                options.log.fine("Key A: " + ShahenStrings.toBooleanString(get_distinguishing_key_a()));
                options.log.fine("Key B: " + ShahenStrings.toBooleanString(get_distinguishing_key_b()));
                options.log.fine("Output A: " + ShahenStrings.toBooleanString(get_distinguishing_output_a()));
                options.log.fine("Output B: " + ShahenStrings.toBooleanString(get_distinguishing_output_b()));
            }

            if (options.verify_steps) {
                Boolean[] key_a = get_distinguishing_key_a();
                Boolean[] key_b = get_distinguishing_key_b();
                Boolean[] output_a = get_distinguishing_output_a();
                Boolean[] output_b = get_distinguishing_output_b();
                options.found_key = key_a;
                if (verify_step(new_input, key_a, key_b, output_a, output_b)) {
                    inter_result.verifyStep = 1;
                    options.log.fine("[Iteration Stage" + iter_num_str + "] Step was verified");
                } else {
                    inter_result.verifyStep = 0;
                    options.last_result.intermediateResults.add(inter_result);
                    options.results.save_intermediate_result(inter_result);

                    throw new LL_Exception("[Iteration Stage" + iter_num_str + "] Step's results are INCORRECT!");
                }
            }

            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);
            return 1;
        } else {
            if (status == CplexStatus.AbortTimeLim) {
                options.result_found = "Iterative Stage " + iter_num + " -- Timeout";
                options.log.warning("[" + options.timing.getLastElapsedTimePretty() + "] Result from Solve Key, Stage "
                        + iter_num + ": " + status);

                inter_result.result = -1;
                inter_result.resultString = "Timeout";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return -1;
            } else {
                options.log.info("[" + options.timing.getLastElapsedTimePretty() + "] Result from Solve Key, Stage "
                        + iter_num + ": " + status);

                inter_result.result = 0;
                inter_result.resultString = "No Input Found";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return 0;
            }
        }
    }

    private void setup_key_model() throws IloException {
        nk_key_wires = cplex_new_key.boolVarArray(options.circuit.keySize(),
                options.circuit.key_input_order.toArray(new String[0]));

        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_a) {
                eq.added_to_model_a = true;
                eq.setKeys(nk_key_wires, null);
                eq.addToCplex(options.reduction, cplex_new_key, false);
            }
        }
    }

    /**
     * Creates a new CPLEX instance and sets up the initial conditions for the distinguishing input generator model
     * @param key
     * @throws IloException
     */
    private void setup_distinguishing_input_model() throws IloException {
        // Setup the variables
        di_input_wires = cplex_distinguishing_input.boolVarArray(options.circuit.inputSize(),
                options.circuit.normal_input_order.toArray(new String[0]));
        di_key_wires = cplex_distinguishing_input.boolVarArray(options.circuit.keySize(),
                options.circuit.key_input_order.toArray(new String[0]));

        String[] last_key_wires = new String[options.circuit.key_input_order.size()];
        for (int i = 0; i < last_key_wires.length; i++) {
            last_key_wires[i] = "prev_" + options.circuit.key_input_order.get(i);
        }
        di_last_key_wires = cplex_distinguishing_input.boolVarArray(options.circuit.keySize(), last_key_wires);

        // Setup the DIFF circuit
        diff_circuit = new LL_CPLEX_DiffCircuit(options.log, options.circuit, "diff_unknown_key", options.circuit,
                "diff_known_key");
        diff_circuit.circuita.inputs = di_input_wires;
        diff_circuit.circuita.key = di_key_wires;

        diff_circuit.circuitb.inputs = di_input_wires;
        diff_circuit.circuitb.key = di_last_key_wires;
        diff_circuit.addToCplex(options.reduction, cplex_distinguishing_input);

        // Setup the NotEqual Circuit
        noteq_circuit = new LL_CPLEX_NotEqualCircuit(options.log, di_key_wires, di_last_key_wires);
        noteq_circuit.addToCplex(cplex_distinguishing_input);

        // Add all Equal Circuits (can be many from the preload step)
        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_b) {
                eq.added_to_model_b = true;
                eq.setKeys(di_key_wires, null);
                eq.addToCplex(options.reduction, cplex_distinguishing_input, false);
            }
        }

    }

    /**
     * Updates the Distinguishing Input model {@link #cplex_distinguishing_input} with the key found from the 
     * New Key model {@link #cplex_new_key}.
     * 
     * Performs the following steps:
     * <ol>
     * <li>Error Checking key
     * <li>Add any EqualCircuits not found in model already ({@link #add_equal_to_distinguishing_input()})
     * <li>Update the Last Key wires in the model ({@link #di_last_key_wires}) with the provided key values
     * </ol>
     * @param key
     * @throws IloException
     */
    private void update_distinguishing_input_model(Boolean[] key) throws IloException {
        if (key == null) throw new IllegalArgumentException("key must not be null");
        if (key.length != di_last_key_wires.length)
            throw new IllegalArgumentException("key must be the same length as the key in the circuit. key.length = "
                    + key.length + "; expected legnth = " + di_last_key_wires.length);

        add_equal_to_distinguishing_input();

        // Update the assumptions
        for (int i = 0; i < di_last_key_wires.length; i++) {
            if (key[i]) {
                di_last_key_wires[i].setLB(1);
                di_last_key_wires[i].setUB(1);
            } else {
                di_last_key_wires[i].setLB(0);
                di_last_key_wires[i].setUB(0);
            }
        }
    }

    /**
     * Updates the New Key model {@link #cplex_new_key} with the input found from the 
     * Distinguishing Input model {@link #cplex_distinguishing_input}.
     * 
     * Performs the following steps:
     * <ol>
     * <li>Error Checking new_input
     * <li>Add any EqualCircuits not found in model already ({@link #add_equal_to_new_key()})
     * </ol>
     * @param new_input
     * @throws IloException
     */
    private void update_key_model(Boolean[] new_input) throws IloException {
        if (new_input == null) throw new IllegalArgumentException("new_input must not be null");
        new_equal_circuit(new_input, "f", equal_circuits.size());

        add_equal_to_new_key();
    }

    /**
     * Adds any Equal Circuits stored in {@link #equal_circuits} that have not been added before, 
     * to the new key model {@link #cplex_new_key}.
     * 
     * NOTE: uses {@link LL_CPLEX_EqualCircuit#added_to_model_a} to know if a circuit have been added. 
     * @throws IloException
     */
    private void add_equal_to_new_key() throws IloException {
        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_a) {
                eq.added_to_model_a = true;
                eq.setKeys(nk_key_wires, null);
                eq.addToCplex(options.reduction, cplex_new_key, false);
            }
        }
    }

    /**
     * Adds any Equal Circuits stored in {@link #equal_circuits} that have not been added before, 
     * to the distinguishing input model {@link #cplex_distinguishing_input}.
     * 
     * NOTE: uses {@link LL_CPLEX_EqualCircuit#added_to_model_b} to know if a circuit have been added. 
     * @throws IloException
     */
    private void add_equal_to_distinguishing_input() throws IloException {
        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_b) {
                eq.added_to_model_b = true;
                eq.setKeys(di_key_wires, null);
                eq.addToCplex(options.reduction, cplex_distinguishing_input, false);
            }
        }
    }

    private void new_equal_circuit(Boolean[] inputs, String prefix, int count) {
        LL_CPLEX_EqualCircuit equal = new LL_CPLEX_EqualCircuit(//
                options.log,//
                options.circuit, //
                inputs, //
                null, //
                null, //
                "ce_" + prefix + "_" + count + "_a", //
                "ce_" + prefix + "_" + count + "_b"//
        );

        equal_circuits.add(equal);
    }

    /**
     * 
     * @param filename
     * @param stage The following values are allowed:
     *  <ul>
     *      <li>0&nbsp;&nbsp; -- First stage, only the diff circuit</li>
     *      <li>&ge;1 -- Iterative stage (1, 2, 3, 4, ...), Diff Circuit and Equal Circuits</li>
     *      <li>-1 &nbsp;-- Last Stage, only Equal Circuits</li>
     *  </ul>
     */
    public void export_new_key_model(String filename, int stage, boolean verbose_comments) {
        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();

        variables.append("dvar boolean " + WIRE_TYPE.KEY + "[0.." + (options.circuit.keySize() - 1) + "];\n");

        constraints.append("subject to {\n");

        int constraint_count = 1;
        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            constraint_count = eq.addToILPFile(options.reduction, constraints, variables, "", null, constraint_count,
                    false, verbose_comments);
        }

        constraints.append("\n}");

        add_comment_and_variables(constraints, variables);
        // Write out model to file name, if exception, then write to stdout
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(constraints.toString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out ILP Model to file: " + filename + "\n\n");
            System.out.println(constraints.toString());
        }
    }
    /**
     * 
     * @param filename
     * @param stage The following values are allowed:
     *  <ul>
     *      <li>0&nbsp;&nbsp; -- First stage, only the diff circuit</li>
     *      <li>&ge;1 -- Iterative stage (1, 2, 3, 4, ...), Diff Circuit and Equal Circuits</li>
     *      <li>-1 &nbsp;-- Last Stage, only Equal Circuits</li>
     *  </ul>
     */
    public void export_new_distinguishing_input_model(String filename, boolean verbose_comments) {
        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();

        constraints.append("subject to {\n");

        String keya_name = null;
        String keyb_name = null;

        int constraint_count = diff_circuit.addToILPFile(options.reduction, constraints, variables, 1,
                verbose_comments);

        keya_name = diff_circuit.circuita.circuit_name;
        keyb_name = diff_circuit.circuitb.circuit_name;
        constraint_count = noteq_circuit.addToILPFile(constraints, variables,
                keya_name + "_" + WIRE_TYPE.KEY.toString(), keyb_name + "_" + WIRE_TYPE.KEY.toString(),
                constraint_count, verbose_comments);

        for (LL_CPLEX_EqualCircuit eq : equal_circuits) {
            constraint_count = eq.addToILPFile(options.reduction, constraints, variables, keya_name, keyb_name,
                    constraint_count, false, verbose_comments);
        }

        constraints.append("\n}");

        add_comment_and_variables(constraints, variables);
        // Write out model to file name, if exception, then write to stdout
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(constraints.toString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out ILP Model to file: " + filename + "\n\n");
            System.out.println(constraints.toString());
        }
    }

    /**
     * Sets all of the tuning parameters for CPLEX 
     * @throws IloException 
     */
    public void setup_settings() throws IloException {
        options.setup_cplex_settings(cplex_distinguishing_input);
        options.setup_cplex_settings(cplex_new_key);
    }
    public static void add_comment_and_variables(StringBuilder sb, StringBuilder sb_vars) {
        StringBuilder sb2 = new StringBuilder();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        sb2.append("/******************************************************\n")//
                .append(" * Model Created by Shahen Logic Locking\n")//
                .append(" * Date: " + formatter.format(new Date(System.currentTimeMillis())) + "\n")//
                .append(" ******************************************************/\n\n");

        sb2.append(sb_vars);

        sb.insert(0, sb2);
    }

    /**
     * Validate that the inputs is indeed a distinguishing input, 
     * and check that both keys produce the correct output for all equal circuits in 
     * {@link LL_CPLEX_Solver_v3#equal_circuits}. 
     * @param forced_inputs
     * @param new_key
     * @param old_key
     * @return boolean TRUE when the inputs and keys are validated by the circuit, FALSE otherwise
     */
    public boolean verify_step(Boolean[] input, Boolean[] key_a, Boolean[] key_b, Boolean[] output_a,
            Boolean[] output_b) {

        // CHECK IF IT IS A DISTINGUISHING INPUT
        boolean diff_result = true;
        if (input != null) {
            diff_result = diff_circuit.verify(input, key_a, key_b, output_a, output_b);

            if (diff_result) {
                options.log.fine("[VerifyStep] Diff Circuit Verified.");
            }
        }

        boolean noteq_result = noteq_circuit.verify(key_a, key_b);

        // CONFIRM KEYS MATCH PREVIOUS EQUAL CIRCUITS
        boolean equal_result = verify_key(key_a);

        // Confirm that the distinguishing input is different
        boolean not_same = input_different_from_previous(input);

        return diff_result && equal_result && noteq_result && not_same;
    }

    /**
     * Verifies that the new distinguishing input is different from previous distinguishing inputs
     * @param input
     * @return
     */
    public boolean input_different_from_previous(Boolean[] input) {
        for (LL_CPLEX_EqualCircuit e : equal_circuits) {
            if (LL_SolverI.are_equal(input, e.inputs)) {
                options.log.severe("[VerifyStep] The distinguishing input is the same from previous equal circuit.");
                return false;
            }
        }
        options.log.fine("[VerifyStep] Distinguishing Input is Different from Previous");

        return true;
    }

    boolean verify_key(Boolean[] new_key) {
        return verify_keys(new_key, null);
    }

    boolean verify_keys(Boolean[] key_a, Boolean[] key_b) {
        boolean equal_result = true;
        options.log.fine("Verifying " + equal_circuits.size() + " Equal Circuits.");
        for (LL_CPLEX_EqualCircuit e : equal_circuits) {
            equal_result &= e.verify(key_a, key_b);
        }

        if (equal_result) {
            options.log.fine("[VerifyStep] Equal Circuits Verified.");
        }

        return equal_result;
    }

    /** Stops CPLEX from writing out to the console */
    @Override
    public void quiet() {
        cplex_new_key.setOut(null);
        cplex_new_key.setWarning(null);
        cplex_distinguishing_input.setOut(null);
        cplex_distinguishing_input.setWarning(null);
    }

    @Override
    public void destroy() {
        cplex_distinguishing_input.end();
        cplex_new_key.end();
        equal_circuits.clear();
    }
    private void printSplit() {
        if (options.log.isLoggable(Level.INFO)) {
            System.out.println("################################################");
            System.out.println("################################################");
        }
    }
    @Override
    public String get_name() {
        return "LL_CPLEX_Solver_v3";
    }

    @Override
    public String get_version() {
        return VERSION;
    }

    @Override
    public String get_result_found() {
        return options.result_found;
    }

    @Override
    public Boolean[] get_found_key() {
        return options.found_key;
    }

    @Override
    public LL_SolverOptions get_options() {
        return options;
    }
}
