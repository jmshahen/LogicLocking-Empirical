package shahen.logiclocking.solver;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

import gurobi.*;
import shahen.logiclocking.LL_Exception;
import shahen.logiclocking.circuit.gurobi.*;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;
import shahen.logiclocking.results.LL_IntermediateResult;
import shahen.string.ShahenStrings;

public class LL_GUROBI_Solver implements LL_SolverI {
    public static String VERSION = "v1.2";
    public LL_SolverOptions options;

    public GRBEnv new_key_env;
    public GRBEnv distinguishing_input_env;

    public GRBModel gurobi_new_key;
    public GRBModel gurobi_distinguishing_input;
    /** We only require 1 Diff Circuit. */
    public LL_GUROBI_DiffCircuit diff_circuit;
    /** We only require 1 Not Equal Circuit. */
    public LL_GUROBI_NotEqualCircuit noteq_circuit;
    public ArrayList<LL_GUROBI_EqualCircuit> equal_circuits = new ArrayList<>();

    /** Stores the key that was found during the New Key step */
    private GRBVar[] nk_key_wires = null;
    /** Stores the key found in the new distinguishing input; not required */
    private GRBVar[] di_key_wires = null;
    /** Stores the input found in the new distinguishing input; required */
    private GRBVar[] di_input_wires = null;
    /** Stores the ASSUMPTIONS for the last key. Change the assumptions for each iteration to the last key found. */
    private GRBVar[] di_last_key_wires = null;

    public LL_GUROBI_Solver(LL_SolverOptions options) throws GRBException {
        this.options = options;

        new_key_env = new GRBEnv();
        distinguishing_input_env = new GRBEnv();

        gurobi_new_key = new GRBModel(new_key_env);
        gurobi_distinguishing_input = new GRBModel(distinguishing_input_env);

        if (!options.debug) quiet();
    }

    public Boolean[] get_new_key() throws GRBException {
        return LL_GUROBI_Circuit.getVarBoolArray(gurobi_new_key, nk_key_wires);
    }
    public Boolean[] get_distinguishing_key_a() throws GRBException {
        return LL_GUROBI_Circuit.getVarBoolArray(gurobi_distinguishing_input, di_key_wires);
    }
    public Boolean[] get_distinguishing_key_b() throws GRBException {
        return LL_GUROBI_Circuit.getVarBoolArray(gurobi_distinguishing_input, di_last_key_wires);
    }
    public Boolean[] get_distinguishing_output_a() throws GRBException {
        return diff_circuit.circuita.getOutputVarBoolArray(gurobi_distinguishing_input);
    }
    public Boolean[] get_distinguishing_output_b() throws GRBException {
        return diff_circuit.circuitb.getOutputVarBoolArray(gurobi_distinguishing_input);
    }
    /**
     * Get the values of the new distinguishing input, and for any inputs that are not set, set them to FALSE.
     * @return
     * @throws IloException 
     */
    public Boolean[] get_distinguishing_input() throws GRBException {
        return LL_GUROBI_Circuit.getVarBoolArray(gurobi_distinguishing_input, di_input_wires);
    }

    /**
     * 
     * @return <ul>
     *  <li>-1 -- GUROBI timed out
     *  <li> 0 -- Unable to find a key
     *  <li> 1 -- Able to find a key
     * </ul>
     * @throws LL_Exception 
     * @throws IloException
     */
    public int solve() throws GRBException, LL_Exception {
        /*TIMING*/options.startEvent = options.timing.startTimer(options.timer_key + ".solve()");

        LL_SolverI.start_result(this, options);

        if (options.debug) {
            File f = new File("logs/" + options.folder_name + "/");
            if (!f.exists()) f.mkdir();
        }

        setup_settings();
        preload();

        // Must be after PRELOAD
        setup_key_model();
        setup_distinguishing_input_model();

        int result;
        do {
            printSplit();
            options.iter_count++;
            options.log.config("Beginning Stage " + options.iter_count + " for file: " + options.circuit.filename);

            result = solve_for_key(options.iter_count);
            if (result == 1) {
                options.found_key = get_new_key();

                result = solve_for_new_distinguishing_input(options.found_key, options.iter_count);
                if (result == 1) {
                    Boolean[] new_input = get_distinguishing_input();
                    update_key_model(new_input);
                }
            }
        } while (result == 1);

        if (options.found_key != null && result != -1) {
            result = 1;
        }

        LL_SolverI.finish_result(options, result);

        /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve()");
        return result;
    }

    /** Create any preloaded circuits that might be requested */
    public void preload() {
        if (options.preload_file != null) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(options.preload_file));

                String line = br.readLine();
                int p = 0;
                while (line != null) {
                    Boolean[] input = null;
                    try {
                        input = ShahenStrings.booleanStringToArray(line);
                    } catch (Exception e) {
                        continue;
                    }

                    new_equal_circuit(input, "preload", p);

                    line = br.readLine();
                    p++;

                    if (options.preload_num != null) {
                        if (options.preload_num < p) {
                            options.log.info("Skipping rest of the preload inputs");
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                options.log.warning(
                        "Unable to read distinguishing inputs from file: " + options.preload_file.getAbsolutePath());
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {}
                }
            }

        } else if (options.preload_num != null) {
            for (int p = 0; p < options.preload_num; p++) {
                new_equal_circuit(options.circuit.getRandomInputs(), "preload", p);
            }
        } else {
            new_equal_circuit(options.circuit.getZeroInputs(), "default", 0);
            new_equal_circuit(options.circuit.getOneInputs(), "default", 1);
        }
    }
    /**
     * 
     * @return <ul>
     *  <li>-1 -- GUROBI timed out
     *  <li> 0 -- Unable to find a key
     *  <li> 1 -- Able to find a key
     * </ul>
     * @throws LL_Exception 
     * @throws IloException
     */
    private int solve_for_key(int iter_num) throws GRBException, LL_Exception {
        /*TIMING*/options.timing.startTimer(options.timer_key + ".solve_for_key(" + iter_num + ")");

        LL_IntermediateResult inter_result = new LL_IntermediateResult(options.last_result);
        inter_result.stage = 4;
        inter_result.stageString = "New Key";
        inter_result.numSolverConstraints = gurobi_new_key.get(GRB.IntAttr.NumConstrs);
        inter_result.numSolverVariables = gurobi_new_key.get(GRB.IntAttr.NumVars);
        inter_result.numCircuits = equal_circuits.size();
        inter_result.iterNum = options.last_result.intermediateResults.size() + 1;
        inter_result.keyIterNum = iter_num;
        inter_result.start();

        if (options.debug) {
            gurobi_new_key.write(
                    "logs/" + options.folder_name + "/new_key_" + ShahenStrings.zeroPad(iter_num, 2) + "_stage.lp");
            export_new_key_model(
                    "logs/" + options.folder_name + "/new_key_" + ShahenStrings.zeroPad(iter_num, 2) + "_stage.ilp",
                    iter_num, true);
        }

        int new_stagetimeout = LL_SolverI.calculate_stagetimeout(options);
        options.log.config("[GUROBI SETTING] Updating Stage Timeout to " + new_stagetimeout + " seconds");
        if (new_stagetimeout > 0) {
            gurobi_new_key.set(GRB.DoubleParam.TimeLimit, new_stagetimeout);
        } else if (new_stagetimeout == 0) { return -1; }

        options.log.info("Solving for New Key, Stage " + iter_num + "; # Constraints="
                + inter_result.numSolverConstraints + "; # Vars=" + inter_result.numSolverVariables);
        gurobi_new_key.optimize();
        inter_result.finish();
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve_for_key(" + iter_num + ")");

        int status = gurobi_new_key.get(GRB.IntAttr.Status);
        // options.log.info("[STATUS] " + status);

        if (status == GRB.Status.OPTIMAL) {
            options.found_key = get_new_key();

            String new_key_str = ShahenStrings.toBooleanString(options.found_key);
            String actual_key_str = ShahenStrings.toBooleanString(options.circuit.known_key_list);
            String iter_num_str = ShahenStrings.zeroPad(iter_num, 2);

            inter_result.result = 1;
            inter_result.resultString = "Found Key";
            inter_result.correctKey = actual_key_str.equals(new_key_str) ? 1 : 0;
            inter_result.inputKeyJSON = "{\"key\":\"" + new_key_str + "\"}";

            options.log.fine("[" + options.timing.getLastElapsedTimePretty() + "] Key A from Iteration Stage "
                    + iter_num_str + ": " + new_key_str);
            options.log.fine("Actual Key from Circuit      : " + actual_key_str);
            options.log.fine("Key A == Actual Key => " + actual_key_str.equals(new_key_str));

            LL_SolverI.save_new_key(options, new_key_str, (iter_num > 1) ? true : false);

            if (options.verify_steps) {
                if (verify_key(options.found_key)) {
                    inter_result.verifyStep = 1;
                    options.log.fine("[Iteration Stage" + iter_num_str + "] Step was verified");
                } else {
                    inter_result.verifyStep = 0;
                    options.last_result.intermediateResults.add(inter_result);
                    options.results.save_intermediate_result(inter_result);

                    throw new LL_Exception("[Iteration Stage" + iter_num_str + "] Step's results are INCORRECT!");
                }
            }

            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 1;
        } else {
            if (status == GRB.Status.TIME_LIMIT) {
                options.result_found = "Iterative Stage " + iter_num + " -- Timeout";
                options.log.warning("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Result from Solve Key, Stage " + iter_num + ": " + status);

                inter_result.result = -1;
                inter_result.resultString = "Timeout";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return -1;
            } else {
                options.log.info("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                        + "] Result from Solve Key, Stage " + iter_num + ": " + status);

                inter_result.result = 0;
                inter_result.resultString = "No Key Found";
                inter_result.verifyStep = -1;
                options.last_result.intermediateResults.add(inter_result);
                options.results.save_intermediate_result(inter_result);

                return 0;
            }
        }
    }
    /**
     * 
     * @return <ul>
     *  <li>-1 -- GUROBI timed out
     *  <li> 0 -- Unable to find a distinguishing input
     *  <li> 1 -- Able to find a new distinguishing input
     * </ul>
     * @throws LL_Exception 
     * @throws IloException
     */
    private int solve_for_new_distinguishing_input(Boolean[] key, int iter_num) throws GRBException, LL_Exception {
        /*TIMING*/options.timing.startTimer(options.timer_key + ".solve_for_input(" + iter_num + ")");
        update_distinguishing_input_model(key);

        LL_IntermediateResult inter_result = new LL_IntermediateResult(options.last_result);
        inter_result.stage = 5;
        inter_result.stageString = "New Distinguishing Input";
        inter_result.numSolverConstraints = gurobi_new_key.get(GRB.IntAttr.NumConstrs);
        inter_result.numSolverVariables = gurobi_new_key.get(GRB.IntAttr.NumVars);
        inter_result.numCircuits = equal_circuits.size() + 2; // +2 is for the diff circuit
        inter_result.iterNum = options.last_result.intermediateResults.size() + 1;
        inter_result.inputIterNum = iter_num;
        inter_result.start();

        if (options.debug) {
            String num_str = ShahenStrings.zeroPad(iter_num, 2);
            gurobi_distinguishing_input
                    .write("logs/" + options.folder_name + "/new_distinguishing_input_" + num_str + "_stage.lp");
            export_new_distinguishing_input_model(
                    "logs/" + options.folder_name + "/new_distinguishing_input_" + num_str + "_stage.ilp", true);

            try {
                FileWriter fw = new FileWriter(
                        "logs/" + options.folder_name + "/simplified_circuit_" + num_str + ".bench");
                fw.append(diff_circuit.circuitb.circuit.getBenchString());
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        int new_stagetimeout = LL_SolverI.calculate_stagetimeout(options);
        options.log.config("[GUROBI SETTING] Updating Stage Timeout to " + new_stagetimeout + " seconds");
        if (new_stagetimeout > 0) {
            gurobi_distinguishing_input.set(GRB.DoubleParam.TimeLimit, new_stagetimeout);
        } else if (new_stagetimeout == 0) { return -1; }

        options.log.info("Solving for New Distinguishing Input, Stage " + iter_num + "; # Constraints="
                + gurobi_distinguishing_input.get(GRB.IntAttr.NumConstrs) + "; # Bin Vars="
                + gurobi_distinguishing_input.get(GRB.IntAttr.NumVars));
        gurobi_distinguishing_input.optimize();
        inter_result.finish();
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".solve_for_input(" + iter_num + ")");

        int status = gurobi_distinguishing_input.get(GRB.IntAttr.Status);
        // options.log.info("[STATUS] " + status);

        if (status == GRB.Status.OPTIMAL) {
            Boolean[] new_input = get_distinguishing_input();

            String new_input_str = ShahenStrings.toBooleanString(new_input);
            String iter_num_str = ShahenStrings.zeroPad(iter_num, 2);
            options.log.info("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                    + "] Input from Distinguishing Input, Stage " + iter_num_str + ": " + new_input_str);

            inter_result.result = 1;
            inter_result.resultString = "Found Key";
            inter_result.inputKeyJSON = "{\"input\":\"" + new_input_str + "\"}";

            LL_SolverI.save_distinguishing_input(options, new_input_str, (iter_num > 1) ? true : false);

            if (options.log.isLoggable(Level.FINE)) {
                options.log.fine("Input: " + ShahenStrings.toBooleanString(new_input));
                options.log.fine("Key A: " + ShahenStrings.toBooleanString(get_distinguishing_key_a()));
                options.log.fine("Key B: " + ShahenStrings.toBooleanString(get_distinguishing_key_b()));
                options.log.fine("Output A: " + ShahenStrings.toBooleanString(get_distinguishing_output_a()));
                options.log.fine("Output B: " + ShahenStrings.toBooleanString(get_distinguishing_output_b()));
            }

            if (options.verify_steps) {
                options.found_key = get_distinguishing_key_a();
                Boolean[] output_new = diff_circuit.circuita.getOutputVarBoolArray(gurobi_distinguishing_input);
                Boolean[] output_old = diff_circuit.circuitb.getOutputVarBoolArray(gurobi_distinguishing_input);
                if (verify_step(new_input, key, options.found_key, output_old, output_new)) {
                    options.log.config("[Iteration Stage" + iter_num_str + "] Step was verified");
                    inter_result.verifyStep = 1;
                } else {
                    inter_result.verifyStep = 0;
                    options.last_result.intermediateResults.add(inter_result);
                    options.results.save_intermediate_result(inter_result);

                    throw new LL_Exception("[Iteration Stage" + iter_num_str + "] Step's results are INCORRECT!");
                }
            }

            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 1;
        } else if (status == GRB.Status.TIME_LIMIT) {
            options.result_found = "Iterative Stage " + iter_num + " -- Timeout";
            options.log.warning("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                    + "] Result from Solve Key, Stage " + iter_num + ": Time Limit Reached (" + status + ")");

            inter_result.result = -1;
            inter_result.resultString = "Timeout";
            inter_result.verifyStep = -1;
            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return -1;
        } else if (status == GRB.Status.INFEASIBLE) {
            options.log.info("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                    + "] Result from Solve Input, Stage " + iter_num + ": Infeasible");

            inter_result.result = 0;
            inter_result.resultString = "No Input Found";
            inter_result.verifyStep = -1;
            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 0;
        } else {
            options.log.warning("[" + ShahenStrings.prettyDuration(options.timing.getLastElapsedTime())
                    + "] Result from Solve Input, Stage " + iter_num + ": Unknown (" + status + ")");

            inter_result.result = 0;
            inter_result.resultString = "No Input Found";
            inter_result.verifyStep = -1;
            options.last_result.intermediateResults.add(inter_result);
            options.results.save_intermediate_result(inter_result);

            return 0;
        }
    }

    private void setup_key_model() throws GRBException {
        nk_key_wires = gurobi_new_key.addVars(options.circuit.keySize(), GRB.BINARY);

        for (int i = 0; i < nk_key_wires.length; i++) {
            nk_key_wires[i].set(GRB.StringAttr.VarName, options.circuit.key_input_order.get(i));
        }

        for (LL_GUROBI_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_a) {
                eq.added_to_model_a = true;
                eq.setKeys(nk_key_wires, null);
                eq.addToGurobi(options.reduction, gurobi_new_key, false);
            }
        }
    }

    /**
     * Creates a new GUROBI instance and sets up the initial conditions for the distinguishing input generator model
     * @param key
     * @throws IloException
     */
    private void setup_distinguishing_input_model() throws GRBException {
        di_input_wires = gurobi_distinguishing_input.addVars(options.circuit.inputSize(), GRB.BINARY);
        di_key_wires = gurobi_distinguishing_input.addVars(options.circuit.keySize(), GRB.BINARY);
        di_last_key_wires = gurobi_distinguishing_input.addVars(options.circuit.keySize(), GRB.BINARY);

        // Names (for options.debugging)
        for (int i = 0; i < di_input_wires.length; i++) {
            di_input_wires[i].set(GRB.StringAttr.VarName, "input_" + options.circuit.normal_input_order.get(i));
        }
        for (int i = 0; i < di_key_wires.length; i++) {
            di_key_wires[i].set(GRB.StringAttr.VarName, "key_" + options.circuit.key_input_order.get(i));
        }
        for (int i = 0; i < di_last_key_wires.length; i++) {
            di_last_key_wires[i].set(GRB.StringAttr.VarName, "prev_key_" + options.circuit.key_input_order.get(i));
        }

        diff_circuit = new LL_GUROBI_DiffCircuit(options.log, options.circuit, "diff_unknown_key", options.circuit,
                "diff_known_key");
        diff_circuit.circuita.inputs = di_input_wires;
        diff_circuit.circuita.key = di_key_wires;

        diff_circuit.circuitb.inputs = di_input_wires;
        diff_circuit.circuitb.key = di_last_key_wires;
        diff_circuit.addToGurobi(options.reduction, gurobi_distinguishing_input);

        noteq_circuit = new LL_GUROBI_NotEqualCircuit(options.log, di_key_wires, di_last_key_wires);
        noteq_circuit.addToGurobi(gurobi_distinguishing_input);

        for (LL_GUROBI_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_b) {
                eq.added_to_model_b = true;
                eq.setKeys(di_key_wires, null);
                eq.addToGurobi(options.reduction, gurobi_distinguishing_input, false);
            }
        }

    }

    /**
     * Updates the Distinguishing Input model {@link #gurobi_distinguishing_input} with the key found from the 
     * New Key model {@link #gurobi_new_key}.
     * 
     * Performs the following steps:
     * <ol>
     * <li>Error Checking key
     * <li>Add any EqualCircuits not found in model already ({@link #add_equal_to_distinguishing_input()})
     * <li>Update the Last Key wires in the model ({@link #di_last_key_wires}) with the provided key values
     * </ol>
     * @param key
     * @throws IloException
     */
    private void update_distinguishing_input_model(Boolean[] key) throws GRBException {
        if (key == null) throw new IllegalArgumentException("key must not be null");
        if (key.length != di_last_key_wires.length)
            throw new IllegalArgumentException("key must be the same length as the key in the circuit. key.length = "
                    + key.length + "; expected legnth = " + di_last_key_wires.length);

        add_equal_to_distinguishing_input();

        // Update the assumptions
        for (int i = 0; i < di_last_key_wires.length; i++) {
            if (key[i]) {
                di_last_key_wires[i].set(GRB.DoubleAttr.LB, 1.0);
                di_last_key_wires[i].set(GRB.DoubleAttr.UB, 1.0);
            } else {
                di_last_key_wires[i].set(GRB.DoubleAttr.LB, 0.0);
                di_last_key_wires[i].set(GRB.DoubleAttr.UB, 0.0);
            }
        }
    }

    /**
     * Updates the New Key model {@link #gurobi_new_key} with the input found from the 
     * Distinguishing Input model {@link #gurobi_distinguishing_input}.
     * 
     * Performs the following steps:
     * <ol>
     * <li>Error Checking new_input
     * <li>Add any EqualCircuits not found in model already ({@link #add_equal_to_new_key()})
     * </ol>
     * @param new_input
     * @throws IloException
     */
    private void update_key_model(Boolean[] new_input) throws GRBException {
        if (new_input == null) throw new IllegalArgumentException("new_input must not be null");
        new_equal_circuit(new_input, "found", equal_circuits.size());

        add_equal_to_new_key();
    }

    /**
     * Adds any Equal Circuits stored in {@link #equal_circuits} that have not been added before, 
     * to the new key model {@link #gurobi_new_key}.
     * 
     * NOTE: uses {@link LL_GUROBI_EqualCircuit#added_to_model_a} to know if a circuit have been added. 
     * @throws IloException
     */
    private void add_equal_to_new_key() throws GRBException {
        for (LL_GUROBI_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_a) {
                eq.added_to_model_a = true;
                eq.setKeys(nk_key_wires, null);
                eq.addToGurobi(options.reduction, gurobi_new_key, false);
            }
        }
    }

    /**
     * Adds any Equal Circuits stored in {@link #equal_circuits} that have not been added before, 
     * to the distinguishing input model {@link #gurobi_distinguishing_input}.
     * 
     * NOTE: uses {@link LL_GUROBI_EqualCircuit#added_to_model_b} to know if a circuit have been added. 
     * @throws IloException
     */
    private void add_equal_to_distinguishing_input() throws GRBException {
        for (LL_GUROBI_EqualCircuit eq : equal_circuits) {
            if (!eq.added_to_model_b) {
                eq.added_to_model_b = true;
                eq.setKeys(di_key_wires, null);
                eq.addToGurobi(options.reduction, gurobi_distinguishing_input, false);
            }
        }
    }

    private void new_equal_circuit(Boolean[] inputs, String prefix, int count) {
        LL_GUROBI_EqualCircuit equal = new LL_GUROBI_EqualCircuit(//
                options.log,//
                options.circuit, //
                inputs, //
                null, //
                null, //
                "ce_" + prefix + "_" + count + "_a", //
                "ce_" + prefix + "_" + count + "_b"//
        );

        equal_circuits.add(equal);
    }

    /**
     * 
     * @param filename
     * @param stage The following values are allowed:
     *  <ul>
     *      <li>0&nbsp;&nbsp; -- First stage, only the diff circuit</li>
     *      <li>&ge;1 -- Iterative stage (1, 2, 3, 4, ...), Diff Circuit and Equal Circuits</li>
     *      <li>-1 &nbsp;-- Last Stage, only Equal Circuits</li>
     *  </ul>
     */
    public void export_new_key_model(String filename, int stage, boolean verbose_comments) {
        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();

        variables.append("dvar boolean " + WIRE_TYPE.KEY + "[0.." + (options.circuit.keySize() - 1) + "];\n");

        constraints.append("subject to {\n");
        int constraint_count = 1;
        for (LL_GUROBI_EqualCircuit eq : equal_circuits) {
            constraint_count = eq.addToILPFile(options.reduction, constraints, variables, "", null, constraint_count,
                    false, verbose_comments);
        }

        constraints.append("\n}");

        add_comment_and_variables(constraints, variables);
        // Write out model to file name, if exception, then write to stdout
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(constraints.toString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out ILP Model to file: " + filename + "\n\n");
            System.out.println(constraints.toString());
        }
    }
    /**
     * 
     * @param filename
     * @param stage The following values are allowed:
     *  <ul>
     *      <li>0&nbsp;&nbsp; -- First stage, only the diff circuit</li>
     *      <li>&ge;1 -- Iterative stage (1, 2, 3, 4, ...), Diff Circuit and Equal Circuits</li>
     *      <li>-1 &nbsp;-- Last Stage, only Equal Circuits</li>
     *  </ul>
     */
    public void export_new_distinguishing_input_model(String filename, boolean verbose_comments) {
        StringBuilder constraints = new StringBuilder();
        StringBuilder variables = new StringBuilder();

        constraints.append("subject to {\n");

        String keya_name = null;
        String keyb_name = null;
        int constraint_count = 1;
        constraint_count = diff_circuit.addToILPFile(options.reduction, constraints, variables, constraint_count,
                verbose_comments);

        keya_name = diff_circuit.circuita.circuit_name;
        constraint_count = noteq_circuit.addToILPFile(constraints, variables,
                keya_name + "_" + WIRE_TYPE.KEY.toString(), keyb_name, constraint_count, verbose_comments);

        for (LL_GUROBI_EqualCircuit eq : equal_circuits) {
            constraint_count = eq.addToILPFile(options.reduction, constraints, variables, keya_name, keyb_name,
                    constraint_count, false, verbose_comments);
        }

        constraints.append("\n}");

        add_comment_and_variables(constraints, variables);
        // Write out model to file name, if exception, then write to stdout
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
            if (writer != null) {
                writer.write(constraints.toString());
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to write out ILP Model to file: " + filename + "\n\n");
            System.out.println(constraints.toString());
        }
    }

    /**
     * Sets all of the tuning parameters for GUROBI 
     * @throws IloException 
     */
    public void setup_settings() throws GRBException {
        options.setup_gurobi_settings(gurobi_distinguishing_input);
        options.setup_gurobi_settings(gurobi_new_key);
        // if (cplex_parallel != null) {
        // gurobi_new_key.setParam(IloCplex.IntParam.ParallelMode,
        // (cplex_parallel) ? IloCplex.ParallelMode.Opportunistic : IloCplex.ParallelMode.Deterministic);
        // gurobi_distinguishing_input.setParam(IloCplex.IntParam.ParallelMode,
        // (cplex_parallel) ? IloCplex.ParallelMode.Opportunistic : IloCplex.ParallelMode.Deterministic);
        // }
    }
    public static void add_comment_and_variables(StringBuilder sb, StringBuilder sb_vars) {
        StringBuilder sb2 = new StringBuilder();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        sb2.append("/******************************************************\n")//
                .append(" * Model Created by Shahen Logic Locking\n")//
                .append(" * Date: " + formatter.format(new Date(System.currentTimeMillis())) + "\n")//
                .append(" ******************************************************/\n\n");

        sb2.append(sb_vars);

        sb.insert(0, sb2);
    }

    /**
     * Validate that the inputs is indeed a distinguishing input, 
     * and check that both keys produce the correct output for all equal circuits in 
     * {@link LL_GUROBI_Solver_v3#equal_circuits}. 
     * @param forced_inputs
     * @param new_key
     * @param old_key
     * @return boolean TRUE when the inputs and keys are validated by the circuit, FALSE otherwise
     */
    public boolean verify_step(Boolean[] input, Boolean[] new_key, Boolean[] old_key, Boolean[] output_a,
            Boolean[] output_b) {

        // CHECK IF IT IS A DISTINGUISHING INPUT
        boolean diff_result = true;
        if (input != null) {
            diff_result = diff_circuit.verify(input, new_key, old_key, output_a, output_b);

            if (diff_result) {
                options.log.fine("[VerifyStep] Diff Circuit Verified.");
            }
        }

        boolean noteq_result = noteq_circuit.verify(new_key, old_key);

        // CONFIRM KEYS MATCH PREVIOUS EQUAL CIRCUITS
        boolean equal_result = verify_key(new_key);

        // Confirm that the distinguishing input is different
        boolean not_same = input_different_from_previous(input);

        return diff_result && equal_result && noteq_result && not_same;
    }

    /**
     * Verifies that the new distinguishing input is different from previous distinguishing inputs
     * @param input
     * @return
     */
    public boolean input_different_from_previous(Boolean[] input) {
        for (LL_GUROBI_EqualCircuit e : equal_circuits) {
            if (LL_SolverI.are_equal(input, e.inputs)) {
                options.log.severe("[VerifyStep] The distinguishing input is the same from previous equal circuit.");
                return false;
            }
        }
        options.log.fine("[VerifyStep] Distinguishing Input is Different from Previous");

        return true;
    }

    boolean verify_key(Boolean[] new_key) {
        return verify_keys(new_key, null);
    }

    boolean verify_keys(Boolean[] key_a, Boolean[] key_b) {
        boolean equal_result = true;
        for (LL_GUROBI_EqualCircuit e : equal_circuits) {
            equal_result = equal_result && e.verify(key_a, key_b);
        }

        if (equal_result) {
            options.log.fine("[VerifyStep] Equal Circuits Verified.");
        }

        return equal_result;
    }

    private void printSplit() {
        if (options.log.isLoggable(Level.INFO)) {
            System.out.println("################################################");
            System.out.println("################################################");
        }
    }

    /**
     * Stops GUROBI from writing out to the console
     * @throws GRBException 
     */
    @Override
    public void quiet() {
        try {
            gurobi_new_key.set(GRB.IntParam.OutputFlag, 0);
            gurobi_distinguishing_input.set(GRB.IntParam.OutputFlag, 0);
        } catch (GRBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        gurobi_distinguishing_input.dispose();
        gurobi_new_key.dispose();
        try {
            distinguishing_input_env.dispose();
        } catch (GRBException e) {}
        try {
            new_key_env.dispose();
        } catch (GRBException e) {}

        equal_circuits.clear();
    }

    @Override
    public String get_name() {
        return "LL_GUROBI_Solver_v1";
    }

    @Override
    public String get_version() {
        return VERSION;
    }

    @Override
    public String get_result_found() {
        return options.result_found;
    }

    @Override
    public Boolean[] get_found_key() {
        return options.found_key;
    }

    @Override
    public LL_SolverOptions get_options() {
        return options;
    }
}
