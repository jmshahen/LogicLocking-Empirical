package shahen.logiclocking.solver;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ilog.concert.IloException;
import shahen.logiclocking.results.LL_Result;
import shahen.string.ShahenStrings;
import shahen.timing.ShahenTimingEvent;

public interface LL_SolverI {
    /** Returns the name of the solver being used */
    public String get_name();

    /** Returns the name + version of the solver being used */
    public String get_version();

    /** Returns a boolean array of the found key, or null if no key has been found */
    public Boolean[] get_found_key();

    /** Returns the string representation of the result found (key found/timeout/error) */
    public String get_result_found();

    /** Returns the solver's options */
    public LL_SolverOptions get_options();

    /**
     * 
     * @return <ul>
     *  <li>-1 -- CPLEX timed out
     *  <li> 0 -- Unable to find a distinguishing input
     *  <li> 1 -- Able to find a new distinguishing input
     * </ul>
     * @throws IloException
     */
    public int solve() throws Exception;

    public void preload();

    public void setup_settings() throws Exception;

    /**
     * Validate that the inputs is indeed a distinguishing input, 
     * and check that both keys produce the correct output for all equal circuits in 
     * {@link LL_CPLEX_Solver_v1#equal_circuits}. 
     * @param inputs
     * @param key_a
     * @param key_b
     * @return boolean TRUE when the inputs and keys are validated by the circuit, FALSE otherwise
     */
    public boolean verify_step(Boolean[] input, Boolean[] key_a, Boolean[] key_b, Boolean[] output_a,
            Boolean[] output_b);

    /**
     * Stops the solver from writing out to the console
     */
    public void quiet();

    /**
     * To be used if there is a positive integer in {@link #solver_timeout}.
     * 
     * Uses {@link #startEvent}; If null, then it will create the start event for now.
     * 
     * @return <ul>
     *     <li>-1 -- no stage timeout
     *     <li>&nbsp;0 -- we have already timedout
     *     <li>&gt;0 -- set the timeout to this value 
     *     </ul>
     */
    static int calculate_stagetimeout(LL_SolverOptions options) {
        if (options.startEvent == null) {
            options.startEvent = new ShahenTimingEvent();
            options.startEvent.setStartTimeNow();
        }
        if (options.solver_timeout == null || options.solver_timeout <= 0) return -1;

        int runningTime = options.startEvent.durationSec().intValue();

        if (runningTime >= options.solver_timeout) return 0;
        if (options.solver_stagetimeout != null && options.solver_stagetimeout > 0) {
            return Math.min(options.solver_stagetimeout, runningTime);
        }

        return options.solver_timeout - runningTime;
    }

    static void garbage_collection(LL_SolverOptions options, int iter_num) {
        /*TIMING*/options.timing.startTimer(options.timer_key + ".garbage_collection(" + iter_num + ")");
        Runtime rt = Runtime.getRuntime();
        long byte_gigabyte = 1073741824;
        long byte_megabyte = 1048576;
        long free = rt.freeMemory();
        double free_gb = free / byte_gigabyte;
        long total = rt.totalMemory();
        long max = rt.maxMemory();
        DecimalFormat df = new DecimalFormat("#.#");

        String free_mem = (free > byte_gigabyte) ? df.format(free_gb) + "GB" : df.format(free / byte_megabyte) + "MB";
        String total_mem = (total > byte_gigabyte)
                ? df.format(total / byte_gigabyte) + "GB"
                : df.format(total / byte_megabyte) + "MB";
        String max_mem = (max > byte_gigabyte)
                ? df.format(max / byte_gigabyte) + "GB"
                : df.format(max / byte_megabyte) + "MB";

        System.out.println("Free Memory: " + free_mem + "; Total Memory: " + total_mem + "; Max Memory: " + max_mem);

        System.gc();

        if (free_gb > 10) {
            System.out.println("#####################################################################");
            System.out.println("# Starting Garbage Collector (" + options.garbage_collector_duration_sec + " sec)");
            System.out.println("#####################################################################");
            System.gc();
            try {
                // to allow GC do its job
                Thread.sleep(options.garbage_collector_duration_sec * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("#####################################################################");
            System.out.println("# End of Garbage Collector");
            System.out.println("#####################################################################");
        }
        /*TIMING*/options.timing.stopTimer(options.timer_key + ".garbage_collection(" + iter_num + ")");
    }

    public static void add_comment_and_variables(StringBuilder sb, StringBuilder sb_vars) {
        StringBuilder sb2 = new StringBuilder();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        sb2.append("/******************************************************\n")//
                .append(" * Model Created by Shahen Logic Locking\n")//
                .append(" * Date: " + formatter.format(new Date(System.currentTimeMillis())) + "\n")//
                .append(" ******************************************************/\n\n");

        sb2.append(sb_vars);

        sb.insert(0, sb2);
    }

    /**
     * Given 2 boolean arrays, checks if the contents are equal.
     * 
     * Null values in the arrays are allowed. 
     * @param in1
     * @param in2
     * @return
     */
    public static boolean are_equal(Boolean[] in1, Boolean[] in2) {
        if (in1 == null && in2 == null) return true;
        if (in1 == null || in2 == null) return false;
        if (in1.length != in2.length) return false;

        for (int i = 0; i < in1.length; i++) {
            if (in1[i] == null && in2[i] == null) continue;
            if (in1[i] == null && in2[i] != null) return false;
            if (in1[i] != null && in2[i] == null) return false;
            if (!in1[i].equals(in2[i])) return false;
        }

        return true;
    }

    /**
     * 
     * @param distinguishing_input
     */
    static void save_distinguishing_input(LL_SolverOptions options, String distinguishing_input, boolean append) {
        if (!options.dump_distinguishing_inputs) return;

        try {
            FileWriter fw = new FileWriter(options.dump_distinguishing_inputs_file, append);
            fw.append(distinguishing_input);
            fw.append("\n");
            fw.close();
        } catch (IOException e) {
            options.log.warning("Unable to Save Distinguishing Input to File ("
                    + options.dump_distinguishing_inputs_file.getAbsolutePath() + "): " + distinguishing_input);
        }
    }
    /**
     * 
     * @param new_key_str
     */
    static void save_new_key(LL_SolverOptions options, String new_key_str, boolean append) {
        if (!options.dump_new_keys) return;

        try {
            FileWriter fw = new FileWriter(options.dump_new_keys_file, append);
            fw.append(new_key_str);
            fw.append("\n");
            fw.close();
        } catch (IOException e) {
            options.log.warning("Unable to Save New Key to File (" + options.dump_new_keys_file.getAbsolutePath()
                    + "): " + new_key_str);
        }
    }

    /**
     * Start the solver's result and store relevant information, best to store as much information,
     * in case the result crashes
     * @param solver
     * @param options
     */
    static void start_result(LL_SolverI solver, LL_SolverOptions options) {
        options.last_result = options.results.create_new_result(solver, options.circuit);
        options.last_result.start();
        options.results.update_result(options.last_result);
    }

    static void finish_result(LL_SolverOptions options, int result) {
        options.result_found = LL_Result.resultToString(result);
        options.last_result.finish();
        options.last_result.result = result;
        options.last_result.resultString = options.result_found;
        options.last_result.keyBoolString = ShahenStrings.toBooleanString(options.found_key);
        String actual_key_str = ShahenStrings.toBooleanString(options.circuit.known_key_list);
        options.last_result.correctKey = (actual_key_str != null
                && actual_key_str.equals(options.last_result.keyBoolString)) ? 1 : 0;
        options.results.update_result(options.last_result);
    }

    /**
     * Call this method to speed up with garbage collection
     */
    public void destroy();
}
