package shahen.logiclocking.solver;

import java.io.File;
import java.util.logging.Logger;

import gurobi.*;
import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.reduction.LL_ReductionI;
import shahen.logiclocking.reduction.LL_Standard_Reduction;
import shahen.logiclocking.results.LL_Result;
import shahen.logiclocking.results.LL_ResultsManager;
import shahen.timing.ShahenTiming;
import shahen.timing.ShahenTimingEvent;

public class LL_SolverOptions {
    public Logger log;
    public ShahenTiming timing;
    public LL_ResultsManager results;
    public String timer_key = "LL_Solver";
    public String[] args = new String[0];

    /** Stores a reference of the locked circuit */
    public LL_Circuit circuit;

    /** Debug setting. Increased log messages, Export models before solving each step */
    public boolean debug = false;
    /** When TRUE, after each Satisfiable result from CPLEX, we will check that the inpu+key pairs match the output reported */
    public boolean verify_steps = false;
    /** When TRUE, after each Satisfiable result from CPLEX, we will write the found distinguishing input to a file. */
    public boolean dump_distinguishing_inputs = false;
    public File dump_distinguishing_inputs_file = new File("logs/distinguishing_inputs.txt");
    /** When TRUE, after each Satisfiable result from CPLEX, we will write the new key found a file. */
    public boolean dump_new_keys = false;
    public File dump_new_keys_file = new File("logs/new_keys.txt");
    /** Stores the name of the folder to be used when storing the debugging files */
    public String folder_name = "unknown_circuit";

    public LL_ReductionI reduction = null;

    /** The number of iteration stages that occurred before a result was found or it timed out */
    public int iter_count = 0;
    /** The last key that was found */
    public Boolean[] found_key = null;

    /** When TRUE, incrementally build up the circuit. Otherwise, clear the model and build the entire thing each time. */
    public boolean incremental_build = false;
    /** Stores the method number for how the result was found (which step of the execution) */
    public String result_found = null;
    /** The number of threads to limit CPLEX to, NULL to not limit the number of threads */
    public Integer solver_threads = null;
    /** The algorithm number to set CPLEX to, NULL to not change the default algorithm */
    public Integer solver_algorithm = null;
    /** The MIP emphasis number to set CPLEX to, NULL to not change the default algorithm */
    public Integer solver_mip_emphasis = null;
    /** The timeout the solver will use to calculate the maximum {@link #solver_stagetimeout}; null for no timeout */
    public Integer solver_timeout = null;
    /** The timeout to give CPLEX/GUROBI for each stage in the solution process; null for no timeout */
    public Integer solver_stagetimeout = null;
    /** Change the Parallel Mode Switch option to Opportunistic when TRUE */
    public Boolean cplex_parallel = null;
    /** The number of randomly generated equal circuits to populate Stage 0 with */
    public Integer preload_num = null;
    /** The file to load the distinguishing inputs. */
    public File preload_file = null;
    /** Add an extra Key A != Key B constraint for the Diff Circuit */
    public Boolean extra_key_constraint = null;

    /** The number of seconds to pause the threads execution and allow the garbage collection thread to run. */
    public int garbage_collector_duration_sec = 10;

    /** Holds the starting event for when {@link #solve()} is called. Used when calculating the time left. */
    public ShahenTimingEvent startEvent;
    /** Holds the {@link LL_Result} created {@link #solve()} is called. */
    public LL_Result last_result = null;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the reduction algorithm to use.
     * If NULL, then uses {@link LL_Standard_Reduction} as the default. 
     * @param r
     */
    public void set_reduction(LL_ReductionI r) {
        if (r == null) {
            reduction = new LL_Standard_Reduction(log);
        } else {
            reduction = r;
        }
    }

    public void setup_cplex_settings(IloCplex cplex) throws IloException {
        if (solver_threads != null) {
            cplex.setParam(IloCplex.Param.Threads, solver_threads);
        }
        if (solver_algorithm != null) {
            cplex.setParam(IloCplex.Param.RootAlgorithm, solver_algorithm);
        }
        if (solver_mip_emphasis != null) {
            cplex.setParam(IloCplex.IntParam.MIPEmphasis, solver_mip_emphasis);
        }
        if (solver_stagetimeout != null) {
            cplex.setParam(IloCplex.Param.TimeLimit, solver_stagetimeout);
        }
        if (cplex_parallel != null) {
            cplex.setParam(IloCplex.IntParam.ParallelMode,
                    (cplex_parallel) ? IloCplex.ParallelMode.Opportunistic : IloCplex.ParallelMode.Deterministic);
        }
    }

    public void setup_gurobi_settings(GRBModel gurobi) throws GRBException {
        if (solver_threads != null) {
            gurobi.set(GRB.IntParam.Threads, solver_threads);
        }
        if (solver_algorithm != null) {
            gurobi.set(GRB.IntParam.Method, solver_algorithm);
        }
        if (solver_mip_emphasis != null) {
            gurobi.set(GRB.IntParam.MIPFocus, solver_mip_emphasis);
        }
        if (solver_stagetimeout != null) {
            gurobi.set(GRB.DoubleParam.TimeLimit, solver_stagetimeout);
        }
    }
}
