package shahen.logiclocking.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.opl.IloCplex;

class LL_Cplex_Tests {

    @BeforeAll
    static void setUpBeforeClass() throws Exception {}

    @Test
    void lege_test() throws IloException {
        IloCplex cplex = new IloCplex();

        IloNumVar x = cplex.numVar(0, 10, "x");
        // DO NOT USE .ge() MUST USE THE addGe() versions
        cplex.addRange(10, x, 15);

        assertTrue(cplex.solve());
        cplex.output().println("Solution status: " + cplex.getStatus());

        int x_val = (int) cplex.getValue(x);
        assertEquals(x_val, 10);
        cplex.output().println("Solution value: " + x_val);
    }

}
