package shahen.logiclocking.testing;

import static org.junit.Assert.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;

class LL_Gate_Tests {

    @BeforeAll
    static void setUpBeforeClass() throws Exception {}

    @Test
    void test_mux_gate_propagate() {
        LL_Mux g = new LL_Mux();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire s = new LL_Wire("s", 2, WIRE_TYPE.KEY);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.s = s;
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(false);
        in2.set_value(true);
        g.propagate();
        assertNull(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "000=0",//
                "001=0",//
                "010=0",//
                "011=1",//
                "100=1",//
                "101=0",//
                "110=1",//
                "111=1"};
        for (String t : tests) {
            String[] split = t.split("=");

            in1.set_value(split[0].charAt(0) == '1');
            in2.set_value(split[0].charAt(1) == '1');
            s.set_value(split[0].charAt(2) == '1');
            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', out.get_value());
        }
    }
    @Test
    void test_mux_0x_gate_propagate() {
        LL_Mux g = new LL_Mux();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire s = new LL_Wire("s", 2, WIRE_TYPE.KEY);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.s = s;
        g.out = out;

        // Create Simplified Gate
        in1.set_value(false);
        LL_Gate sg = g.simplify();
        assertNotNull(sg);
        assertTrue(sg instanceof LL_Mux0X);

        // Test: simple propagation, "in2 s = out" (in1 = 0)
        String[] tests = {//
                "000=0",//
                "001=0",//
                "010=0",//
                "011=1"//
        };
        for (String t : tests) {
            String[] split = t.split("=");

            // in1.set_value(split[0].charAt(0) == '1');
            in2.set_value(split[0].charAt(1) == '1');
            s.set_value(split[0].charAt(2) == '1');
            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', out.get_value());
        }
    }
    @Test
    void test_mux_1x_gate_propagate() {
        LL_Mux g = new LL_Mux();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire s = new LL_Wire("s", 2, WIRE_TYPE.KEY);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.s = s;
        g.out = out;

        // Create Simplified Gate
        in1.set_value(true);
        LL_Gate sg = g.simplify();
        assertNotNull(sg);
        assertTrue(sg instanceof LL_Mux1X);

        // Test: simple propagation, "in2 s = out" (in1 = 1)
        String[] tests = {//
                "100=1",//
                "101=0",//
                "110=1",//
                "111=1"//
        };
        for (String t : tests) {
            String[] split = t.split("=");

            // in1.set_value(split[0].charAt(0) == '1');
            in2.set_value(split[0].charAt(1) == '1');
            s.set_value(split[0].charAt(2) == '1');
            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', out.get_value());
        }
    }
    @Test
    void test_mux_x0_gate_propagate() {
        LL_Mux g = new LL_Mux();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire s = new LL_Wire("s", 2, WIRE_TYPE.KEY);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.s = s;
        g.out = out;

        // Create Simplified Gate
        in2.set_value(false);
        LL_Gate sg = g.simplify();
        assertNotNull(sg);
        assertTrue(sg instanceof LL_MuxX0);

        // Test: simple propagation, "in1 in2 s = out"
        String[] tests = {//
                "000=0",//
                "001=0",//
                "100=1",//
                "101=0"//
        };
        for (String t : tests) {
            String[] split = t.split("=");

            in1.set_value(split[0].charAt(0) == '1');
            // in2.set_value(split[0].charAt(1) == '1');
            s.set_value(split[0].charAt(2) == '1');
            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', out.get_value());
        }
    }
    @Test
    void test_mux_x1_gate_propagate() {
        LL_Mux g = new LL_Mux();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire s = new LL_Wire("s", 2, WIRE_TYPE.KEY);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.s = s;
        g.out = out;

        // Create Simplified Gate
        in2.set_value(true);
        LL_Gate sg = g.simplify();
        assertNotNull(sg);
        assertTrue(sg instanceof LL_MuxX1);

        // Test: simple propagation, "in1 in2 s = out"
        String[] tests = {//
                "010=0",//
                "011=1",//
                "110=1",//
                "111=1"//
        };
        for (String t : tests) {
            String[] split = t.split("=");

            in1.set_value(split[0].charAt(0) == '1');
            // in2.set_value(split[0].charAt(1) == '1');
            s.set_value(split[0].charAt(2) == '1');
            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', out.get_value());
        }
    }
    @Test
    void test_buf_gate_propagate() {
        LL_Buf g = new LL_Buf();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.out = out;

        // Test: not enough information to propagate
        g.propagate();
        assertNull(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "0=0",//
                "1=1"//
        };
        test_1_input_gate(g, tests);
    }

    @Test
    void test_not_gate_propagate() {
        LL_Not g = new LL_Not();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.out = out;

        // Test: not enough information to propagate
        g.propagate();
        assertNull(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "0=1",//
                "1=0"//
        };
        test_1_input_gate(g, tests);
    }

    private void test_1_input_gate(LL_Gate g, String[] tests) {
        for (String t : tests) {
            String[] split = t.split("=");

            g.in1.set_value(split[0].charAt(0) == '1');
            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', g.out.get_value());
        }
    }

    @Test
    void test_and_gate_propagate() {
        LL_And g = new LL_And();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(true);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(false);
        g.infer_value();
        assertFalse(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "00=0",//
                "01=0",//
                "10=0",//
                "11=1"//
        };
        test_2_input_gate(g, tests);
    }
    @Test
    void test_or_gate_propagate() {
        LL_Or g = new LL_Or();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(false);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(true);
        g.infer_value();
        assertTrue(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "00=0",//
                "01=1",//
                "10=1",//
                "11=1"//
        };
        test_2_input_gate(g, tests);
    }
    @Test
    void test_nand_gate_propagate() {
        LL_Nand g = new LL_Nand();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(true);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(false);
        g.infer_value();
        assertTrue(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "00=1",//
                "01=1",//
                "10=1",//
                "11=0"//
        };
        test_2_input_gate(g, tests);
    }
    @Test
    void test_nor_gate_propagate() {
        LL_Nor g = new LL_Nor();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(false);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(true);
        g.infer_value();
        assertFalse(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "00=1",//
                "01=0",//
                "10=0",//
                "11=0"//
        };
        test_2_input_gate(g, tests);
    }
    @Test
    void test_xor_gate_propagate() {
        LL_Xor g = new LL_Xor();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(false);
        g.propagate();
        assertNull(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "00=0",//
                "01=1",//
                "10=1",//
                "11=0"//
        };
        test_2_input_gate(g, tests);
    }
    @Test
    void test_xnor_gate_propagate() {
        LL_Xnor g = new LL_Xnor();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 2, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(false);
        g.propagate();
        assertNull(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "00=1",//
                "01=0",//
                "10=0",//
                "11=1"//
        };
        test_2_input_gate(g, tests);
    }
    private void test_2_input_gate(LL_Gate g, String[] tests) {
        for (String t : tests) {
            String[] split = t.split("=");

            g.in1.set_value(split[0].charAt(0) == '1');
            g.in2.set_value(split[0].charAt(1) == '1');
            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', g.out.get_value());
        }
    }
    @Test
    void test_large_and_gate_propagate() {
        LL_Large_And g = new LL_Large_And();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire in3 = new LL_Wire("in3", 2, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.inputs.add(in1);
        g.inputs.add(in2);
        g.inputs.add(in3);
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(true);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(false);
        g.infer_value();
        assertFalse(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "000=0",//
                "001=0",//
                "010=0",//
                "011=0",//
                "100=0",//
                "101=0",//
                "110=0",//
                "111=1"};
        test_large_input_gate(g, tests);
    }

    @Test
    void test_large_nand_gate_propagate() {
        LL_Large_Nand g = new LL_Large_Nand();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire in3 = new LL_Wire("in3", 2, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.inputs.add(in1);
        g.inputs.add(in2);
        g.inputs.add(in3);
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(true);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(false);
        g.infer_value();
        assertTrue(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "000=1",//
                "001=1",//
                "010=1",//
                "011=1",//
                "100=1",//
                "101=1",//
                "110=1",//
                "111=0"//
        };
        test_large_input_gate(g, tests);
    }

    @Test
    void test_large_or_gate_propagate() {
        LL_Large_Or g = new LL_Large_Or();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire in3 = new LL_Wire("in3", 2, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.inputs.add(in1);
        g.inputs.add(in2);
        g.inputs.add(in3);
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(false);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(true);
        g.infer_value();
        assertTrue(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "000=0",//
                "001=1",//
                "010=1",//
                "011=1",//
                "100=1",//
                "101=1",//
                "110=1",//
                "111=1"//
        };
        test_large_input_gate(g, tests);
    }

    @Test
    void test_large_nor_gate_propagate() {
        LL_Large_Nor g = new LL_Large_Nor();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire in3 = new LL_Wire("in3", 2, WIRE_TYPE.INPUT);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.inputs.add(in1);
        g.inputs.add(in2);
        g.inputs.add(in3);
        g.out = out;

        // Test: not enough information to propagate
        in1.set_value(false);
        g.propagate();
        assertNull(out.get_value());

        // Test: enough information to infer
        in1.set_value(true);
        g.infer_value();
        assertFalse(out.get_value());

        // Test: simple propagation
        String[] tests = {//
                "000=1",//
                "001=0",//
                "010=0",//
                "011=0",//
                "100=0",//
                "101=0",//
                "110=0",//
                "111=0"};
        test_large_input_gate(g, tests);
    }

    private void test_large_input_gate(LL_LargeGate g, String[] tests) {
        for (String t : tests) {
            String[] split = t.split("=");
            for (int i = 0; i < split[0].length(); i++) {
                g.inputs.get(i).set_value(split[0].charAt(i) == '1');
            }

            g.propagate();
            assertEquals("Propagate Test: " + t, split[1].charAt(0) == '1', g.out.get_value());
        }
    }

    @Test
    void test_mux_gate_simplify() {
        LL_Mux g = new LL_Mux();
        LL_Wire in1 = new LL_Wire("in1", 0, WIRE_TYPE.INPUT);
        LL_Wire in2 = new LL_Wire("in2", 1, WIRE_TYPE.INPUT);
        LL_Wire s = new LL_Wire("s", 2, WIRE_TYPE.KEY);
        LL_Wire out = new LL_Wire("out", 3, WIRE_TYPE.OUTPUT);

        g.in1 = in1;
        g.in2 = in2;
        g.s = s;
        g.out = out;

        // Test: both inputs are false; in1=0,in2=0,s=X
        in1.set_value(false);
        in2.set_value(false);
        s.reset_wire();
        LL_Gate gs = g.simplify();
        assertTrue(gs instanceof LL_Off);

        // Test: both inputs are true; in1=1,in2=1,s=X
        in1.set_value(true);
        in2.set_value(true);
        s.reset_wire();
        gs = g.simplify();
        assertTrue(gs instanceof LL_On);

        // Test: both inputs are true; in1=1,in2=1,s=0
        in1.set_value(true);
        in2.set_value(true);
        s.set_value(false);
        gs = g.simplify();
        assertTrue(gs instanceof LL_On);

        // Test: both inputs are true; in1=1,in2=1,s=1
        in1.set_value(true);
        in2.set_value(true);
        s.set_value(true);
        gs = g.simplify();
        assertTrue(gs instanceof LL_On);

        // Test: setting in1=X,in2=X,s=0
        in1.reset_wire();
        in2.reset_wire();
        s.set_value(false);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Buf);

        // Test: setting in1=X,in2=X,s=1
        in1.reset_wire();
        in2.reset_wire();
        s.set_value(true);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Buf);

        // Test: setting in1=X,in2=0,s=0
        in1.reset_wire();
        in2.set_value(false);
        s.set_value(false);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Buf);

        // Test: setting in1=X,in2=1,s=0
        in1.reset_wire();
        in2.set_value(true);
        s.set_value(false);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Buf);

        // Test: setting in1=0,in2=X,s=1
        in1.set_value(false);
        in2.reset_wire();
        s.set_value(true);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Buf);

        // Test: setting in1=1,in2=X,s=1
        in1.set_value(true);
        in2.reset_wire();
        s.set_value(true);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Buf);

        // Test: setting in1=0,in2=X,s=0
        in1.set_value(false);
        in2.reset_wire();
        s.set_value(false);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Off);

        // Test: setting in1=1,in2=X,s=0
        in1.set_value(true);
        in2.reset_wire();
        s.set_value(false);
        gs = g.simplify();
        assertTrue(gs instanceof LL_On);

        // Test: setting in1=X,in2=0,s=1
        in1.reset_wire();
        in2.set_value(false);
        s.set_value(true);
        gs = g.simplify();
        assertTrue(gs instanceof LL_Off);

        // Test: setting in1=X,in2=1,s=1
        in1.reset_wire();
        in2.set_value(true);
        s.set_value(true);
        gs = g.simplify();
        assertTrue(gs instanceof LL_On);

        // Test: setting in1 != in2, in1=0,in2=1,s=X
        in1.set_value(false);
        in2.set_value(true);
        s.reset_wire();
        gs = g.simplify();
        assertTrue(gs instanceof LL_Buf);

        // Test: setting in1 != in2, in1=1,in2=0,s=X
        in1.set_value(true);
        in2.set_value(false);
        s.reset_wire();
        gs = g.simplify();
        assertTrue(gs instanceof LL_Not);

        // Test: setting in1=0,in2=X,s=X
        in1.set_value(false);
        in2.reset_wire();
        s.reset_wire();
        gs = g.simplify();
        assertTrue(gs instanceof LL_Mux0X);

        // Test: setting in1=1,in2=X,s=X
        in1.set_value(true);
        in2.reset_wire();
        s.reset_wire();
        gs = g.simplify();
        assertTrue(gs instanceof LL_Mux1X);

        // Test: setting in1=X,in2=0,s=X
        in1.reset_wire();
        in2.set_value(false);
        s.reset_wire();
        gs = g.simplify();
        assertTrue(gs instanceof LL_MuxX0);

        // Test: setting in1=X,in2=1,s=X
        in1.reset_wire();
        in2.set_value(true);
        s.reset_wire();
        gs = g.simplify();
        assertTrue(gs instanceof LL_MuxX1);

    }

}
