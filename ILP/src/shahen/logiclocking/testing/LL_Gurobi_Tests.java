package shahen.logiclocking.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import gurobi.*;
import ilog.concert.IloException;

class LL_Gurobi_Tests {

    @BeforeAll
    static void setUpBeforeClass() throws Exception {}

    @Test
    void lege_test() throws IloException, GRBException {
        GRBEnv env = new GRBEnv();
        GRBModel gurobi = new GRBModel(env);

        GRBVar x = gurobi.addVar(0.0, 10.0, 0, GRB.INTEGER, "x");
        GRBLinExpr lin = new GRBLinExpr();
        lin.addTerm(1, x);
        gurobi.addRange(lin, 10, 15, null);

        gurobi.optimize();
        assertEquals(GRB.Status.OPTIMAL, gurobi.get(GRB.IntAttr.Status));
        assertEquals(10.0, x.get(GRB.DoubleAttr.X));
    }

    @Test
    public void test_large_or() {
        String circuit_str = "# key=1\n" + //
                "INPUT(pi00)\n" + //
                "INPUT(pi01)\n" + //
                "INPUT(keyinput0)\n" +//
                "OUTPUT(po0)\n" + //
                "po0 = or(pi00, keyinput0, pi01)";
    }

}
