package shahen.logiclocking.testing;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.logging.Logger;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import gurobi.*;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.opl.IloCplex;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_NotEqualCircuit;
import shahen.logiclocking.circuit.gurobi.LL_GUROBI_NotEqualCircuit;

class LL_NotEqualCircuit_Tests {
    public static Logger log;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        log = Logger.getAnonymousLogger();
    }

    @Test
    void test_1_bit_gurobi() throws GRBException {
        GRBEnv env = new GRBEnv();
        GRBModel gurobi = new GRBModel(env);

        GRBVar[] in1 = gurobi.addVars(1, GRB.BINARY);
        GRBVar[] in2 = gurobi.addVars(1, GRB.BINARY);
        double a, b;

        LL_GUROBI_NotEqualCircuit noteq = new LL_GUROBI_NotEqualCircuit(log, in1, in2);
        noteq.addToGurobi(gurobi);

        gurobi.optimize();
        assertEquals(GRB.Status.OPTIMAL, gurobi.get(GRB.IntAttr.Status));

        a = in1[0].get(GRB.DoubleAttr.X);
        b = in2[0].get(GRB.DoubleAttr.X);
        assertTrue(a == 0.0 || a == 1.0);
        assertTrue(b == 0.0 || b == 1.0);
        assertTrue(a != b);

        // in1[0] == 1
        in1[0].set(GRB.DoubleAttr.LB, 1.0);
        in1[0].set(GRB.DoubleAttr.UB, 1.0);
        gurobi.optimize();
        assertEquals(GRB.Status.OPTIMAL, gurobi.get(GRB.IntAttr.Status));

        a = in1[0].get(GRB.DoubleAttr.X);
        b = in2[0].get(GRB.DoubleAttr.X);
        assertTrue(a == 1.0);
        assertTrue(b == 0.0);

        // in1[0] == 0
        in1[0].set(GRB.DoubleAttr.LB, 0.0);
        in1[0].set(GRB.DoubleAttr.UB, 0.0);
        gurobi.optimize();
        assertEquals(GRB.Status.OPTIMAL, gurobi.get(GRB.IntAttr.Status));

        a = in1[0].get(GRB.DoubleAttr.X);
        b = in2[0].get(GRB.DoubleAttr.X);
        assertTrue(a == 0.0);
        assertTrue(b == 1.0);

        // in1[0] == 0 && in2[0] == 0
        in1[0].set(GRB.DoubleAttr.LB, 0.0);
        in1[0].set(GRB.DoubleAttr.UB, 0.0);
        in2[0].set(GRB.DoubleAttr.LB, 0.0);
        in2[0].set(GRB.DoubleAttr.UB, 0.0);
        gurobi.optimize();
        assertEquals(GRB.Status.INFEASIBLE, gurobi.get(GRB.IntAttr.Status));
    }
    @Test
    void test_2_bit_gurobi() throws GRBException {
        GRBEnv env = new GRBEnv();
        GRBModel gurobi = new GRBModel(env);

        GRBVar[] var_in1 = gurobi.addVars(2, GRB.BINARY);
        GRBVar[] var_in2 = gurobi.addVars(2, GRB.BINARY);
        double[] in1 = new double[2];
        double[] in2 = new double[2];

        LL_GUROBI_NotEqualCircuit noteq = new LL_GUROBI_NotEqualCircuit(log, var_in1, var_in2);
        noteq.addToGurobi(gurobi);

        gurobi.optimize();
        assertEquals(GRB.Status.OPTIMAL, gurobi.get(GRB.IntAttr.Status));

        in1[0] = var_in1[0].get(GRB.DoubleAttr.X);
        in1[1] = var_in1[1].get(GRB.DoubleAttr.X);
        in2[0] = var_in2[0].get(GRB.DoubleAttr.X);
        in2[1] = var_in2[1].get(GRB.DoubleAttr.X);
        assertTrue(in1[0] == 0.0 || in1[0] == 1.0);
        assertTrue(in1[1] == 0.0 || in1[1] == 1.0);
        assertTrue(in2[0] == 0.0 || in2[0] == 1.0);
        assertTrue(in2[1] == 0.0 || in2[1] == 1.0);
        assertTrue(Math.abs(in1[0] - in2[0]) + Math.abs(in1[1] - in2[1]) > 0);

        // in1[0] == 1
        var_in1[0].set(GRB.DoubleAttr.LB, 1.0);
        var_in1[0].set(GRB.DoubleAttr.UB, 1.0);
        gurobi.optimize();
        assertEquals(GRB.Status.OPTIMAL, gurobi.get(GRB.IntAttr.Status));

        in1[0] = var_in1[0].get(GRB.DoubleAttr.X);
        in1[1] = var_in1[1].get(GRB.DoubleAttr.X);
        in2[0] = var_in2[0].get(GRB.DoubleAttr.X);
        in2[1] = var_in2[1].get(GRB.DoubleAttr.X);
        assertTrue(in1[0] == 1.0);
        assertTrue(Math.abs(in1[0] - in2[0]) + Math.abs(in1[1] - in2[1]) > 0);

        // in1[0] == 1 && in1[1] == 1
        var_in1[0].set(GRB.DoubleAttr.LB, 1.0);
        var_in1[0].set(GRB.DoubleAttr.UB, 1.0);
        var_in1[1].set(GRB.DoubleAttr.LB, 1.0);
        var_in1[1].set(GRB.DoubleAttr.UB, 1.0);
        gurobi.optimize();
        assertEquals(GRB.Status.OPTIMAL, gurobi.get(GRB.IntAttr.Status));

        in1[0] = var_in1[0].get(GRB.DoubleAttr.X);
        in1[1] = var_in1[1].get(GRB.DoubleAttr.X);
        in2[0] = var_in2[0].get(GRB.DoubleAttr.X);
        in2[1] = var_in2[1].get(GRB.DoubleAttr.X);
        assertTrue(in1[0] == 1.0);
        assertTrue(in1[1] == 1.0);
        assertTrue(Math.abs(in1[0] - in2[0]) + Math.abs(in1[1] - in2[1]) > 0);
    }
    @Test
    void test_1_bit_cplex() throws IloException {
        IloCplex cplex = new IloCplex();

        IloIntVar[] in1 = cplex.boolVarArray(1);
        IloIntVar[] in2 = cplex.boolVarArray(1);
        boolean a, b;

        LL_CPLEX_NotEqualCircuit noteq = new LL_CPLEX_NotEqualCircuit(log, in1, in2);
        noteq.addToCplex(cplex);

        assertTrue(cplex.solve());

        a = LL_CPLEX_Circuit.getVarBoolArray(cplex, in1)[0];
        b = LL_CPLEX_Circuit.getVarBoolArray(cplex, in2)[0];
        assertTrue(a == false || a == true);
        assertTrue(b == false || b == true);
        assertTrue(a != b);

        // in1[0] == 1
        in1[0].setLB(1.0);
        in1[0].setUB(1.0);
        assertTrue(cplex.solve());

        a = LL_CPLEX_Circuit.getVarBoolArray(cplex, in1)[0];
        b = LL_CPLEX_Circuit.getVarBoolArray(cplex, in2)[0];
        assertTrue(a == true);
        assertTrue(b == false);

        // in1[0] == 0
        in1[0].setLB(0.0);
        in1[0].setUB(0.0);
        assertTrue(cplex.solve());

        a = LL_CPLEX_Circuit.getVarBoolArray(cplex, in1)[0];
        b = LL_CPLEX_Circuit.getVarBoolArray(cplex, in2)[0];
        assertTrue(a == false);
        assertTrue(b == true);

        // in1[0] == 0 && in2[0] == 0
        in1[0].setLB(0.0);
        in1[0].setUB(0.0);
        in2[0].setLB(0.0);
        in2[0].setUB(0.0);
        assertFalse(cplex.solve());
    }
    @Test
    void test_2_bit_cplex() throws IloException {
        IloCplex cplex = new IloCplex();

        IloIntVar[] var_in1 = cplex.boolVarArray(2);
        IloIntVar[] var_in2 = cplex.boolVarArray(2);
        Boolean[] in1 = new Boolean[2];
        Boolean[] in2 = new Boolean[2];

        LL_CPLEX_NotEqualCircuit noteq = new LL_CPLEX_NotEqualCircuit(log, var_in1, var_in2);
        noteq.addToCplex(cplex);

        assertTrue(cplex.solve());

        in1 = LL_CPLEX_Circuit.getVarBoolArray(cplex, var_in1);
        in2 = LL_CPLEX_Circuit.getVarBoolArray(cplex, var_in2);
        assertTrue(in1[0] == false || in1[0] == true);
        assertTrue(in1[1] == false || in1[1] == true);
        assertTrue(in2[0] == false || in2[0] == true);
        assertTrue(in2[1] == false || in2[1] == true);
        assertTrue(Math.abs(toint(in1[0]) - toint(in2[0])) + Math.abs(toint(in1[1]) - toint(in2[1])) > 0);

        // in1[0] == 1
        var_in1[0].setLB(1.0);
        var_in1[0].setUB(1.0);
        assertTrue(cplex.solve());

        in1 = LL_CPLEX_Circuit.getVarBoolArray(cplex, var_in1);
        in2 = LL_CPLEX_Circuit.getVarBoolArray(cplex, var_in2);
        assertTrue(in1[0] == true);
        assertTrue(Math.abs(toint(in1[0]) - toint(in2[0])) + Math.abs(toint(in1[1]) - toint(in2[1])) > 0);

        // in1[0] == 1 && in1[1] == 1
        var_in1[0].setLB(1.0);
        var_in1[0].setUB(1.0);
        var_in1[1].setLB(1.0);
        var_in1[1].setUB(1.0);
        assertTrue(cplex.solve());

        in1 = LL_CPLEX_Circuit.getVarBoolArray(cplex, var_in1);
        in2 = LL_CPLEX_Circuit.getVarBoolArray(cplex, var_in2);
        assertTrue(in1[0] == true);
        assertTrue(in1[1] == true);
        assertTrue(Math.abs(toint(in1[0]) - toint(in2[0])) + Math.abs(toint(in1[1]) - toint(in2[1])) > 0);
    }

    private int toint(Boolean boolean1) {
        return boolean1 ? 1 : 0;
    }

}
