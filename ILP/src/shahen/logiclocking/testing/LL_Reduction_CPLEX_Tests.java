package shahen.logiclocking.testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ilog.concert.IloException;
import ilog.opl.IloCplex;
import shahen.logiclocking.LL_ILPInstance;
import shahen.logiclocking.circuit.LL_Circuit;
import shahen.logiclocking.circuit.LL_SimplifiedCircuit;
import shahen.logiclocking.circuit.cplex.LL_CPLEX_Circuit;
import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.reduction.*;
import shahen.string.ShahenStrings;

class LL_Reduction_CPLEX_Tests {
    public static Logger log;
    public static IloCplex cplex;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        log = Logger.getAnonymousLogger();
        cplex = new IloCplex();
        cplex.setOut(null);
        cplex.setWarning(null);
    }

    @Test
    public void not_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_output_wire("out");
        circuit.add_not_gate("in1", "out");

        test_all(circuit);
    }

    @Test
    public void buf_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_output_wire("out");
        circuit.add_buf_gate("in1", "out");

        test_all(circuit);
    }

    @Test
    public void or_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_output_wire("out");
        circuit.add_or_gate("in1", "in2", "out");

        test_all(circuit);
    }

    @Test
    public void nor_gate_cplex_standard() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_output_wire("out");
        circuit.add_nor_gate("in1", "in2", "out");

        test_all(circuit);
    }

    @Test
    public void and_gate_cplex_standard() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_output_wire("out");
        circuit.add_and_gate("in1", "in2", "out");

        test_all(circuit);
    }

    @Test
    public void nand_gate_cplex_standard() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_output_wire("out");
        circuit.add_nand_gate("in1", "in2", "out");

        test_all(circuit);
    }

    @Test
    public void xor_gate_cplex_standard() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_output_wire("out");
        circuit.add_xor_gate("in1", "in2", "out");

        test_all(circuit);
    }

    @Test
    public void xnor_gate_cplex_standard() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_output_wire("out");
        circuit.add_xnor_gate("in1", "in2", "out");

        test_all(circuit);
    }

    @Test
    public void large_and_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("in3");
        circuit.add_output_wire("out");
        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("in1");
        inputs.add("in2");
        inputs.add("in3");
        circuit.add_large_and_gate(inputs, "out");

        test_all(circuit);
    }

    @Test
    public void large_or_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("in3");
        circuit.add_output_wire("out");
        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("in1");
        inputs.add("in2");
        inputs.add("in3");
        circuit.add_large_or_gate(inputs, "out");

        test_all(circuit);
    }

    @Test
    public void large_nand_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("in3");
        circuit.add_output_wire("out");
        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("in1");
        inputs.add("in2");
        inputs.add("in3");
        circuit.add_large_nand_gate(inputs, "out");

        test_all(circuit);
    }

    @Test
    public void large_nor_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("in3");
        circuit.add_output_wire("out");
        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("in1");
        inputs.add("in2");
        inputs.add("in3");
        circuit.add_large_nor_gate(inputs, "out");

        test_all(circuit);
    }

    @Test
    public void mux_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("s");
        circuit.add_output_wire("out");
        circuit.add_mux_gate(new LL_Mux(), "in1", "in2", "s", "out");

        test_all(circuit);
    }

    @Test
    public void mux_0x_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("keyinput1");
        circuit.add_output_wire("out");
        circuit.add_mux_gate(new LL_Mux(), "in1", "in2", "keyinput1", "out");

        LL_SimplifiedCircuit sc = new LL_SimplifiedCircuit(circuit, new Boolean[]{false, null}, null);

        test_all(sc);
    }

    @Test
    public void mux_1x_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("keyinput1");
        circuit.add_output_wire("out");
        circuit.add_mux_gate(new LL_Mux(), "in1", "in2", "keyinput1", "out");

        LL_SimplifiedCircuit sc = new LL_SimplifiedCircuit(circuit, new Boolean[]{true, null}, null);

        test_all(sc);
    }

    @Test
    public void mux_x0_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("keyinput1");
        circuit.add_output_wire("out");
        circuit.add_mux_gate(new LL_Mux(), "in1", "in2", "keyinput1", "out");

        LL_SimplifiedCircuit sc = new LL_SimplifiedCircuit(circuit, new Boolean[]{null, false}, null);

        test_all(sc);
    }

    @Test
    public void mux_x1_gate_cplex() throws IloException {
        LL_Circuit circuit = new LL_Circuit();
        circuit.add_input_wire("in1");
        circuit.add_input_wire("in2");
        circuit.add_input_wire("keyinput1");
        circuit.add_output_wire("out");
        circuit.add_mux_gate(new LL_Mux(), "in1", "in2", "keyinput1", "out");

        LL_SimplifiedCircuit sc = new LL_SimplifiedCircuit(circuit, new Boolean[]{null, true}, null);

        test_all(sc);
    }

    private static void test_all(LL_Circuit circuit) throws IloException {
        test_gate(circuit, new LL_Standard_Reduction(log));
        test_gate(circuit, new LL_Reduced_Reduction(log));
        test_gate(circuit, new LL_CNFSAT_Reduction(log));
    }

    private static void test_gate(LL_Circuit circuit, LL_ReductionI reduction) throws IloException {
        cplex.clearModel();
        LL_Gate g = circuit.gates.get(circuit.outputs.get("out"));
        System.out.println(g.gate_name() + " - " + reduction.toString());

        LL_CPLEX_Circuit cc = new LL_CPLEX_Circuit(circuit, "c");
        Boolean[] test = new Boolean[g.numInputs() + 1];
        for (int i = 0; i < test.length; i++)
            test[i] = false;

        cc.addToCplex(reduction, cplex, null, null);

        while (test != null) {
            test_inputs(cc, test, g, reduction);
            test = LL_ILPInstance.getNextInput(test);
        }
        System.out.println("");
    }

    private static void test_inputs(LL_CPLEX_Circuit cc, Boolean[] test, LL_Gate g, LL_ReductionI reduction)
            throws IloException {
        assertEquals(cc.inputs.length + cc.key.length + 1, test.length);
        assertEquals(g.numInputs() + 1, test.length);

        double val;

        for (int i = 0; i < cc.inputs.length; i++) {
            val = test[i] ? 1.0 : 0.0;
            cc.inputs[i].setLB(val);
            cc.inputs[i].setUB(val);
        }
        for (int i = 0; i < cc.key.length; i++) {
            val = test[i + cc.inputs.length] ? 1.0 : 0.0;
            cc.key[i].setLB(val);
            cc.key[i].setUB(val);
        }
        val = test[test.length - 1] ? 1.0 : 0.0;
        cc.outputs[0].setLB(val);
        cc.outputs[0].setUB(val);

        if (g.numInputs() == 1) {
            g.in1.set_value(test[0]);
        } else if (g.numInputs() == 2 && !(g instanceof LL_Mux)) {
            g.in1.set_value(test[0]);
            g.in2.set_value(test[1]);
        } else if (g instanceof LL_Mux) {
            int i = 0;
            if (g.in1 != null) {
                g.in1.set_value(test[i]);
                i++;
            }
            if (g.in2 != null) {
                g.in2.set_value(test[i]);
                i++;
            }
            ((LL_Mux) g).s.set_value(test[i]);
        } else {
            for (int i = 0; i < g.numInputs(); i++) {
                ((LL_LargeGate) g).inputs.get(i).set_value(test[i]);
            }
        }

        assertTrue(g.propagate());

        boolean result = cplex.solve();
        boolean expected_feasible = test[test.length - 1] == g.out.get_value();
        String test_str = ShahenStrings.toBooleanString(test);
        System.out.print(" * Testing: " + test_str + " - " + g + "...");
        if (result) {
            System.out.println("Able to find a solution " + (expected_feasible ? "[Success]" : "[Failed]"));

            if (expected_feasible != result) {
                cplex.exportModel("logs/" + reduction.toString() + "_" + g.gate_name() + "_" + test_str + ".lp");
            }

            assertTrue(expected_feasible);
        } else {
            System.out.println("Unable to find a solution " + ((!expected_feasible) ? "[Success]" : "[Failed]"));

            if (expected_feasible != result) {
                cplex.exportModel("logs/" + reduction.toString() + "_" + g.gate_name() + "_" + test_str + ".lp");
            }
            assertFalse(expected_feasible);
        }
    }
}
