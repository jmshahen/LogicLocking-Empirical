package shahen.logiclocking.testing;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import shahen.logiclocking.circuit.*;
import shahen.logiclocking.circuit.gates.*;
import shahen.logiclocking.circuit.wires.LL_Wire;
import shahen.logiclocking.circuit.wires.WIRE_TYPE;

class LL_SimplifyCircuit_Tests {

    @BeforeAll
    static void setUpBeforeClass() throws Exception {}

    public LL_Circuit get_circuit() {
        LL_Circuit c = new LL_Circuit();

        // INPUT(pi00)
        c.add_input_wire("pi00");
        // INPUT(pi01)
        c.add_input_wire("pi01");
        // INPUT(pi02)
        c.add_input_wire("pi02");
        // INPUT(pi03)
        c.add_input_wire("pi03");
        // INPUT(pi04)
        c.add_input_wire("pi04");
        // INPUT(keyinput0)
        c.add_input_wire("keyinput0");
        // INPUT(keyinput1)
        c.add_input_wire("keyinput1");
        // OUTPUT(po0)
        c.add_output_wire("po0");
        // OUTPUT(po1)
        c.add_output_wire("po1");
        // n01 = and(pi00,pi01)
        c.add_and_gate("pi00", "pi01", "n01");
        // n02 = or(pi01,pi02)
        c.add_or_gate("pi01", "pi02", "n02");
        // n03 = and(pi00,pi03)
        c.add_and_gate("pi00", "pi03", "n03");
        // n04 = not(pi04)
        c.add_not_gate("pi04", "n04");
        // n05 = nand(n02, pi03)
        c.add_nand_gate("n02", "pi03", "n05");
        // n06 = buf(n03)
        c.add_buf_gate("n03", "n06");
        // po0 = nand(n05,n09$enc)
        c.add_nand_gate("n05", "n09$enc", "po0");
        // po1 = nor(n06, n10$enc)
        c.add_nor_gate("n06", "n10$enc", "po1");
        // n09$enc = xor(keyinput0,n01)
        c.add_xor_gate("keyinput0", "n01", "n09$enc");
        // n10$enc = xnor(keyinput1, n04)
        c.add_xnor_gate("keyinput1", "n04", "n10$enc");

        return c;
    }

    public LL_Gate get_and_gate() {
        LL_Gate and = new LL_And();
        and.in1 = new LL_Wire("in1", 1, WIRE_TYPE.INPUT);
        and.in2 = new LL_Wire("in2", 2, WIRE_TYPE.INPUT);
        and.out = new LL_Wire("out", 2, WIRE_TYPE.INTERNAL);
        return and;
    }
    public LL_Gate get_or_gate() {
        LL_Gate or = new LL_Or();
        or.in1 = new LL_Wire("in1", 1, WIRE_TYPE.INPUT);
        or.in2 = new LL_Wire("in2", 2, WIRE_TYPE.INPUT);
        or.out = new LL_Wire("out", 2, WIRE_TYPE.INTERNAL);
        return or;
    }
    public LL_Gate get_nor_gate() {
        LL_Gate nor = new LL_Nor();
        nor.in1 = new LL_Wire("in1", 1, WIRE_TYPE.INPUT);
        nor.in2 = new LL_Wire("in2", 2, WIRE_TYPE.INPUT);
        nor.out = new LL_Wire("out", 2, WIRE_TYPE.INTERNAL);
        return nor;
    }
    public LL_Gate get_nand_gate() {
        LL_Gate nand = new LL_Nand();
        nand.in1 = new LL_Wire("in1", 1, WIRE_TYPE.INPUT);
        nand.in2 = new LL_Wire("in2", 2, WIRE_TYPE.INPUT);
        nand.out = new LL_Wire("out", 2, WIRE_TYPE.INTERNAL);
        return nand;
    }

    @Test
    void test_simplify_and_gate() {
        LL_Gate and = get_and_gate();
        LL_Gate sg;

        // No inferred Values
        and.infer_value();
        sg = and.simplify();
        assertTrue(and.equals(sg));

        // 0 as the first input
        and.in1.set_value(false);
        and.infer_value();
        sg = and.simplify();
        assertTrue(sg.equals(new LL_Off(and.out)));
        and.reset();

        // 0 as the second input
        and.in2.set_value(false);
        and.infer_value();
        sg = and.simplify();
        assertTrue(sg.equals(new LL_Off(and.out)));
        and.reset();

        // 0 as the first input, 1 as the second input
        and.in1.set_value(false);
        and.in2.set_value(true);
        and.infer_value();
        sg = and.simplify();
        assertTrue(sg.equals(new LL_Off(and.out)));
        and.reset();

        // 0 as the second input, 1 as the first input
        and.in2.set_value(false);
        and.in1.set_value(true);
        and.infer_value();
        sg = and.simplify();
        assertTrue(sg.equals(new LL_Off(and.out)));
        and.reset();

        // 1 as the first input
        and.in1.set_value(true);
        and.infer_value();
        sg = and.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Buf);
        and.reset();

        // 1 as the second input
        and.in2.set_value(true);
        and.infer_value();
        sg = and.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Buf);
        and.reset();
    }

    @Test
    void test_simplify_or_gate() {
        LL_Gate or = get_or_gate();
        LL_Gate sg;

        // No inferred Values
        or.infer_value();
        sg = or.simplify();
        assertTrue(or.equals(sg));

        // 1 as the first input
        or.in1.set_value(true);
        or.infer_value();
        sg = or.simplify();
        assertTrue(sg.equals(new LL_On(or.out)));
        or.reset();

        // 1 as the second input
        or.in2.set_value(true);
        or.infer_value();
        sg = or.simplify();
        assertTrue(sg.equals(new LL_On(or.out)));
        or.reset();

        // 0 as the first input, 1 as the second input
        or.in1.set_value(false);
        or.in2.set_value(true);
        or.infer_value();
        sg = or.simplify();
        assertTrue(sg.equals(new LL_On(or.out)));
        or.reset();

        // 0 as the second input, 1 as the first input
        or.in2.set_value(false);
        or.in1.set_value(true);
        or.infer_value();
        sg = or.simplify();
        assertTrue(sg.equals(new LL_On(or.out)));
        or.reset();

        // 0 as the first input
        or.in1.set_value(false);
        or.infer_value();
        sg = or.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Buf);
        or.reset();

        // 0 as the second input
        or.in2.set_value(false);
        or.infer_value();
        sg = or.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Buf);
        or.reset();
    }

    @Test
    void test_simplify_nand_gate() {
        LL_Gate nand = get_nand_gate();
        LL_Gate sg;

        // No inferred Values
        nand.infer_value();
        sg = nand.simplify();
        assertTrue(nand.equals(sg));

        // 0 as the first input
        nand.in1.set_value(false);
        nand.infer_value();
        sg = nand.simplify();
        assertTrue(sg.equals(new LL_On(nand.out)));
        nand.reset();

        // 0 as the second input
        nand.in2.set_value(false);
        nand.infer_value();
        sg = nand.simplify();
        assertTrue(sg.equals(new LL_On(nand.out)));
        nand.reset();

        // 0 as the first input, 1 as the second input
        nand.in1.set_value(false);
        nand.in2.set_value(true);
        nand.infer_value();
        sg = nand.simplify();
        assertTrue(sg.equals(new LL_On(nand.out)));
        nand.reset();

        // 0 as the second input, 1 as the first input
        nand.in2.set_value(false);
        nand.in1.set_value(true);
        nand.infer_value();
        sg = nand.simplify();
        assertTrue(sg.equals(new LL_On(nand.out)));
        nand.reset();

        // 1 as the first input
        nand.in1.set_value(true);
        nand.infer_value();
        sg = nand.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Not);
        nand.reset();

        // 1 as the second input
        nand.in2.set_value(true);
        nand.infer_value();
        sg = nand.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Not);
        nand.reset();
    }

    @Test
    void test_simplify_nor_gate() {
        LL_Gate nor = get_nor_gate();
        LL_Gate sg;

        // No inferred Values
        nor.infer_value();
        sg = nor.simplify();
        assertTrue(nor.equals(sg));

        // 1 as the first input
        nor.in1.set_value(true);
        nor.infer_value();
        sg = nor.simplify();
        assertTrue(sg.equals(new LL_Off(nor.out)));
        nor.reset();

        // 1 as the second input
        nor.in2.set_value(true);
        nor.infer_value();
        sg = nor.simplify();
        assertTrue(sg.equals(new LL_Off(nor.out)));
        nor.reset();

        // 0 as the first input, 1 as the second input
        nor.in1.set_value(false);
        nor.in2.set_value(true);
        nor.infer_value();
        sg = nor.simplify();
        assertTrue(sg.equals(new LL_Off(nor.out)));
        nor.reset();

        // 0 as the second input, 1 as the first input
        nor.in2.set_value(false);
        nor.in1.set_value(true);
        nor.infer_value();
        sg = nor.simplify();
        assertTrue(sg.equals(new LL_Off(nor.out)));
        nor.reset();

        // 0 as the first input
        nor.in1.set_value(false);
        nor.infer_value();
        sg = nor.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Not);
        nor.reset();

        // 0 as the second input
        nor.in2.set_value(false);
        nor.infer_value();
        sg = nor.simplify();
        // System.out.println("SG: " + sg.longString());
        assertTrue(sg instanceof LL_Not);
        nor.reset();
    }
}
