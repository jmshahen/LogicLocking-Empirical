"""
ECE406, W'19, Assignment 6, Problem 1

You are allowed to define any subroutines you like for yourself.
You are *not* allowed to import anything.
"""

def tripletoint(i, j, k):
    return 8*(i-1) + 2*j + k - 1

def inttotriple(d):
    i = (d-1)//8 + 1
    j = (d - 8*(i-1) - 1)//2 + 1
    k = (d-1)%2
    return i,j,k

def gencnf(puzzle):
    """
    Implement this
    """
    constr = []

    # First all the constraints independent of the puzzle
    for i in range(1, 5):
        l = [(i,1), (i,2), (i,3), (i,4)]
        for a in range(len(l)):
            for b in range(a+1,len(l)):
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])

    for j in range(1, 5):
        l = [(1,j), (2,j), (3,j), (4,j)]
        for a in range(len(l)):
            for b in range(a+1,len(l)):
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])

    for i in range(1,4,2):
        l = [(i,1), (i,2), (i+1,1), (i+1,2)]
        for a in range(len(l)):
            for b in range(a+1,len(l)):
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])

    for i in range(1,4,2):
        l = [(i,3), (i,4), (i+1,3), (i+1,4)]
        for a in range(len(l)):
            for b in range(a+1,len(l)):
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        -1*tripletoint(l[a][0], l[a][1], 0),
                        -1*tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        -1*tripletoint(l[a][0], l[a][1], 1),
                        -1*tripletoint(l[b][0], l[b][1], 1)
                        ])
                constr.append([
                        tripletoint(l[a][0], l[a][1], 0),
                        tripletoint(l[b][0], l[b][1], 0),
                        tripletoint(l[a][0], l[a][1], 1),
                        tripletoint(l[b][0], l[b][1], 1)
                        ])

    # Finally, constraint from the puzzle
    for i in range(len(puzzle)):
        for j in range(len(puzzle)):
            if puzzle[i][j] >= 0:
                b0 = (puzzle[i][j])//2
                b1 = (puzzle[i][j])%2

                if not b0:
                    constr.append([-1*tripletoint(i+1,j+1,0)])
                if b0:
                    constr.append([tripletoint(i+1,j+1,0)])
                if not b1:
                    constr.append([-1*tripletoint(i+1,j+1,1)])
                if b1:
                    constr.append([tripletoint(i+1,j+1,1)])

    return constr

def solutionfromsatisfaction(satisfaction):
    """
    Implement this
    """

    sol = []
    for i in range(1,5):
        thisrow = []
        for j in range(1,5):
            m = tripletoint(i, j, 0)
            l = tripletoint(i, j, 1)

            if m in satisfaction:
                bm = 1
            else:
                bm = 0

            if l in satisfaction:
                bl = 1
            else:
                bl = 0

            thisrow.append((bm << 1) | bl)
        sol.append(thisrow)
    return sol

