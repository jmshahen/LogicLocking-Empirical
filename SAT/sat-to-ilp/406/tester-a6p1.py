#! /usr/bin/python3

import random
import pycosat
from a6p1solution import gencnf, solutionfromsatisfaction

"""
ECE406, W'19, Assignment 6, Problem 1
"""

def checksolution(puzzle, solution):
    # Our tests will be stronger than this.
    # We will individually test your gencnf() and 
    # solutionfromsatisfaction

    if len(puzzle) != len(solution):
        return False

    # Check if puzzle's filled squares are maintained
    # Also check that every entry in solution is valid
    for i in range(len(puzzle)):
        if len(puzzle[i]) != len(solution[i]):
            return False
        for j in range(len(puzzle[i])):
            if type(solution[i][j]) != int:
                return False
            if solution[i][j] < 0 or solution[i][j] > 3:
                return False
            if puzzle[i][j] != -1 and puzzle[i][j] != solution[i][j]:
                return False

    # Check each row
    checklist = [False, False, False, False]
    checklist[solution[0][0]] = True
    checklist[solution[0][1]] = True
    checklist[solution[0][2]] = True
    checklist[solution[0][3]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[1][0]] = True
    checklist[solution[1][1]] = True
    checklist[solution[1][2]] = True
    checklist[solution[1][3]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[2][0]] = True
    checklist[solution[2][1]] = True
    checklist[solution[2][2]] = True
    checklist[solution[2][3]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[3][0]] = True
    checklist[solution[3][1]] = True
    checklist[solution[3][2]] = True
    checklist[solution[3][3]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    # Check each column
    checklist = [False, False, False, False]
    checklist[solution[0][0]] = True
    checklist[solution[1][0]] = True
    checklist[solution[2][0]] = True
    checklist[solution[3][0]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[0][1]] = True
    checklist[solution[1][1]] = True
    checklist[solution[2][1]] = True
    checklist[solution[3][1]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[0][2]] = True
    checklist[solution[1][2]] = True
    checklist[solution[2][2]] = True
    checklist[solution[3][2]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[0][3]] = True
    checklist[solution[1][3]] = True
    checklist[solution[2][3]] = True
    checklist[solution[3][3]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    # Check each 2x2 square
    checklist = [False, False, False, False]
    checklist[solution[0][0]] = True
    checklist[solution[0][1]] = True
    checklist[solution[1][0]] = True
    checklist[solution[1][1]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[0][2]] = True
    checklist[solution[0][3]] = True
    checklist[solution[1][2]] = True
    checklist[solution[1][3]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[2][0]] = True
    checklist[solution[2][1]] = True
    checklist[solution[3][0]] = True
    checklist[solution[3][1]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    checklist = [False, False, False, False]
    checklist[solution[2][2]] = True
    checklist[solution[2][3]] = True
    checklist[solution[3][2]] = True
    checklist[solution[3][3]] = True
    for j in range(4):
        if not checklist[j]:
            return False

    return True

def genilp(cnf):
    print('Maximize')
    print(' obj: 0')
    print('Subject To')
    maxvar = 0
    for i in range(len(cnf)): # clause #
        nneg = 0 # # negative variables in this clause
        for j in range(len(cnf[i])):
            if j == 0:
                print(' ', end='')
            if(cnf[i][j] < 0):
                nneg = nneg + 1
                print('-', end=' ')
            elif j > 0:
                print('+', end=' ')
            print('x'+str(abs(cnf[i][j])), end=' ')
            if maxvar < abs(cnf[i][j]):
                maxvar = abs(cnf[i][j])
        print('>', str(1 - nneg))

    print('Binary')
    for i in range(maxvar):
        print(' x'+str(i+1))
    print('End')

def checksatcnf(cnf, sat):
    # check if sat satisfies cnf
    for i in range(len(cnf)):
        thisclause = False
        for j in range(len(cnf[i])):
            l = cnf[i][j]
            v = abs(l)
            if sat[v-1] == l:
                thisclause = True
                break

        if thisclause == False:
            return False
    return True


def main():
    """
    A couple of tests
    """
    puzzle = [[1, -1, 0, -1], [-1, -1, 1, 2], [0, 3, -1, 1], [2, -1, -1, 0]]
    cnf = gencnf(puzzle)
    satisfaction = pycosat.solve(cnf)
    #print('cnf vs satisfaction: ', checksatcnf(cnf, satisfaction))
    #print(cnf)
    #print(satisfaction)
    #ilp = genilp(cnf)

    puzzle = [[-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1], [-1, -1, -1, -1]]
    cnf = gencnf(puzzle)
    satisfaction = pycosat.solve(cnf)

    #print(cnf)
    #print(satisfaction)
    #ilp = genilp(cnf)

    #sat = [-1, -2, 3, 4, -5, 6, 7, -8, -9, 10, 11, -12, 13, 14, -15, -16, 17, 18, -19, -20, 21, -22, -23, 24, 25, -26, -27, 28, -29, -30, 31, 32]
    sat = [-1,-2,3,-4,-5,6,7,8,-9,10,11,12,-13,-14,15,-16,17,-18,-19,-20,21,22,-23,24,25,26,-27,28,29,-30,-31,-32]
    print('cnf vs satisfaction: ', checksatcnf(cnf, sat))

    puzzle = [[-1, -1, 3, -1], [3, -1, -1, 0], [-1, -1, 0, -1], [2, -1, -1, 3]]
    cnf = gencnf(puzzle)
    satisfaction = pycosat.solve(cnf)

    #print(cnf)
    #print(satisfaction)
    #ilp = genilp(cnf)

    puzzle = [[0, 1, -1, -1], [2, -1, -1, -1], [1, -1, 2, 0], [-1, -1, -1, -1]]
    cnf = gencnf(puzzle)
    satisfaction = pycosat.solve(cnf)

    #print(cnf)
    #print(satisfaction)
    #ilp = genilp(cnf)

    puzzle = [[-1,-1,0,-1], [1,-1,-1,-1], [0,-1,-1,-1], [-1,-1,2,-1]]
    cnf = gencnf(puzzle)
    satisfaction = pycosat.solve(cnf)

    #print(cnf)
    #print(satisfaction)
    #ilp = genilp(cnf)

if __name__ == '__main__':
    main()
