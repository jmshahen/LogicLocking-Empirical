#include "gurobi_c++.h"
#include <stdio.h>
#include <iostream>

extern void optimizeAndOutput(int, GRBModel&, GRBVar *, int);

int main() {
    GRBEnv *env = 0;
    GRBVar *v = 0;

    try {
        env = new GRBEnv();
        env->set(GRB_IntParam_OutputFlag, 0);
        GRBModel model = GRBModel(*env);
        model.set(GRB_StringAttr_ModelName, "mymodel");
        v = model.addVars(3, GRB_BINARY);

        GRBLinExpr l[2];

        l[0] = 0;
        l[0] += v[0];
        l[0] -= v[1];

        model.addConstr(l[0] >= 0);

        l[1] = 0;
        l[1] -= v[0];
        l[1] -= v[1];
        l[1] -= v[1];

        model.addConstr(l[1] >= -2);

        (v[1]).set(GRB_DoubleAttr_LB, 1.0);
        (v[1]).set(GRB_DoubleAttr_UB, 1.0);

        int attempt = 0;
        optimizeAndOutput(++attempt, model, v, 3);

        (v[1]).set(GRB_DoubleAttr_LB, 0.0);
        (v[1]).set(GRB_DoubleAttr_UB, 1.0);
        (v[0]).set(GRB_DoubleAttr_LB, 0.0);
        (v[0]).set(GRB_DoubleAttr_UB, 0.0);

        optimizeAndOutput(++attempt, model, v, 3);

        (v[0]).set(GRB_DoubleAttr_LB, 1.0);
        (v[0]).set(GRB_DoubleAttr_UB, 1.0);

        optimizeAndOutput(++attempt, model, v, 3);
    }
    catch (GRBException e) {
        std::cout << "Exception:  " <<  e.getErrorCode() << ", " << e.getMessage() << std::endl;
    }

    delete[] v;
    delete env;

    return 0;
}
