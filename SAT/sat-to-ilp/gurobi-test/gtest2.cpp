#include "gurobi_c++.h"
#include <stdio.h>
#include <iostream>

extern void optimizeAndOutput(int, GRBModel&, GRBVar *, int);

int main() {
    GRBEnv *env = 0;
    GRBVar *v = 0;
    int attempt = 0;

    try {
        env = new GRBEnv();
        env->set(GRB_IntParam_OutputFlag, 0);
        GRBModel model = GRBModel(*env);
        model.set(GRB_StringAttr_ModelName, "mymodel");
        const int nv = 3;
        v = model.addVars(2*nv + 3, GRB_BINARY);

        GRBVar cone[2];
        cone[0] = v[0]; cone[1] = v[3];
        model.addGenConstrOr(v[6], cone, 2);

        GRBVar ctwo[3];
        ctwo[0] = v[1]; ctwo[1] = v[3]; ctwo[2] = v[5];
        model.addGenConstrOr(v[7], ctwo, 3);

        GRBVar f[2];
        f[0] = v[6]; f[1] = v[7];
        model.addGenConstrAnd(v[8], f, 2);

        for(int i = 0; i < 3; i++) {
            GRBLinExpr l = 0;

            l += v[2*i];
            l -= v[2*i+1];
            model.addConstr(l == 1);
        }
        model.addConstr(v[8] == 1);

        model.optimize();
        optimizeAndOutput(++attempt, model, v, 9);
    }
    catch (GRBException e) {
        std::cout << "Exception:  " <<  e.getErrorCode() << ", " << e.getMessage() << std::endl;
    }

    delete[] v;
    delete env;

    return 0;
}
