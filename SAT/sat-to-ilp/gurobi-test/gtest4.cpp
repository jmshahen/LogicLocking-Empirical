#include "gurobi_c++.h"
#include <stdio.h>
#include <iostream>

extern void optimizeAndOutput(int, GRBModel&, GRBVar *, int);

int main() {
    GRBEnv *env = 0;
    GRBVar *v = 0;

    try {
        env = new GRBEnv();
        env->set(GRB_IntParam_OutputFlag, 0);
        GRBModel model = GRBModel(*env);
        v = model.addVars(2, GRB_BINARY);

        GRBLinExpr l = 0;
        l += v[0];

        (v[1]).set(GRB_DoubleAttr_LB, 0.0);
        (v[1]).set(GRB_DoubleAttr_UB, 0.0);

        int nneg = 0;
        nneg += 1;

        model.addConstr(l >= nneg);
        optimizeAndOutput(0, model, v, 2);

        (v[1]).set(GRB_DoubleAttr_LB, 1.0);
        (v[1]).set(GRB_DoubleAttr_UB, 1.0);

        optimizeAndOutput(0, model, v, 2);
    }
    catch (GRBException e) {
        std::cout << "Exception:  " <<  e.getErrorCode() << ", " << e.getMessage() << std::endl;
    }

    delete[] v;
    delete env;

    return 0;
}
