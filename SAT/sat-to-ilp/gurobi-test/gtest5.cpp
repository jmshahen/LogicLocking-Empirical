#include "gurobi_c++.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

extern void optimizeAndOutput(int, GRBModel&, GRBVar *, int);

int main() {
    int nvars = 0, inint;

    std::ifstream myfile;
    myfile.open("myfile.txt");
    while(myfile >> inint) {
        if(abs(inint) > nvars) nvars = abs(inint);
    }
    myfile.close();

    std::cout << "nvars: " << nvars << std::endl << std::flush;

    GRBEnv *env = 0;
    GRBVar *guro_var = 0;

    try {
        env = new GRBEnv();
        env->set(GRB_IntParam_OutputFlag, 0);
        GRBModel model = GRBModel(*env);
        guro_var = model.addVars(nvars, GRB_BINARY);

        myfile.open("myfile.txt");
        GRBLinExpr linexpr = 0;
        int nneg = 0;

        while(myfile >> inint) {
            int lit = inint; // literal
            int var = abs(inint); // variable

            if(lit < 0) {
                linexpr -= guro_var[var-1];
                nneg += 1;
            }
            else if (lit > 0) {
                linexpr += guro_var[var-1];
            }
            else {
                nneg = 1 - nneg;
                model.addConstr(linexpr >= nneg);
                linexpr = nneg = 0;
            }
        }

        myfile.close();

        model.setObjective(GRBLinExpr(0.0));
        optimizeAndOutput(0, model, guro_var, nvars);
    }
    catch (GRBException e) {
        std::cout << "Exception:  " <<  e.getErrorCode() << ", " << e.getMessage() << std::endl;
    }

    delete[] guro_var;
    delete env;

    return 0;
}
