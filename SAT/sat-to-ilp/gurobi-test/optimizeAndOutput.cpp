#include "gurobi_c++.h"
#include <stdio.h>
#include <iostream>

void optimizeAndOutput(int attempt,
                       GRBModel &model,
                       GRBVar *v,
                       int nvars)
{
    model.optimize();
    int status = model.get(GRB_IntAttr_Status);
    printf("---------- Attempt %d -----------\n", attempt);
    if(status == GRB_INFEASIBLE) {
        printf("\tInfeasible\n");
    }
    else if(status == GRB_OPTIMAL) {
        for(int i = 0; i < nvars; i++) {
            //printf("\t%d: ", i);
            double r = (v[i]).get(GRB_DoubleAttr_X);
            ((v[i]).get(GRB_DoubleAttr_X) < 1.0) ? std::cout << "0 " : std::cout << "1 ";
            //std::cout << r << std::endl;
        }
    }
    std::cout << std::endl;
}
