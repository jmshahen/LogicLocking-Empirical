#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <strings.h>
#include "lglib.h"

bool containsclosebrack(char *s) {
    for(int i = 0; s[i] != 0; i++) {
        if(s[i] == ']') return true;
    }
    return false;
}

int skipopenbrack(char *s) {
    int ret = 0;
    while(s[ret] == '[') {
        ret++;
    }
    return ret;
}

static void clauses_print(void *p, int lit) {
    FILE *out = (FILE *)(p);
    if(lit) fprintf(out, "%d ", lit);
    else fprintf(out, "0\n");
}

int old_main(int argc, char *argv[]) {
    LGL *lgl = lglinit();
    int res;

    FILE *fp = fopen(argv[1], "r");
    if(fp == NULL) {
        perror("fopen");
        return 1;
    }

    char s[64];
    int nv = 0;
    while(fscanf(fp, "%s", s) > 0) {
        int i = skipopenbrack(s);
        int l = atoi(&(s[i]));
        lgladd(lgl, l);
        if(containsclosebrack(s)) {
            lgladd(lgl, 0);
        }

        if(l < 0) l *= -1;
        if(l > nv) nv = l;
    }

    lglfreeze(lgl, 1);
    lglfreeze(lgl, 2);
    lglfreeze(lgl, 3);

    if(lglsat(lgl) == LGL_UNSATISFIABLE) {
        printf("UNSAT\n");
        lglrelease(lgl);
        return 0;
    }

    printf("SAT: ");
    for(int i = 0; i < nv; i++) {
        if(lglderef(lgl, i+1) < 0) {
            printf("-");
        }
        printf("%d ", i+1);
    }
    printf("\n"); fflush(stdout);

    lgladd(lgl, 1); lgladd(lgl, 2); lgladd(lgl, 3); lgladd(lgl, 0);
    lgladd(lgl,4); lgladd(lgl, -5); lgladd(lgl, 0);
    nv += 2;

    if(lglsat(lgl) == LGL_UNSATISFIABLE) {
        printf("UNSAT\n");
    }
    else {
        printf("SAT: ");
        for(int i = 0; i < nv; i++) {
            if(lglderef(lgl, i+1) < 0) {
                printf("-");
            }
            printf("%d ", i+1);
        }
        printf("\n"); fflush(stdout);
    }

    lglrelease(lgl);
    return 0;
}

int main() {
    LGL *lgl = lglinit();
    int res;

    lgladd(lgl,1); lgladd(lgl,-2); lgladd(lgl,0);
    lgladd(lgl,2); lgladd(lgl,0);
    lglfreeze(lgl, 1); lglfreeze(lgl, 2);

    lglassume(lgl, 1);
    if(lglsat(lgl) == LGL_SATISFIABLE) {
        printf("%d, %d\n", lglderef(lgl, 1), lglderef(lgl, 2));
    }
    else {
        printf("UNSAT\n");
    }

    return 0;
}
