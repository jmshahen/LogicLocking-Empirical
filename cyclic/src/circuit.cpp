#include "circuit.h"
#include <vector>
#include <map>
#include <stdlib.h>
#include <time.h>
#include <sstream>
#include <string>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <igraph.h>

using namespace std;
using namespace boost;

Circuit::Circuit() {
    gate_count = 0;
}

Circuit::~Circuit() {
    
}

int Circuit::read_bench(char* circuit_file_name) {
    ifstream circuit_file;
    circuit_file.open(circuit_file_name);
    string line;
    vector<string> outputs;
    graph = new igraph_t;
    // igraph_i_set_attribute_table(&igraph_cattribute_table);
    igraph_empty(graph, 0, IGRAPH_DIRECTED);

    map<string, int> vertex_ids;

    // node_table.insert(pair<string, node>("LOGIC0", node()));
    // node_table.insert(pair<string, node>("LOGIC1", node()));

    // int current_id = 0;
    while (getline(circuit_file, line)) {
        if (line[0] == '#') continue;
        if (line == "") continue;
        if (line.substr(0,5) == "INPUT") { 
	    string input_name = line.substr(6, line.length()-7); // model_file << "IVAR " + input_name + " : boolean;\n"; 
	    // igraph_add_vertices(graph, 1, 0);
	    //SETVAN(graph, "type", current_id, "INPUT");
	    // vertex_ids[input_name] = current_id;	    
            // current_id++;
node n;
	    n.camouflaged = false;
            n.output = false;
            n.type = "INPUT";
            node_table.insert(pair<string, node>(input_name, n));
	    continue;
	}
        if (line.substr(0,6) == "OUTPUT") { 
	    string output_name = line.substr(7, line.length()-8);
	    // igraph_add_vertices(graph, 1, 0);
            // SETVAS(graph, "type", current_id, "OUTPUT");
	    node n;
	    n.camouflaged = false;
            n.output = true;
            
            // node_table.insert(pair<string, node>(output_name, n));
            node_table[output_name].camouflaged = false;
            node_table[output_name].output = true;
	    continue; 
	}
        vector<string> fields;
        split(fields, line, is_any_of( "(),= " ), token_compress_on);

        if (fields[1] == "DFF") {
	    node n; n.camouflaged = false; n.type = "STATE"; n.output = false;
	    n.fanins.push_back(fields[2]); 
            // igraph_add_vertices(graph, 1, 0);
            // SETVAS(graph, "type", current_id, "DFF");

	    // node_table.insert(pair<string, node>(fields[0], n));
            node_table[fields[0]].camouflaged = false;
            node_table[fields[0]].type = "STATE";
            node_table[fields[0]].fanins.push_back(fields[2]);
            // node_table[fields[0]].id = current_id++;
            continue;
        }
        node n;
	// node_table[fields[0]].id = current_id++;
        node_table[fields[0]].type = fields[1]; 
	node_table[fields[0]].camouflaged = false;
        // node_table[fields[0]].output = false;
        for (int i = 2; i < fields.size() - 1; i++) {
            node_table[fields[0]].fanins.push_back(fields[i]);
        }
        if (fields[1] != "NOT") gate_count++;
    }

    int current_id = 0;
    for (map<string,node>::iterator it = node_table.begin(); it != node_table.end(); ++it) {
        igraph_add_vertices(graph, 1, 0);
        it->second.id = current_id;
        SETVAS(graph, "name", current_id++, it->first.c_str());
	cerr << "Checking..." << endl;
	if (it->second.type != "AND" && it->second.type != "NOT" && it->second.type != "INPUT")
		cerr << "here it is: " << it->second.type << endl;
    }
   
    for (map<string,node>::iterator it = node_table.begin(); it != node_table.end(); ++it) {
        if (it->second.type != "STATE") 
            for (int i = 0; i < it->second.fanins.size(); i++)
                igraph_add_edge(graph, node_table[it->second.fanins[i]].id, it->second.id);
    }

    for (map<string,node>::iterator it = node_table.begin(); it != node_table.end(); ++it) {

        if (it->second.type == "STATE") {
            string type = node_table[it->second.fanins[0]].type;
            if (type == "NAND" || type == "OR") initial_state[it->first] = false;
            if (type == "NOR" || type == "AND" || type == "NOT") initial_state[it->first] = false;
            
        }
    }
    return 0;
}

int Circuit::adjacency_list(char* file_name) {
    ofstream output;
    output.open(file_name);
    output << "#vertices " << igraph_vcount(graph) << endl;
    igraph_adjlist_t adjlist;
    igraph_adjlist_init(graph, &adjlist, IGRAPH_OUT);

    for (int i = 0; i < igraph_vcount(graph); i++) {
        igraph_vector_int_t* neighbours = igraph_adjlist_get(&adjlist, i);
        for (int j = 0; j < igraph_vector_int_size(neighbours); j++) {
	    output << (int) VECTOR(*neighbours)[j] + 1 << " ";
	}
	output << "0" << endl;
	igraph_vector_int_destroy(neighbours);
    }	
    igraph_adjlist_destroy(&adjlist);

    return 0;
}

#ifdef CYCLIC 

int Circuit::cyclify_random(int key_length) {

    vector<int> vertex_ids;
    for (int i = 0; i < igraph_vcount(graph); i++) {
        igraph_vs_t vid;
        igraph_vs_1(&vid, i);
        igraph_vector_t res;
        igraph_vector_init(&res, 0);
        igraph_degree(graph, &res, vid, IGRAPH_OUT, false);

        if (VECTOR(res)[0] > 1) vertex_ids.push_back(i);
    }
    vector<int> selected;
    for (int i = 0; i < key_length; i++) {
        int index = rand() % vertex_ids.size();
        selected.push_back(vertex_ids[index]);
        vertex_ids.erase(vertex_ids.begin() + index);
    }

    igraph_t* copy = new igraph_t;
    igraph_copy(copy, graph);

    cout << igraph_vcount(graph);
    for (int i = 0; i < key_length; i++) {
        igraph_vector_t order;
        igraph_vector_init(&order, 0);
    
        igraph_bfs(graph, selected[i], NULL, IGRAPH_OUT, false, NULL, &order, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
        // for (int j = 0; j < igraph_vector_size(&order); j++) 
        //    cout << VECTOR(order)[j] << endl;


        igraph_vs_t vid;
        igraph_vs_1(&vid, selected[i]);
        igraph_vector_ptr_t res;
        igraph_vector_ptr_init(&res, 0);
        igraph_neighborhood(graph, &res, vid, igraph_ecount(graph), IGRAPH_OUT);

        cout << "neighborhood " << igraph_vector_size((igraph_vector_t*) VECTOR(res)[0]) << endl;
        // igraph_vector_destroy(&res);
        int size = igraph_vector_size(&order);
        int selection = rand() % igraph_vector_size((igraph_vector_t*) VECTOR(res)[0]); 
        igraph_real_t ff = VECTOR(* (igraph_vector_t*) VECTOR(res)[0])[selection];
        int key_input_id = igraph_vcount(copy);
        igraph_add_vertices(copy, 1, 0);

        node key_gate;
        key_gate.type = rand() % 2 == 0? "NAND": "NOR";
        key_gate.output = false;
        key_gate.camouflaged = true;
        key_gate.fanins.resize(0);
        key_gate.fanins[0] = "LOGIC0";
        key_gate.fanins[1] = "LOGIC1";

        string key_bit_name = "k";
        key_bit_name += itoa(i);
        node_table.insert(pair<string, node>(key_bit_name, key_gate));        

        int mux_id = igraph_vcount(copy);
        igraph_add_vertices(copy, 1, 0);

        node mux;
        mux.type = "MUX";
        if (node_table[VAS(copy, selected[i], "name")].output) {
            node_table[VAS(copy, selected[i], "name")].output = false;
            mux.output = true;
        }
        else
            mux.output = false;
        mux.camouflaged = false;
        mux.fanins.resize(3);
        mux.fanins[0] = VAS(copy, ff, "name");
        mux.fanins[1] = VAS(copy, selected[i], "name");
        mux.fanins[2] = key_bit_name;

        string mux_name = "MUX";
        mux_name += itoa(i);
        node_table.insert(pair<int,node>(mux_name, mux));

	SETVAS(copy, "Type", mux_id, "MUX");
        // igraph_vs_t vs;
        // igraph_vs_adj(&vs, selected[i], IGRAPH_OUT);
        igraph_vector_t neis;
	igraph_vector_init(&neis, 0);
        igraph_neighbors(graph, &neis, selected[i], IGRAPH_OUT);
        igraph_es_t es;
        igraph_es_incident(&es, selected[i], IGRAPH_OUT);
        igraph_delete_edges(copy, es);

        for (int j = 0; j < igraph_vector_size(&neis); j++) {
            string nei_name = VAS(copy, VECTOR(neis)[j], "name");
            int k;
            for (k = 0; k < node_table[nei_name].fanins.size(); k++)
                if (node_table[nei_name].fanins[k] == VAS(copy, selected[i], "name")) break;
            
            node_table[nei_name].fanins[k] = mux_name;
        }

        for (int j = 0; j < igraph_vector_size(&neis); j++) {
            igraph_add_edge(copy, mux_id, VECTOR(neis)[j]);
        }
        igraph_add_edge(copy, key_input_id, mux_id);
        igraph_add_edge(copy, ff, mux_id);
        igraph_add_edge(copy, selected[i], mux_id);
        igraph_vector_destroy(&order);
     }

     int no;
     igraph_vector_t csize;
     igraph_vector_init(&csize, 0);
     igraph_vector_t membership;
     igraph_vector_init(&membership, 0);
     igraph_clusters(copy, &membership, &csize, &no, IGRAPH_STRONG);
    
     
     for (int i = 0; i < no; i++)
         cout << VECTOR(csize)[i] << endl;
     cout << igraph_vcount(copy) << endl;


     // Create as many copies of each vertex in a cluster as there are vertices within the cluster 
     for (int i = 0; i < no; i++) {
         vector<vector<int>> vm; // vertex map
         vm.resize(VECTOR(csize)[i]);
         // vm[0].resize(VECTOR(csize)[i]);
         
         for (int j = 0; j < igraph_vector_size(&membership); j++)
             if (VECTOR(membership)[j] == i)
                 vm[0].push_back(j);    
         for (int j = 1; j < VECTOR(csize)[i]; j++)
             for (int k = 0; k < VECTOR(csize)[i]; k++) {
                 igraph_add_vertices(copy, 1, 0);
                 node n;
                 n.type = node_table[VAS(copy, vm[0][k], "name")].type;
                 n.output = false;
                 n.camouflaged = false;
                 string name = VAS(copy, vm[0][k], "name");
                 name += itoa(j);
                 node_table.insert(pair<string,node>(name, n));
                 vm[j][k] = igraph_vcount(copy);
             }

         for (int j = 0; j < VECTOR(csize)[i]; j++) {
             // find vertex neighbours
             igraph_vector_t neis;
             igraph_vector_init($neis, 0);
             igraph_neighbors(copy, &neis, vm[0][j], IGRAPH_OUT);
             
             // remove edges within cluster
             igraph_es_t es;
             igraph_es_incident(&es, vm[0][j], IGRAPH_OUT);
             igraph_delete_edges(copy, es);

             // introduce edges between cluster copies
             for (int l = 0; l < igraph_vector_size(&neis); l++) {
                 int h;
                 for (h = 0; h < VECTOR(csize)[i]; h++)
                     if (vm[0][h] == VECTOR(neis)[l]) break;
                 if (h == VECTOR(cize)[i]) {
                     igraph_edge(copy, vm[VECTOR(csize)[i]-1][j], VECTOR(neis)[l]);
                     for (int r = 0; r < node_table[VAS(copy, VECTOR(neis)[l], "name")].fanins.size(); r++) 
                         if (node_table[VAS(copy, VECTOR(neis)[l], "name")].fanins[r] == VAS(copy, vm[0][j], "name"))
                             node_table[VAS(copy, VECTOR(neis)[l], "name")].fanins[r] = VAS(copy, vm[VECTOR(csize)[i]-1][j], "name");
                 }
                 else 
                     for (int k = 0; k < VECTOR(csize)[i]-1; k++) {
                         igraph_edge(copy, vm[k][j], vm[k+1][h]);
                         node_table[VAS(copy, vm[k+1][h], "name")].fanins.push_back(VAS(copy, vm[k][j], "name"));
                     }

             }
     }

     igraph_vector_destroy(&membership);
     igraph_destroy(copy);
     return no;
}

#endif

void Circuit::remove_input_buffers() {
    for (map<string,node>::iterator it = node_table.begin(); it != node_table.end(); ++it) {
        if (it->second.type == "STATE") {
            string input = it->second.fanins[0];
            vector<string> cone;
            cone.push_back(input);
            //for (int i = 0; i < node_table[input].fanins.size(); i++) {
            //   if (node_table[node_table[input].fanins[i]].type = "STATE") continue;
            //   cone.push_back(node_table[input].fanins[i]);
            //}
            bool done, not_buffer = false;
            do {
                done = true;
                for (int i = 0; i < cone.size(); i++) {
                    for (int j = 0; j < node_table[cone[i]].fanins.size(); j++) {
                        if (node_table[node_table[cone[i]].fanins[j]].type == "STATE") { not_buffer = true; break; }
                        bool found  = false;
                        for (int k = 0; k < cone.size(); k++)
                            if (cone[k] == node_table[cone[i]].fanins[j]) { found = true; break; }
                        if (!found) { if (done) done = false; cone.push_back(node_table[cone[i]].fanins[j]); }
                    }
                    if (not_buffer) break;
                }
                if (not_buffer) break;
            } while (!done);
            if (!not_buffer) cout << "Buffer found!" << endl;
        }
    }
}

// percentage is percentage of gates in the netlist to camouflage
// xnor_or_xor is used to specify whether to camouflage XNOR or XOR gates

int Circuit::camouflage_random(float percentage, bool xnor_or_xor) {     
    vector<string> camouflaging_candidates;
    for (std::map<string, node>::iterator it=node_table.begin(); it!=node_table.end(); ++it)
	if (it->second.type == "NAND" || it->second.type == "NOR" || it->second.type == "XOR") camouflaging_candidates.push_back(it->first);
    srand(time(NULL));
    for (int i = 0; i < percentage * gate_count; i++) {
        int index = rand() % camouflaging_candidates.size();
        node_table[camouflaging_candidates[index]].camouflaged = true;
        camouflaging_candidates.erase(camouflaging_candidates.begin() + index);
    }
    return 0;
}

int Circuit::decamouflage() { // set camouflaging status of all gates in the netlist to false
    for (std::map<string, node>::iterator it=node_table.begin(); it!=node_table.end(); ++it)
        it->second.camouflaged = false;
    return 0;
}

string Circuit::generate_next_state_condition() {
    string ret = "("; bool first = true;
    for (map<string,node>::iterator it=node_table.begin(); it != node_table.end(); ++it) {
        if (/*it->second.type == "STATE" || */ it->second.output) {
        if (first) {
            first = false;
        }
        else {
            ret += " & ";
        }
        ret += "(" + it->first + "_1 = " + it->first + "_2)";
        }
    }
    ret += ")";
    string temp = ret + " -> X " + ret;
    return temp;
}

string Circuit::generate_smv_model(int flag) {
    stringstream model;
    vector<string> input_list, camouflaged_gate_list, output_list;
    model << "MODULE main\n";
    for (map<string,node>::iterator it=node_table.begin(); it != node_table.end(); ++it) {
        node n = it->second;
        if (n.type == "INPUT") {
            model << "DEFINE " + it->first + "_1 := " << it->first << ";\nDEFINE " + it->first + "_2 := " << it->first << ";\n";    
            input_list.push_back(it->first);
            continue;
        }
        if (n.output /*|| n.type == "STATE"*/) output_list.push_back(it->first);
if (n.type == "STATE") {
            model << "VAR " + it->first + "_1 : boolean;\n";
	    model << "VAR " + it->first + "_2 : boolean;\n";

	    if (flag == 0) model << "INIT (" + it->first + "_1 = " << (initial_state[it->first]? "TRUE": "FALSE") << ") & (" + it->first + "_2 = " << (initial_state[it->first]? "TRUE": "FALSE") << ");\n";
            else model << "INIT " << it->first << "_1 = " << it->first << "_2;\n";

	    model << "TRANS next(" + it->first + "_1) = " + n.fanins[0] + "_1;\n";
	    model << "TRANS next(" + it->first + "_2) = " + n.fanins[0] + "_2;\n";
	    continue;
	}
	
        
        //model << "VAR " << it->first << "_1 : boolean;\n";
	//model << "VAR " << it->first << "_2 : boolean;\n";
 
        
        if (n.camouflaged) { 
            camouflaged_gate_list.push_back(it->first);
	    model << "FROZENVAR " + it->first + "_id_1 : {nand, nor};\n";
            model << "FROZENVAR " + it->first + "_id_2 : {nand, nor};\n";
	    model << "DEFINE " + it->first + "_1 := (\n";
	    model << "\tcase\n";
	    model << "\t\t" + it->first + "_id_1 = nand : !(" + n.fanins[0] + "_1";
	    for (int j = 1; j < n.fanins.size(); j++) model << " & " + n.fanins[j] + "_1";
	    model << ");\n";
	    model << "\t\t" + it->first + "_id_1 = nor : !(" + n.fanins[0] + "_1";
	    for (int j = 1; j < n.fanins.size(); j++) model << " | " + n.fanins[j] + "_1";
	    model << ");\n" ;
	    model << "esac);\n";
	    model << "DEFINE " + it->first + "_2 := (\n";
	    model << "\tcase\n";
	    model << "\t\t" + it->first + "_id_2 = nand : !(" + n.fanins[0] + "_2";
	    for (int j = 1; j < n.fanins.size(); j++) model << " & " + n.fanins[j] + "_2";
	    model << ");\n";
	    model << "\t\t" + it->first + "_id_2 = nor : !(" + n.fanins[0] + "_2";
	    for (int j = 1; j < n.fanins.size(); j++) model << " | " + n.fanins[j] + "_2";
	    model << ");\n" ;
            model << "esac);\n";
	    continue; 			 
	}
		// switch(gates[i].type) {
        model << "DEFINE LOGIC0 := false;\n";
        model << "DEFINE LOGIC1 := true;\n";
       
	if (n.type == "NOT") {
            model << "DEFINE " + it->first + "_1 := !" + n.fanins[0] + "_1;\n";
	    model << "DEFINE " + it->first + "_2 := !" + n.fanins[0] + "_2;\n";
	}
	if (n.type == "AND") {
	    model << "DEFINE " + it->first + "_1 := (" + n.fanins[0] + "_1";
	    for (int j = 1; j < n.fanins.size(); j++) model << " & " + n.fanins[j] + "_1";
	    model << ");\n";
	    model << "DEFINE " + it->first + "_2 := (" + n.fanins[0] + "_2";
	    for (int j = 1; j < n.fanins.size(); j++) model << " & " + n.fanins[j] + "_2";
	    model << ");\n";
        }
        if (n.type == "NAND") {
		        model << "DEFINE " + it->first + "_1 := !(" + n.fanins[0] + "_1";
			for (int j = 1; j < n.fanins.size(); j++) model << " & " + n.fanins[j] + "_1";
			model << ");\n";
			model << "DEFINE " + it->first + "_2 := !(" + n.fanins[0] + "_2";
			for (int j = 1; j < n.fanins.size(); j++) model << " & " + n.fanins[j] + "_2";
			model << ");\n";
		    }
		    if (n.type == "OR") {
		        model << "DEFINE " + it->first + "_1 := (" + n.fanins[0] + "_1";
		        for (int j = 1; j < n.fanins.size(); j++) model << " | " + n.fanins[j] + "_1";
			model << ");\n";
			model << "DEFINE " + it->first + "_2 := (" + n.fanins[0] + "_2";
			for (int j = 1; j < n.fanins.size(); j++) model << " | " + n.fanins[j] + "_2";
			model << ");\n";
		    }
		    if (n.type == "NOR") {
			model << "DEFINE " + it->first + "_1 := !(" + n.fanins[0] + "_1";
			for (int j = 1; j < n.fanins.size(); j++) model << " | " + n.fanins[j] + "_1";
			model << ");\n";
			model << "DEFINE " + it->first + "_2 := !(" + n.fanins[0] + "_2";
			for (int j = 1; j < n.fanins.size(); j++) model << " | " + n.fanins[j] + "_2";
			model << ");\n";
		    }
        if (n.type == "MUX") {
            model << "DEFINE " + it->first + "_1 := !" + n.fanins[2] + "_1 & " + n.fanins[0] + "_1 + " + n.fanins[2] + "_1 & " + n.fanins[1] + "_1;\n";
            model << "DEFINE " + it->first + "_2 := !" + n.fanins[2] + "_2 & " + n.fanins[0] + "_2 + " + n.fanins[2] + "_2 & " + n.fanins[1] + "_2;\n";
        }  
    }

    // constraint to ensure the two completions differ in at least one camouflaged gate
    model << "INIT";
    for (int i = 0; i < camouflaged_gate_list.size(); i++) model << (i == 0? " ": " | ") << "(" + camouflaged_gate_list[i] + "_id_1 != " + camouflaged_gate_list[i] + "_id_2)";
    model << ";\n";

    // constraint to ensure the input sequences to the two completions are the same
    for (int i = 0; i < input_list.size(); i++) model << "VAR " << input_list[i] << " : boolean;\n";

    // constraint to make sure that the values on internal wires are consistent
    

    // ensure two completions are sequentially equivalent	    
    if (flag == 0) {
    model << "LTLSPEC G";
    for (int i = 0; i < output_list.size(); i++) model << (i == 0? " (": " & ") << "(" << output_list[i] << "_1 = " << output_list[i] << "_2)";
    model << ");\n";
    model << "CTLSPEC AG";
    for (int i = 0; i < output_list.size(); i++) model << (i == 0? " (": " & ") << "(" << output_list[i] << "_1 = " << output_list[i] << "_2)";
    model << ");\n";
    }
    else {
        model << "LTLSPEC " << generate_next_state_condition() << ";\n";
    }
    return model.str();
}

class Circuit_simulate_input_sequence {
    Circuit* self;
    vector<map<string,bool> > retval;
    vector<map<string,bool> > output; 
    map<string,bool> evaluated, values;
     
    bool evaluate(string gate_name) {
        if (evaluated[gate_name]) return values[gate_name];
        else {
            node n = self->node_table[gate_name];
            for (int i = 0; i < n.fanins.size(); i++) evaluate(n.fanins[i]);
            if (n.type == "NOT") values[gate_name] = !evaluate(n.fanins[0]);
            if (n.type == "AND") {
                values[gate_name] = true;
                for (int j = 0; j < n.fanins.size(); j++) 
                    if (!evaluate(n.fanins[j])) { 
                        values[gate_name] = false;
                        break;
                    }
            }
            if (n.type == "NAND") {
                values[gate_name] = false;
                for (int j = 0; j < n.fanins.size(); j++) 
                    if (!evaluate(n.fanins[j])) { 
                        values[gate_name] = true;
                        break;
                    }
            } 
            if (n.type == "OR") {
                values[gate_name] = false;
                for (int j = 0; j < n.fanins.size(); j++) 
                    if (evaluate(n.fanins[j])) { 
                        values[gate_name] = true;
                        break;
                    }
            } 
            if (n.type == "NOR") {
                values[gate_name] = true;
                for (int j = 0; j < n.fanins.size(); j++) 
                    if (evaluate(n.fanins[j])) { 
                        values[gate_name] = false;
                        break;
                    }
            }
        }
        evaluated[gate_name] = true;
        return values[gate_name];
    }
    public:
        Circuit_simulate_input_sequence(Circuit* c, map<string,bool> initial_state, vector<map<string,bool> > input_sequence) {
            self = c;
            for (int cycle = 0; cycle < input_sequence.size(); cycle++) { // loop through sequential cycles
                for (map<string,node>::iterator it = c->node_table.begin(); it != c->node_table.end(); ++it) evaluated[it->first] = false;

                for (map<string,node>::iterator it = c->node_table.begin(); it != c->node_table.end(); ++it) {
                    evaluated[it->first] = false;
                    if (it->second.type == "INPUT") { 
                        evaluated[it->first] = true; 
                        values[it->first] = input_sequence[cycle][it->first];
                    }
                    else if (it->second.type == "STATE") {
                        evaluated[it->first] = true;
                        if (cycle == 0) values[it->first] = c->initial_state[it->first]; // initial_state[it->first];
                        else values[it->first] = values[it->second.fanins[0]];
                    }
                }
                for (map<string,node>::iterator it = c->node_table.begin(); it != c->node_table.end(); ++it) {
                    evaluate(it->first);
                }
                // for (map<string,bool>::iterator it = values.begin(); it != values.end(); ++it) cout << it->first << " = " << it->second << endl;
                retval.push_back(values);
            }
        }
        operator vector<map<string,bool> >() { return retval; }
};

vector<map<string,bool> > Circuit::simulate_input_sequence(map<string,bool> initial_state, vector<map<string, bool> > input_sequence) { 
    return Circuit_simulate_input_sequence(this, initial_state, input_sequence); 
}

//vector<string, GateType> decamouflage() {
//    build_model();
//    
//    while (counter_example_exists())
//        update_model();
//    }
//    return completion();
//}

//vector<string, GateType> SMVAttack::completion() {
//}

//void SMVAttack::build_model() {
//}

//bool SMVAttack::counter_example_exists() {
//}

// Given sequential behavior, generate smv constraint to make sure the two completions comply with sequential behaviour
string Circuit::generate_smv_sequential_constraint(int constraint_number, map<string, bool> initial_state, vector<map<string,bool> > input_sequence, vector<map<string,bool> > output_sequence) {
    stringstream constraint;
    // constraint << "DEFINE \n";

    // Define intermediate circuit values
    for (int cycle = 0; cycle < input_sequence.size(); cycle++) {
        ostringstream temp;
        temp << constraint_number << "_" << cycle;
        string number = temp.str();
        for (map<string,node>::iterator it = node_table.begin(); it != node_table.end(); ++it) {
            node n = it->second; string node_name = it->first;
            // constraint << "VAR " << node_name << "_" << number << "_1 : boolean;\n";
            // constraint << "VAR " << node_name << "_" << number << "_2 : boolean;\n";

            if (n.type == "INPUT") {
                constraint << "DEFINE " << node_name << "_" << number << "_1" << " := " << (input_sequence[cycle][node_name]? "TRUE": "FALSE") << ";\n";
                constraint << "DEFINE " << node_name << "_" << number << "_2" << " := " << (input_sequence[cycle][node_name]? "TRUE": "FALSE") << ";\n";
            }
            if (n.type == "STATE") {
                if (cycle == 0) {
                    constraint << "DEFINE " << node_name << "_" << constraint_number << "_0_1 := " << (this->initial_state[it->first]? "TRUE": "FALSE") << ";\n"; // << (initial_state[node_name]? "TRUE": "FALSE") << ";\n";
                    constraint << "DEFINE " << node_name << "_" << constraint_number << "_0_2 := " << (this->initial_state[it->first]? "TRUE": "FALSE") << ";\n"; // << (initial_state[node_name]? "TRUE": "FALSE") << ";\n";
                }
                // if (cycle != input_sequence.size() - 1) {
                    constraint << "DEFINE " << node_name << "_" << constraint_number << "_" << cycle + 1 << "_1" << " := " << n.fanins[0] << "_" << number << "_1;\n";
                    constraint << "DEFINE " << node_name << "_" << constraint_number << "_" << cycle + 1 << "_2" << " := " << n.fanins[0] << "_" << number << "_2;\n";
                // }
                // constraint << "INIT " << node_name << "_" << constraint_number << "_" << cycle + 1 << "_1 = " << node_name << "_" << constraint_number << "_" << cycle + 1 << "_2;\n";
 
            }
            if (n.output) {
                constraint << "INIT " << it->first << "_" << number << "_1 = " << (output_sequence[cycle][it->first]? "TRUE": "FALSE") << ";\n";
                constraint << "INIT " << it->first << "_" << number << "_2 = " << (output_sequence[cycle][it->first]? "TRUE": "FALSE") << ";\n";
            }
            if (n.camouflaged) { 
                // camouflaged_gate_list->push_back(it->first);
	        // model << "FROZENVAR " + it->first + "_id_1 : {nand, nor};\n";
            //model << "FROZENVAR " + it->first + "_id_2 : {nand, nor};\n";
	        constraint << "DEFINE " << node_name << "_" << number << "_1" << " := (\n";
	        constraint << "\tcase\n";
	        constraint << "\t\t" << node_name << "_id_1 = nand : !(" << n.fanins[0] << "_" << number << "_1";
	        for (int j = 1; j < n.fanins.size(); j++) constraint << " & " << n.fanins[j] << "_" << number << "_1";
	        constraint << ");\n";
	        constraint << "\t\t" << node_name <<  "_id_1 = nor : !(" << n.fanins[0] << "_" << number << "_1";
	        for (int j = 1; j < n.fanins.size(); j++) constraint << " | " << n.fanins[j] << "_" << number << "_1";
	        constraint << ");\n" ;
	        constraint << "esac);\n";
	        constraint << "DEFINE " << it->first << "_" << number << "_2 := (\n";
	        constraint << "\tcase\n";
	        constraint << "\t\t" << it->first << "_id_2 = nand : !(" << n.fanins[0] << "_" << number << "_2";
	        for (int j = 1; j < n.fanins.size(); j++) constraint << " & " << n.fanins[j] << "_" << number << "_2";
	        constraint << ");\n";
	        constraint << "\t\t" << it->first << "_id_2 = nor : !(" << n.fanins[0] << "_" << number << "_2";
	        for (int j = 1; j < n.fanins.size(); j++) constraint << " | " << n.fanins[j] << "_" << number << "_2";
	        constraint << ");\n" ;
                constraint << "esac);\n";
	        continue; 			 
	    }
		// switch(gates[i].type) {
	    if (n.type == "NOT") {
                constraint << "DEFINE " << it->first << "_" << number << "_1 := !" << n.fanins[0] << "_" << number << "_1;\n";
	        constraint << "DEFINE " << it->first << "_" << number << "_2 := !" << n.fanins[0] << "_" << number << "_2;\n";
	    }
	    if (n.type == "AND") {
	        constraint << "DEFINE " << it->first << "_" << number << "_1 := (" << n.fanins[0] << "_" << number << "_1";
	        for (int j = 1; j < n.fanins.size(); j++) constraint << " & " << n.fanins[j] << "_" << number << "_1";
	        constraint << ");\n";
	        constraint << "DEFINE " << it->first << "_" << number << "_2 := (" << n.fanins[0] << "_" << number << "_2";
	        for (int j = 1; j < n.fanins.size(); j++) constraint << " & " << n.fanins[j] << "_" << number << "_2";
	        constraint << ");\n";
            }
            if (n.type == "NAND") {
	        constraint << "DEFINE " << it->first << "_" << number << "_1 := !(" << n.fanins[0] << "_" << number << "_1";
	        for (int j = 1; j < n.fanins.size(); j++) constraint << " & " << n.fanins[j] << "_" << number << "_1";
	        constraint << ");\n";
		constraint << "DEFINE " << it->first << "_" << number << "_2 := !(" << n.fanins[0] << "_" << number << "_2";
		for (int j = 1; j < n.fanins.size(); j++) constraint << " & " << n.fanins[j] << "_" << number << "_2";
		constraint << ");\n";
	    }
	    if (n.type == "OR") {
	        constraint << "DEFINE " << it->first << "_" << number << "_1 := (" << n.fanins[0] << "_" << number << "_1";
		for (int j = 1; j < n.fanins.size(); j++) constraint << " | " << n.fanins[j] << "_" << number << "_1";
		constraint << ");\n";
		constraint << "DEFINE " << it->first << "_" << number << "_2 := (" << n.fanins[0] << "_" << number << "_2";
		for (int j = 1; j < n.fanins.size(); j++) constraint << " | " << n.fanins[j] << "_" << number << "_2";
		constraint << ");\n";
	    }
            if (n.type == "NOR") {
		constraint << "DEFINE " << it->first << "_" << number << "_1 := !(" << n.fanins[0] << "_" << number << "_1";
		for (int j = 1; j < n.fanins.size(); j++) constraint << " | " << n.fanins[j] << "_" << number << "_1";
		constraint << ");\n";
		constraint << "DEFINE " << it->first << "_" << number << "_2 := !(" << n.fanins[0] << "_" << number << "_2";
		constraint << ");\n";
	    }
            
	}
    }
    return constraint.str();
}
#include <circuit.h>
