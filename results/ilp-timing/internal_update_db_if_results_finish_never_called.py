import sys
import os
import sqlite3


def pretty_time_delta(milliseconds):
    milliseconds = int(milliseconds)
    seconds, milliseconds = divmod(milliseconds, 1000)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%dd %dh %dm %ds %dms' % (days, hours, minutes, seconds, milliseconds)
    elif hours > 0:
        return '%dh %dm %ds %dms' % (hours, minutes, seconds, milliseconds)
    elif minutes > 0:
        return '%dm %ds %dms' % (minutes, seconds, milliseconds)
    elif seconds > 0:
        return '%ds %dms' % (seconds, milliseconds)
    else:
        return '%dms' % (milliseconds)


intermediate_last_sql = r'select * from IntermediateResult where idResult = <IDHERE> order by idIntermediateResult DESC'
intermediate_first_sql = r'select * from IntermediateResult where idResult = <IDHERE> order by idIntermediateResult ASC'


def get_data(c, myid):
    '''
    Returns: (idResult, result, resultString, numStages, lastNumConstraints, lastNumVariables, lastNumCircuits, 
    dateStart, dateFinish,totalDurationMilliseconds,totalDurationPretty) 
    '''
    cur = c.cursor()
    cur.execute(intermediate_last_sql.replace('<IDHERE>', f'{myid}'))
    rs = cur.fetchone()

    cur = c.cursor()
    cur.execute(intermediate_first_sql.replace('<IDHERE>', f'{myid}'))
    rs_first = cur.fetchone()

    result = rs[2]
    resultString = rs[3]

    if result == 0 and resultString == 'No Input Found':
        result = 1
        resultString = 'Key Found'

    totalDurationMilliseconds = int(rs[15]) - int(rs_first[14])
    totalDurationPretty = pretty_time_delta(totalDurationMilliseconds)

    return (rs[1], result, resultString, rs[8], rs[11], rs[12], rs[13], rs_first[14], rs[15], totalDurationMilliseconds,
            totalDurationPretty)


def update(conn, idResult, result, resultString, numStages, lastNumConstraints, lastNumVariables, lastNumCircuits,
           dateStart, dateFinish, totalDurationMilliseconds, totalDurationPretty):
    cur = conn.cursor()
    cur.execute(f'''UPDATE Result SET
    result = '{result}',
    resultString = '{resultString}',
    lastNumConstraints = '{lastNumConstraints}',
    lastNumVariables = '{lastNumVariables}',
    lastNumCircuits = '{lastNumCircuits}',
    numStages = '{numStages}',
    dateStart = '{dateStart}',
    dateFinish = '{dateFinish}',
    totalDurationMilliseconds = '{totalDurationMilliseconds}',
    totalDurationPretty = '{totalDurationPretty}'
    WHERE idResult = '{idResult}';''')
    conn.commit()


def grab(conn, myid):
    a = get_data(conn, myid)
    update(conn, *a)


def help():
    print('<script> (<FILENAME>)')


if __name__ == "__main__":
    if len(sys.argv) == 2:
        if sys.argv[1] == '--help':
            help()
            sys.exit(-1)

        filepath = sys.argv[1]
    elif len(sys.argv) == 1:
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        filepath = 'LogicLockingResults.sqlite3'
    else:
        help()
        sys.exit(-1)

    conn = sqlite3.connect(filepath)

    if conn:
        while True:
            myid = input('Enter Id or Range (Enter invalid number to exit): ')

            if '-' in myid:
                s = myid.split('-')
                if len(s) == 2:
                    idmin = int(s[0])
                    idmax = int(s[1])

                    for myid_r in range(idmin, idmax + 1):
                        print(f'Grabbing and updating row {myid_r} in range {myid}')
                        grab(conn, myid_r)
                else:
                    print('Invalid range! Example range 1-50')
            else:
                myid = int(myid)
                print(f'Grabbing and updating row {myid}')
                grab(conn, myid)
    else:
        print(f'ERROR: could not connect to {filepath}, with the current working directory: {os.getcwd()}')