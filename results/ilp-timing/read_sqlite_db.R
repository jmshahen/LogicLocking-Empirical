library(DBI)
library(tidyverse)
library(lubridate)
library(stringr)

# Create an ephemeral in-memory RSQLite database
con = dbConnect(RSQLite::SQLite(), "LogicLockingResults.sqlite3")

tables = dbListTables(con)

# Error checking
stopifnot(is.element("Circuit", tables))
stopifnot(is.element("Result", tables))
stopifnot(is.element("IntermediateResult", tables))

# Get Circuit Data
res = dbSendQuery(con, "SELECT * FROM Circuit")
circuits = as_tibble(dbFetch(res))
dbClearResult(res)

# Get Results Data
res = dbSendQuery(con, "SELECT * FROM Result")
results = as_tibble(dbFetch(res))
dbClearResult(res)

# Get IntermediateResult Data
res = dbSendQuery(con, "SELECT * FROM IntermediateResult")
inter_results = as_tibble(dbFetch(res))
dbClearResult(res)

# Disconnect from the database
dbDisconnect(con)

