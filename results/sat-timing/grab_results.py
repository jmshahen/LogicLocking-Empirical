import re
import glob

csv_results_file = 'sat_timing_results.csv'

elapsed_time_re = r'([0-9]+):([0-9]+\.[0-9]+)elapsed'
elapsed_time_hr_re = r'([0-9]+):([0-9]+):([0-9]+)elapsed'
user_time_re = r'([0-9]+\.[0-9]+)user'
system_time_re = r'([0-9]+\.[0-9]+)system'
iteration_num_re = r'iteration: ([0-9]+)'
solved_re = r'finished solver loop.'
foldername_re = r'^([a-zA-Z0-9]+)\.'
filename_re = r'\.([a-zA-Z0-9]+)_'
encoding_re = r'enc([0-9]+)'

def main():
    with open(csv_results_file, 'w') as csv_results:
        csv_results.write('File,Folder,Encoding,Elapsed(sec),User(sec),System(sec),Iteration(num),Solved\n')
        for my_filename in glob.glob('*.txt'):
            print(f'Working on file: {my_filename}')

            with open(my_filename, 'r') as f:
                contents = f.read()
                foldername = get_folder_name(my_filename)
                filename = get_file_name(my_filename)
                encoding = get_encoding(my_filename)

                usertime = get_user_time(contents)
                systemtime = get_system_time(contents)
                elapsedtime = get_elapsed_time(contents)
                iterationnum = get_iteration_num(contents)
                keyfound = get_key_found(contents)
                print(f'File Name    : {filename}')
                print(f'Folder Name  : {foldername}')
                print(f'Encoding     : {encoding}')
                print(f'User Time    : {usertime} sec')
                print(f'System Time  : {systemtime} sec')
                print(f'Elapsed Time : {elapsedtime} sec')
                print(f'Iteration num: {iterationnum}')
                print(f'Key Found    : {keyfound}')
                print('')

                csv_results.write(f'"{filename}","{foldername}","{encoding}",{elapsedtime},{usertime},{systemtime},{iterationnum},{keyfound}\n')

def get_folder_name(filename):
    p = re.compile(foldername_re)
    s = p.search(filename)

    if s:
        return s.group(1)
    else:
        print('Unable to find folder name')
        return 'NULL'

def get_file_name(filename):
    p = re.compile(filename_re)
    s = p.search(filename)

    if s:
        return s.group(1)
    else:
        print('Unable to find file name')
        return 'NULL'

def get_encoding(filename):
    p = re.compile(encoding_re)
    s = p.search(filename)

    if s:
        return int(s.group(1))
    else:
        print('Unable to find encoding')
        return 'NULL'

def get_user_time(content):
    p = re.compile(user_time_re)
    s = p.search(content)

    if s:
        return float(s.group(1))
    else:
        print('Unable to find user time')
        return 'NULL'

def get_system_time(content):
    p = re.compile(system_time_re)
    s = p.search(content)

    if s:
        return float(s.group(1))
    else:
        print('Unable to find system time')
        return 'NULL'

def get_elapsed_time(content):
    p = re.compile(elapsed_time_re)
    s = p.search(content)

    if s:
        mins = int(s.group(1))
        secs = float(s.group(2))
        return mins * 60 + secs
    else:
        p = re.compile(elapsed_time_hr_re)
        s = p.search(content)

        if s:
            hrs = int(s.group(1))
            mins = int(s.group(2))
            secs = int(s.group(3))
            return hrs * 3600 + mins * 60 + secs
        else:
            print('Unable to find elapsed time')
            return 'NULL'

def get_iteration_num(content):
    p = re.compile(iteration_num_re)
    s = p.findall(content)

    if s:
        return int(s[-1])
    else:
        print('Unable to find iteration num')
        return 'NULL'

def get_key_found(content):
    p = re.compile(solved_re)
    s = p.search(content)

    if s:
        return 1
    else:
        return '0'

if __name__ == "__main__":
    main()  
