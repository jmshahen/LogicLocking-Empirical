library(ggplot2)
library(plyr)
library(tidyr)

## Readin data
library(readr)
data <- read_csv("sat_timing_results.csv",
                    na = "NULL")

names(data)[1] = "file"
names(data)[2] = "folder"
names(data)[3] = "encoding"
names(data)[4] = "elapsed"
names(data)[5] = "user"
names(data)[6] = "system"
names(data)[7] = "iter_num"
names(data)[8] = "solved"
data$file = factor(data$file)
data$folder = factor(data$folder)
data$encoding = factor(data$encoding)
data$solved = factor(data$solved)

###########################################################################
###########################################################################
###########################################################################

g = ggplot(data, aes(x=as.numeric(file), y=elapsed))
g = g + geom_hline(yintercept=36000, linetype="dashed", color = "red")
g = g + geom_text(data=data.frame(x=1,y=36000), aes(x, y), label='10 hrs', vjust=-1, color='red')
g = g + geom_line(aes(color=folder))
g = g + geom_point(aes(color=folder, shape=folder), size=2)
g = g + scale_x_continuous(labels=levels(data$file), breaks=1:nlevels(data$file))
g = g + ggtitle('SAT Timing Results (Encoding 50 if available)')
g = g + ylab('Time (sec)')
g = g + xlab('File')
plot(g)

